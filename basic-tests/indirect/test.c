#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char *a = malloc(1024);	
	unsigned int size = 0;

  	klee_make_symbolic(a, 1024, "a");
	memcpy(&size, a, 4);

	if (size)
	{
		a[1025] = 0x0;
	}
	else
	{
		a[1021] = 0x0;
	}
	return 0;
}

