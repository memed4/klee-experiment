; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

module asm ".section .gnu.warning.gets"
module asm "\09.previous"

%struct.__STDIO_FILE_STRUCT.424 = type { i16, [2 x i8], i32, i8*, i8*, i8*, i8*, i8*, i8*, %struct.__STDIO_FILE_STRUCT.424*, [2 x i32], %struct.__mbstate_t.423 }
%struct.__mbstate_t.423 = type { i32, i32 }
%struct.Elf64_auxv_t = type { i64, %union.anon.645 }
%union.anon.645 = type { i64 }
%struct.stat.644 = type { i64, i64, i64, i32, i32, i32, i32, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, [3 x i64] }
%struct.termios.442 = type { i32, i32, i32, i32, i8, [32 x i8], i32, i32 }
%struct.__kernel_termios = type { i32, i32, i32, i32, i8, [19 x i8] }

@.str = private unnamed_addr constant [5 x i8] c"buff\00", align 1
@__evoke_link_warning_gets = internal constant [57 x i8] c"the 'gets' function is dangerous and should not be used.\00", section ".gnu.warning.gets\0A#APP\0A\09#", align 16
@llvm.used = appending global [1 x i8*] [i8* getelementptr inbounds ([57 x i8]* @__evoke_link_warning_gets, i32 0, i32 0)], section "llvm.metadata"
@__libc_stack_end = global i8* null, align 8
@.str5 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@__uclibc_progname = hidden global i8* getelementptr inbounds ([1 x i8]* @.str5, i32 0, i32 0), align 8
@__environ = global i8** null, align 8
@__pagesize = global i64 0, align 8
@__uClibc_init.been_there_done_that = internal global i32 0, align 4
@__app_fini = hidden global void ()* null, align 8
@__rtld_fini = hidden global void ()* null, align 8
@.str1 = private unnamed_addr constant [10 x i8] c"/dev/null\00", align 1
@_stdio_streams = internal global [3 x %struct.__STDIO_FILE_STRUCT.424] [%struct.__STDIO_FILE_STRUCT.424 { i16 544, [2 x i8] zeroinitializer, i32 0, i8* null, i8* null, i8* null, i8* null, i8* null, i8* null, %struct.__STDIO_FILE_STRUCT.424* bitcast (i8*
@stdin = global %struct.__STDIO_FILE_STRUCT.424* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i32 0), align 8
@stdout = global %struct.__STDIO_FILE_STRUCT.424* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.424*), align 8
@stderr = global %struct.__STDIO_FILE_STRUCT.424* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams to i8*), i64 160) to %struct.__STDIO_FILE_STRUCT.424*), align 8
@__stdin = global %struct.__STDIO_FILE_STRUCT.424* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i32 0), align 8
@__stdout = global %struct.__STDIO_FILE_STRUCT.424* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.424*), align 8
@_stdio_openlist = global %struct.__STDIO_FILE_STRUCT.424* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i32 0), align 8
@errno = global i32 0, align 4
@h_errno = global i32 0, align 4
@.str44 = private unnamed_addr constant [60 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c\00", align 1
@.str145 = private unnamed_addr constant [15 x i8] c"divide by zero\00", align 1
@.str2 = private unnamed_addr constant [8 x i8] c"div.err\00", align 1
@.str3 = private unnamed_addr constant [8 x i8] c"IGNORED\00", align 1
@.str14 = private unnamed_addr constant [16 x i8] c"overshift error\00", align 1
@.str25 = private unnamed_addr constant [14 x i8] c"overshift.err\00", align 1
@.str6 = private unnamed_addr constant [51 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_range.c\00", align 1
@.str17 = private unnamed_addr constant [14 x i8] c"invalid range\00", align 1
@.str28 = private unnamed_addr constant [5 x i8] c"user\00", align 1

@getchar = alias i32 ()* @getchar_unlocked
@environ = alias weak i8*** @__environ
@fgetc_unlocked = alias i32 (%struct.__STDIO_FILE_STRUCT.424*)* @__fgetc_unlocked
@getc_unlocked = alias i32 (%struct.__STDIO_FILE_STRUCT.424*)* @__fgetc_unlocked
@fgetc = alias i32 (%struct.__STDIO_FILE_STRUCT.424*)* @__fgetc_unlocked
@getc = alias i32 (%struct.__STDIO_FILE_STRUCT.424*)* @__fgetc_unlocked
@fflush = alias i32 (%struct.__STDIO_FILE_STRUCT.424*)* @fflush_unlocked

; Function Attrs: nounwind uwtable
define i32 @__user_main() #0 {
  %1 = alloca i32, align 4
  %buff = alloca [10 x i8], align 1
  store i32 0, i32* %1
  %2 = getelementptr inbounds [10 x i8]* %buff, i32 0, i32 0, !dbg !513
  %3 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %2, i64 10, i8* getelementptr inbounds ([5 x i8]* @.str, i32 0, i32 0)), !dbg !513
  %4 = getelementptr inbounds [10 x i8]* %buff, i32 0, i32 0, !dbg !514
  %5 = call i8* @gets(i8* %4), !dbg !514
  ret i32 0, !dbg !515
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

declare i32 @klee_make_symbolic(...) #2

; Function Attrs: nounwind uwtable
define i8* @gets(i8* %s) #0 {
  %1 = alloca i8*, align 8
  %p = alloca i8*, align 8
  %c = alloca i32, align 4
  store i8* %s, i8** %1, align 8
  %2 = load i8** %1, align 8, !dbg !516
  store i8* %2, i8** %p, align 8, !dbg !516
  br label %3, !dbg !517

; <label>:3                                       ; preds = %12, %0
  %4 = call i32 @getchar_unlocked() #11, !dbg !517
  store i32 %4, i32* %c, align 4, !dbg !517
  %5 = icmp ne i32 %4, -1, !dbg !517
  br i1 %5, label %6, label %.critedge, !dbg !517

; <label>:6                                       ; preds = %3
  %7 = load i32* %c, align 4, !dbg !517
  %8 = trunc i32 %7 to i8, !dbg !517
  %9 = load i8** %p, align 8, !dbg !517
  store i8 %8, i8* %9, align 1, !dbg !517
  %10 = sext i8 %8 to i32, !dbg !517
  %11 = icmp ne i32 %10, 10, !dbg !517
  br i1 %11, label %12, label %.critedge

; <label>:12                                      ; preds = %6
  %13 = load i8** %p, align 8, !dbg !518
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !518
  store i8* %14, i8** %p, align 8, !dbg !518
  br label %3, !dbg !520

.critedge:                                        ; preds = %3, %6
  %15 = load i32* %c, align 4, !dbg !521
  %16 = icmp eq i32 %15, -1, !dbg !521
  br i1 %16, label %21, label %17, !dbg !521

; <label>:17                                      ; preds = %.critedge
  %18 = load i8** %1, align 8, !dbg !521
  %19 = load i8** %p, align 8, !dbg !521
  %20 = icmp eq i8* %18, %19, !dbg !521
  br i1 %20, label %21, label %22, !dbg !521

; <label>:21                                      ; preds = %17, %.critedge
  store i8* null, i8** %1, align 8, !dbg !523
  br label %24, !dbg !525

; <label>:22                                      ; preds = %17
  %23 = load i8** %p, align 8, !dbg !526
  store i8 0, i8* %23, align 1, !dbg !526
  br label %24

; <label>:24                                      ; preds = %22, %21
  %25 = load i8** %1, align 8, !dbg !528
  ret i8* %25, !dbg !528
}

; Function Attrs: nounwind uwtable
define i32 @getchar_unlocked() #0 {
  %stream = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %1 = load %struct.__STDIO_FILE_STRUCT.424** @stdin, align 8, !dbg !529
  store %struct.__STDIO_FILE_STRUCT.424* %1, %struct.__STDIO_FILE_STRUCT.424** %stream, align 8, !dbg !529
  %2 = load %struct.__STDIO_FILE_STRUCT.424** %stream, align 8, !dbg !530
  %3 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %2, i32 0, i32 5, !dbg !530
  %4 = load i8** %3, align 8, !dbg !530
  %5 = load %struct.__STDIO_FILE_STRUCT.424** %stream, align 8, !dbg !530
  %6 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %5, i32 0, i32 7, !dbg !530
  %7 = load i8** %6, align 8, !dbg !530
  %8 = icmp ult i8* %4, %7, !dbg !530
  %9 = load %struct.__STDIO_FILE_STRUCT.424** %stream, align 8, !dbg !530
  br i1 %8, label %10, label %16, !dbg !530

; <label>:10                                      ; preds = %0
  %11 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %9, i32 0, i32 5, !dbg !530
  %12 = load i8** %11, align 8, !dbg !530
  %13 = getelementptr inbounds i8* %12, i32 1, !dbg !530
  store i8* %13, i8** %11, align 8, !dbg !530
  %14 = load i8* %12, align 1, !dbg !530
  %15 = zext i8 %14 to i32, !dbg !530
  br label %18, !dbg !530

; <label>:16                                      ; preds = %0
  %17 = call i32 @__fgetc_unlocked(%struct.__STDIO_FILE_STRUCT.424* %9) #11, !dbg !530
  br label %18, !dbg !530

; <label>:18                                      ; preds = %16, %10
  %19 = phi i32 [ %15, %10 ], [ %17, %16 ], !dbg !530
  ret i32 %19, !dbg !530
}

; Function Attrs: nounwind uwtable
define void @__uClibc_init() #0 {
  %1 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !531
  %2 = icmp ne i32 %1, 0, !dbg !531
  br i1 %2, label %8, label %3, !dbg !531

; <label>:3                                       ; preds = %0
  %4 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !533
  %5 = add nsw i32 %4, 1, !dbg !533
  store i32 %5, i32* @__uClibc_init.been_there_done_that, align 4, !dbg !533
  store i64 4096, i64* @__pagesize, align 8, !dbg !534
  %6 = icmp ne i64 1, 0, !dbg !535
  br i1 %6, label %7, label %8, !dbg !535

; <label>:7                                       ; preds = %3
  call void @_stdio_init() #11, !dbg !537
  br label %8, !dbg !537

; <label>:8                                       ; preds = %0, %7, %3
  ret void, !dbg !538
}

; Function Attrs: nounwind readnone
declare i64 @llvm.expect.i64(i64, i64) #1

; Function Attrs: nounwind uwtable
define void @__uClibc_fini() #0 {
  %1 = load void ()** @__app_fini, align 8, !dbg !539
  %2 = icmp ne void ()* %1, null, !dbg !539
  br i1 %2, label %3, label %5, !dbg !539

; <label>:3                                       ; preds = %0
  %4 = load void ()** @__app_fini, align 8, !dbg !541
  call void %4() #11, !dbg !541
  br label %5, !dbg !541

; <label>:5                                       ; preds = %3, %0
  %6 = load void ()** @__rtld_fini, align 8, !dbg !542
  %7 = icmp ne void ()* %6, null, !dbg !542
  br i1 %7, label %8, label %10, !dbg !542

; <label>:8                                       ; preds = %5
  %9 = load void ()** @__rtld_fini, align 8, !dbg !544
  call void %9() #11, !dbg !544
  br label %10, !dbg !544

; <label>:10                                      ; preds = %8, %5
  ret void, !dbg !545
}

; Function Attrs: noreturn nounwind uwtable
define void @__uClibc_main(i32 (i32, i8**, i8**)* %main, i32 %argc, i8** %argv, void ()* %app_init, void ()* %app_fini, void ()* %rtld_fini, i8* %stack_end) #3 {
  %1 = alloca i32 (i32, i8**, i8**)*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca void ()*, align 8
  %5 = alloca void ()*, align 8
  %6 = alloca void ()*, align 8
  %7 = alloca i8*, align 8
  %aux_dat = alloca i64*, align 8
  %auxvt = alloca [15 x %struct.Elf64_auxv_t], align 16
  %auxv_entry = alloca %struct.Elf64_auxv_t*, align 8
  store i32 (i32, i8**, i8**)* %main, i32 (i32, i8**, i8**)** %1, align 8
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  store void ()* %app_init, void ()** %4, align 8
  store void ()* %app_fini, void ()** %5, align 8
  store void ()* %rtld_fini, void ()** %6, align 8
  store i8* %stack_end, i8** %7, align 8
  %8 = load i8** %7, align 8, !dbg !546
  store i8* %8, i8** @__libc_stack_end, align 8, !dbg !546
  %9 = load void ()** %6, align 8, !dbg !547
  store void ()* %9, void ()** @__rtld_fini, align 8, !dbg !547
  %10 = load i32* %2, align 4, !dbg !548
  %11 = add nsw i32 %10, 1, !dbg !548
  %12 = sext i32 %11 to i64, !dbg !548
  %13 = load i8*** %3, align 8, !dbg !548
  %14 = getelementptr inbounds i8** %13, i64 %12, !dbg !548
  store i8** %14, i8*** @__environ, align 8, !dbg !548
  %15 = load i8*** @__environ, align 8, !dbg !549
  %16 = bitcast i8** %15 to i8*, !dbg !549
  %17 = load i8*** %3, align 8, !dbg !549
  %18 = load i8** %17, align 8, !dbg !549
  %19 = icmp eq i8* %16, %18, !dbg !549
  br i1 %19, label %20, label %25, !dbg !549

; <label>:20                                      ; preds = %0
  %21 = load i32* %2, align 4, !dbg !551
  %22 = sext i32 %21 to i64, !dbg !551
  %23 = load i8*** %3, align 8, !dbg !551
  %24 = getelementptr inbounds i8** %23, i64 %22, !dbg !551
  store i8** %24, i8*** @__environ, align 8, !dbg !551
  br label %25, !dbg !553

; <label>:25                                      ; preds = %20, %0
  %26 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i32 0, !dbg !554
  %27 = bitcast %struct.Elf64_auxv_t* %26 to i8*, !dbg !554
  %28 = call i8* @memset(i8* %27, i32 0, i64 240) #12, !dbg !554
  %29 = load i8*** @__environ, align 8, !dbg !555
  %30 = bitcast i8** %29 to i64*, !dbg !555
  store i64* %30, i64** %aux_dat, align 8, !dbg !555
  br label %31, !dbg !556

; <label>:31                                      ; preds = %31, %25
  %32 = load i64** %aux_dat, align 8, !dbg !556
  %33 = load i64* %32, align 8, !dbg !556
  %34 = icmp ne i64 %33, 0, !dbg !556
  %35 = load i64** %aux_dat, align 8, !dbg !557
  %36 = getelementptr inbounds i64* %35, i32 1, !dbg !557
  store i64* %36, i64** %aux_dat, align 8, !dbg !557
  br i1 %34, label %31, label %37, !dbg !556

; <label>:37                                      ; preds = %31, %57
  %38 = load i64** %aux_dat, align 8, !dbg !559
  %39 = load i64* %38, align 8, !dbg !559
  %40 = icmp ne i64 %39, 0, !dbg !559
  br i1 %40, label %41, label %60, !dbg !559

; <label>:41                                      ; preds = %37
  %42 = load i64** %aux_dat, align 8, !dbg !560
  %43 = bitcast i64* %42 to %struct.Elf64_auxv_t*, !dbg !560
  store %struct.Elf64_auxv_t* %43, %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !560
  %44 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !562
  %45 = getelementptr inbounds %struct.Elf64_auxv_t* %44, i32 0, i32 0, !dbg !562
  %46 = load i64* %45, align 8, !dbg !562
  %47 = icmp ule i64 %46, 14, !dbg !562
  br i1 %47, label %48, label %57, !dbg !562

; <label>:48                                      ; preds = %41
  %49 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !564
  %50 = getelementptr inbounds %struct.Elf64_auxv_t* %49, i32 0, i32 0, !dbg !564
  %51 = load i64* %50, align 8, !dbg !564
  %52 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 %51, !dbg !564
  %53 = bitcast %struct.Elf64_auxv_t* %52 to i8*, !dbg !564
  %54 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !564
  %55 = bitcast %struct.Elf64_auxv_t* %54 to i8*, !dbg !564
  %56 = call i8* @memcpy(i8* %53, i8* %55, i64 16) #12, !dbg !564
  br label %57, !dbg !566

; <label>:57                                      ; preds = %48, %41
  %58 = load i64** %aux_dat, align 8, !dbg !567
  %59 = getelementptr inbounds i64* %58, i64 2, !dbg !567
  store i64* %59, i64** %aux_dat, align 8, !dbg !567
  br label %37, !dbg !568

; <label>:60                                      ; preds = %37
  call void @__uClibc_init() #11, !dbg !569
  %61 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !570
  %62 = getelementptr inbounds %struct.Elf64_auxv_t* %61, i32 0, i32 1, !dbg !570
  %63 = bitcast %union.anon.645* %62 to i64*, !dbg !570
  %64 = load i64* %63, align 8, !dbg !570
  %65 = icmp ne i64 %64, 0, !dbg !570
  br i1 %65, label %66, label %71, !dbg !570

; <label>:66                                      ; preds = %60
  %67 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !570
  %68 = getelementptr inbounds %struct.Elf64_auxv_t* %67, i32 0, i32 1, !dbg !570
  %69 = bitcast %union.anon.645* %68 to i64*, !dbg !570
  %70 = load i64* %69, align 8, !dbg !570
  br label %71, !dbg !570

; <label>:71                                      ; preds = %60, %66
  %72 = phi i64 [ %70, %66 ], [ 4096, %60 ], !dbg !570
  store i64 %72, i64* @__pagesize, align 8, !dbg !570
  %73 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !571
  %74 = getelementptr inbounds %struct.Elf64_auxv_t* %73, i32 0, i32 1, !dbg !571
  %75 = bitcast %union.anon.645* %74 to i64*, !dbg !571
  %76 = load i64* %75, align 8, !dbg !571
  %77 = icmp eq i64 %76, -1, !dbg !571
  br i1 %77, label %78, label %81, !dbg !571

; <label>:78                                      ; preds = %71
  %79 = call i32 @__check_suid() #11, !dbg !571
  %80 = icmp ne i32 %79, 0, !dbg !571
  br i1 %80, label %107, label %81, !dbg !571

; <label>:81                                      ; preds = %78, %71
  %82 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !571
  %83 = getelementptr inbounds %struct.Elf64_auxv_t* %82, i32 0, i32 1, !dbg !571
  %84 = bitcast %union.anon.645* %83 to i64*, !dbg !571
  %85 = load i64* %84, align 8, !dbg !571
  %86 = icmp ne i64 %85, -1, !dbg !571
  br i1 %86, label %87, label %108, !dbg !571

; <label>:87                                      ; preds = %81
  %88 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !571
  %89 = getelementptr inbounds %struct.Elf64_auxv_t* %88, i32 0, i32 1, !dbg !571
  %90 = bitcast %union.anon.645* %89 to i64*, !dbg !571
  %91 = load i64* %90, align 8, !dbg !571
  %92 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 12, !dbg !571
  %93 = getelementptr inbounds %struct.Elf64_auxv_t* %92, i32 0, i32 1, !dbg !571
  %94 = bitcast %union.anon.645* %93 to i64*, !dbg !571
  %95 = load i64* %94, align 8, !dbg !571
  %96 = icmp ne i64 %91, %95, !dbg !571
  br i1 %96, label %107, label %97, !dbg !571

; <label>:97                                      ; preds = %87
  %98 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 13, !dbg !571
  %99 = getelementptr inbounds %struct.Elf64_auxv_t* %98, i32 0, i32 1, !dbg !571
  %100 = bitcast %union.anon.645* %99 to i64*, !dbg !571
  %101 = load i64* %100, align 8, !dbg !571
  %102 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 14, !dbg !571
  %103 = getelementptr inbounds %struct.Elf64_auxv_t* %102, i32 0, i32 1, !dbg !571
  %104 = bitcast %union.anon.645* %103 to i64*, !dbg !571
  %105 = load i64* %104, align 8, !dbg !571
  %106 = icmp ne i64 %101, %105, !dbg !571
  br i1 %106, label %107, label %108, !dbg !571

; <label>:107                                     ; preds = %97, %87, %78
  call void @__check_one_fd(i32 0, i32 131072) #11, !dbg !573
  call void @__check_one_fd(i32 1, i32 131074) #11, !dbg !575
  call void @__check_one_fd(i32 2, i32 131074) #11, !dbg !576
  br label %108, !dbg !577

; <label>:108                                     ; preds = %107, %97, %81
  %109 = load i8*** %3, align 8, !dbg !578
  %110 = load i8** %109, align 8, !dbg !578
  store i8* %110, i8** @__uclibc_progname, align 8, !dbg !578
  %111 = load void ()** %5, align 8, !dbg !579
  store void ()* %111, void ()** @__app_fini, align 8, !dbg !579
  %112 = load void ()** %4, align 8, !dbg !580
  %113 = icmp ne void ()* %112, null, !dbg !580
  br i1 %113, label %114, label %116, !dbg !580

; <label>:114                                     ; preds = %108
  %115 = load void ()** %4, align 8, !dbg !582
  call void %115() #11, !dbg !582
  br label %116, !dbg !584

; <label>:116                                     ; preds = %114, %108
  %117 = icmp ne i64 1, 0, !dbg !585
  br i1 %117, label %118, label %120, !dbg !585

; <label>:118                                     ; preds = %116
  %119 = call i32* @__errno_location() #13, !dbg !587
  store i32 0, i32* %119, align 4, !dbg !587
  br label %120, !dbg !587

; <label>:120                                     ; preds = %118, %116
  %121 = icmp ne i64 1, 0, !dbg !588
  br i1 %121, label %122, label %124, !dbg !588

; <label>:122                                     ; preds = %120
  %123 = call i32* @__h_errno_location() #13, !dbg !590
  store i32 0, i32* %123, align 4, !dbg !590
  br label %124, !dbg !590

; <label>:124                                     ; preds = %122, %120
  %125 = load i32 (i32, i8**, i8**)** %1, align 8, !dbg !591
  %126 = load i32* %2, align 4, !dbg !591
  %127 = load i8*** %3, align 8, !dbg !591
  %128 = load i8*** @__environ, align 8, !dbg !591
  %129 = call i32 %125(i32 %126, i8** %127, i8** %128) #11, !dbg !591
  call void @exit(i32 %129) #14, !dbg !591
  unreachable, !dbg !591
}

; Function Attrs: noreturn nounwind
declare void @exit(i32) #4

declare i32 @fcntl(i32, i32, ...) #2

declare i32 @open(i8*, i32, ...) #2

; Function Attrs: nounwind
declare i32 @fstat(i32, %struct.stat.644*) #5

; Function Attrs: noreturn nounwind
declare void @abort() #4

; Function Attrs: nounwind
declare i32 @getuid() #5

; Function Attrs: nounwind
declare i32 @geteuid() #5

; Function Attrs: nounwind
declare i32 @getgid() #5

; Function Attrs: nounwind
declare i32 @getegid() #5

; Function Attrs: nounwind uwtable
define internal i32 @__check_suid() #0 {
  %1 = alloca i32, align 4
  %uid = alloca i32, align 4
  %euid = alloca i32, align 4
  %gid = alloca i32, align 4
  %egid = alloca i32, align 4
  %2 = call i32 @getuid() #12, !dbg !592
  store i32 %2, i32* %uid, align 4, !dbg !592
  %3 = call i32 @geteuid() #12, !dbg !594
  store i32 %3, i32* %euid, align 4, !dbg !594
  %4 = call i32 @getgid() #12, !dbg !595
  store i32 %4, i32* %gid, align 4, !dbg !595
  %5 = call i32 @getegid() #12, !dbg !596
  store i32 %5, i32* %egid, align 4, !dbg !596
  %6 = load i32* %uid, align 4, !dbg !597
  %7 = load i32* %euid, align 4, !dbg !597
  %8 = icmp eq i32 %6, %7, !dbg !597
  br i1 %8, label %9, label %14, !dbg !597

; <label>:9                                       ; preds = %0
  %10 = load i32* %gid, align 4, !dbg !597
  %11 = load i32* %egid, align 4, !dbg !597
  %12 = icmp eq i32 %10, %11, !dbg !597
  br i1 %12, label %13, label %14, !dbg !597

; <label>:13                                      ; preds = %9
  store i32 0, i32* %1, !dbg !599
  br label %15, !dbg !599

; <label>:14                                      ; preds = %9, %0
  store i32 1, i32* %1, !dbg !601
  br label %15, !dbg !601

; <label>:15                                      ; preds = %14, %13
  %16 = load i32* %1, !dbg !602
  ret i32 %16, !dbg !602
}

; Function Attrs: nounwind uwtable
define internal void @__check_one_fd(i32 %fd, i32 %mode) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %st = alloca %struct.stat.644, align 8
  %nullfd = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store i32 %mode, i32* %2, align 4
  %3 = load i32* %1, align 4, !dbg !603
  %4 = call i32 (i32, i32, ...)* @fcntl(i32 %3, i32 1) #11, !dbg !603
  %5 = icmp eq i32 %4, -1, !dbg !603
  br i1 %5, label %6, label %10, !dbg !603

; <label>:6                                       ; preds = %0
  %7 = call i32* @__errno_location() #13, !dbg !603
  %8 = load i32* %7, align 4, !dbg !603
  %9 = icmp eq i32 %8, 9, !dbg !603
  br label %10

; <label>:10                                      ; preds = %6, %0
  %11 = phi i1 [ false, %0 ], [ %9, %6 ]
  %12 = xor i1 %11, true
  %13 = xor i1 %12, true
  %14 = zext i1 %13 to i32
  %15 = sext i32 %14 to i64
  %16 = icmp ne i64 %15, 0
  br i1 %16, label %17, label %38

; <label>:17                                      ; preds = %10
  %18 = load i32* %2, align 4, !dbg !605
  %19 = call i32 (i8*, i32, ...)* @open(i8* getelementptr inbounds ([10 x i8]* @.str1, i32 0, i32 0), i32 %18) #11, !dbg !605
  store i32 %19, i32* %nullfd, align 4, !dbg !605
  %20 = load i32* %nullfd, align 4, !dbg !607
  %21 = load i32* %1, align 4, !dbg !607
  %22 = icmp ne i32 %20, %21, !dbg !607
  br i1 %22, label %37, label %23, !dbg !607

; <label>:23                                      ; preds = %17
  %24 = load i32* %1, align 4, !dbg !607
  %25 = call i32 @fstat(i32 %24, %struct.stat.644* %st) #12, !dbg !607
  %26 = icmp ne i32 %25, 0, !dbg !607
  br i1 %26, label %37, label %27, !dbg !607

; <label>:27                                      ; preds = %23
  %28 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 3, !dbg !607
  %29 = load i32* %28, align 4, !dbg !607
  %30 = and i32 %29, 61440, !dbg !607
  %31 = icmp eq i32 %30, 8192, !dbg !607
  br i1 %31, label %32, label %37, !dbg !607

; <label>:32                                      ; preds = %27
  %33 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 7, !dbg !607
  %34 = load i64* %33, align 8, !dbg !607
  %35 = call i64 @gnu_dev_makedev(i32 1, i32 3) #12, !dbg !609
  %36 = icmp ne i64 %34, %35, !dbg !609
  br i1 %36, label %37, label %38, !dbg !609

; <label>:37                                      ; preds = %32, %27, %23, %17
  call void @abort() #14, !dbg !610
  unreachable, !dbg !610

; <label>:38                                      ; preds = %32, %10
  ret void, !dbg !612
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @gnu_dev_makedev(i32 %__major, i32 %__minor) #6 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 %__major, i32* %1, align 4
  store i32 %__minor, i32* %2, align 4
  %3 = load i32* %2, align 4, !dbg !613
  %4 = and i32 %3, 255, !dbg !613
  %5 = load i32* %1, align 4, !dbg !613
  %6 = and i32 %5, 4095, !dbg !613
  %int_cast_to_i64 = zext i32 8 to i64
  call void @klee_overshift_check(i64 32, i64 %int_cast_to_i64), !dbg !613
  %7 = shl i32 %6, 8, !dbg !613
  %8 = or i32 %4, %7, !dbg !613
  %9 = zext i32 %8 to i64, !dbg !613
  %10 = load i32* %2, align 4, !dbg !613
  %11 = and i32 %10, -256, !dbg !613
  %12 = zext i32 %11 to i64, !dbg !613
  %int_cast_to_i641 = bitcast i64 12 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i641), !dbg !613
  %13 = shl i64 %12, 12, !dbg !613
  %14 = or i64 %9, %13, !dbg !613
  %15 = load i32* %1, align 4, !dbg !613
  %16 = and i32 %15, -4096, !dbg !613
  %17 = zext i32 %16 to i64, !dbg !613
  %int_cast_to_i642 = bitcast i64 32 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i642), !dbg !613
  %18 = shl i64 %17, 32, !dbg !613
  %19 = or i64 %14, %18, !dbg !613
  ret i64 %19, !dbg !613
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__errno_location() #7 {
  ret i32* @errno, !dbg !615
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__h_errno_location() #7 {
  ret i32* @h_errno, !dbg !616
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_term() #0 {
  %ptr = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %1 = load %struct.__STDIO_FILE_STRUCT.424** @_stdio_openlist, align 8, !dbg !617
  store %struct.__STDIO_FILE_STRUCT.424* %1, %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !617
  br label %2, !dbg !617

; <label>:2                                       ; preds = %15, %0
  %3 = load %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !617
  %4 = icmp ne %struct.__STDIO_FILE_STRUCT.424* %3, null, !dbg !617
  br i1 %4, label %5, label %19, !dbg !617

; <label>:5                                       ; preds = %2
  %6 = load %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !619
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %6, i32 0, i32 0, !dbg !619
  %8 = load i16* %7, align 2, !dbg !619
  %9 = zext i16 %8 to i32, !dbg !619
  %10 = and i32 %9, 64, !dbg !619
  %11 = icmp ne i32 %10, 0, !dbg !619
  br i1 %11, label %12, label %15, !dbg !619

; <label>:12                                      ; preds = %5
  %13 = load %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !622
  %14 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.424* %13) #11, !dbg !622
  br label %15, !dbg !624

; <label>:15                                      ; preds = %5, %12
  %16 = load %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !617
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %16, i32 0, i32 9, !dbg !617
  %18 = load %struct.__STDIO_FILE_STRUCT.424** %17, align 8, !dbg !617
  store %struct.__STDIO_FILE_STRUCT.424* %18, %struct.__STDIO_FILE_STRUCT.424** %ptr, align 8, !dbg !617
  br label %2, !dbg !617

; <label>:19                                      ; preds = %2
  ret void, !dbg !625
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_init() #0 {
  %old_errno = alloca i32, align 4
  %1 = load i32* @errno, align 4, !dbg !626
  store i32 %1, i32* %old_errno, align 4, !dbg !626
  %2 = call i32 @isatty(i32 0) #12, !dbg !627
  %3 = sub nsw i32 1, %2, !dbg !627
  %4 = mul i32 %3, 256, !dbg !627
  %5 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !627
  %6 = zext i16 %5 to i32, !dbg !627
  %7 = xor i32 %6, %4, !dbg !627
  %8 = trunc i32 %7 to i16, !dbg !627
  store i16 %8, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !627
  %9 = call i32 @isatty(i32 1) #12, !dbg !628
  %10 = sub nsw i32 1, %9, !dbg !628
  %11 = mul i32 %10, 256, !dbg !628
  %12 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !628
  %13 = zext i16 %12 to i32, !dbg !628
  %14 = xor i32 %13, %11, !dbg !628
  %15 = trunc i32 %14 to i16, !dbg !628
  store i16 %15, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !628
  %16 = load i32* %old_errno, align 4, !dbg !629
  store i32 %16, i32* @errno, align 4, !dbg !629
  ret void, !dbg !630
}

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.424* noalias %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %bufsize = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %1, align 8
  %2 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !631
  %3 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %2, i32 0, i32 5, !dbg !631
  %4 = load i8** %3, align 8, !dbg !631
  %5 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !631
  %6 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %5, i32 0, i32 3, !dbg !631
  %7 = load i8** %6, align 8, !dbg !631
  %8 = ptrtoint i8* %4 to i64, !dbg !631
  %9 = ptrtoint i8* %7 to i64, !dbg !631
  %10 = sub i64 %8, %9, !dbg !631
  store i64 %10, i64* %bufsize, align 8, !dbg !631
  %11 = icmp ne i64 %10, 0, !dbg !631
  br i1 %11, label %12, label %24, !dbg !631

; <label>:12                                      ; preds = %0
  %13 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !633
  %14 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %13, i32 0, i32 3, !dbg !633
  %15 = load i8** %14, align 8, !dbg !633
  %16 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !633
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %16, i32 0, i32 5, !dbg !633
  store i8* %15, i8** %17, align 8, !dbg !633
  %18 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !635
  %19 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !635
  %20 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %19, i32 0, i32 3, !dbg !635
  %21 = load i8** %20, align 8, !dbg !635
  %22 = load i64* %bufsize, align 8, !dbg !635
  %23 = call i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.424* %18, i8* %21, i64 %22) #11, !dbg !635
  br label %24, !dbg !636

; <label>:24                                      ; preds = %12, %0
  %25 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !637
  %26 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %25, i32 0, i32 5, !dbg !637
  %27 = load i8** %26, align 8, !dbg !637
  %28 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !637
  %29 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %28, i32 0, i32 3, !dbg !637
  %30 = load i8** %29, align 8, !dbg !637
  %31 = ptrtoint i8* %27 to i64, !dbg !637
  %32 = ptrtoint i8* %30 to i64, !dbg !637
  %33 = sub i64 %31, %32, !dbg !637
  ret i64 %33, !dbg !637
}

; Function Attrs: nounwind uwtable
define i32 @__fgetc_unlocked(%struct.__STDIO_FILE_STRUCT.424* %stream) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %uc = alloca i8, align 1
  %uc1 = alloca i8, align 1
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %2, align 8
  %3 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !638
  %4 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %3, i32 0, i32 5, !dbg !638
  %5 = load i8** %4, align 8, !dbg !638
  %6 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !638
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %6, i32 0, i32 7, !dbg !638
  %8 = load i8** %7, align 8, !dbg !638
  %9 = icmp ult i8* %5, %8, !dbg !638
  %10 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !640
  br i1 %9, label %11, label %17, !dbg !638

; <label>:11                                      ; preds = %0
  %12 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %10, i32 0, i32 5, !dbg !640
  %13 = load i8** %12, align 8, !dbg !640
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !640
  store i8* %14, i8** %12, align 8, !dbg !640
  %15 = load i8* %13, align 1, !dbg !640
  %16 = zext i8 %15 to i32, !dbg !640
  store i32 %16, i32* %1, !dbg !640
  br label %124, !dbg !640

; <label>:17                                      ; preds = %0
  %18 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %10, i32 0, i32 0, !dbg !642
  %19 = load i16* %18, align 2, !dbg !642
  %20 = zext i16 %19 to i32, !dbg !642
  %21 = and i32 %20, 131, !dbg !642
  %22 = icmp ugt i32 %21, 128, !dbg !642
  br i1 %22, label %27, label %23, !dbg !642

; <label>:23                                      ; preds = %17
  %24 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !644
  %25 = call i32 @__stdio_trans2r_o(%struct.__STDIO_FILE_STRUCT.424* %24, i32 128) #11, !dbg !644
  %26 = icmp ne i32 %25, 0, !dbg !644
  br i1 %26, label %123, label %27, !dbg !644

; <label>:27                                      ; preds = %23, %17
  %28 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !645
  %29 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %28, i32 0, i32 0, !dbg !645
  %30 = load i16* %29, align 2, !dbg !645
  %31 = zext i16 %30 to i32, !dbg !645
  %32 = and i32 %31, 2, !dbg !645
  %33 = icmp ne i32 %32, 0, !dbg !645
  %34 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !648
  br i1 %33, label %35, label %52, !dbg !645

; <label>:35                                      ; preds = %27
  %36 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %34, i32 0, i32 0, !dbg !648
  %37 = load i16* %36, align 2, !dbg !648
  %38 = add i16 %37, -1, !dbg !648
  store i16 %38, i16* %36, align 2, !dbg !648
  %39 = zext i16 %37 to i32, !dbg !648
  %40 = and i32 %39, 1, !dbg !648
  %41 = sext i32 %40 to i64, !dbg !648
  %42 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !648
  %43 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %42, i32 0, i32 10, !dbg !648
  %44 = getelementptr inbounds [2 x i32]* %43, i32 0, i64 %41, !dbg !648
  %45 = load i32* %44, align 4, !dbg !648
  %46 = trunc i32 %45 to i8, !dbg !648
  store i8 %46, i8* %uc, align 1, !dbg !648
  %47 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !650
  %48 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %47, i32 0, i32 10, !dbg !650
  %49 = getelementptr inbounds [2 x i32]* %48, i32 0, i64 1, !dbg !650
  store i32 0, i32* %49, align 4, !dbg !650
  %50 = load i8* %uc, align 1, !dbg !651
  %51 = zext i8 %50 to i32, !dbg !651
  store i32 %51, i32* %1, !dbg !651
  br label %124, !dbg !651

; <label>:52                                      ; preds = %27
  %53 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %34, i32 0, i32 6, !dbg !652
  %54 = load i8** %53, align 8, !dbg !652
  %55 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !652
  %56 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %55, i32 0, i32 5, !dbg !652
  %57 = load i8** %56, align 8, !dbg !652
  %58 = ptrtoint i8* %54 to i64, !dbg !652
  %59 = ptrtoint i8* %57 to i64, !dbg !652
  %60 = sub i64 %58, %59, !dbg !652
  %61 = icmp ne i64 %60, 0, !dbg !652
  %62 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !654
  br i1 %61, label %63, label %69, !dbg !652

; <label>:63                                      ; preds = %52
  %64 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %62, i32 0, i32 5, !dbg !654
  %65 = load i8** %64, align 8, !dbg !654
  %66 = getelementptr inbounds i8* %65, i32 1, !dbg !654
  store i8* %66, i8** %64, align 8, !dbg !654
  %67 = load i8* %65, align 1, !dbg !654
  %68 = zext i8 %67 to i32, !dbg !654
  store i32 %68, i32* %1, !dbg !654
  br label %124, !dbg !654

; <label>:69                                      ; preds = %52
  %70 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %62, i32 0, i32 2, !dbg !656
  %71 = load i32* %70, align 4, !dbg !656
  %72 = icmp eq i32 %71, -2, !dbg !656
  %73 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !658
  %74 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %73, i32 0, i32 0, !dbg !658
  %75 = load i16* %74, align 2, !dbg !658
  %76 = zext i16 %75 to i32, !dbg !658
  br i1 %72, label %77, label %80, !dbg !656

; <label>:77                                      ; preds = %69
  %78 = or i32 %76, 4, !dbg !658
  %79 = trunc i32 %78 to i16, !dbg !658
  store i16 %79, i16* %74, align 2, !dbg !658
  store i32 -1, i32* %1, !dbg !660
  br label %124, !dbg !660

; <label>:80                                      ; preds = %69
  %81 = and i32 %76, 768, !dbg !661
  %82 = icmp ne i32 %81, 0, !dbg !661
  br i1 %82, label %83, label %85, !dbg !661

; <label>:83                                      ; preds = %80
  %84 = call i32 @fflush_unlocked(%struct.__STDIO_FILE_STRUCT.424* bitcast (%struct.__STDIO_FILE_STRUCT.424** @_stdio_openlist to %struct.__STDIO_FILE_STRUCT.424*)) #11, !dbg !663
  br label %85, !dbg !665

; <label>:85                                      ; preds = %83, %80
  %86 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !666
  %87 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %86, i32 0, i32 4, !dbg !666
  %88 = load i8** %87, align 8, !dbg !666
  %89 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !666
  %90 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %89, i32 0, i32 3, !dbg !666
  %91 = load i8** %90, align 8, !dbg !666
  %92 = ptrtoint i8* %88 to i64, !dbg !666
  %93 = ptrtoint i8* %91 to i64, !dbg !666
  %94 = sub i64 %92, %93, !dbg !666
  %95 = icmp ne i64 %94, 0, !dbg !666
  %96 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !668
  br i1 %95, label %97, label %117, !dbg !666

; <label>:97                                      ; preds = %85
  %98 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %96, i32 0, i32 3, !dbg !668
  %99 = load i8** %98, align 8, !dbg !668
  %100 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !668
  %101 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %100, i32 0, i32 7, !dbg !668
  store i8* %99, i8** %101, align 8, !dbg !668
  %102 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !670
  %103 = call i64 @__stdio_rfill(%struct.__STDIO_FILE_STRUCT.424* %102) #11, !dbg !670
  %104 = icmp ne i64 %103, 0, !dbg !670
  br i1 %104, label %105, label %123, !dbg !670

; <label>:105                                     ; preds = %97
  %106 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !672
  %107 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %106, i32 0, i32 6, !dbg !672
  %108 = load i8** %107, align 8, !dbg !672
  %109 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !672
  %110 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %109, i32 0, i32 7, !dbg !672
  store i8* %108, i8** %110, align 8, !dbg !672
  %111 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !674
  %112 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %111, i32 0, i32 5, !dbg !674
  %113 = load i8** %112, align 8, !dbg !674
  %114 = getelementptr inbounds i8* %113, i32 1, !dbg !674
  store i8* %114, i8** %112, align 8, !dbg !674
  %115 = load i8* %113, align 1, !dbg !674
  %116 = zext i8 %115 to i32, !dbg !674
  store i32 %116, i32* %1, !dbg !674
  br label %124, !dbg !674

; <label>:117                                     ; preds = %85
  %118 = call i64 @__stdio_READ(%struct.__STDIO_FILE_STRUCT.424* %96, i8* %uc1, i64 1) #11, !dbg !675
  %119 = icmp ne i64 %118, 0, !dbg !675
  br i1 %119, label %120, label %123, !dbg !675

; <label>:120                                     ; preds = %117
  %121 = load i8* %uc1, align 1, !dbg !678
  %122 = zext i8 %121 to i32, !dbg !678
  store i32 %122, i32* %1, !dbg !678
  br label %124, !dbg !678

; <label>:123                                     ; preds = %97, %117, %23
  store i32 -1, i32* %1, !dbg !680
  br label %124, !dbg !680

; <label>:124                                     ; preds = %123, %120, %105, %77, %63, %35, %11
  %125 = load i32* %1, !dbg !681
  ret i32 %125, !dbg !681
}

; Function Attrs: nounwind uwtable
define i8* @memcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !682
  store i8* %4, i8** %r1, align 8, !dbg !682
  %5 = load i8** %2, align 8, !dbg !683
  store i8* %5, i8** %r2, align 8, !dbg !683
  br label %6, !dbg !684

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !684
  %8 = icmp ne i64 %7, 0, !dbg !684
  br i1 %8, label %9, label %17, !dbg !684

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !685
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !685
  store i8* %11, i8** %r2, align 8, !dbg !685
  %12 = load i8* %10, align 1, !dbg !685
  %13 = load i8** %r1, align 8, !dbg !685
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !685
  store i8* %14, i8** %r1, align 8, !dbg !685
  store i8 %12, i8* %13, align 1, !dbg !685
  %15 = load i64* %3, align 8, !dbg !687
  %16 = add i64 %15, -1, !dbg !687
  store i64 %16, i64* %3, align 8, !dbg !687
  br label %6, !dbg !688

; <label>:17                                      ; preds = %6
  %18 = load i8** %1, align 8, !dbg !689
  ret i8* %18, !dbg !689
}

; Function Attrs: nounwind uwtable
define i8* @memset(i8* %s, i32 %c, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i64, align 8
  %p = alloca i8*, align 8
  store i8* %s, i8** %1, align 8
  store i32 %c, i32* %2, align 4
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !690
  store i8* %4, i8** %p, align 8, !dbg !690
  br label %5, !dbg !691

; <label>:5                                       ; preds = %8, %0
  %6 = load i64* %3, align 8, !dbg !691
  %7 = icmp ne i64 %6, 0, !dbg !691
  br i1 %7, label %8, label %15, !dbg !691

; <label>:8                                       ; preds = %5
  %9 = load i32* %2, align 4, !dbg !692
  %10 = trunc i32 %9 to i8, !dbg !692
  %11 = load i8** %p, align 8, !dbg !692
  %12 = getelementptr inbounds i8* %11, i32 1, !dbg !692
  store i8* %12, i8** %p, align 8, !dbg !692
  store i8 %10, i8* %11, align 1, !dbg !692
  %13 = load i64* %3, align 8, !dbg !694
  %14 = add i64 %13, -1, !dbg !694
  store i64 %14, i64* %3, align 8, !dbg !694
  br label %5, !dbg !695

; <label>:15                                      ; preds = %5
  %16 = load i8** %1, align 8, !dbg !696
  ret i8* %16, !dbg !696
}

; Function Attrs: nounwind uwtable
define i32 @isatty(i32 %fd) #0 {
  %1 = alloca i32, align 4
  %term = alloca %struct.termios.442, align 4
  store i32 %fd, i32* %1, align 4
  %2 = load i32* %1, align 4, !dbg !697
  %3 = call i32 @tcgetattr(i32 %2, %struct.termios.442* %term) #12, !dbg !697
  %4 = icmp eq i32 %3, 0, !dbg !697
  %5 = zext i1 %4 to i32, !dbg !697
  ret i32 %5, !dbg !697
}

; Function Attrs: nounwind uwtable
define i32 @tcgetattr(i32 %fd, %struct.termios.442* %termios_p) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.termios.442*, align 8
  %k_termios = alloca %struct.__kernel_termios, align 4
  %retval = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store %struct.termios.442* %termios_p, %struct.termios.442** %2, align 8
  %3 = load i32* %1, align 4, !dbg !698
  %4 = call i32 (i32, i64, ...)* @ioctl(i32 %3, i64 21505, %struct.__kernel_termios* %k_termios) #12, !dbg !698
  store i32 %4, i32* %retval, align 4, !dbg !698
  %5 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 0, !dbg !699
  %6 = load i32* %5, align 4, !dbg !699
  %7 = load %struct.termios.442** %2, align 8, !dbg !699
  %8 = getelementptr inbounds %struct.termios.442* %7, i32 0, i32 0, !dbg !699
  store i32 %6, i32* %8, align 4, !dbg !699
  %9 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 1, !dbg !700
  %10 = load i32* %9, align 4, !dbg !700
  %11 = load %struct.termios.442** %2, align 8, !dbg !700
  %12 = getelementptr inbounds %struct.termios.442* %11, i32 0, i32 1, !dbg !700
  store i32 %10, i32* %12, align 4, !dbg !700
  %13 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 2, !dbg !701
  %14 = load i32* %13, align 4, !dbg !701
  %15 = load %struct.termios.442** %2, align 8, !dbg !701
  %16 = getelementptr inbounds %struct.termios.442* %15, i32 0, i32 2, !dbg !701
  store i32 %14, i32* %16, align 4, !dbg !701
  %17 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 3, !dbg !702
  %18 = load i32* %17, align 4, !dbg !702
  %19 = load %struct.termios.442** %2, align 8, !dbg !702
  %20 = getelementptr inbounds %struct.termios.442* %19, i32 0, i32 3, !dbg !702
  store i32 %18, i32* %20, align 4, !dbg !702
  %21 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 4, !dbg !703
  %22 = load i8* %21, align 1, !dbg !703
  %23 = load %struct.termios.442** %2, align 8, !dbg !703
  %24 = getelementptr inbounds %struct.termios.442* %23, i32 0, i32 4, !dbg !703
  store i8 %22, i8* %24, align 1, !dbg !703
  %25 = load %struct.termios.442** %2, align 8, !dbg !704
  %26 = getelementptr inbounds %struct.termios.442* %25, i32 0, i32 5, !dbg !704
  %27 = getelementptr inbounds [32 x i8]* %26, i32 0, i64 0, !dbg !704
  %28 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 5, !dbg !704
  %29 = getelementptr inbounds [19 x i8]* %28, i32 0, i64 0, !dbg !704
  %30 = call i8* @mempcpy(i8* %27, i8* %29, i64 19) #12, !dbg !704
  %31 = call i8* @memset(i8* %30, i32 0, i64 13) #12, !dbg !704
  %32 = load i32* %retval, align 4, !dbg !707
  ret i32 %32, !dbg !707
}

; Function Attrs: nounwind
declare i32 @ioctl(i32, i64, ...) #5

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_READ(%struct.__STDIO_FILE_STRUCT.424* %stream, i8* %buf, i64 %bufsize) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %rv = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %1, align 8
  store i8* %buf, i8** %2, align 8
  store i64 %bufsize, i64* %3, align 8
  store i64 0, i64* %rv, align 8, !dbg !708
  %4 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !709
  %5 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %4, i32 0, i32 0, !dbg !709
  %6 = load i16* %5, align 2, !dbg !709
  %7 = zext i16 %6 to i32, !dbg !709
  %8 = and i32 %7, 4, !dbg !709
  %9 = icmp ne i32 %8, 0, !dbg !709
  br i1 %9, label %35, label %10, !dbg !709

; <label>:10                                      ; preds = %0
  %11 = load i64* %3, align 8, !dbg !711
  %12 = icmp ugt i64 %11, 9223372036854775807, !dbg !711
  br i1 %12, label %13, label %14, !dbg !711

; <label>:13                                      ; preds = %10
  store i64 9223372036854775807, i64* %3, align 8, !dbg !714
  br label %14, !dbg !716

; <label>:14                                      ; preds = %13, %10
  %15 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !717
  %16 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %15, i32 0, i32 2, !dbg !717
  %17 = load i32* %16, align 4, !dbg !717
  %18 = load i8** %2, align 8, !dbg !717
  %19 = load i64* %3, align 8, !dbg !717
  %20 = call i64 @read(i32 %17, i8* %18, i64 %19) #11, !dbg !717
  store i64 %20, i64* %rv, align 8, !dbg !717
  %21 = icmp sle i64 %20, 0, !dbg !717
  br i1 %21, label %22, label %35, !dbg !717

; <label>:22                                      ; preds = %14
  %23 = load i64* %rv, align 8, !dbg !719
  %24 = icmp eq i64 %23, 0, !dbg !719
  %25 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !722
  %26 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %25, i32 0, i32 0, !dbg !722
  %27 = load i16* %26, align 2, !dbg !722
  %28 = zext i16 %27 to i32, !dbg !722
  br i1 %24, label %29, label %32, !dbg !719

; <label>:29                                      ; preds = %22
  %30 = or i32 %28, 4, !dbg !722
  %31 = trunc i32 %30 to i16, !dbg !722
  store i16 %31, i16* %26, align 2, !dbg !722
  br label %35, !dbg !724

; <label>:32                                      ; preds = %22
  %33 = or i32 %28, 8, !dbg !725
  %34 = trunc i32 %33 to i16, !dbg !725
  store i16 %34, i16* %26, align 2, !dbg !725
  store i64 0, i64* %rv, align 8, !dbg !727
  br label %35

; <label>:35                                      ; preds = %14, %32, %29, %0
  %36 = load i64* %rv, align 8, !dbg !728
  ret i64 %36, !dbg !728
}

declare i64 @read(i32, i8*, i64) #2

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.424* %stream, i8* %buf, i64 %bufsize) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i64, align 8
  %todo = alloca i64, align 8
  %rv = alloca i64, align 8
  %stodo = alloca i64, align 8
  %s = alloca i8*, align 8
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %2, align 8
  store i8* %buf, i8** %3, align 8
  store i64 %bufsize, i64* %4, align 8
  %5 = load i64* %4, align 8, !dbg !729
  store i64 %5, i64* %todo, align 8, !dbg !729
  br label %6, !dbg !730

; <label>:6                                       ; preds = %23, %0
  %7 = load i64* %todo, align 8, !dbg !731
  %8 = icmp eq i64 %7, 0, !dbg !731
  br i1 %8, label %9, label %11, !dbg !731

; <label>:9                                       ; preds = %6
  %10 = load i64* %4, align 8, !dbg !734
  store i64 %10, i64* %1, !dbg !734
  br label %95, !dbg !734

; <label>:11                                      ; preds = %6
  %12 = load i64* %todo, align 8, !dbg !736
  %13 = icmp ule i64 %12, 9223372036854775807, !dbg !736
  %14 = load i64* %todo, align 8, !dbg !736
  %15 = select i1 %13, i64 %14, i64 9223372036854775807, !dbg !736
  store i64 %15, i64* %stodo, align 8, !dbg !736
  %16 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !737
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %16, i32 0, i32 2, !dbg !737
  %18 = load i32* %17, align 4, !dbg !737
  %19 = load i8** %3, align 8, !dbg !737
  %20 = load i64* %stodo, align 8, !dbg !737
  %21 = call i64 @write(i32 %18, i8* %19, i64 %20) #11, !dbg !737
  store i64 %21, i64* %rv, align 8, !dbg !737
  %22 = icmp sge i64 %21, 0, !dbg !737
  br i1 %22, label %23, label %30, !dbg !737

; <label>:23                                      ; preds = %11
  %24 = load i64* %rv, align 8, !dbg !739
  %25 = load i64* %todo, align 8, !dbg !739
  %26 = sub i64 %25, %24, !dbg !739
  store i64 %26, i64* %todo, align 8, !dbg !739
  %27 = load i64* %rv, align 8, !dbg !741
  %28 = load i8** %3, align 8, !dbg !741
  %29 = getelementptr inbounds i8* %28, i64 %27, !dbg !741
  store i8* %29, i8** %3, align 8, !dbg !741
  br label %6, !dbg !742

; <label>:30                                      ; preds = %11
  %31 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !743
  %32 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %31, i32 0, i32 0, !dbg !743
  %33 = load i16* %32, align 2, !dbg !743
  %34 = zext i16 %33 to i32, !dbg !743
  %35 = or i32 %34, 8, !dbg !743
  %36 = trunc i32 %35 to i16, !dbg !743
  store i16 %36, i16* %32, align 2, !dbg !743
  %37 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !745
  %38 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %37, i32 0, i32 4, !dbg !745
  %39 = load i8** %38, align 8, !dbg !745
  %40 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !745
  %41 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %40, i32 0, i32 3, !dbg !745
  %42 = load i8** %41, align 8, !dbg !745
  %43 = ptrtoint i8* %39 to i64, !dbg !745
  %44 = ptrtoint i8* %42 to i64, !dbg !745
  %45 = sub i64 %43, %44, !dbg !745
  store i64 %45, i64* %stodo, align 8, !dbg !745
  %46 = icmp ne i64 %45, 0, !dbg !745
  br i1 %46, label %47, label %91, !dbg !745

; <label>:47                                      ; preds = %30
  %48 = load i64* %stodo, align 8, !dbg !747
  %49 = load i64* %todo, align 8, !dbg !747
  %50 = icmp ugt i64 %48, %49, !dbg !747
  br i1 %50, label %51, label %53, !dbg !747

; <label>:51                                      ; preds = %47
  %52 = load i64* %todo, align 8, !dbg !750
  store i64 %52, i64* %stodo, align 8, !dbg !750
  br label %53, !dbg !752

; <label>:53                                      ; preds = %51, %47
  %54 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !753
  %55 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %54, i32 0, i32 3, !dbg !753
  %56 = load i8** %55, align 8, !dbg !753
  store i8* %56, i8** %s, align 8, !dbg !753
  br label %57, !dbg !754

; <label>:57                                      ; preds = %70, %53
  %58 = load i8** %3, align 8, !dbg !755
  %59 = load i8* %58, align 1, !dbg !755
  %60 = load i8** %s, align 8, !dbg !755
  store i8 %59, i8* %60, align 1, !dbg !755
  %61 = zext i8 %59 to i32, !dbg !755
  %62 = icmp eq i32 %61, 10, !dbg !755
  br i1 %62, label %63, label %70, !dbg !755

; <label>:63                                      ; preds = %57
  %64 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !755
  %65 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %64, i32 0, i32 0, !dbg !755
  %66 = load i16* %65, align 2, !dbg !755
  %67 = zext i16 %66 to i32, !dbg !755
  %68 = and i32 %67, 256, !dbg !755
  %69 = icmp ne i32 %68, 0, !dbg !755
  br i1 %69, label %78, label %70, !dbg !755

; <label>:70                                      ; preds = %63, %57
  %71 = load i8** %s, align 8, !dbg !758
  %72 = getelementptr inbounds i8* %71, i32 1, !dbg !758
  store i8* %72, i8** %s, align 8, !dbg !758
  %73 = load i8** %3, align 8, !dbg !759
  %74 = getelementptr inbounds i8* %73, i32 1, !dbg !759
  store i8* %74, i8** %3, align 8, !dbg !759
  %75 = load i64* %stodo, align 8, !dbg !760
  %76 = add nsw i64 %75, -1, !dbg !760
  store i64 %76, i64* %stodo, align 8, !dbg !760
  %77 = icmp ne i64 %76, 0, !dbg !760
  br i1 %77, label %57, label %78, !dbg !760

; <label>:78                                      ; preds = %63, %70
  %79 = load i8** %s, align 8, !dbg !761
  %80 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !761
  %81 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %80, i32 0, i32 5, !dbg !761
  store i8* %79, i8** %81, align 8, !dbg !761
  %82 = load i8** %s, align 8, !dbg !762
  %83 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !762
  %84 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %83, i32 0, i32 3, !dbg !762
  %85 = load i8** %84, align 8, !dbg !762
  %86 = ptrtoint i8* %82 to i64, !dbg !762
  %87 = ptrtoint i8* %85 to i64, !dbg !762
  %88 = sub i64 %86, %87, !dbg !762
  %89 = load i64* %todo, align 8, !dbg !762
  %90 = sub i64 %89, %88, !dbg !762
  store i64 %90, i64* %todo, align 8, !dbg !762
  br label %91, !dbg !763

; <label>:91                                      ; preds = %78, %30
  %92 = load i64* %4, align 8, !dbg !764
  %93 = load i64* %todo, align 8, !dbg !764
  %94 = sub i64 %92, %93, !dbg !764
  store i64 %94, i64* %1, !dbg !764
  br label %95, !dbg !764

; <label>:95                                      ; preds = %91, %9
  %96 = load i64* %1, !dbg !765
  ret i64 %96, !dbg !765
}

declare i64 @write(i32, i8*, i64) #2

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_rfill(%struct.__STDIO_FILE_STRUCT.424* noalias %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %rv = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %1, align 8
  %2 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !766
  %3 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !766
  %4 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %3, i32 0, i32 3, !dbg !766
  %5 = load i8** %4, align 8, !dbg !766
  %6 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !766
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %6, i32 0, i32 4, !dbg !766
  %8 = load i8** %7, align 8, !dbg !766
  %9 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !766
  %10 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %9, i32 0, i32 3, !dbg !766
  %11 = load i8** %10, align 8, !dbg !766
  %12 = ptrtoint i8* %8 to i64, !dbg !766
  %13 = ptrtoint i8* %11 to i64, !dbg !766
  %14 = sub i64 %12, %13, !dbg !766
  %15 = call i64 @__stdio_READ(%struct.__STDIO_FILE_STRUCT.424* %2, i8* %5, i64 %14) #11, !dbg !766
  store i64 %15, i64* %rv, align 8, !dbg !766
  %16 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !767
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %16, i32 0, i32 3, !dbg !767
  %18 = load i8** %17, align 8, !dbg !767
  %19 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !767
  %20 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %19, i32 0, i32 5, !dbg !767
  store i8* %18, i8** %20, align 8, !dbg !767
  %21 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !768
  %22 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %21, i32 0, i32 3, !dbg !768
  %23 = load i8** %22, align 8, !dbg !768
  %24 = load i64* %rv, align 8, !dbg !768
  %25 = getelementptr inbounds i8* %23, i64 %24, !dbg !768
  %26 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !768
  %27 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %26, i32 0, i32 6, !dbg !768
  store i8* %25, i8** %27, align 8, !dbg !768
  %28 = load i64* %rv, align 8, !dbg !769
  ret i64 %28, !dbg !769
}

; Function Attrs: nounwind uwtable
define hidden i32 @__stdio_trans2r_o(%struct.__STDIO_FILE_STRUCT.424* noalias %stream, i32 %oflag) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %3 = alloca i32, align 4
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %2, align 8
  store i32 %oflag, i32* %3, align 4
  %4 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !770
  %5 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %4, i32 0, i32 0, !dbg !770
  %6 = load i16* %5, align 2, !dbg !770
  %7 = zext i16 %6 to i32, !dbg !770
  %8 = load i32* %3, align 4, !dbg !770
  %9 = and i32 %7, %8, !dbg !770
  %10 = icmp ne i32 %9, 0, !dbg !770
  br i1 %10, label %26, label %11, !dbg !770

; <label>:11                                      ; preds = %0
  %12 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !772
  %13 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %12, i32 0, i32 0, !dbg !772
  %14 = load i16* %13, align 2, !dbg !772
  %15 = zext i16 %14 to i32, !dbg !772
  %16 = and i32 %15, 2176, !dbg !772
  %17 = icmp ne i32 %16, 0, !dbg !772
  br i1 %17, label %33, label %18, !dbg !772

; <label>:18                                      ; preds = %11
  %19 = load i32* %3, align 4, !dbg !775
  %20 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !775
  %21 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %20, i32 0, i32 0, !dbg !775
  %22 = load i16* %21, align 2, !dbg !775
  %23 = zext i16 %22 to i32, !dbg !775
  %24 = or i32 %23, %19, !dbg !775
  %25 = trunc i32 %24 to i16, !dbg !775
  store i16 %25, i16* %21, align 2, !dbg !775
  br label %26, !dbg !776

; <label>:26                                      ; preds = %18, %0
  %27 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !777
  %28 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %27, i32 0, i32 0, !dbg !777
  %29 = load i16* %28, align 2, !dbg !777
  %30 = zext i16 %29 to i32, !dbg !777
  %31 = and i32 %30, 16, !dbg !777
  %32 = icmp ne i32 %31, 0, !dbg !777
  br i1 %32, label %33, label %41, !dbg !777

; <label>:33                                      ; preds = %26, %11
  store i32 9, i32* @errno, align 4, !dbg !779
  br label %34, !dbg !779

; <label>:34                                      ; preds = %48, %33
  %35 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !781
  %36 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %35, i32 0, i32 0, !dbg !781
  %37 = load i16* %36, align 2, !dbg !781
  %38 = zext i16 %37 to i32, !dbg !781
  %39 = or i32 %38, 8, !dbg !781
  %40 = trunc i32 %39 to i16, !dbg !781
  store i16 %40, i16* %36, align 2, !dbg !781
  store i32 -1, i32* %1, !dbg !782
  br label %71, !dbg !782

; <label>:41                                      ; preds = %26
  %42 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !783
  %43 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %42, i32 0, i32 0, !dbg !783
  %44 = load i16* %43, align 2, !dbg !783
  %45 = zext i16 %44 to i32, !dbg !783
  %46 = and i32 %45, 64, !dbg !783
  %47 = icmp ne i32 %46, 0, !dbg !783
  br i1 %47, label %48, label %64, !dbg !783

; <label>:48                                      ; preds = %41
  %49 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !785
  %50 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.424* %49) #11, !dbg !785
  %51 = icmp ne i64 %50, 0, !dbg !785
  br i1 %51, label %34, label %52, !dbg !785

; <label>:52                                      ; preds = %48
  %53 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !788
  %54 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %53, i32 0, i32 3, !dbg !788
  %55 = load i8** %54, align 8, !dbg !788
  %56 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !788
  %57 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %56, i32 0, i32 8, !dbg !788
  store i8* %55, i8** %57, align 8, !dbg !788
  %58 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !789
  %59 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %58, i32 0, i32 0, !dbg !789
  %60 = load i16* %59, align 2, !dbg !789
  %61 = zext i16 %60 to i32, !dbg !789
  %62 = and i32 %61, -65, !dbg !789
  %63 = trunc i32 %62 to i16, !dbg !789
  store i16 %63, i16* %59, align 2, !dbg !789
  br label %64, !dbg !790

; <label>:64                                      ; preds = %52, %41
  %65 = load %struct.__STDIO_FILE_STRUCT.424** %2, align 8, !dbg !791
  %66 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %65, i32 0, i32 0, !dbg !791
  %67 = load i16* %66, align 2, !dbg !791
  %68 = zext i16 %67 to i32, !dbg !791
  %69 = or i32 %68, 1, !dbg !791
  %70 = trunc i32 %69 to i16, !dbg !791
  store i16 %70, i16* %66, align 2, !dbg !791
  store i32 0, i32* %1, !dbg !792
  br label %71, !dbg !792

; <label>:71                                      ; preds = %64, %34
  %72 = load i32* %1, !dbg !793
  ret i32 %72, !dbg !793
}

; Function Attrs: nounwind uwtable
define i32 @fflush_unlocked(%struct.__STDIO_FILE_STRUCT.424* %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.424*, align 8
  %retval = alloca i32, align 4
  %bufmask = alloca i16, align 2
  store %struct.__STDIO_FILE_STRUCT.424* %stream, %struct.__STDIO_FILE_STRUCT.424** %1, align 8
  store i32 0, i32* %retval, align 4, !dbg !794
  store i16 256, i16* %bufmask, align 2, !dbg !795
  %2 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !796
  %3 = icmp eq %struct.__STDIO_FILE_STRUCT.424* %2, bitcast (%struct.__STDIO_FILE_STRUCT.424** @_stdio_openlist to %struct.__STDIO_FILE_STRUCT.424*), !dbg !796
  br i1 %3, label %4, label %5, !dbg !796

; <label>:4                                       ; preds = %0
  store %struct.__STDIO_FILE_STRUCT.424* null, %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !798
  store i16 0, i16* %bufmask, align 2, !dbg !800
  br label %5, !dbg !801

; <label>:5                                       ; preds = %4, %0
  %6 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !802
  %7 = icmp ne %struct.__STDIO_FILE_STRUCT.424* %6, null, !dbg !802
  br i1 %7, label %52, label %8, !dbg !802

; <label>:8                                       ; preds = %5
  %9 = load %struct.__STDIO_FILE_STRUCT.424** @_stdio_openlist, align 8, !dbg !804
  store %struct.__STDIO_FILE_STRUCT.424* %9, %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !804
  br label %10, !dbg !806

; <label>:10                                      ; preds = %48, %8
  %11 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !806
  %12 = icmp ne %struct.__STDIO_FILE_STRUCT.424* %11, null, !dbg !806
  br i1 %12, label %13, label %76, !dbg !806

; <label>:13                                      ; preds = %10
  %14 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !807
  %15 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %14, i32 0, i32 0, !dbg !807
  %16 = load i16* %15, align 2, !dbg !807
  %17 = zext i16 %16 to i32, !dbg !807
  %18 = and i32 %17, 64, !dbg !807
  %19 = icmp ne i32 %18, 0, !dbg !807
  br i1 %19, label %20, label %48, !dbg !807

; <label>:20                                      ; preds = %13
  %21 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !810
  %22 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %21, i32 0, i32 0, !dbg !810
  %23 = load i16* %22, align 2, !dbg !810
  %24 = zext i16 %23 to i32, !dbg !810
  %25 = load i16* %bufmask, align 2, !dbg !810
  %26 = zext i16 %25 to i32, !dbg !810
  %27 = or i32 %24, %26, !dbg !810
  %28 = xor i32 %27, 320, !dbg !810
  %29 = and i32 %28, 832, !dbg !810
  %30 = icmp ne i32 %29, 0, !dbg !810
  br i1 %30, label %48, label %31, !dbg !810

; <label>:31                                      ; preds = %20
  %32 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !813
  %33 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.424* %32) #11, !dbg !813
  %34 = icmp ne i64 %33, 0, !dbg !813
  br i1 %34, label %47, label %35, !dbg !813

; <label>:35                                      ; preds = %31
  %36 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !816
  %37 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %36, i32 0, i32 3, !dbg !816
  %38 = load i8** %37, align 8, !dbg !816
  %39 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !816
  %40 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %39, i32 0, i32 8, !dbg !816
  store i8* %38, i8** %40, align 8, !dbg !816
  %41 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !818
  %42 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %41, i32 0, i32 0, !dbg !818
  %43 = load i16* %42, align 2, !dbg !818
  %44 = zext i16 %43 to i32, !dbg !818
  %45 = and i32 %44, -65, !dbg !818
  %46 = trunc i32 %45 to i16, !dbg !818
  store i16 %46, i16* %42, align 2, !dbg !818
  br label %48, !dbg !819

; <label>:47                                      ; preds = %31
  store i32 -1, i32* %retval, align 4, !dbg !820
  br label %48

; <label>:48                                      ; preds = %20, %47, %35, %13
  %49 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !822
  %50 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %49, i32 0, i32 9, !dbg !822
  %51 = load %struct.__STDIO_FILE_STRUCT.424** %50, align 8, !dbg !822
  store %struct.__STDIO_FILE_STRUCT.424* %51, %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !822
  br label %10, !dbg !823

; <label>:52                                      ; preds = %5
  %53 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !824
  %54 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %53, i32 0, i32 0, !dbg !824
  %55 = load i16* %54, align 2, !dbg !824
  %56 = zext i16 %55 to i32, !dbg !824
  %57 = and i32 %56, 64, !dbg !824
  %58 = icmp ne i32 %57, 0, !dbg !824
  br i1 %58, label %59, label %76, !dbg !824

; <label>:59                                      ; preds = %52
  %60 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !826
  %61 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.424* %60) #11, !dbg !826
  %62 = icmp ne i64 %61, 0, !dbg !826
  br i1 %62, label %75, label %63, !dbg !826

; <label>:63                                      ; preds = %59
  %64 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !829
  %65 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %64, i32 0, i32 3, !dbg !829
  %66 = load i8** %65, align 8, !dbg !829
  %67 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !829
  %68 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %67, i32 0, i32 8, !dbg !829
  store i8* %66, i8** %68, align 8, !dbg !829
  %69 = load %struct.__STDIO_FILE_STRUCT.424** %1, align 8, !dbg !831
  %70 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.424* %69, i32 0, i32 0, !dbg !831
  %71 = load i16* %70, align 2, !dbg !831
  %72 = zext i16 %71 to i32, !dbg !831
  %73 = and i32 %72, -65, !dbg !831
  %74 = trunc i32 %73 to i16, !dbg !831
  store i16 %74, i16* %70, align 2, !dbg !831
  br label %76, !dbg !832

; <label>:75                                      ; preds = %59
  store i32 -1, i32* %retval, align 4, !dbg !833
  br label %76

; <label>:76                                      ; preds = %52, %75, %63, %10
  %77 = load i32* %retval, align 4, !dbg !835
  ret i32 %77, !dbg !835
}

; Function Attrs: nounwind uwtable
define i8* @mempcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !836
  store i8* %4, i8** %r1, align 8, !dbg !836
  %5 = load i8** %2, align 8, !dbg !837
  store i8* %5, i8** %r2, align 8, !dbg !837
  br label %6, !dbg !838

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !838
  %8 = icmp ne i64 %7, 0, !dbg !838
  br i1 %8, label %9, label %17, !dbg !838

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !839
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !839
  store i8* %11, i8** %r2, align 8, !dbg !839
  %12 = load i8* %10, align 1, !dbg !839
  %13 = load i8** %r1, align 8, !dbg !839
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !839
  store i8* %14, i8** %r1, align 8, !dbg !839
  store i8 %12, i8* %13, align 1, !dbg !839
  %15 = load i64* %3, align 8, !dbg !841
  %16 = add i64 %15, -1, !dbg !841
  store i64 %16, i64* %3, align 8, !dbg !841
  br label %6, !dbg !842

; <label>:17                                      ; preds = %6
  %18 = load i8** %r1, align 8, !dbg !843
  ret i8* %18, !dbg !843
}

define i32 @main(i32, i8**) {
entry:
  call void @__uClibc_main(i32 (i32, i8**, i8**)* bitcast (i32 ()* @__user_main to i32 (i32, i8**, i8**)*), i32 %0, i8** %1, void ()* null, void ()* null, void ()* null, i8* null)
  unreachable
}

; Function Attrs: nounwind ssp uwtable
define void @klee_div_zero_check(i64 %z) #8 {
  %1 = icmp eq i64 %z, 0, !dbg !844
  br i1 %1, label %2, label %3, !dbg !844

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([60 x i8]* @.str44, i64 0, i64 0), i32 14, i8* getelementptr inbounds ([15 x i8]* @.str145, i64 0, i64 0), i8* getelementptr inbounds ([8 x i8]* @.str2, i64 0, i64 0)) #14, !dbg !846
  unreachable, !dbg !846

; <label>:3                                       ; preds = %0
  ret void, !dbg !847
}

; Function Attrs: noreturn
declare void @klee_report_error(i8*, i32, i8*, i8*) #9

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata) #1

; Function Attrs: nounwind ssp uwtable
define i32 @klee_int(i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = bitcast i32* %x to i8*, !dbg !848
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %1, i64 4, i8* %name) #12, !dbg !848
  %2 = load i32* %x, align 4, !dbg !849, !tbaa !850
  ret i32 %2, !dbg !849
}

; Function Attrs: nounwind ssp uwtable
define void @klee_overshift_check(i64 %bitWidth, i64 %shift) #8 {
  %1 = icmp ult i64 %shift, %bitWidth, !dbg !854
  br i1 %1, label %3, label %2, !dbg !854

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([8 x i8]* @.str3, i64 0, i64 0), i32 0, i8* getelementptr inbounds ([16 x i8]* @.str14, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8]* @.str25, i64 0, i64 0)) #14, !dbg !856
  unreachable, !dbg !856

; <label>:3                                       ; preds = %0
  ret void, !dbg !858
}

; Function Attrs: nounwind ssp uwtable
define i32 @klee_range(i32 %start, i32 %end, i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = icmp slt i32 %start, %end, !dbg !859
  br i1 %1, label %3, label %2, !dbg !859

; <label>:2                                       ; preds = %0
  call void @klee_report_error(i8* getelementptr inbounds ([51 x i8]* @.str6, i64 0, i64 0), i32 17, i8* getelementptr inbounds ([14 x i8]* @.str17, i64 0, i64 0), i8* getelementptr inbounds ([5 x i8]* @.str28, i64 0, i64 0)) #14, !dbg !861
  unreachable, !dbg !861

; <label>:3                                       ; preds = %0
  %4 = add nsw i32 %start, 1, !dbg !862
  %5 = icmp eq i32 %4, %end, !dbg !862
  br i1 %5, label %21, label %6, !dbg !862

; <label>:6                                       ; preds = %3
  %7 = bitcast i32* %x to i8*, !dbg !864
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %7, i64 4, i8* %name) #12, !dbg !864
  %8 = icmp eq i32 %start, 0, !dbg !866
  %9 = load i32* %x, align 4, !dbg !868, !tbaa !850
  br i1 %8, label %10, label %13, !dbg !866

; <label>:10                                      ; preds = %6
  %11 = icmp ult i32 %9, %end, !dbg !868
  %12 = zext i1 %11 to i64, !dbg !868
  call void @klee_assume(i64 %12) #12, !dbg !868
  br label %19, !dbg !870

; <label>:13                                      ; preds = %6
  %14 = icmp sge i32 %9, %start, !dbg !871
  %15 = zext i1 %14 to i64, !dbg !871
  call void @klee_assume(i64 %15) #12, !dbg !871
  %16 = load i32* %x, align 4, !dbg !873, !tbaa !850
  %17 = icmp slt i32 %16, %end, !dbg !873
  %18 = zext i1 %17 to i64, !dbg !873
  call void @klee_assume(i64 %18) #12, !dbg !873
  br label %19

; <label>:19                                      ; preds = %13, %10
  %20 = load i32* %x, align 4, !dbg !874, !tbaa !850
  br label %21, !dbg !874

; <label>:21                                      ; preds = %19, %3
  %.0 = phi i32 [ %20, %19 ], [ %start, %3 ]
  ret i32 %.0, !dbg !875
}

declare void @klee_assume(i64) #10

; Function Attrs: nounwind ssp uwtable
define weak i8* @memmove(i8* %dst, i8* %src, i64 %count) #8 {
  %1 = icmp eq i8* %src, %dst, !dbg !876
  br i1 %1, label %.loopexit, label %2, !dbg !876

; <label>:2                                       ; preds = %0
  %3 = icmp ugt i8* %src, %dst, !dbg !878
  br i1 %3, label %.preheader, label %18, !dbg !878

.preheader:                                       ; preds = %2
  %4 = icmp eq i64 %count, 0, !dbg !880
  br i1 %4, label %.loopexit, label %.lr.ph.preheader, !dbg !880

.lr.ph.preheader:                                 ; preds = %.preheader
  %n.vec = and i64 %count, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %5 = add i64 %count, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep11 = getelementptr i8* %src, i64 %5
  %scevgep = getelementptr i8* %dst, i64 %5
  %bound1 = icmp uge i8* %scevgep, %src
  %bound0 = icmp uge i8* %scevgep11, %dst
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %src, i64 %n.vec
  %ptr.ind.end13 = getelementptr i8* %dst, i64 %n.vec
  %rev.ind.end = sub i64 %count, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %src, i64 %index
  %next.gep110 = getelementptr i8* %dst, i64 %index
  %6 = bitcast i8* %next.gep to <16 x i8>*, !dbg !880
  %wide.load = load <16 x i8>* %6, align 1, !dbg !880
  %next.gep.sum586 = or i64 %index, 16, !dbg !880
  %7 = getelementptr i8* %src, i64 %next.gep.sum586, !dbg !880
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !880
  %wide.load207 = load <16 x i8>* %8, align 1, !dbg !880
  %9 = bitcast i8* %next.gep110 to <16 x i8>*, !dbg !880
  store <16 x i8> %wide.load, <16 x i8>* %9, align 1, !dbg !880
  %10 = getelementptr i8* %dst, i64 %next.gep.sum586, !dbg !880
  %11 = bitcast i8* %10 to <16 x i8>*, !dbg !880
  store <16 x i8> %wide.load207, <16 x i8>* %11, align 1, !dbg !880
  %index.next = add i64 %index, 32
  %12 = icmp eq i64 %index.next, %n.vec
  br i1 %12, label %middle.block, label %vector.body, !llvm.loop !882

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %src, %.lr.ph.preheader ], [ %src, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val12 = phi i8* [ %dst, %.lr.ph.preheader ], [ %dst, %vector.memcheck ], [ %ptr.ind.end13, %vector.body ]
  %resume.val14 = phi i64 [ %count, %.lr.ph.preheader ], [ %count, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %count
  br i1 %cmp.n, label %.loopexit, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %b.04 = phi i8* [ %14, %.lr.ph ], [ %resume.val, %middle.block ]
  %a.03 = phi i8* [ %16, %.lr.ph ], [ %resume.val12, %middle.block ]
  %.02 = phi i64 [ %13, %.lr.ph ], [ %resume.val14, %middle.block ]
  %13 = add i64 %.02, -1, !dbg !880
  %14 = getelementptr inbounds i8* %b.04, i64 1, !dbg !880
  %15 = load i8* %b.04, align 1, !dbg !880, !tbaa !885
  %16 = getelementptr inbounds i8* %a.03, i64 1, !dbg !880
  store i8 %15, i8* %a.03, align 1, !dbg !880, !tbaa !885
  %17 = icmp eq i64 %13, 0, !dbg !880
  br i1 %17, label %.loopexit, label %.lr.ph, !dbg !880, !llvm.loop !886

; <label>:18                                      ; preds = %2
  %19 = add i64 %count, -1, !dbg !887
  %20 = icmp eq i64 %count, 0, !dbg !889
  br i1 %20, label %.loopexit, label %.lr.ph9, !dbg !889

.lr.ph9:                                          ; preds = %18
  %21 = getelementptr inbounds i8* %src, i64 %19, !dbg !890
  %22 = getelementptr inbounds i8* %dst, i64 %19, !dbg !887
  %n.vec215 = and i64 %count, -32
  %cmp.zero217 = icmp eq i64 %n.vec215, 0
  br i1 %cmp.zero217, label %middle.block210, label %vector.memcheck224

vector.memcheck224:                               ; preds = %.lr.ph9
  %bound1221 = icmp ule i8* %21, %dst
  %bound0220 = icmp ule i8* %22, %src
  %memcheck.conflict223 = and i1 %bound0220, %bound1221
  %.sum = sub i64 %19, %n.vec215
  %rev.ptr.ind.end = getelementptr i8* %src, i64 %.sum
  %rev.ptr.ind.end229 = getelementptr i8* %dst, i64 %.sum
  %rev.ind.end231 = sub i64 %count, %n.vec215
  br i1 %memcheck.conflict223, label %middle.block210, label %vector.body209

vector.body209:                                   ; preds = %vector.body209, %vector.memcheck224
  %index212 = phi i64 [ %index.next234, %vector.body209 ], [ 0, %vector.memcheck224 ]
  %.sum440 = sub i64 %19, %index212
  %next.gep236.sum = add i64 %.sum440, -15, !dbg !889
  %23 = getelementptr i8* %src, i64 %next.gep236.sum, !dbg !889
  %24 = bitcast i8* %23 to <16 x i8>*, !dbg !889
  %wide.load434 = load <16 x i8>* %24, align 1, !dbg !889
  %reverse = shufflevector <16 x i8> %wide.load434, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !889
  %.sum505 = add i64 %.sum440, -31, !dbg !889
  %25 = getelementptr i8* %src, i64 %.sum505, !dbg !889
  %26 = bitcast i8* %25 to <16 x i8>*, !dbg !889
  %wide.load435 = load <16 x i8>* %26, align 1, !dbg !889
  %reverse436 = shufflevector <16 x i8> %wide.load435, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !889
  %reverse437 = shufflevector <16 x i8> %reverse, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !889
  %27 = getelementptr i8* %dst, i64 %next.gep236.sum, !dbg !889
  %28 = bitcast i8* %27 to <16 x i8>*, !dbg !889
  store <16 x i8> %reverse437, <16 x i8>* %28, align 1, !dbg !889
  %reverse438 = shufflevector <16 x i8> %reverse436, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !889
  %29 = getelementptr i8* %dst, i64 %.sum505, !dbg !889
  %30 = bitcast i8* %29 to <16 x i8>*, !dbg !889
  store <16 x i8> %reverse438, <16 x i8>* %30, align 1, !dbg !889
  %index.next234 = add i64 %index212, 32
  %31 = icmp eq i64 %index.next234, %n.vec215
  br i1 %31, label %middle.block210, label %vector.body209, !llvm.loop !891

middle.block210:                                  ; preds = %vector.body209, %vector.memcheck224, %.lr.ph9
  %resume.val225 = phi i8* [ %21, %.lr.ph9 ], [ %21, %vector.memcheck224 ], [ %rev.ptr.ind.end, %vector.body209 ]
  %resume.val227 = phi i8* [ %22, %.lr.ph9 ], [ %22, %vector.memcheck224 ], [ %rev.ptr.ind.end229, %vector.body209 ]
  %resume.val230 = phi i64 [ %count, %.lr.ph9 ], [ %count, %vector.memcheck224 ], [ %rev.ind.end231, %vector.body209 ]
  %new.indc.resume.val232 = phi i64 [ 0, %.lr.ph9 ], [ 0, %vector.memcheck224 ], [ %n.vec215, %vector.body209 ]
  %cmp.n233 = icmp eq i64 %new.indc.resume.val232, %count
  br i1 %cmp.n233, label %.loopexit, label %scalar.ph211

scalar.ph211:                                     ; preds = %scalar.ph211, %middle.block210
  %b.18 = phi i8* [ %33, %scalar.ph211 ], [ %resume.val225, %middle.block210 ]
  %a.17 = phi i8* [ %35, %scalar.ph211 ], [ %resume.val227, %middle.block210 ]
  %.16 = phi i64 [ %32, %scalar.ph211 ], [ %resume.val230, %middle.block210 ]
  %32 = add i64 %.16, -1, !dbg !889
  %33 = getelementptr inbounds i8* %b.18, i64 -1, !dbg !889
  %34 = load i8* %b.18, align 1, !dbg !889, !tbaa !885
  %35 = getelementptr inbounds i8* %a.17, i64 -1, !dbg !889
  store i8 %34, i8* %a.17, align 1, !dbg !889, !tbaa !885
  %36 = icmp eq i64 %32, 0, !dbg !889
  br i1 %36, label %.loopexit, label %scalar.ph211, !dbg !889, !llvm.loop !892

.loopexit:                                        ; preds = %scalar.ph211, %middle.block210, %18, %.lr.ph, %middle.block, %.preheader, %0
  ret i8* %dst, !dbg !893
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #4 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-floa
attributes #5 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false
attributes #6 = { inlinehint nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use
attributes #7 = { nounwind readnone uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #8 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nobuiltin }
attributes #12 = { nobuiltin nounwind }
attributes #13 = { nobuiltin nounwind readnone }
attributes #14 = { nobuiltin noreturn nounwind }

!llvm.dbg.cu = !{!0, !9, !24, !30, !69, !77, !82, !131, !162, !192, !204, !212, !219, !244, !250, !280, !312, !343, !373, !403, !411, !421, !431, !441, !453, !467, !481, !495}
!llvm.module.flags = !{!510, !511}
!llvm.ident = !{!512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512, !512}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !2, metadata !2, metadata !""} ; [ 
!1 = metadata !{metadata !"test.c", metadata !"/home/klee/workspace/basic-tests/stdin"}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 4, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @__user_main, null, null, metadata !2, i32 5} ; [ DW_TAG_subprogra
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/klee/workspace/basic-tests/stdin/test.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{metadata !8}
!8 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!9 = metadata !{i32 786449, metadata !10, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !11, metadata !18, metadata !2, metadata !""} ;
!10 = metadata !{metadata !"libc/stdio/gets.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"gets", metadata !"gets", metadata !"", i32 18, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*)* @gets, null, null, metadata !2, i32 19} ; [ DW_TAG_subpr
!13 = metadata !{i32 786473, metadata !10}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/gets.c]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{metadata !16, metadata !16}
!16 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !17} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from char]
!17 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!18 = metadata !{metadata !19}
!19 = metadata !{i32 786484, i32 0, null, metadata !"__evoke_link_warning_gets", metadata !"__evoke_link_warning_gets", metadata !"", metadata !13, i32 10, metadata !20, i32 1, i32 1, [57 x i8]* @__evoke_link_warning_gets, null} ; [ DW_TAG_variable ] [__
!20 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 456, i64 8, i32 0, i32 0, metadata !21, metadata !22, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 456, align 8, offset 0] [from ]
!21 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from char]
!22 = metadata !{metadata !23}
!23 = metadata !{i32 786465, i64 0, i64 57}       ; [ DW_TAG_subrange_type ] [0, 56]
!24 = metadata !{i32 786449, metadata !25, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !26, metadata !2, metadata !2, metadata !""} ;
!25 = metadata !{metadata !"libc/stdio/getchar_unlocked.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!26 = metadata !{metadata !27}
!27 = metadata !{i32 786478, metadata !28, metadata !29, metadata !"getchar_unlocked", metadata !"getchar_unlocked", metadata !"", i32 18, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @getchar_unlocked, null, null, metad
!28 = metadata !{metadata !"libc/stdio/getchar.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!29 = metadata !{i32 786473, metadata !28}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/getchar.c]
!30 = metadata !{i32 786449, metadata !31, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !32, metadata !58, metadata !2, metadata !""} 
!31 = metadata !{metadata !"libc/misc/internals/__uClibc_main.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!32 = metadata !{metadata !33, metadata !37, metadata !38, metadata !47, metadata !50, metadata !57}
!33 = metadata !{i32 786478, metadata !31, metadata !34, metadata !"__uClibc_init", metadata !"__uClibc_init", metadata !"", i32 187, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_init, null, null, metadata !2
!34 = metadata !{i32 786473, metadata !31}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!35 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !36, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!36 = metadata !{null}
!37 = metadata !{i32 786478, metadata !31, metadata !34, metadata !"__uClibc_fini", metadata !"__uClibc_fini", metadata !"", i32 251, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_fini, null, null, metadata !2
!38 = metadata !{i32 786478, metadata !31, metadata !34, metadata !"__uClibc_main", metadata !"__uClibc_main", metadata !"", i32 278, metadata !39, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32 (i32, i8**, i8**)*, i32, i8**, void ()
!39 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !40, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!40 = metadata !{null, metadata !41, metadata !8, metadata !44, metadata !45, metadata !45, metadata !45, metadata !46}
!41 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !42} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!42 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !43, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!43 = metadata !{metadata !8, metadata !8, metadata !44, metadata !44}
!44 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !16} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!45 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !35} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!46 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!47 = metadata !{i32 786478, metadata !31, metadata !34, metadata !"__check_one_fd", metadata !"__check_one_fd", metadata !"", i32 136, metadata !48, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @__check_one_fd, null, null, m
!48 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !49, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!49 = metadata !{null, metadata !8, metadata !8}
!50 = metadata !{i32 786478, metadata !51, metadata !52, metadata !"gnu_dev_makedev", metadata !"gnu_dev_makedev", metadata !"", i32 54, metadata !53, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i32, i32)* @gnu_dev_makedev, null, null, 
!51 = metadata !{metadata !"./include/sys/sysmacros.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!52 = metadata !{i32 786473, metadata !51}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!53 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !54, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!54 = metadata !{metadata !55, metadata !56, metadata !56}
!55 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!56 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!57 = metadata !{i32 786478, metadata !31, metadata !34, metadata !"__check_suid", metadata !"__check_suid", metadata !"", i32 155, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @__check_suid, null, null, metadata !2, i32 
!58 = metadata !{metadata !59, metadata !60, metadata !62, metadata !63, metadata !66, metadata !67, metadata !68}
!59 = metadata !{i32 786484, i32 0, null, metadata !"__libc_stack_end", metadata !"__libc_stack_end", metadata !"", metadata !34, i32 52, metadata !46, i32 0, i32 1, i8** @__libc_stack_end, null} ; [ DW_TAG_variable ] [__libc_stack_end] [line 52] [def]
!60 = metadata !{i32 786484, i32 0, null, metadata !"__uclibc_progname", metadata !"__uclibc_progname", metadata !"", metadata !34, i32 110, metadata !61, i32 0, i32 1, i8** @__uclibc_progname, null} ; [ DW_TAG_variable ] [__uclibc_progname] [line 110] [
!61 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !21} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!62 = metadata !{i32 786484, i32 0, null, metadata !"__environ", metadata !"__environ", metadata !"", metadata !34, i32 125, metadata !44, i32 0, i32 1, i8*** @__environ, null} ; [ DW_TAG_variable ] [__environ] [line 125] [def]
!63 = metadata !{i32 786484, i32 0, null, metadata !"__pagesize", metadata !"__pagesize", metadata !"", metadata !34, i32 129, metadata !64, i32 0, i32 1, i64* @__pagesize, null} ; [ DW_TAG_variable ] [__pagesize] [line 129] [def]
!64 = metadata !{i32 786454, metadata !31, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!65 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!66 = metadata !{i32 786484, i32 0, metadata !33, metadata !"been_there_done_that", metadata !"been_there_done_that", metadata !"", metadata !34, i32 189, metadata !8, i32 1, i32 1, i32* @__uClibc_init.been_there_done_that, null} ; [ DW_TAG_variable ] [b
!67 = metadata !{i32 786484, i32 0, null, metadata !"__app_fini", metadata !"__app_fini", metadata !"", metadata !34, i32 244, metadata !45, i32 0, i32 1, void ()** @__app_fini, null} ; [ DW_TAG_variable ] [__app_fini] [line 244] [def]
!68 = metadata !{i32 786484, i32 0, null, metadata !"__rtld_fini", metadata !"__rtld_fini", metadata !"", metadata !34, i32 247, metadata !45, i32 0, i32 1, void ()** @__rtld_fini, null} ; [ DW_TAG_variable ] [__rtld_fini] [line 247] [def]
!69 = metadata !{i32 786449, metadata !70, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !71, metadata !2, metadata !2, metadata !""} ;
!70 = metadata !{metadata !"libc/misc/internals/__errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!71 = metadata !{metadata !72}
!72 = metadata !{i32 786478, metadata !70, metadata !73, metadata !"__errno_location", metadata !"__errno_location", metadata !"", i32 11, metadata !74, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__errno_location, null, null, met
!73 = metadata !{i32 786473, metadata !70}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__errno_location.c]
!74 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !75, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!75 = metadata !{metadata !76}
!76 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!77 = metadata !{i32 786449, metadata !78, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !79, metadata !2, metadata !2, metadata !""} ;
!78 = metadata !{metadata !"libc/misc/internals/__h_errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!79 = metadata !{metadata !80}
!80 = metadata !{i32 786478, metadata !78, metadata !81, metadata !"__h_errno_location", metadata !"__h_errno_location", metadata !"", i32 10, metadata !74, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__h_errno_location, null, nul
!81 = metadata !{i32 786473, metadata !78}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__h_errno_location.c]
!82 = metadata !{i32 786449, metadata !83, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !84, metadata !88, metadata !2, metadata !""} 
!83 = metadata !{metadata !"libc/stdio/_stdio.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!84 = metadata !{metadata !85, metadata !87}
!85 = metadata !{i32 786478, metadata !83, metadata !86, metadata !"_stdio_term", metadata !"_stdio_term", metadata !"", i32 210, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_term, null, null, metadata !2, i32 
!86 = metadata !{i32 786473, metadata !83}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!87 = metadata !{i32 786478, metadata !83, metadata !86, metadata !"_stdio_init", metadata !"_stdio_init", metadata !"", i32 277, metadata !35, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_init, null, null, metadata !2, i32 
!88 = metadata !{metadata !89, metadata !122, metadata !123, metadata !124, metadata !125, metadata !126, metadata !127}
!89 = metadata !{i32 786484, i32 0, null, metadata !"stdin", metadata !"stdin", metadata !"", metadata !86, i32 154, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @stdin, null} ; [ DW_TAG_variable ] [stdin] [line 154] [def]
!90 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !91} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!91 = metadata !{i32 786454, metadata !83, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !92} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!92 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !94, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offset
!93 = metadata !{metadata !"./include/bits/uClibc_stdio.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!94 = metadata !{metadata !95, metadata !97, metadata !102, metadata !103, metadata !105, metadata !106, metadata !107, metadata !108, metadata !109, metadata !110, metadata !112, metadata !115}
!95 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!96 = metadata !{i32 786468, null, null, metadata !"unsigned short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned short] [line 0, size 16, align 16, offset 0, enc DW_ATE_unsigned]
!97 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!98 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 16, i64 8, i32 0, i32 0, metadata !99, metadata !100, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 16, align 8, offset 0] [from unsigned char]
!99 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!100 = metadata !{metadata !101}
!101 = metadata !{i32 786465, i64 0, i64 2}       ; [ DW_TAG_subrange_type ] [0, 1]
!102 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!103 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!104 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !99} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from unsigned char]
!105 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!106 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!107 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!108 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!109 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!110 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !111} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!111 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !92} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!112 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!113 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 32, i32 0, i32 0, metadata !114, metadata !100, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 64, align 32, offset 0] [from wchar_t]
!114 = metadata !{i32 786454, metadata !93, null, metadata !"wchar_t", i32 65, i64 0, i64 0, i64 0, i32 0, metadata !8} ; [ DW_TAG_typedef ] [wchar_t] [line 65, size 0, align 0, offset 0] [from int]
!115 = metadata !{i32 786445, metadata !93, metadata !92, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !116} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!116 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !117} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!117 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !119, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!118 = metadata !{metadata !"./include/wchar.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!119 = metadata !{metadata !120, metadata !121}
!120 = metadata !{i32 786445, metadata !118, metadata !117, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!121 = metadata !{i32 786445, metadata !118, metadata !117, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!122 = metadata !{i32 786484, i32 0, null, metadata !"stdout", metadata !"stdout", metadata !"", metadata !86, i32 155, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @stdout, null} ; [ DW_TAG_variable ] [stdout] [line 155] [def]
!123 = metadata !{i32 786484, i32 0, null, metadata !"stderr", metadata !"stderr", metadata !"", metadata !86, i32 156, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @stderr, null} ; [ DW_TAG_variable ] [stderr] [line 156] [def]
!124 = metadata !{i32 786484, i32 0, null, metadata !"__stdin", metadata !"__stdin", metadata !"", metadata !86, i32 159, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @__stdin, null} ; [ DW_TAG_variable ] [__stdin] [line 159] [def]
!125 = metadata !{i32 786484, i32 0, null, metadata !"__stdout", metadata !"__stdout", metadata !"", metadata !86, i32 162, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @__stdout, null} ; [ DW_TAG_variable ] [__stdout] [line 162] [def]
!126 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_openlist", metadata !"_stdio_openlist", metadata !"", metadata !86, i32 180, metadata !90, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.424** @_stdio_openlist, null} ; [ DW_TAG_variable ] [_stdio_
!127 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_streams", metadata !"_stdio_streams", metadata !"", metadata !86, i32 131, metadata !128, i32 1, i32 1, [3 x %struct.__STDIO_FILE_STRUCT.424]* @_stdio_streams, null} ; [ DW_TAG_variable ] [_std
!128 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1920, i64 64, i32 0, i32 0, metadata !91, metadata !129, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 1920, align 64, offset 0] [from FILE]
!129 = metadata !{metadata !130}
!130 = metadata !{i32 786465, i64 0, i64 3}       ; [ DW_TAG_subrange_type ] [0, 2]
!131 = metadata !{i32 786449, metadata !132, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !133, metadata !2, metadata !2, metadata !""
!132 = metadata !{metadata !"libc/stdio/_wcommit.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!133 = metadata !{metadata !134}
!134 = metadata !{i32 786478, metadata !132, metadata !135, metadata !"__stdio_wcommit", metadata !"__stdio_wcommit", metadata !"", i32 17, metadata !136, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.424*)* @
!135 = metadata !{i32 786473, metadata !132}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!136 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !137, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!137 = metadata !{metadata !138, metadata !139}
!138 = metadata !{i32 786454, metadata !132, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!139 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !140} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!140 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !141} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!141 = metadata !{i32 786454, metadata !132, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !142} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!142 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !143, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!143 = metadata !{metadata !144, metadata !145, metadata !146, metadata !147, metadata !148, metadata !149, metadata !150, metadata !151, metadata !152, metadata !153, metadata !155, metadata !156}
!144 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!145 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!146 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!147 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!148 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!149 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!150 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!151 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!152 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!153 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !154} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!154 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !142} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!155 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!156 = metadata !{i32 786445, metadata !93, metadata !142, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !157} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!157 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !158} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!158 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !159, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!159 = metadata !{metadata !160, metadata !161}
!160 = metadata !{i32 786445, metadata !118, metadata !158, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!161 = metadata !{i32 786445, metadata !118, metadata !158, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!162 = metadata !{i32 786449, metadata !163, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !164, metadata !2, metadata !2, metadata !""
!163 = metadata !{metadata !"libc/stdio/fgetc_unlocked.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!164 = metadata !{metadata !165}
!165 = metadata !{i32 786478, metadata !166, metadata !167, metadata !"__fgetc_unlocked", metadata !"__fgetc_unlocked", metadata !"", i32 22, metadata !168, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct.__STDIO_FILE_STRUCT.424*)*
!166 = metadata !{metadata !"libc/stdio/fgetc.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!167 = metadata !{i32 786473, metadata !166}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!168 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !169, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!169 = metadata !{metadata !8, metadata !170}
!170 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !171} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!171 = metadata !{i32 786454, metadata !166, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !172} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!172 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !173, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!173 = metadata !{metadata !174, metadata !175, metadata !176, metadata !177, metadata !178, metadata !179, metadata !180, metadata !181, metadata !182, metadata !183, metadata !185, metadata !186}
!174 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!175 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!176 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!177 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!178 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!179 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!180 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!181 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!182 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!183 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !184} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!184 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !172} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!185 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!186 = metadata !{i32 786445, metadata !93, metadata !172, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !187} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!187 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !188} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!188 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !189, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!189 = metadata !{metadata !190, metadata !191}
!190 = metadata !{i32 786445, metadata !118, metadata !188, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!191 = metadata !{i32 786445, metadata !118, metadata !188, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!192 = metadata !{i32 786449, metadata !193, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !194, metadata !2, metadata !2, metadata !""
!193 = metadata !{metadata !"libc/string/memcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!194 = metadata !{metadata !195}
!195 = metadata !{i32 786478, metadata !193, metadata !196, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 18, metadata !197, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !2, i32 
!196 = metadata !{i32 786473, metadata !193}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!197 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !198, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!198 = metadata !{metadata !46, metadata !199, metadata !200, metadata !203}
!199 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !46} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!200 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !201} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!201 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !202} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!202 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!203 = metadata !{i32 786454, metadata !193, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!204 = metadata !{i32 786449, metadata !205, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !206, metadata !2, metadata !2, metadata !""
!205 = metadata !{metadata !"libc/string/memset.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!206 = metadata !{metadata !207}
!207 = metadata !{i32 786478, metadata !205, metadata !208, metadata !"memset", metadata !"memset", metadata !"", i32 17, metadata !209, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i32, i64)* @memset, null, null, metadata !2, i32 
!208 = metadata !{i32 786473, metadata !205}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!209 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !210, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!210 = metadata !{metadata !46, metadata !46, metadata !8, metadata !211}
!211 = metadata !{i32 786454, metadata !205, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!212 = metadata !{i32 786449, metadata !213, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !214, metadata !2, metadata !2, metadata !""
!213 = metadata !{metadata !"libc/termios/isatty.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!214 = metadata !{metadata !215}
!215 = metadata !{i32 786478, metadata !213, metadata !216, metadata !"isatty", metadata !"isatty", metadata !"", i32 26, metadata !217, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @isatty, null, null, metadata !2, i32 27} ; [ DW
!216 = metadata !{i32 786473, metadata !213}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/isatty.c]
!217 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !218, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!218 = metadata !{metadata !8, metadata !8}
!219 = metadata !{i32 786449, metadata !220, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !221, metadata !2, metadata !2, metadata !""
!220 = metadata !{metadata !"libc/termios/tcgetattr.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!221 = metadata !{metadata !222}
!222 = metadata !{i32 786478, metadata !220, metadata !223, metadata !"tcgetattr", metadata !"tcgetattr", metadata !"", i32 38, metadata !224, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, %struct.termios.442*)* @tcgetattr, null, nu
!223 = metadata !{i32 786473, metadata !220}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!224 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !225, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!225 = metadata !{metadata !8, metadata !8, metadata !226}
!226 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !227} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from termios]
!227 = metadata !{i32 786451, metadata !228, null, metadata !"termios", i32 30, i64 480, i64 32, i32 0, i32 0, null, metadata !229, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [termios] [line 30, size 480, align 32, offset 0] [def] [from ]
!228 = metadata !{metadata !"./include/bits/termios.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!229 = metadata !{metadata !230, metadata !232, metadata !233, metadata !234, metadata !235, metadata !237, metadata !241, metadata !243}
!230 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_iflag", i32 32, i64 32, i64 32, i64 0, i32 0, metadata !231} ; [ DW_TAG_member ] [c_iflag] [line 32, size 32, align 32, offset 0] [from tcflag_t]
!231 = metadata !{i32 786454, metadata !228, null, metadata !"tcflag_t", i32 27, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [tcflag_t] [line 27, size 0, align 0, offset 0] [from unsigned int]
!232 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_oflag", i32 33, i64 32, i64 32, i64 32, i32 0, metadata !231} ; [ DW_TAG_member ] [c_oflag] [line 33, size 32, align 32, offset 32] [from tcflag_t]
!233 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_cflag", i32 34, i64 32, i64 32, i64 64, i32 0, metadata !231} ; [ DW_TAG_member ] [c_cflag] [line 34, size 32, align 32, offset 64] [from tcflag_t]
!234 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_lflag", i32 35, i64 32, i64 32, i64 96, i32 0, metadata !231} ; [ DW_TAG_member ] [c_lflag] [line 35, size 32, align 32, offset 96] [from tcflag_t]
!235 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_line", i32 36, i64 8, i64 8, i64 128, i32 0, metadata !236} ; [ DW_TAG_member ] [c_line] [line 36, size 8, align 8, offset 128] [from cc_t]
!236 = metadata !{i32 786454, metadata !228, null, metadata !"cc_t", i32 25, i64 0, i64 0, i64 0, i32 0, metadata !99} ; [ DW_TAG_typedef ] [cc_t] [line 25, size 0, align 0, offset 0] [from unsigned char]
!237 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_cc", i32 37, i64 256, i64 8, i64 136, i32 0, metadata !238} ; [ DW_TAG_member ] [c_cc] [line 37, size 256, align 8, offset 136] [from ]
!238 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 256, i64 8, i32 0, i32 0, metadata !236, metadata !239, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 256, align 8, offset 0] [from cc_t]
!239 = metadata !{metadata !240}
!240 = metadata !{i32 786465, i64 0, i64 32}      ; [ DW_TAG_subrange_type ] [0, 31]
!241 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_ispeed", i32 38, i64 32, i64 32, i64 416, i32 0, metadata !242} ; [ DW_TAG_member ] [c_ispeed] [line 38, size 32, align 32, offset 416] [from speed_t]
!242 = metadata !{i32 786454, metadata !228, null, metadata !"speed_t", i32 26, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [speed_t] [line 26, size 0, align 0, offset 0] [from unsigned int]
!243 = metadata !{i32 786445, metadata !228, metadata !227, metadata !"c_ospeed", i32 39, i64 32, i64 32, i64 448, i32 0, metadata !242} ; [ DW_TAG_member ] [c_ospeed] [line 39, size 32, align 32, offset 448] [from speed_t]
!244 = metadata !{i32 786449, metadata !245, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !2, metadata !246, metadata !2, metadata !""
!245 = metadata !{metadata !"libc/misc/internals/errno.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!246 = metadata !{metadata !247, metadata !249}
!247 = metadata !{i32 786484, i32 0, null, metadata !"errno", metadata !"errno", metadata !"", metadata !248, i32 7, metadata !8, i32 0, i32 1, i32* @errno, null} ; [ DW_TAG_variable ] [errno] [line 7] [def]
!248 = metadata !{i32 786473, metadata !245}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/errno.c]
!249 = metadata !{i32 786484, i32 0, null, metadata !"h_errno", metadata !"h_errno", metadata !"", metadata !248, i32 8, metadata !8, i32 0, i32 1, i32* @h_errno, null} ; [ DW_TAG_variable ] [h_errno] [line 8] [def]
!250 = metadata !{i32 786449, metadata !251, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !252, metadata !2, metadata !2, metadata !""
!251 = metadata !{metadata !"libc/stdio/_READ.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!252 = metadata !{metadata !253}
!253 = metadata !{i32 786478, metadata !251, metadata !254, metadata !"__stdio_READ", metadata !"__stdio_READ", metadata !"", i32 26, metadata !255, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.424*, i8*, i64
!254 = metadata !{i32 786473, metadata !251}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!255 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !256, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!256 = metadata !{metadata !257, metadata !258, metadata !104, metadata !257}
!257 = metadata !{i32 786454, metadata !251, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!258 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !259} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!259 = metadata !{i32 786454, metadata !251, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !260} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!260 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !261, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!261 = metadata !{metadata !262, metadata !263, metadata !264, metadata !265, metadata !266, metadata !267, metadata !268, metadata !269, metadata !270, metadata !271, metadata !273, metadata !274}
!262 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!263 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!264 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!265 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!266 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!267 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!268 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!269 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!270 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!271 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !272} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!272 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !260} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!273 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!274 = metadata !{i32 786445, metadata !93, metadata !260, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !275} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!275 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !276} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!276 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !277, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!277 = metadata !{metadata !278, metadata !279}
!278 = metadata !{i32 786445, metadata !118, metadata !276, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!279 = metadata !{i32 786445, metadata !118, metadata !276, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!280 = metadata !{i32 786449, metadata !281, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !282, metadata !2, metadata !2, metadata !""
!281 = metadata !{metadata !"libc/stdio/_WRITE.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!282 = metadata !{metadata !283}
!283 = metadata !{i32 786478, metadata !281, metadata !284, metadata !"__stdio_WRITE", metadata !"__stdio_WRITE", metadata !"", i32 33, metadata !285, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.424*, i8*, i
!284 = metadata !{i32 786473, metadata !281}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!285 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !286, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!286 = metadata !{metadata !287, metadata !288, metadata !310, metadata !287}
!287 = metadata !{i32 786454, metadata !281, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!288 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !289} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!289 = metadata !{i32 786454, metadata !281, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !290} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!290 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !291, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!291 = metadata !{metadata !292, metadata !293, metadata !294, metadata !295, metadata !296, metadata !297, metadata !298, metadata !299, metadata !300, metadata !301, metadata !303, metadata !304}
!292 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!293 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!294 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!295 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!296 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!297 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!298 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!299 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!300 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!301 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !302} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!302 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !290} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!303 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!304 = metadata !{i32 786445, metadata !93, metadata !290, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !305} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!305 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !306} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!306 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !307, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!307 = metadata !{metadata !308, metadata !309}
!308 = metadata !{i32 786445, metadata !118, metadata !306, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!309 = metadata !{i32 786445, metadata !118, metadata !306, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!310 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !311} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!311 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !99} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from unsigned char]
!312 = metadata !{i32 786449, metadata !313, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !314, metadata !2, metadata !2, metadata !""
!313 = metadata !{metadata !"libc/stdio/_rfill.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!314 = metadata !{metadata !315}
!315 = metadata !{i32 786478, metadata !313, metadata !316, metadata !"__stdio_rfill", metadata !"__stdio_rfill", metadata !"", i32 22, metadata !317, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.424*)* @__st
!316 = metadata !{i32 786473, metadata !313}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_rfill.c]
!317 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !318, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!318 = metadata !{metadata !319, metadata !320}
!319 = metadata !{i32 786454, metadata !313, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!320 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !321} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!321 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !322} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!322 = metadata !{i32 786454, metadata !313, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !323} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!323 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !324, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!324 = metadata !{metadata !325, metadata !326, metadata !327, metadata !328, metadata !329, metadata !330, metadata !331, metadata !332, metadata !333, metadata !334, metadata !336, metadata !337}
!325 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!326 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!327 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!328 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!329 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!330 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!331 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!332 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!333 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!334 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !335} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!335 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !323} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!336 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!337 = metadata !{i32 786445, metadata !93, metadata !323, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !338} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!338 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !339} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!339 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !340, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!340 = metadata !{metadata !341, metadata !342}
!341 = metadata !{i32 786445, metadata !118, metadata !339, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!342 = metadata !{i32 786445, metadata !118, metadata !339, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!343 = metadata !{i32 786449, metadata !344, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !345, metadata !2, metadata !2, metadata !""
!344 = metadata !{metadata !"libc/stdio/_trans2r.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!345 = metadata !{metadata !346}
!346 = metadata !{i32 786478, metadata !344, metadata !347, metadata !"__stdio_trans2r_o", metadata !"__stdio_trans2r_o", metadata !"", i32 25, metadata !348, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct.__STDIO_FILE_STRUCT.424*
!347 = metadata !{i32 786473, metadata !344}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!348 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !349, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!349 = metadata !{metadata !8, metadata !350, metadata !8}
!350 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !351} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!351 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !352} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!352 = metadata !{i32 786454, metadata !344, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !353} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!353 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !354, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!354 = metadata !{metadata !355, metadata !356, metadata !357, metadata !358, metadata !359, metadata !360, metadata !361, metadata !362, metadata !363, metadata !364, metadata !366, metadata !367}
!355 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!356 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!357 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!358 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!359 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!360 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!361 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!362 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!363 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!364 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !365} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!365 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !353} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!366 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!367 = metadata !{i32 786445, metadata !93, metadata !353, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !368} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!368 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !369} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!369 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !370, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!370 = metadata !{metadata !371, metadata !372}
!371 = metadata !{i32 786445, metadata !118, metadata !369, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!372 = metadata !{i32 786445, metadata !118, metadata !369, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!373 = metadata !{i32 786449, metadata !374, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !375, metadata !2, metadata !2, metadata !""
!374 = metadata !{metadata !"libc/stdio/fflush_unlocked.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!375 = metadata !{metadata !376}
!376 = metadata !{i32 786478, metadata !377, metadata !378, metadata !"fflush_unlocked", metadata !"fflush_unlocked", metadata !"", i32 69, metadata !379, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (%struct.__STDIO_FILE_STRUCT.424*)* @
!377 = metadata !{metadata !"libc/stdio/fflush.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!378 = metadata !{i32 786473, metadata !377}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!379 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !380, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!380 = metadata !{metadata !8, metadata !381}
!381 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !382} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!382 = metadata !{i32 786454, metadata !377, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !383} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!383 = metadata !{i32 786451, metadata !93, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !384, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!384 = metadata !{metadata !385, metadata !386, metadata !387, metadata !388, metadata !389, metadata !390, metadata !391, metadata !392, metadata !393, metadata !394, metadata !396, metadata !397}
!385 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!386 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !98} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!387 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!388 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!389 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!390 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!391 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!392 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!393 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !104} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!394 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !395} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!395 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !383} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!396 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !113} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!397 = metadata !{i32 786445, metadata !93, metadata !383, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !398} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!398 = metadata !{i32 786454, metadata !93, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !399} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!399 = metadata !{i32 786451, metadata !118, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !400, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!400 = metadata !{metadata !401, metadata !402}
!401 = metadata !{i32 786445, metadata !118, metadata !399, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !114} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!402 = metadata !{i32 786445, metadata !118, metadata !399, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !114} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!403 = metadata !{i32 786449, metadata !404, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !405, metadata !2, metadata !2, metadata !""
!404 = metadata !{metadata !"libc/string/mempcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!405 = metadata !{metadata !406}
!406 = metadata !{i32 786478, metadata !404, metadata !407, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 20, metadata !408, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !2, i
!407 = metadata !{i32 786473, metadata !404}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!408 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !409, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!409 = metadata !{metadata !46, metadata !199, metadata !200, metadata !410}
!410 = metadata !{i32 786454, metadata !404, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!411 = metadata !{i32 786449, metadata !412, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !413, metadata !2, metadata !2, metadata !""} 
!412 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!413 = metadata !{metadata !414}
!414 = metadata !{i32 786478, metadata !412, metadata !415, metadata !"klee_div_zero_check", metadata !"klee_div_zero_check", metadata !"", i32 12, metadata !416, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64)* @klee_div_zero_check, 
!415 = metadata !{i32 786473, metadata !412}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!416 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !417, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!417 = metadata !{null, metadata !418}
!418 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!419 = metadata !{metadata !420}
!420 = metadata !{i32 786689, metadata !414, metadata !"z", metadata !415, i32 16777228, metadata !418, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [z] [line 12]
!421 = metadata !{i32 786449, metadata !422, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !423, metadata !2, metadata !2, metadata !""} 
!422 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_int.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!423 = metadata !{metadata !424}
!424 = metadata !{i32 786478, metadata !422, metadata !425, metadata !"klee_int", metadata !"klee_int", metadata !"", i32 13, metadata !426, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i8*)* @klee_int, null, null, metadata !428, i32 13}
!425 = metadata !{i32 786473, metadata !422}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_int.c]
!426 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !427, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!427 = metadata !{metadata !8, metadata !61}
!428 = metadata !{metadata !429, metadata !430}
!429 = metadata !{i32 786689, metadata !424, metadata !"name", metadata !425, i32 16777229, metadata !61, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!430 = metadata !{i32 786688, metadata !424, metadata !"x", metadata !425, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!431 = metadata !{i32 786449, metadata !432, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !433, metadata !2, metadata !2, metadata !""} 
!432 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!433 = metadata !{metadata !434}
!434 = metadata !{i32 786478, metadata !432, metadata !435, metadata !"klee_overshift_check", metadata !"klee_overshift_check", metadata !"", i32 20, metadata !436, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64, i64)* @klee_overshift
!435 = metadata !{i32 786473, metadata !432}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!436 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !437, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!437 = metadata !{null, metadata !55, metadata !55}
!438 = metadata !{metadata !439, metadata !440}
!439 = metadata !{i32 786689, metadata !434, metadata !"bitWidth", metadata !435, i32 16777236, metadata !55, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [bitWidth] [line 20]
!440 = metadata !{i32 786689, metadata !434, metadata !"shift", metadata !435, i32 33554452, metadata !55, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [shift] [line 20]
!441 = metadata !{i32 786449, metadata !442, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !443, metadata !2, metadata !2, metadata !""} 
!442 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_range.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!443 = metadata !{metadata !444}
!444 = metadata !{i32 786478, metadata !442, metadata !445, metadata !"klee_range", metadata !"klee_range", metadata !"", i32 13, metadata !446, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i32, i32, i8*)* @klee_range, null, null, metada
!445 = metadata !{i32 786473, metadata !442}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!446 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !447, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!447 = metadata !{metadata !8, metadata !8, metadata !8, metadata !61}
!448 = metadata !{metadata !449, metadata !450, metadata !451, metadata !452}
!449 = metadata !{i32 786689, metadata !444, metadata !"start", metadata !445, i32 16777229, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [start] [line 13]
!450 = metadata !{i32 786689, metadata !444, metadata !"end", metadata !445, i32 33554445, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [end] [line 13]
!451 = metadata !{i32 786689, metadata !444, metadata !"name", metadata !445, i32 50331661, metadata !61, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!452 = metadata !{i32 786688, metadata !444, metadata !"x", metadata !445, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!453 = metadata !{i32 786449, metadata !454, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !455, metadata !2, metadata !2, metadata !""} 
!454 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!455 = metadata !{metadata !456}
!456 = metadata !{i32 786478, metadata !454, metadata !457, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 12, metadata !458, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !461, i32
!457 = metadata !{i32 786473, metadata !454}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memcpy.c]
!458 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !459, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!459 = metadata !{metadata !46, metadata !46, metadata !201, metadata !460}
!460 = metadata !{i32 786454, metadata !454, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!461 = metadata !{metadata !462, metadata !463, metadata !464, metadata !465, metadata !466}
!462 = metadata !{i32 786689, metadata !456, metadata !"destaddr", metadata !457, i32 16777228, metadata !46, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 12]
!463 = metadata !{i32 786689, metadata !456, metadata !"srcaddr", metadata !457, i32 33554444, metadata !201, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 12]
!464 = metadata !{i32 786689, metadata !456, metadata !"len", metadata !457, i32 50331660, metadata !460, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 12]
!465 = metadata !{i32 786688, metadata !456, metadata !"dest", metadata !457, i32 13, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 13]
!466 = metadata !{i32 786688, metadata !456, metadata !"src", metadata !457, i32 14, metadata !61, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 14]
!467 = metadata !{i32 786449, metadata !468, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !469, metadata !2, metadata !2, metadata !""} 
!468 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memmove.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!469 = metadata !{metadata !470}
!470 = metadata !{i32 786478, metadata !468, metadata !471, metadata !"memmove", metadata !"memmove", metadata !"", i32 12, metadata !472, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memmove, null, null, metadata !475, 
!471 = metadata !{i32 786473, metadata !468}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!472 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !473, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!473 = metadata !{metadata !46, metadata !46, metadata !201, metadata !474}
!474 = metadata !{i32 786454, metadata !468, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!475 = metadata !{metadata !476, metadata !477, metadata !478, metadata !479, metadata !480}
!476 = metadata !{i32 786689, metadata !470, metadata !"dst", metadata !471, i32 16777228, metadata !46, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 12]
!477 = metadata !{i32 786689, metadata !470, metadata !"src", metadata !471, i32 33554444, metadata !201, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [src] [line 12]
!478 = metadata !{i32 786689, metadata !470, metadata !"count", metadata !471, i32 50331660, metadata !474, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 12]
!479 = metadata !{i32 786688, metadata !470, metadata !"a", metadata !471, i32 13, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 13]
!480 = metadata !{i32 786688, metadata !470, metadata !"b", metadata !471, i32 14, metadata !61, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [b] [line 14]
!481 = metadata !{i32 786449, metadata !482, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !483, metadata !2, metadata !2, metadata !""} 
!482 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/mempcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!483 = metadata !{metadata !484}
!484 = metadata !{i32 786478, metadata !482, metadata !485, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 11, metadata !486, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !489, 
!485 = metadata !{i32 786473, metadata !482}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/mempcpy.c]
!486 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !487, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!487 = metadata !{metadata !46, metadata !46, metadata !201, metadata !488}
!488 = metadata !{i32 786454, metadata !482, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!489 = metadata !{metadata !490, metadata !491, metadata !492, metadata !493, metadata !494}
!490 = metadata !{i32 786689, metadata !484, metadata !"destaddr", metadata !485, i32 16777227, metadata !46, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 11]
!491 = metadata !{i32 786689, metadata !484, metadata !"srcaddr", metadata !485, i32 33554443, metadata !201, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 11]
!492 = metadata !{i32 786689, metadata !484, metadata !"len", metadata !485, i32 50331659, metadata !488, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 11]
!493 = metadata !{i32 786688, metadata !484, metadata !"dest", metadata !485, i32 12, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 12]
!494 = metadata !{i32 786688, metadata !484, metadata !"src", metadata !485, i32 13, metadata !61, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 13]
!495 = metadata !{i32 786449, metadata !496, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !497, metadata !2, metadata !2, metadata !""} 
!496 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memset.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!497 = metadata !{metadata !498}
!498 = metadata !{i32 786478, metadata !496, metadata !499, metadata !"memset", metadata !"memset", metadata !"", i32 11, metadata !500, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i32, i64)* @memset, null, null, metadata !503, i32
!499 = metadata !{i32 786473, metadata !496}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memset.c]
!500 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !501, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!501 = metadata !{metadata !46, metadata !46, metadata !8, metadata !502}
!502 = metadata !{i32 786454, metadata !496, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !65} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!503 = metadata !{metadata !504, metadata !505, metadata !506, metadata !507}
!504 = metadata !{i32 786689, metadata !498, metadata !"dst", metadata !499, i32 16777227, metadata !46, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 11]
!505 = metadata !{i32 786689, metadata !498, metadata !"s", metadata !499, i32 33554443, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [s] [line 11]
!506 = metadata !{i32 786689, metadata !498, metadata !"count", metadata !499, i32 50331659, metadata !502, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 11]
!507 = metadata !{i32 786688, metadata !498, metadata !"a", metadata !499, i32 12, metadata !508, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 12]
!508 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !509} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!509 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from char]
!510 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!511 = metadata !{i32 1, metadata !"Debug Info Version", i32 1}
!512 = metadata !{metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)"}
!513 = metadata !{i32 7, i32 0, metadata !4, null}
!514 = metadata !{i32 9, i32 0, metadata !4, null}
!515 = metadata !{i32 12, i32 0, metadata !4, null}
!516 = metadata !{i32 20, i32 0, metadata !12, null}
!517 = metadata !{i32 28, i32 0, metadata !12, null}
!518 = metadata !{i32 29, i32 0, metadata !519, null}
!519 = metadata !{i32 786443, metadata !10, metadata !12, i32 28, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/gets.c]
!520 = metadata !{i32 30, i32 0, metadata !519, null}
!521 = metadata !{i32 31, i32 0, metadata !522, null}
!522 = metadata !{i32 786443, metadata !10, metadata !12, i32 31, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/gets.c]
!523 = metadata !{i32 32, i32 0, metadata !524, null}
!524 = metadata !{i32 786443, metadata !10, metadata !522, i32 31, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/gets.c]
!525 = metadata !{i32 33, i32 0, metadata !524, null}
!526 = metadata !{i32 34, i32 0, metadata !527, null}
!527 = metadata !{i32 786443, metadata !10, metadata !522, i32 33, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/gets.c]
!528 = metadata !{i32 39, i32 0, metadata !12, null}
!529 = metadata !{i32 20, i32 0, metadata !27, null}
!530 = metadata !{i32 22, i32 0, metadata !27, null}
!531 = metadata !{i32 191, i32 0, metadata !532, null}
!532 = metadata !{i32 786443, metadata !31, metadata !33, i32 191, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!533 = metadata !{i32 193, i32 0, metadata !33, null}
!534 = metadata !{i32 197, i32 0, metadata !33, null}
!535 = metadata !{i32 238, i32 0, metadata !536, null}
!536 = metadata !{i32 786443, metadata !31, metadata !33, i32 238, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!537 = metadata !{i32 239, i32 0, metadata !536, null}
!538 = metadata !{i32 240, i32 0, metadata !33, null}
!539 = metadata !{i32 263, i32 0, metadata !540, null}
!540 = metadata !{i32 786443, metadata !31, metadata !37, i32 263, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!541 = metadata !{i32 264, i32 0, metadata !540, null}
!542 = metadata !{i32 266, i32 0, metadata !543, null}
!543 = metadata !{i32 786443, metadata !31, metadata !37, i32 266, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!544 = metadata !{i32 267, i32 0, metadata !543, null}
!545 = metadata !{i32 268, i32 0, metadata !37, null}
!546 = metadata !{i32 288, i32 0, metadata !38, null}
!547 = metadata !{i32 291, i32 0, metadata !38, null}
!548 = metadata !{i32 294, i32 0, metadata !38, null}
!549 = metadata !{i32 298, i32 0, metadata !550, null}
!550 = metadata !{i32 786443, metadata !31, metadata !38, i32 298, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!551 = metadata !{i32 300, i32 0, metadata !552, null}
!552 = metadata !{i32 786443, metadata !31, metadata !550, i32 298, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!553 = metadata !{i32 301, i32 0, metadata !552, null}
!554 = metadata !{i32 305, i32 0, metadata !38, null}
!555 = metadata !{i32 306, i32 0, metadata !38, null}
!556 = metadata !{i32 307, i32 0, metadata !38, null}
!557 = metadata !{i32 308, i32 0, metadata !558, null}
!558 = metadata !{i32 786443, metadata !31, metadata !38, i32 307, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!559 = metadata !{i32 311, i32 0, metadata !38, null}
!560 = metadata !{i32 312, i32 0, metadata !561, null}
!561 = metadata !{i32 786443, metadata !31, metadata !38, i32 311, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!562 = metadata !{i32 313, i32 0, metadata !563, null}
!563 = metadata !{i32 786443, metadata !31, metadata !561, i32 313, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!564 = metadata !{i32 314, i32 0, metadata !565, null}
!565 = metadata !{i32 786443, metadata !31, metadata !563, i32 313, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!566 = metadata !{i32 315, i32 0, metadata !565, null}
!567 = metadata !{i32 316, i32 0, metadata !561, null}
!568 = metadata !{i32 317, i32 0, metadata !561, null}
!569 = metadata !{i32 323, i32 0, metadata !38, null}
!570 = metadata !{i32 327, i32 0, metadata !38, null}
!571 = metadata !{i32 331, i32 0, metadata !572, null}
!572 = metadata !{i32 786443, metadata !31, metadata !38, i32 331, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!573 = metadata !{i32 336, i32 0, metadata !574, null}
!574 = metadata !{i32 786443, metadata !31, metadata !572, i32 335, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!575 = metadata !{i32 337, i32 0, metadata !574, null}
!576 = metadata !{i32 338, i32 0, metadata !574, null}
!577 = metadata !{i32 339, i32 0, metadata !574, null}
!578 = metadata !{i32 342, i32 0, metadata !38, null}
!579 = metadata !{i32 354, i32 0, metadata !38, null}
!580 = metadata !{i32 370, i32 0, metadata !581, null}
!581 = metadata !{i32 786443, metadata !31, metadata !38, i32 370, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!582 = metadata !{i32 371, i32 0, metadata !583, null}
!583 = metadata !{i32 786443, metadata !31, metadata !581, i32 370, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!584 = metadata !{i32 372, i32 0, metadata !583, null}
!585 = metadata !{i32 391, i32 0, metadata !586, null}
!586 = metadata !{i32 786443, metadata !31, metadata !38, i32 391, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!587 = metadata !{i32 392, i32 0, metadata !586, null}
!588 = metadata !{i32 395, i32 0, metadata !589, null}
!589 = metadata !{i32 786443, metadata !31, metadata !38, i32 395, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!590 = metadata !{i32 396, i32 0, metadata !589, null}
!591 = metadata !{i32 401, i32 0, metadata !38, null}
!592 = metadata !{i32 160, i32 0, metadata !593, null}
!593 = metadata !{i32 786443, metadata !31, metadata !57} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!594 = metadata !{i32 161, i32 0, metadata !593, null}
!595 = metadata !{i32 162, i32 0, metadata !593, null}
!596 = metadata !{i32 163, i32 0, metadata !593, null}
!597 = metadata !{i32 165, i32 0, metadata !598, null}
!598 = metadata !{i32 786443, metadata !31, metadata !593, i32 165, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!599 = metadata !{i32 166, i32 0, metadata !600, null}
!600 = metadata !{i32 786443, metadata !31, metadata !598, i32 165, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!601 = metadata !{i32 168, i32 0, metadata !593, null}
!602 = metadata !{i32 169, i32 0, metadata !593, null}
!603 = metadata !{i32 139, i32 0, metadata !604, null}
!604 = metadata !{i32 786443, metadata !31, metadata !47, i32 139, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!605 = metadata !{i32 143, i32 0, metadata !606, null}
!606 = metadata !{i32 786443, metadata !31, metadata !604, i32 140, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!607 = metadata !{i32 147, i32 0, metadata !608, null}
!608 = metadata !{i32 786443, metadata !31, metadata !606, i32 147, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!609 = metadata !{i32 148, i32 18, metadata !608, null}
!610 = metadata !{i32 150, i32 0, metadata !611, null}
!611 = metadata !{i32 786443, metadata !31, metadata !608, i32 149, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!612 = metadata !{i32 153, i32 0, metadata !47, null}
!613 = metadata !{i32 56, i32 0, metadata !614, null}
!614 = metadata !{i32 786443, metadata !51, metadata !50} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!615 = metadata !{i32 13, i32 0, metadata !72, null}
!616 = metadata !{i32 12, i32 0, metadata !80, null}
!617 = metadata !{i32 258, i32 0, metadata !618, null}
!618 = metadata !{i32 786443, metadata !83, metadata !85, i32 258, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!619 = metadata !{i32 261, i32 0, metadata !620, null}
!620 = metadata !{i32 786443, metadata !83, metadata !621, i32 261, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!621 = metadata !{i32 786443, metadata !83, metadata !618, i32 258, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!622 = metadata !{i32 262, i32 0, metadata !623, null}
!623 = metadata !{i32 786443, metadata !83, metadata !620, i32 261, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!624 = metadata !{i32 263, i32 0, metadata !623, null}
!625 = metadata !{i32 274, i32 0, metadata !85, null}
!626 = metadata !{i32 280, i32 0, metadata !87, null}
!627 = metadata !{i32 282, i32 0, metadata !87, null}
!628 = metadata !{i32 283, i32 0, metadata !87, null}
!629 = metadata !{i32 284, i32 0, metadata !87, null}
!630 = metadata !{i32 291, i32 0, metadata !87, null}
!631 = metadata !{i32 23, i32 0, metadata !632, null}
!632 = metadata !{i32 786443, metadata !132, metadata !134, i32 23, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!633 = metadata !{i32 24, i32 0, metadata !634, null}
!634 = metadata !{i32 786443, metadata !132, metadata !632, i32 23, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!635 = metadata !{i32 25, i32 0, metadata !634, null}
!636 = metadata !{i32 26, i32 0, metadata !634, null}
!637 = metadata !{i32 28, i32 0, metadata !134, null}
!638 = metadata !{i32 27, i32 0, metadata !639, null}
!639 = metadata !{i32 786443, metadata !166, metadata !165, i32 27, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!640 = metadata !{i32 28, i32 0, metadata !641, null}
!641 = metadata !{i32 786443, metadata !166, metadata !639, i32 27, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!642 = metadata !{i32 33, i32 0, metadata !643, null}
!643 = metadata !{i32 786443, metadata !166, metadata !165, i32 33, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!644 = metadata !{i32 34, i32 0, metadata !643, null}
!645 = metadata !{i32 36, i32 0, metadata !646, null}
!646 = metadata !{i32 786443, metadata !166, metadata !647, i32 36, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!647 = metadata !{i32 786443, metadata !166, metadata !643, i32 35, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!648 = metadata !{i32 37, i32 0, metadata !649, null}
!649 = metadata !{i32 786443, metadata !166, metadata !646, i32 36, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!650 = metadata !{i32 38, i32 0, metadata !649, null}
!651 = metadata !{i32 40, i32 0, metadata !649, null}
!652 = metadata !{i32 43, i32 0, metadata !653, null}
!653 = metadata !{i32 786443, metadata !166, metadata !647, i32 43, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!654 = metadata !{i32 44, i32 0, metadata !655, null}
!655 = metadata !{i32 786443, metadata !166, metadata !653, i32 43, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!656 = metadata !{i32 48, i32 0, metadata !657, null}
!657 = metadata !{i32 786443, metadata !166, metadata !647, i32 48, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!658 = metadata !{i32 49, i32 0, metadata !659, null}
!659 = metadata !{i32 786443, metadata !166, metadata !657, i32 48, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!660 = metadata !{i32 50, i32 0, metadata !659, null}
!661 = metadata !{i32 56, i32 0, metadata !662, null}
!662 = metadata !{i32 786443, metadata !166, metadata !647, i32 56, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!663 = metadata !{i32 57, i32 0, metadata !664, null}
!664 = metadata !{i32 786443, metadata !166, metadata !662, i32 56, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!665 = metadata !{i32 58, i32 0, metadata !664, null} ; [ DW_TAG_imported_module ]
!666 = metadata !{i32 60, i32 0, metadata !667, null}
!667 = metadata !{i32 786443, metadata !166, metadata !647, i32 60, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!668 = metadata !{i32 61, i32 0, metadata !669, null}
!669 = metadata !{i32 786443, metadata !166, metadata !667, i32 60, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!670 = metadata !{i32 62, i32 0, metadata !671, null}
!671 = metadata !{i32 786443, metadata !166, metadata !669, i32 62, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!672 = metadata !{i32 63, i32 0, metadata !673, null}
!673 = metadata !{i32 786443, metadata !166, metadata !671, i32 62, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!674 = metadata !{i32 64, i32 0, metadata !673, null}
!675 = metadata !{i32 68, i32 0, metadata !676, null}
!676 = metadata !{i32 786443, metadata !166, metadata !677, i32 68, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!677 = metadata !{i32 786443, metadata !166, metadata !667, i32 66, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!678 = metadata !{i32 69, i32 0, metadata !679, null}
!679 = metadata !{i32 786443, metadata !166, metadata !676, i32 68, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fgetc.c]
!680 = metadata !{i32 74, i32 0, metadata !165, null}
!681 = metadata !{i32 75, i32 0, metadata !165, null}
!682 = metadata !{i32 20, i32 0, metadata !195, null}
!683 = metadata !{i32 21, i32 0, metadata !195, null}
!684 = metadata !{i32 28, i32 0, metadata !195, null}
!685 = metadata !{i32 29, i32 0, metadata !686, null}
!686 = metadata !{i32 786443, metadata !193, metadata !195, i32 28, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!687 = metadata !{i32 30, i32 0, metadata !686, null}
!688 = metadata !{i32 31, i32 0, metadata !686, null}
!689 = metadata !{i32 34, i32 0, metadata !195, null}
!690 = metadata !{i32 19, i32 0, metadata !207, null}
!691 = metadata !{i32 27, i32 0, metadata !207, null}
!692 = metadata !{i32 28, i32 0, metadata !693, null}
!693 = metadata !{i32 786443, metadata !205, metadata !207, i32 27, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!694 = metadata !{i32 29, i32 0, metadata !693, null}
!695 = metadata !{i32 30, i32 0, metadata !693, null}
!696 = metadata !{i32 32, i32 0, metadata !207, null}
!697 = metadata !{i32 30, i32 0, metadata !215, null}
!698 = metadata !{i32 43, i32 0, metadata !222, null}
!699 = metadata !{i32 45, i32 0, metadata !222, null}
!700 = metadata !{i32 46, i32 0, metadata !222, null}
!701 = metadata !{i32 47, i32 0, metadata !222, null}
!702 = metadata !{i32 48, i32 0, metadata !222, null}
!703 = metadata !{i32 49, i32 0, metadata !222, null}
!704 = metadata !{i32 61, i32 0, metadata !705, null}
!705 = metadata !{i32 786443, metadata !220, metadata !706, i32 60, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!706 = metadata !{i32 786443, metadata !220, metadata !222, i32 58, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!707 = metadata !{i32 79, i32 0, metadata !222, null}
!708 = metadata !{i32 29, i32 0, metadata !253, null}
!709 = metadata !{i32 38, i32 0, metadata !710, null}
!710 = metadata !{i32 786443, metadata !251, metadata !253, i32 38, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!711 = metadata !{i32 39, i32 0, metadata !712, null}
!712 = metadata !{i32 786443, metadata !251, metadata !713, i32 39, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!713 = metadata !{i32 786443, metadata !251, metadata !710, i32 38, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!714 = metadata !{i32 40, i32 0, metadata !715, null}
!715 = metadata !{i32 786443, metadata !251, metadata !712, i32 39, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!716 = metadata !{i32 41, i32 0, metadata !715, null}
!717 = metadata !{i32 47, i32 0, metadata !718, null}
!718 = metadata !{i32 786443, metadata !251, metadata !713, i32 47, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!719 = metadata !{i32 48, i32 0, metadata !720, null}
!720 = metadata !{i32 786443, metadata !251, metadata !721, i32 48, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!721 = metadata !{i32 786443, metadata !251, metadata !718, i32 47, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!722 = metadata !{i32 49, i32 0, metadata !723, null}
!723 = metadata !{i32 786443, metadata !251, metadata !720, i32 48, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!724 = metadata !{i32 50, i32 0, metadata !723, null}
!725 = metadata !{i32 52, i32 0, metadata !726, null}
!726 = metadata !{i32 786443, metadata !251, metadata !720, i32 50, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_READ.c]
!727 = metadata !{i32 53, i32 0, metadata !726, null}
!728 = metadata !{i32 68, i32 0, metadata !253, null}
!729 = metadata !{i32 44, i32 0, metadata !283, null}
!730 = metadata !{i32 46, i32 0, metadata !283, null}
!731 = metadata !{i32 47, i32 0, metadata !732, null}
!732 = metadata !{i32 786443, metadata !281, metadata !733, i32 47, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!733 = metadata !{i32 786443, metadata !281, metadata !283, i32 46, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!734 = metadata !{i32 49, i32 0, metadata !735, null}
!735 = metadata !{i32 786443, metadata !281, metadata !732, i32 47, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!736 = metadata !{i32 51, i32 0, metadata !733, null}
!737 = metadata !{i32 52, i32 0, metadata !738, null}
!738 = metadata !{i32 786443, metadata !281, metadata !733, i32 52, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!739 = metadata !{i32 62, i32 0, metadata !740, null}
!740 = metadata !{i32 786443, metadata !281, metadata !738, i32 52, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!741 = metadata !{i32 63, i32 0, metadata !740, null}
!742 = metadata !{i32 101, i32 0, metadata !733, null}
!743 = metadata !{i32 70, i32 0, metadata !744, null}
!744 = metadata !{i32 786443, metadata !281, metadata !738, i32 69, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!745 = metadata !{i32 73, i32 0, metadata !746, null}
!746 = metadata !{i32 786443, metadata !281, metadata !744, i32 73, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!747 = metadata !{i32 76, i32 0, metadata !748, null}
!748 = metadata !{i32 786443, metadata !281, metadata !749, i32 76, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!749 = metadata !{i32 786443, metadata !281, metadata !746, i32 73, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!750 = metadata !{i32 77, i32 0, metadata !751, null}
!751 = metadata !{i32 786443, metadata !281, metadata !748, i32 76, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!752 = metadata !{i32 78, i32 0, metadata !751, null}
!753 = metadata !{i32 80, i32 0, metadata !749, null}
!754 = metadata !{i32 82, i32 0, metadata !749, null}
!755 = metadata !{i32 83, i32 0, metadata !756, null}
!756 = metadata !{i32 786443, metadata !281, metadata !757, i32 83, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!757 = metadata !{i32 786443, metadata !281, metadata !749, i32 82, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!758 = metadata !{i32 88, i32 0, metadata !757, null}
!759 = metadata !{i32 89, i32 0, metadata !757, null}
!760 = metadata !{i32 90, i32 0, metadata !757, null}
!761 = metadata !{i32 92, i32 0, metadata !749, null}
!762 = metadata !{i32 94, i32 0, metadata !749, null}
!763 = metadata !{i32 95, i32 0, metadata !749, null}
!764 = metadata !{i32 99, i32 0, metadata !744, null}
!765 = metadata !{i32 102, i32 0, metadata !283, null}
!766 = metadata !{i32 36, i32 0, metadata !315, null}
!767 = metadata !{i32 38, i32 0, metadata !315, null}
!768 = metadata !{i32 39, i32 0, metadata !315, null}
!769 = metadata !{i32 42, i32 0, metadata !315, null}
!770 = metadata !{i32 34, i32 0, metadata !771, null}
!771 = metadata !{i32 786443, metadata !344, metadata !346, i32 34, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!772 = metadata !{i32 35, i32 0, metadata !773, null}
!773 = metadata !{i32 786443, metadata !344, metadata !774, i32 35, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!774 = metadata !{i32 786443, metadata !344, metadata !771, i32 34, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!775 = metadata !{i32 39, i32 0, metadata !774, null}
!776 = metadata !{i32 40, i32 0, metadata !774, null}
!777 = metadata !{i32 43, i32 0, metadata !778, null}
!778 = metadata !{i32 786443, metadata !344, metadata !346, i32 43, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!779 = metadata !{i32 47, i32 0, metadata !780, null}
!780 = metadata !{i32 786443, metadata !344, metadata !778, i32 43, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!781 = metadata !{i32 51, i32 0, metadata !780, null}
!782 = metadata !{i32 53, i32 0, metadata !780, null}
!783 = metadata !{i32 56, i32 0, metadata !784, null}
!784 = metadata !{i32 786443, metadata !344, metadata !346, i32 56, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!785 = metadata !{i32 58, i32 0, metadata !786, null} ; [ DW_TAG_imported_module ]
!786 = metadata !{i32 786443, metadata !344, metadata !787, i32 58, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!787 = metadata !{i32 786443, metadata !344, metadata !784, i32 56, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_trans2r.c]
!788 = metadata !{i32 63, i32 0, metadata !787, null}
!789 = metadata !{i32 64, i32 0, metadata !787, null}
!790 = metadata !{i32 72, i32 0, metadata !787, null}
!791 = metadata !{i32 74, i32 0, metadata !346, null}
!792 = metadata !{i32 78, i32 0, metadata !346, null}
!793 = metadata !{i32 79, i32 0, metadata !346, null}
!794 = metadata !{i32 73, i32 0, metadata !376, null}
!795 = metadata !{i32 77, i32 0, metadata !376, null}
!796 = metadata !{i32 85, i32 0, metadata !797, null}
!797 = metadata !{i32 786443, metadata !377, metadata !376, i32 85, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!798 = metadata !{i32 86, i32 0, metadata !799, null}
!799 = metadata !{i32 786443, metadata !377, metadata !797, i32 85, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!800 = metadata !{i32 87, i32 0, metadata !799, null}
!801 = metadata !{i32 88, i32 0, metadata !799, null}
!802 = metadata !{i32 90, i32 0, metadata !803, null}
!803 = metadata !{i32 786443, metadata !377, metadata !376, i32 90, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!804 = metadata !{i32 95, i32 0, metadata !805, null}
!805 = metadata !{i32 786443, metadata !377, metadata !803, i32 90, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!806 = metadata !{i32 98, i32 0, metadata !805, null}
!807 = metadata !{i32 103, i32 0, metadata !808, null}
!808 = metadata !{i32 786443, metadata !377, metadata !809, i32 103, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!809 = metadata !{i32 786443, metadata !377, metadata !805, i32 98, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!810 = metadata !{i32 106, i32 0, metadata !811, null}
!811 = metadata !{i32 786443, metadata !377, metadata !812, i32 106, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!812 = metadata !{i32 786443, metadata !377, metadata !808, i32 103, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!813 = metadata !{i32 110, i32 0, metadata !814, null}
!814 = metadata !{i32 786443, metadata !377, metadata !815, i32 110, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!815 = metadata !{i32 786443, metadata !377, metadata !811, i32 109, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!816 = metadata !{i32 111, i32 0, metadata !817, null}
!817 = metadata !{i32 786443, metadata !377, metadata !814, i32 110, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!818 = metadata !{i32 112, i32 0, metadata !817, null}
!819 = metadata !{i32 113, i32 0, metadata !817, null}
!820 = metadata !{i32 114, i32 0, metadata !821, null}
!821 = metadata !{i32 786443, metadata !377, metadata !814, i32 113, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!822 = metadata !{i32 119, i32 0, metadata !809, null}
!823 = metadata !{i32 120, i32 0, metadata !809, null}
!824 = metadata !{i32 124, i32 0, metadata !825, null}
!825 = metadata !{i32 786443, metadata !377, metadata !803, i32 124, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!826 = metadata !{i32 125, i32 0, metadata !827, null}
!827 = metadata !{i32 786443, metadata !377, metadata !828, i32 125, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!828 = metadata !{i32 786443, metadata !377, metadata !825, i32 124, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!829 = metadata !{i32 126, i32 0, metadata !830, null}
!830 = metadata !{i32 786443, metadata !377, metadata !827, i32 125, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!831 = metadata !{i32 127, i32 0, metadata !830, null}
!832 = metadata !{i32 128, i32 0, metadata !830, null}
!833 = metadata !{i32 129, i32 0, metadata !834, null}
!834 = metadata !{i32 786443, metadata !377, metadata !827, i32 128, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/fflush.c]
!835 = metadata !{i32 150, i32 0, metadata !376, null}
!836 = metadata !{i32 22, i32 0, metadata !406, null}
!837 = metadata !{i32 23, i32 0, metadata !406, null}
!838 = metadata !{i32 30, i32 0, metadata !406, null}
!839 = metadata !{i32 31, i32 0, metadata !840, null}
!840 = metadata !{i32 786443, metadata !404, metadata !406, i32 30, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!841 = metadata !{i32 32, i32 0, metadata !840, null}
!842 = metadata !{i32 33, i32 0, metadata !840, null}
!843 = metadata !{i32 36, i32 0, metadata !406, null}
!844 = metadata !{i32 13, i32 0, metadata !845, null}
!845 = metadata !{i32 786443, metadata !412, metadata !414, i32 13, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!846 = metadata !{i32 14, i32 0, metadata !845, null}
!847 = metadata !{i32 15, i32 0, metadata !414, null}
!848 = metadata !{i32 15, i32 0, metadata !424, null}
!849 = metadata !{i32 16, i32 0, metadata !424, null}
!850 = metadata !{metadata !851, metadata !851, i64 0}
!851 = metadata !{metadata !"int", metadata !852, i64 0}
!852 = metadata !{metadata !"omnipotent char", metadata !853, i64 0}
!853 = metadata !{metadata !"Simple C/C++ TBAA"}
!854 = metadata !{i32 21, i32 0, metadata !855, null}
!855 = metadata !{i32 786443, metadata !432, metadata !434, i32 21, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!856 = metadata !{i32 27, i32 0, metadata !857, null}
!857 = metadata !{i32 786443, metadata !432, metadata !855, i32 21, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!858 = metadata !{i32 29, i32 0, metadata !434, null}
!859 = metadata !{i32 16, i32 0, metadata !860, null}
!860 = metadata !{i32 786443, metadata !442, metadata !444, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!861 = metadata !{i32 17, i32 0, metadata !860, null}
!862 = metadata !{i32 19, i32 0, metadata !863, null}
!863 = metadata !{i32 786443, metadata !442, metadata !444, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!864 = metadata !{i32 22, i32 0, metadata !865, null}
!865 = metadata !{i32 786443, metadata !442, metadata !863, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!866 = metadata !{i32 25, i32 0, metadata !867, null}
!867 = metadata !{i32 786443, metadata !442, metadata !865, i32 25, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!868 = metadata !{i32 26, i32 0, metadata !869, null}
!869 = metadata !{i32 786443, metadata !442, metadata !867, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!870 = metadata !{i32 27, i32 0, metadata !869, null}
!871 = metadata !{i32 28, i32 0, metadata !872, null}
!872 = metadata !{i32 786443, metadata !442, metadata !867, i32 27, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!873 = metadata !{i32 29, i32 0, metadata !872, null}
!874 = metadata !{i32 32, i32 0, metadata !865, null}
!875 = metadata !{i32 34, i32 0, metadata !444, null}
!876 = metadata !{i32 16, i32 0, metadata !877, null}
!877 = metadata !{i32 786443, metadata !468, metadata !470, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!878 = metadata !{i32 19, i32 0, metadata !879, null}
!879 = metadata !{i32 786443, metadata !468, metadata !470, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!880 = metadata !{i32 20, i32 0, metadata !881, null}
!881 = metadata !{i32 786443, metadata !468, metadata !879, i32 19, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!882 = metadata !{metadata !882, metadata !883, metadata !884}
!883 = metadata !{metadata !"llvm.vectorizer.width", i32 1}
!884 = metadata !{metadata !"llvm.vectorizer.unroll", i32 1}
!885 = metadata !{metadata !852, metadata !852, i64 0}
!886 = metadata !{metadata !886, metadata !883, metadata !884}
!887 = metadata !{i32 22, i32 0, metadata !888, null}
!888 = metadata !{i32 786443, metadata !468, metadata !879, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!889 = metadata !{i32 24, i32 0, metadata !888, null}
!890 = metadata !{i32 23, i32 0, metadata !888, null}
!891 = metadata !{metadata !891, metadata !883, metadata !884}
!892 = metadata !{metadata !892, metadata !883, metadata !884}
!893 = metadata !{i32 28, i32 0, metadata !470, null}
