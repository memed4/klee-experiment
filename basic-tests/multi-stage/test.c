#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char a1[0x10];	
	char a2[0x10];	
	int stage = 0;

  	klee_make_symbolic(a1, sizeof(a1), "a_1");
  	klee_make_symbolic(a2, sizeof(a2), "a_2");
	klee_assume(a1[0xf] == 0);
	klee_assume(a2[0xf] == 0);

	while (1)
	{
		if (stage == 0)
			if (!strcmp(a1, "first"))
			{
				stage += 1;
				continue;
			}

		if (stage == 1)
			if (!strcmp(a2, "second"))
			{
				klee_assume(0);
				break;
			}
	}
	
	return 0;
}

