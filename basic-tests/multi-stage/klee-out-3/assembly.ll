; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.__STDIO_FILE_STRUCT.273 = type { i16, [2 x i8], i32, i8*, i8*, i8*, i8*, i8*, i8*, %struct.__STDIO_FILE_STRUCT.273*, [2 x i32], %struct.__mbstate_t.272 }
%struct.__mbstate_t.272 = type { i32, i32 }
%struct.Elf64_auxv_t = type { i64, %union.anon.645 }
%union.anon.645 = type { i64 }
%struct.stat.644 = type { i64, i64, i64, i32, i32, i32, i32, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, [3 x i64] }
%struct.termios.442 = type { i32, i32, i32, i32, i8, [32 x i8], i32, i32 }
%struct.__kernel_termios = type { i32, i32, i32, i32, i8, [19 x i8] }

@.str = private unnamed_addr constant [4 x i8] c"a_1\00", align 1
@.str1 = private unnamed_addr constant [4 x i8] c"a_2\00", align 1
@.str2 = private unnamed_addr constant [6 x i8] c"first\00", align 1
@.str3 = private unnamed_addr constant [7 x i8] c"second\00", align 1
@__libc_stack_end = global i8* null, align 8
@.str4 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@__uclibc_progname = hidden global i8* getelementptr inbounds ([1 x i8]* @.str4, i32 0, i32 0), align 8
@__environ = global i8** null, align 8
@__pagesize = global i64 0, align 8
@__uClibc_init.been_there_done_that = internal global i32 0, align 4
@__app_fini = hidden global void ()* null, align 8
@__rtld_fini = hidden global void ()* null, align 8
@.str15 = private unnamed_addr constant [10 x i8] c"/dev/null\00", align 1
@_stdio_streams = internal global [3 x %struct.__STDIO_FILE_STRUCT.273] [%struct.__STDIO_FILE_STRUCT.273 { i16 544, [2 x i8] zeroinitializer, i32 0, i8* null, i8* null, i8* null, i8* null, i8* null, i8* null, %struct.__STDIO_FILE_STRUCT.273* bitcast (i8*
@stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@stderr = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 160) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@__stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@__stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@_stdio_openlist = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@errno = global i32 0, align 4
@h_errno = global i32 0, align 4
@.str32 = private unnamed_addr constant [60 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c\00", align 1
@.str133 = private unnamed_addr constant [15 x i8] c"divide by zero\00", align 1
@.str234 = private unnamed_addr constant [8 x i8] c"div.err\00", align 1
@.str335 = private unnamed_addr constant [8 x i8] c"IGNORED\00", align 1
@.str14 = private unnamed_addr constant [16 x i8] c"overshift error\00", align 1
@.str25 = private unnamed_addr constant [14 x i8] c"overshift.err\00", align 1
@.str6 = private unnamed_addr constant [51 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_range.c\00", align 1
@.str17 = private unnamed_addr constant [14 x i8] c"invalid range\00", align 1
@.str28 = private unnamed_addr constant [5 x i8] c"user\00", align 1

@strcoll = alias i32 (i8*, i8*)* @strcmp
@environ = alias weak i8*** @__environ

; Function Attrs: nounwind uwtable
define i32 @__user_main() #0 {
  %1 = alloca i32, align 4
  %a1 = alloca [16 x i8], align 16
  %a2 = alloca [16 x i8], align 16
  %stage = alloca i32, align 4
  store i32 0, i32* %1
  store i32 0, i32* %stage, align 4, !dbg !351
  %2 = getelementptr inbounds [16 x i8]* %a1, i32 0, i32 0, !dbg !352
  %3 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %2, i64 16, i8* getelementptr inbounds ([4 x i8]* @.str, i32 0, i32 0)), !dbg !352
  %4 = getelementptr inbounds [16 x i8]* %a2, i32 0, i32 0, !dbg !353
  %5 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %4, i64 16, i8* getelementptr inbounds ([4 x i8]* @.str1, i32 0, i32 0)), !dbg !353
  %6 = getelementptr inbounds [16 x i8]* %a1, i32 0, i64 15, !dbg !354
  %7 = load i8* %6, align 1, !dbg !354
  %8 = sext i8 %7 to i32, !dbg !354
  %9 = icmp eq i32 %8, 0, !dbg !354
  %10 = zext i1 %9 to i32, !dbg !354
  %11 = call i32 (i32, ...)* bitcast (i32 (...)* @klee_assume to i32 (i32, ...)*)(i32 %10), !dbg !354
  %12 = getelementptr inbounds [16 x i8]* %a2, i32 0, i64 15, !dbg !355
  %13 = load i8* %12, align 1, !dbg !355
  %14 = sext i8 %13 to i32, !dbg !355
  %15 = icmp eq i32 %14, 0, !dbg !355
  %16 = zext i1 %15 to i32, !dbg !355
  %17 = call i32 (i32, ...)* bitcast (i32 (...)* @klee_assume to i32 (i32, ...)*)(i32 %16), !dbg !355
  br label %18, !dbg !356

; <label>:18                                      ; preds = %28, %31, %25, %0
  %19 = load i32* %stage, align 4, !dbg !357
  %20 = icmp eq i32 %19, 0, !dbg !357
  br i1 %20, label %21, label %28, !dbg !357

; <label>:21                                      ; preds = %18
  %22 = getelementptr inbounds [16 x i8]* %a1, i32 0, i32 0, !dbg !360
  %23 = call i32 @strcmp(i8* %22, i8* getelementptr inbounds ([6 x i8]* @.str2, i32 0, i32 0)) #11, !dbg !360
  %24 = icmp ne i32 %23, 0, !dbg !360
  br i1 %24, label %28, label %25, !dbg !360

; <label>:25                                      ; preds = %21
  %26 = load i32* %stage, align 4, !dbg !362
  %27 = add nsw i32 %26, 1, !dbg !362
  store i32 %27, i32* %stage, align 4, !dbg !362
  br label %18, !dbg !364

; <label>:28                                      ; preds = %21, %18
  %29 = load i32* %stage, align 4, !dbg !365
  %30 = icmp eq i32 %29, 1, !dbg !365
  br i1 %30, label %31, label %18, !dbg !365

; <label>:31                                      ; preds = %28
  %32 = getelementptr inbounds [16 x i8]* %a2, i32 0, i32 0, !dbg !367
  %33 = call i32 @strcmp(i8* %32, i8* getelementptr inbounds ([7 x i8]* @.str3, i32 0, i32 0)) #11, !dbg !367
  %34 = icmp ne i32 %33, 0, !dbg !367
  br i1 %34, label %18, label %35, !dbg !367

; <label>:35                                      ; preds = %31
  %36 = call i32 (i32, ...)* bitcast (i32 (...)* @klee_assume to i32 (i32, ...)*)(i32 0), !dbg !369
  ret i32 0, !dbg !371
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

declare i32 @klee_make_symbolic(...) #2

declare i32 @klee_assume(...) #2

; Function Attrs: nounwind readonly uwtable
define i32 @strcmp(i8* %s1, i8* %s2) #3 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %r = alloca i32, align 4
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  br label %3, !dbg !372

; <label>:3                                       ; preds = %13, %0
  %4 = load i8** %1, align 8, !dbg !372
  %5 = load i8* %4, align 1, !dbg !372
  %6 = zext i8 %5 to i32, !dbg !372
  %7 = load i8** %2, align 8, !dbg !372
  %8 = getelementptr inbounds i8* %7, i32 1, !dbg !372
  store i8* %8, i8** %2, align 8, !dbg !372
  %9 = load i8* %7, align 1, !dbg !372
  %10 = zext i8 %9 to i32, !dbg !372
  %11 = sub nsw i32 %6, %10, !dbg !372
  store i32 %11, i32* %r, align 4, !dbg !372
  %12 = icmp eq i32 %11, 0, !dbg !372
  br i1 %12, label %13, label %.critedge, !dbg !372

; <label>:13                                      ; preds = %3
  %14 = load i8** %1, align 8, !dbg !372
  %15 = getelementptr inbounds i8* %14, i32 1, !dbg !372
  store i8* %15, i8** %1, align 8, !dbg !372
  %16 = load i8* %14, align 1, !dbg !372
  %17 = sext i8 %16 to i32, !dbg !372
  %18 = icmp ne i32 %17, 0, !dbg !372
  br i1 %18, label %3, label %.critedge

.critedge:                                        ; preds = %3, %13
  %19 = load i32* %r, align 4, !dbg !373
  ret i32 %19, !dbg !373
}

; Function Attrs: nounwind uwtable
define void @__uClibc_init() #0 {
  %1 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !374
  %2 = icmp ne i32 %1, 0, !dbg !374
  br i1 %2, label %8, label %3, !dbg !374

; <label>:3                                       ; preds = %0
  %4 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !376
  %5 = add nsw i32 %4, 1, !dbg !376
  store i32 %5, i32* @__uClibc_init.been_there_done_that, align 4, !dbg !376
  store i64 4096, i64* @__pagesize, align 8, !dbg !377
  %6 = icmp ne i64 1, 0, !dbg !378
  br i1 %6, label %7, label %8, !dbg !378

; <label>:7                                       ; preds = %3
  call void @_stdio_init() #12, !dbg !380
  br label %8, !dbg !380

; <label>:8                                       ; preds = %0, %7, %3
  ret void, !dbg !381
}

; Function Attrs: nounwind readnone
declare i64 @llvm.expect.i64(i64, i64) #1

; Function Attrs: nounwind uwtable
define void @__uClibc_fini() #0 {
  %1 = load void ()** @__app_fini, align 8, !dbg !382
  %2 = icmp ne void ()* %1, null, !dbg !382
  br i1 %2, label %3, label %5, !dbg !382

; <label>:3                                       ; preds = %0
  %4 = load void ()** @__app_fini, align 8, !dbg !384
  call void %4() #12, !dbg !384
  br label %5, !dbg !384

; <label>:5                                       ; preds = %3, %0
  %6 = load void ()** @__rtld_fini, align 8, !dbg !385
  %7 = icmp ne void ()* %6, null, !dbg !385
  br i1 %7, label %8, label %10, !dbg !385

; <label>:8                                       ; preds = %5
  %9 = load void ()** @__rtld_fini, align 8, !dbg !387
  call void %9() #12, !dbg !387
  br label %10, !dbg !387

; <label>:10                                      ; preds = %8, %5
  ret void, !dbg !388
}

; Function Attrs: noreturn nounwind uwtable
define void @__uClibc_main(i32 (i32, i8**, i8**)* %main, i32 %argc, i8** %argv, void ()* %app_init, void ()* %app_fini, void ()* %rtld_fini, i8* %stack_end) #4 {
  %1 = alloca i32 (i32, i8**, i8**)*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca void ()*, align 8
  %5 = alloca void ()*, align 8
  %6 = alloca void ()*, align 8
  %7 = alloca i8*, align 8
  %aux_dat = alloca i64*, align 8
  %auxvt = alloca [15 x %struct.Elf64_auxv_t], align 16
  %auxv_entry = alloca %struct.Elf64_auxv_t*, align 8
  store i32 (i32, i8**, i8**)* %main, i32 (i32, i8**, i8**)** %1, align 8
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  store void ()* %app_init, void ()** %4, align 8
  store void ()* %app_fini, void ()** %5, align 8
  store void ()* %rtld_fini, void ()** %6, align 8
  store i8* %stack_end, i8** %7, align 8
  %8 = load i8** %7, align 8, !dbg !389
  store i8* %8, i8** @__libc_stack_end, align 8, !dbg !389
  %9 = load void ()** %6, align 8, !dbg !390
  store void ()* %9, void ()** @__rtld_fini, align 8, !dbg !390
  %10 = load i32* %2, align 4, !dbg !391
  %11 = add nsw i32 %10, 1, !dbg !391
  %12 = sext i32 %11 to i64, !dbg !391
  %13 = load i8*** %3, align 8, !dbg !391
  %14 = getelementptr inbounds i8** %13, i64 %12, !dbg !391
  store i8** %14, i8*** @__environ, align 8, !dbg !391
  %15 = load i8*** @__environ, align 8, !dbg !392
  %16 = bitcast i8** %15 to i8*, !dbg !392
  %17 = load i8*** %3, align 8, !dbg !392
  %18 = load i8** %17, align 8, !dbg !392
  %19 = icmp eq i8* %16, %18, !dbg !392
  br i1 %19, label %20, label %25, !dbg !392

; <label>:20                                      ; preds = %0
  %21 = load i32* %2, align 4, !dbg !394
  %22 = sext i32 %21 to i64, !dbg !394
  %23 = load i8*** %3, align 8, !dbg !394
  %24 = getelementptr inbounds i8** %23, i64 %22, !dbg !394
  store i8** %24, i8*** @__environ, align 8, !dbg !394
  br label %25, !dbg !396

; <label>:25                                      ; preds = %20, %0
  %26 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i32 0, !dbg !397
  %27 = bitcast %struct.Elf64_auxv_t* %26 to i8*, !dbg !397
  %28 = call i8* @memset(i8* %27, i32 0, i64 240) #13, !dbg !397
  %29 = load i8*** @__environ, align 8, !dbg !398
  %30 = bitcast i8** %29 to i64*, !dbg !398
  store i64* %30, i64** %aux_dat, align 8, !dbg !398
  br label %31, !dbg !399

; <label>:31                                      ; preds = %31, %25
  %32 = load i64** %aux_dat, align 8, !dbg !399
  %33 = load i64* %32, align 8, !dbg !399
  %34 = icmp ne i64 %33, 0, !dbg !399
  %35 = load i64** %aux_dat, align 8, !dbg !400
  %36 = getelementptr inbounds i64* %35, i32 1, !dbg !400
  store i64* %36, i64** %aux_dat, align 8, !dbg !400
  br i1 %34, label %31, label %37, !dbg !399

; <label>:37                                      ; preds = %31, %57
  %38 = load i64** %aux_dat, align 8, !dbg !402
  %39 = load i64* %38, align 8, !dbg !402
  %40 = icmp ne i64 %39, 0, !dbg !402
  br i1 %40, label %41, label %60, !dbg !402

; <label>:41                                      ; preds = %37
  %42 = load i64** %aux_dat, align 8, !dbg !403
  %43 = bitcast i64* %42 to %struct.Elf64_auxv_t*, !dbg !403
  store %struct.Elf64_auxv_t* %43, %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !403
  %44 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !405
  %45 = getelementptr inbounds %struct.Elf64_auxv_t* %44, i32 0, i32 0, !dbg !405
  %46 = load i64* %45, align 8, !dbg !405
  %47 = icmp ule i64 %46, 14, !dbg !405
  br i1 %47, label %48, label %57, !dbg !405

; <label>:48                                      ; preds = %41
  %49 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !407
  %50 = getelementptr inbounds %struct.Elf64_auxv_t* %49, i32 0, i32 0, !dbg !407
  %51 = load i64* %50, align 8, !dbg !407
  %52 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 %51, !dbg !407
  %53 = bitcast %struct.Elf64_auxv_t* %52 to i8*, !dbg !407
  %54 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !407
  %55 = bitcast %struct.Elf64_auxv_t* %54 to i8*, !dbg !407
  %56 = call i8* @memcpy(i8* %53, i8* %55, i64 16) #13, !dbg !407
  br label %57, !dbg !409

; <label>:57                                      ; preds = %48, %41
  %58 = load i64** %aux_dat, align 8, !dbg !410
  %59 = getelementptr inbounds i64* %58, i64 2, !dbg !410
  store i64* %59, i64** %aux_dat, align 8, !dbg !410
  br label %37, !dbg !411

; <label>:60                                      ; preds = %37
  call void @__uClibc_init() #12, !dbg !412
  %61 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !413
  %62 = getelementptr inbounds %struct.Elf64_auxv_t* %61, i32 0, i32 1, !dbg !413
  %63 = bitcast %union.anon.645* %62 to i64*, !dbg !413
  %64 = load i64* %63, align 8, !dbg !413
  %65 = icmp ne i64 %64, 0, !dbg !413
  br i1 %65, label %66, label %71, !dbg !413

; <label>:66                                      ; preds = %60
  %67 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !413
  %68 = getelementptr inbounds %struct.Elf64_auxv_t* %67, i32 0, i32 1, !dbg !413
  %69 = bitcast %union.anon.645* %68 to i64*, !dbg !413
  %70 = load i64* %69, align 8, !dbg !413
  br label %71, !dbg !413

; <label>:71                                      ; preds = %60, %66
  %72 = phi i64 [ %70, %66 ], [ 4096, %60 ], !dbg !413
  store i64 %72, i64* @__pagesize, align 8, !dbg !413
  %73 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !414
  %74 = getelementptr inbounds %struct.Elf64_auxv_t* %73, i32 0, i32 1, !dbg !414
  %75 = bitcast %union.anon.645* %74 to i64*, !dbg !414
  %76 = load i64* %75, align 8, !dbg !414
  %77 = icmp eq i64 %76, -1, !dbg !414
  br i1 %77, label %78, label %81, !dbg !414

; <label>:78                                      ; preds = %71
  %79 = call i32 @__check_suid() #12, !dbg !414
  %80 = icmp ne i32 %79, 0, !dbg !414
  br i1 %80, label %107, label %81, !dbg !414

; <label>:81                                      ; preds = %78, %71
  %82 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !414
  %83 = getelementptr inbounds %struct.Elf64_auxv_t* %82, i32 0, i32 1, !dbg !414
  %84 = bitcast %union.anon.645* %83 to i64*, !dbg !414
  %85 = load i64* %84, align 8, !dbg !414
  %86 = icmp ne i64 %85, -1, !dbg !414
  br i1 %86, label %87, label %108, !dbg !414

; <label>:87                                      ; preds = %81
  %88 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !414
  %89 = getelementptr inbounds %struct.Elf64_auxv_t* %88, i32 0, i32 1, !dbg !414
  %90 = bitcast %union.anon.645* %89 to i64*, !dbg !414
  %91 = load i64* %90, align 8, !dbg !414
  %92 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 12, !dbg !414
  %93 = getelementptr inbounds %struct.Elf64_auxv_t* %92, i32 0, i32 1, !dbg !414
  %94 = bitcast %union.anon.645* %93 to i64*, !dbg !414
  %95 = load i64* %94, align 8, !dbg !414
  %96 = icmp ne i64 %91, %95, !dbg !414
  br i1 %96, label %107, label %97, !dbg !414

; <label>:97                                      ; preds = %87
  %98 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 13, !dbg !414
  %99 = getelementptr inbounds %struct.Elf64_auxv_t* %98, i32 0, i32 1, !dbg !414
  %100 = bitcast %union.anon.645* %99 to i64*, !dbg !414
  %101 = load i64* %100, align 8, !dbg !414
  %102 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 14, !dbg !414
  %103 = getelementptr inbounds %struct.Elf64_auxv_t* %102, i32 0, i32 1, !dbg !414
  %104 = bitcast %union.anon.645* %103 to i64*, !dbg !414
  %105 = load i64* %104, align 8, !dbg !414
  %106 = icmp ne i64 %101, %105, !dbg !414
  br i1 %106, label %107, label %108, !dbg !414

; <label>:107                                     ; preds = %97, %87, %78
  call void @__check_one_fd(i32 0, i32 131072) #12, !dbg !416
  call void @__check_one_fd(i32 1, i32 131074) #12, !dbg !418
  call void @__check_one_fd(i32 2, i32 131074) #12, !dbg !419
  br label %108, !dbg !420

; <label>:108                                     ; preds = %107, %97, %81
  %109 = load i8*** %3, align 8, !dbg !421
  %110 = load i8** %109, align 8, !dbg !421
  store i8* %110, i8** @__uclibc_progname, align 8, !dbg !421
  %111 = load void ()** %5, align 8, !dbg !422
  store void ()* %111, void ()** @__app_fini, align 8, !dbg !422
  %112 = load void ()** %4, align 8, !dbg !423
  %113 = icmp ne void ()* %112, null, !dbg !423
  br i1 %113, label %114, label %116, !dbg !423

; <label>:114                                     ; preds = %108
  %115 = load void ()** %4, align 8, !dbg !425
  call void %115() #12, !dbg !425
  br label %116, !dbg !427

; <label>:116                                     ; preds = %114, %108
  %117 = icmp ne i64 1, 0, !dbg !428
  br i1 %117, label %118, label %120, !dbg !428

; <label>:118                                     ; preds = %116
  %119 = call i32* @__errno_location() #14, !dbg !430
  store i32 0, i32* %119, align 4, !dbg !430
  br label %120, !dbg !430

; <label>:120                                     ; preds = %118, %116
  %121 = icmp ne i64 1, 0, !dbg !431
  br i1 %121, label %122, label %124, !dbg !431

; <label>:122                                     ; preds = %120
  %123 = call i32* @__h_errno_location() #14, !dbg !433
  store i32 0, i32* %123, align 4, !dbg !433
  br label %124, !dbg !433

; <label>:124                                     ; preds = %122, %120
  %125 = load i32 (i32, i8**, i8**)** %1, align 8, !dbg !434
  %126 = load i32* %2, align 4, !dbg !434
  %127 = load i8*** %3, align 8, !dbg !434
  %128 = load i8*** @__environ, align 8, !dbg !434
  %129 = call i32 %125(i32 %126, i8** %127, i8** %128) #12, !dbg !434
  call void @exit(i32 %129) #15, !dbg !434
  unreachable, !dbg !434
}

; Function Attrs: noreturn nounwind
declare void @exit(i32) #5

declare i32 @fcntl(i32, i32, ...) #2

declare i32 @open(i8*, i32, ...) #2

; Function Attrs: nounwind
declare i32 @fstat(i32, %struct.stat.644*) #6

; Function Attrs: noreturn nounwind
declare void @abort() #5

; Function Attrs: nounwind
declare i32 @getuid() #6

; Function Attrs: nounwind
declare i32 @geteuid() #6

; Function Attrs: nounwind
declare i32 @getgid() #6

; Function Attrs: nounwind
declare i32 @getegid() #6

; Function Attrs: nounwind uwtable
define internal i32 @__check_suid() #0 {
  %1 = alloca i32, align 4
  %uid = alloca i32, align 4
  %euid = alloca i32, align 4
  %gid = alloca i32, align 4
  %egid = alloca i32, align 4
  %2 = call i32 @getuid() #13, !dbg !435
  store i32 %2, i32* %uid, align 4, !dbg !435
  %3 = call i32 @geteuid() #13, !dbg !437
  store i32 %3, i32* %euid, align 4, !dbg !437
  %4 = call i32 @getgid() #13, !dbg !438
  store i32 %4, i32* %gid, align 4, !dbg !438
  %5 = call i32 @getegid() #13, !dbg !439
  store i32 %5, i32* %egid, align 4, !dbg !439
  %6 = load i32* %uid, align 4, !dbg !440
  %7 = load i32* %euid, align 4, !dbg !440
  %8 = icmp eq i32 %6, %7, !dbg !440
  br i1 %8, label %9, label %14, !dbg !440

; <label>:9                                       ; preds = %0
  %10 = load i32* %gid, align 4, !dbg !440
  %11 = load i32* %egid, align 4, !dbg !440
  %12 = icmp eq i32 %10, %11, !dbg !440
  br i1 %12, label %13, label %14, !dbg !440

; <label>:13                                      ; preds = %9
  store i32 0, i32* %1, !dbg !442
  br label %15, !dbg !442

; <label>:14                                      ; preds = %9, %0
  store i32 1, i32* %1, !dbg !444
  br label %15, !dbg !444

; <label>:15                                      ; preds = %14, %13
  %16 = load i32* %1, !dbg !445
  ret i32 %16, !dbg !445
}

; Function Attrs: nounwind uwtable
define internal void @__check_one_fd(i32 %fd, i32 %mode) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %st = alloca %struct.stat.644, align 8
  %nullfd = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store i32 %mode, i32* %2, align 4
  %3 = load i32* %1, align 4, !dbg !446
  %4 = call i32 (i32, i32, ...)* @fcntl(i32 %3, i32 1) #12, !dbg !446
  %5 = icmp eq i32 %4, -1, !dbg !446
  br i1 %5, label %6, label %10, !dbg !446

; <label>:6                                       ; preds = %0
  %7 = call i32* @__errno_location() #14, !dbg !446
  %8 = load i32* %7, align 4, !dbg !446
  %9 = icmp eq i32 %8, 9, !dbg !446
  br label %10

; <label>:10                                      ; preds = %6, %0
  %11 = phi i1 [ false, %0 ], [ %9, %6 ]
  %12 = xor i1 %11, true
  %13 = xor i1 %12, true
  %14 = zext i1 %13 to i32
  %15 = sext i32 %14 to i64
  %16 = icmp ne i64 %15, 0
  br i1 %16, label %17, label %38

; <label>:17                                      ; preds = %10
  %18 = load i32* %2, align 4, !dbg !448
  %19 = call i32 (i8*, i32, ...)* @open(i8* getelementptr inbounds ([10 x i8]* @.str15, i32 0, i32 0), i32 %18) #12, !dbg !448
  store i32 %19, i32* %nullfd, align 4, !dbg !448
  %20 = load i32* %nullfd, align 4, !dbg !450
  %21 = load i32* %1, align 4, !dbg !450
  %22 = icmp ne i32 %20, %21, !dbg !450
  br i1 %22, label %37, label %23, !dbg !450

; <label>:23                                      ; preds = %17
  %24 = load i32* %1, align 4, !dbg !450
  %25 = call i32 @fstat(i32 %24, %struct.stat.644* %st) #13, !dbg !450
  %26 = icmp ne i32 %25, 0, !dbg !450
  br i1 %26, label %37, label %27, !dbg !450

; <label>:27                                      ; preds = %23
  %28 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 3, !dbg !450
  %29 = load i32* %28, align 4, !dbg !450
  %30 = and i32 %29, 61440, !dbg !450
  %31 = icmp eq i32 %30, 8192, !dbg !450
  br i1 %31, label %32, label %37, !dbg !450

; <label>:32                                      ; preds = %27
  %33 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 7, !dbg !450
  %34 = load i64* %33, align 8, !dbg !450
  %35 = call i64 @gnu_dev_makedev(i32 1, i32 3) #13, !dbg !452
  %36 = icmp ne i64 %34, %35, !dbg !452
  br i1 %36, label %37, label %38, !dbg !452

; <label>:37                                      ; preds = %32, %27, %23, %17
  call void @abort() #15, !dbg !453
  unreachable, !dbg !453

; <label>:38                                      ; preds = %32, %10
  ret void, !dbg !455
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @gnu_dev_makedev(i32 %__major, i32 %__minor) #7 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 %__major, i32* %1, align 4
  store i32 %__minor, i32* %2, align 4
  %3 = load i32* %2, align 4, !dbg !456
  %4 = and i32 %3, 255, !dbg !456
  %5 = load i32* %1, align 4, !dbg !456
  %6 = and i32 %5, 4095, !dbg !456
  %int_cast_to_i64 = zext i32 8 to i64
  call void @klee_overshift_check(i64 32, i64 %int_cast_to_i64), !dbg !456
  %7 = shl i32 %6, 8, !dbg !456
  %8 = or i32 %4, %7, !dbg !456
  %9 = zext i32 %8 to i64, !dbg !456
  %10 = load i32* %2, align 4, !dbg !456
  %11 = and i32 %10, -256, !dbg !456
  %12 = zext i32 %11 to i64, !dbg !456
  %int_cast_to_i641 = bitcast i64 12 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i641), !dbg !456
  %13 = shl i64 %12, 12, !dbg !456
  %14 = or i64 %9, %13, !dbg !456
  %15 = load i32* %1, align 4, !dbg !456
  %16 = and i32 %15, -4096, !dbg !456
  %17 = zext i32 %16 to i64, !dbg !456
  %int_cast_to_i642 = bitcast i64 32 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i642), !dbg !456
  %18 = shl i64 %17, 32, !dbg !456
  %19 = or i64 %14, %18, !dbg !456
  ret i64 %19, !dbg !456
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__errno_location() #8 {
  ret i32* @errno, !dbg !458
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__h_errno_location() #8 {
  ret i32* @h_errno, !dbg !459
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_term() #0 {
  %ptr = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %1 = load %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, align 8, !dbg !460
  store %struct.__STDIO_FILE_STRUCT.273* %1, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !460
  br label %2, !dbg !460

; <label>:2                                       ; preds = %15, %0
  %3 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !460
  %4 = icmp ne %struct.__STDIO_FILE_STRUCT.273* %3, null, !dbg !460
  br i1 %4, label %5, label %19, !dbg !460

; <label>:5                                       ; preds = %2
  %6 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !462
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %6, i32 0, i32 0, !dbg !462
  %8 = load i16* %7, align 2, !dbg !462
  %9 = zext i16 %8 to i32, !dbg !462
  %10 = and i32 %9, 64, !dbg !462
  %11 = icmp ne i32 %10, 0, !dbg !462
  br i1 %11, label %12, label %15, !dbg !462

; <label>:12                                      ; preds = %5
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !465
  %14 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* %13) #12, !dbg !465
  br label %15, !dbg !467

; <label>:15                                      ; preds = %5, %12
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !460
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 9, !dbg !460
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %17, align 8, !dbg !460
  store %struct.__STDIO_FILE_STRUCT.273* %18, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !460
  br label %2, !dbg !460

; <label>:19                                      ; preds = %2
  ret void, !dbg !468
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_init() #0 {
  %old_errno = alloca i32, align 4
  %1 = load i32* @errno, align 4, !dbg !469
  store i32 %1, i32* %old_errno, align 4, !dbg !469
  %2 = call i32 @isatty(i32 0) #13, !dbg !470
  %3 = sub nsw i32 1, %2, !dbg !470
  %4 = mul i32 %3, 256, !dbg !470
  %5 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !470
  %6 = zext i16 %5 to i32, !dbg !470
  %7 = xor i32 %6, %4, !dbg !470
  %8 = trunc i32 %7 to i16, !dbg !470
  store i16 %8, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !470
  %9 = call i32 @isatty(i32 1) #13, !dbg !471
  %10 = sub nsw i32 1, %9, !dbg !471
  %11 = mul i32 %10, 256, !dbg !471
  %12 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !471
  %13 = zext i16 %12 to i32, !dbg !471
  %14 = xor i32 %13, %11, !dbg !471
  %15 = trunc i32 %14 to i16, !dbg !471
  store i16 %15, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !471
  %16 = load i32* %old_errno, align 4, !dbg !472
  store i32 %16, i32* @errno, align 4, !dbg !472
  ret void, !dbg !473
}

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* noalias %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %bufsize = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %1, align 8
  %2 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !474
  %3 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %2, i32 0, i32 5, !dbg !474
  %4 = load i8** %3, align 8, !dbg !474
  %5 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !474
  %6 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %5, i32 0, i32 3, !dbg !474
  %7 = load i8** %6, align 8, !dbg !474
  %8 = ptrtoint i8* %4 to i64, !dbg !474
  %9 = ptrtoint i8* %7 to i64, !dbg !474
  %10 = sub i64 %8, %9, !dbg !474
  store i64 %10, i64* %bufsize, align 8, !dbg !474
  %11 = icmp ne i64 %10, 0, !dbg !474
  br i1 %11, label %12, label %24, !dbg !474

; <label>:12                                      ; preds = %0
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !476
  %14 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %13, i32 0, i32 3, !dbg !476
  %15 = load i8** %14, align 8, !dbg !476
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !476
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 5, !dbg !476
  store i8* %15, i8** %17, align 8, !dbg !476
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !478
  %19 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !478
  %20 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %19, i32 0, i32 3, !dbg !478
  %21 = load i8** %20, align 8, !dbg !478
  %22 = load i64* %bufsize, align 8, !dbg !478
  %23 = call i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %18, i8* %21, i64 %22) #12, !dbg !478
  br label %24, !dbg !479

; <label>:24                                      ; preds = %12, %0
  %25 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !480
  %26 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %25, i32 0, i32 5, !dbg !480
  %27 = load i8** %26, align 8, !dbg !480
  %28 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !480
  %29 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %28, i32 0, i32 3, !dbg !480
  %30 = load i8** %29, align 8, !dbg !480
  %31 = ptrtoint i8* %27 to i64, !dbg !480
  %32 = ptrtoint i8* %30 to i64, !dbg !480
  %33 = sub i64 %31, %32, !dbg !480
  ret i64 %33, !dbg !480
}

; Function Attrs: nounwind uwtable
define i8* @memcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !481
  store i8* %4, i8** %r1, align 8, !dbg !481
  %5 = load i8** %2, align 8, !dbg !482
  store i8* %5, i8** %r2, align 8, !dbg !482
  br label %6, !dbg !483

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !483
  %8 = icmp ne i64 %7, 0, !dbg !483
  br i1 %8, label %9, label %17, !dbg !483

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !484
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !484
  store i8* %11, i8** %r2, align 8, !dbg !484
  %12 = load i8* %10, align 1, !dbg !484
  %13 = load i8** %r1, align 8, !dbg !484
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !484
  store i8* %14, i8** %r1, align 8, !dbg !484
  store i8 %12, i8* %13, align 1, !dbg !484
  %15 = load i64* %3, align 8, !dbg !486
  %16 = add i64 %15, -1, !dbg !486
  store i64 %16, i64* %3, align 8, !dbg !486
  br label %6, !dbg !487

; <label>:17                                      ; preds = %6
  %18 = load i8** %1, align 8, !dbg !488
  ret i8* %18, !dbg !488
}

; Function Attrs: nounwind uwtable
define i8* @memset(i8* %s, i32 %c, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i64, align 8
  %p = alloca i8*, align 8
  store i8* %s, i8** %1, align 8
  store i32 %c, i32* %2, align 4
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !489
  store i8* %4, i8** %p, align 8, !dbg !489
  br label %5, !dbg !490

; <label>:5                                       ; preds = %8, %0
  %6 = load i64* %3, align 8, !dbg !490
  %7 = icmp ne i64 %6, 0, !dbg !490
  br i1 %7, label %8, label %15, !dbg !490

; <label>:8                                       ; preds = %5
  %9 = load i32* %2, align 4, !dbg !491
  %10 = trunc i32 %9 to i8, !dbg !491
  %11 = load i8** %p, align 8, !dbg !491
  %12 = getelementptr inbounds i8* %11, i32 1, !dbg !491
  store i8* %12, i8** %p, align 8, !dbg !491
  store i8 %10, i8* %11, align 1, !dbg !491
  %13 = load i64* %3, align 8, !dbg !493
  %14 = add i64 %13, -1, !dbg !493
  store i64 %14, i64* %3, align 8, !dbg !493
  br label %5, !dbg !494

; <label>:15                                      ; preds = %5
  %16 = load i8** %1, align 8, !dbg !495
  ret i8* %16, !dbg !495
}

; Function Attrs: nounwind uwtable
define i32 @isatty(i32 %fd) #0 {
  %1 = alloca i32, align 4
  %term = alloca %struct.termios.442, align 4
  store i32 %fd, i32* %1, align 4
  %2 = load i32* %1, align 4, !dbg !496
  %3 = call i32 @tcgetattr(i32 %2, %struct.termios.442* %term) #13, !dbg !496
  %4 = icmp eq i32 %3, 0, !dbg !496
  %5 = zext i1 %4 to i32, !dbg !496
  ret i32 %5, !dbg !496
}

; Function Attrs: nounwind uwtable
define i32 @tcgetattr(i32 %fd, %struct.termios.442* %termios_p) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.termios.442*, align 8
  %k_termios = alloca %struct.__kernel_termios, align 4
  %retval = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store %struct.termios.442* %termios_p, %struct.termios.442** %2, align 8
  %3 = load i32* %1, align 4, !dbg !497
  %4 = call i32 (i32, i64, ...)* @ioctl(i32 %3, i64 21505, %struct.__kernel_termios* %k_termios) #13, !dbg !497
  store i32 %4, i32* %retval, align 4, !dbg !497
  %5 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 0, !dbg !498
  %6 = load i32* %5, align 4, !dbg !498
  %7 = load %struct.termios.442** %2, align 8, !dbg !498
  %8 = getelementptr inbounds %struct.termios.442* %7, i32 0, i32 0, !dbg !498
  store i32 %6, i32* %8, align 4, !dbg !498
  %9 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 1, !dbg !499
  %10 = load i32* %9, align 4, !dbg !499
  %11 = load %struct.termios.442** %2, align 8, !dbg !499
  %12 = getelementptr inbounds %struct.termios.442* %11, i32 0, i32 1, !dbg !499
  store i32 %10, i32* %12, align 4, !dbg !499
  %13 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 2, !dbg !500
  %14 = load i32* %13, align 4, !dbg !500
  %15 = load %struct.termios.442** %2, align 8, !dbg !500
  %16 = getelementptr inbounds %struct.termios.442* %15, i32 0, i32 2, !dbg !500
  store i32 %14, i32* %16, align 4, !dbg !500
  %17 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 3, !dbg !501
  %18 = load i32* %17, align 4, !dbg !501
  %19 = load %struct.termios.442** %2, align 8, !dbg !501
  %20 = getelementptr inbounds %struct.termios.442* %19, i32 0, i32 3, !dbg !501
  store i32 %18, i32* %20, align 4, !dbg !501
  %21 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 4, !dbg !502
  %22 = load i8* %21, align 1, !dbg !502
  %23 = load %struct.termios.442** %2, align 8, !dbg !502
  %24 = getelementptr inbounds %struct.termios.442* %23, i32 0, i32 4, !dbg !502
  store i8 %22, i8* %24, align 1, !dbg !502
  %25 = load %struct.termios.442** %2, align 8, !dbg !503
  %26 = getelementptr inbounds %struct.termios.442* %25, i32 0, i32 5, !dbg !503
  %27 = getelementptr inbounds [32 x i8]* %26, i32 0, i64 0, !dbg !503
  %28 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 5, !dbg !503
  %29 = getelementptr inbounds [19 x i8]* %28, i32 0, i64 0, !dbg !503
  %30 = call i8* @mempcpy(i8* %27, i8* %29, i64 19) #13, !dbg !503
  %31 = call i8* @memset(i8* %30, i32 0, i64 13) #13, !dbg !503
  %32 = load i32* %retval, align 4, !dbg !506
  ret i32 %32, !dbg !506
}

; Function Attrs: nounwind
declare i32 @ioctl(i32, i64, ...) #6

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %stream, i8* %buf, i64 %bufsize) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i64, align 8
  %todo = alloca i64, align 8
  %rv = alloca i64, align 8
  %stodo = alloca i64, align 8
  %s = alloca i8*, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %2, align 8
  store i8* %buf, i8** %3, align 8
  store i64 %bufsize, i64* %4, align 8
  %5 = load i64* %4, align 8, !dbg !507
  store i64 %5, i64* %todo, align 8, !dbg !507
  br label %6, !dbg !508

; <label>:6                                       ; preds = %23, %0
  %7 = load i64* %todo, align 8, !dbg !509
  %8 = icmp eq i64 %7, 0, !dbg !509
  br i1 %8, label %9, label %11, !dbg !509

; <label>:9                                       ; preds = %6
  %10 = load i64* %4, align 8, !dbg !512
  store i64 %10, i64* %1, !dbg !512
  br label %95, !dbg !512

; <label>:11                                      ; preds = %6
  %12 = load i64* %todo, align 8, !dbg !514
  %13 = icmp ule i64 %12, 9223372036854775807, !dbg !514
  %14 = load i64* %todo, align 8, !dbg !514
  %15 = select i1 %13, i64 %14, i64 9223372036854775807, !dbg !514
  store i64 %15, i64* %stodo, align 8, !dbg !514
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !515
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 2, !dbg !515
  %18 = load i32* %17, align 4, !dbg !515
  %19 = load i8** %3, align 8, !dbg !515
  %20 = load i64* %stodo, align 8, !dbg !515
  %21 = call i64 @write(i32 %18, i8* %19, i64 %20) #12, !dbg !515
  store i64 %21, i64* %rv, align 8, !dbg !515
  %22 = icmp sge i64 %21, 0, !dbg !515
  br i1 %22, label %23, label %30, !dbg !515

; <label>:23                                      ; preds = %11
  %24 = load i64* %rv, align 8, !dbg !517
  %25 = load i64* %todo, align 8, !dbg !517
  %26 = sub i64 %25, %24, !dbg !517
  store i64 %26, i64* %todo, align 8, !dbg !517
  %27 = load i64* %rv, align 8, !dbg !519
  %28 = load i8** %3, align 8, !dbg !519
  %29 = getelementptr inbounds i8* %28, i64 %27, !dbg !519
  store i8* %29, i8** %3, align 8, !dbg !519
  br label %6, !dbg !520

; <label>:30                                      ; preds = %11
  %31 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !521
  %32 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %31, i32 0, i32 0, !dbg !521
  %33 = load i16* %32, align 2, !dbg !521
  %34 = zext i16 %33 to i32, !dbg !521
  %35 = or i32 %34, 8, !dbg !521
  %36 = trunc i32 %35 to i16, !dbg !521
  store i16 %36, i16* %32, align 2, !dbg !521
  %37 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !523
  %38 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %37, i32 0, i32 4, !dbg !523
  %39 = load i8** %38, align 8, !dbg !523
  %40 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !523
  %41 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %40, i32 0, i32 3, !dbg !523
  %42 = load i8** %41, align 8, !dbg !523
  %43 = ptrtoint i8* %39 to i64, !dbg !523
  %44 = ptrtoint i8* %42 to i64, !dbg !523
  %45 = sub i64 %43, %44, !dbg !523
  store i64 %45, i64* %stodo, align 8, !dbg !523
  %46 = icmp ne i64 %45, 0, !dbg !523
  br i1 %46, label %47, label %91, !dbg !523

; <label>:47                                      ; preds = %30
  %48 = load i64* %stodo, align 8, !dbg !525
  %49 = load i64* %todo, align 8, !dbg !525
  %50 = icmp ugt i64 %48, %49, !dbg !525
  br i1 %50, label %51, label %53, !dbg !525

; <label>:51                                      ; preds = %47
  %52 = load i64* %todo, align 8, !dbg !528
  store i64 %52, i64* %stodo, align 8, !dbg !528
  br label %53, !dbg !530

; <label>:53                                      ; preds = %51, %47
  %54 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !531
  %55 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %54, i32 0, i32 3, !dbg !531
  %56 = load i8** %55, align 8, !dbg !531
  store i8* %56, i8** %s, align 8, !dbg !531
  br label %57, !dbg !532

; <label>:57                                      ; preds = %70, %53
  %58 = load i8** %3, align 8, !dbg !533
  %59 = load i8* %58, align 1, !dbg !533
  %60 = load i8** %s, align 8, !dbg !533
  store i8 %59, i8* %60, align 1, !dbg !533
  %61 = zext i8 %59 to i32, !dbg !533
  %62 = icmp eq i32 %61, 10, !dbg !533
  br i1 %62, label %63, label %70, !dbg !533

; <label>:63                                      ; preds = %57
  %64 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !533
  %65 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %64, i32 0, i32 0, !dbg !533
  %66 = load i16* %65, align 2, !dbg !533
  %67 = zext i16 %66 to i32, !dbg !533
  %68 = and i32 %67, 256, !dbg !533
  %69 = icmp ne i32 %68, 0, !dbg !533
  br i1 %69, label %78, label %70, !dbg !533

; <label>:70                                      ; preds = %63, %57
  %71 = load i8** %s, align 8, !dbg !536
  %72 = getelementptr inbounds i8* %71, i32 1, !dbg !536
  store i8* %72, i8** %s, align 8, !dbg !536
  %73 = load i8** %3, align 8, !dbg !537
  %74 = getelementptr inbounds i8* %73, i32 1, !dbg !537
  store i8* %74, i8** %3, align 8, !dbg !537
  %75 = load i64* %stodo, align 8, !dbg !538
  %76 = add nsw i64 %75, -1, !dbg !538
  store i64 %76, i64* %stodo, align 8, !dbg !538
  %77 = icmp ne i64 %76, 0, !dbg !538
  br i1 %77, label %57, label %78, !dbg !538

; <label>:78                                      ; preds = %63, %70
  %79 = load i8** %s, align 8, !dbg !539
  %80 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !539
  %81 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %80, i32 0, i32 5, !dbg !539
  store i8* %79, i8** %81, align 8, !dbg !539
  %82 = load i8** %s, align 8, !dbg !540
  %83 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !540
  %84 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %83, i32 0, i32 3, !dbg !540
  %85 = load i8** %84, align 8, !dbg !540
  %86 = ptrtoint i8* %82 to i64, !dbg !540
  %87 = ptrtoint i8* %85 to i64, !dbg !540
  %88 = sub i64 %86, %87, !dbg !540
  %89 = load i64* %todo, align 8, !dbg !540
  %90 = sub i64 %89, %88, !dbg !540
  store i64 %90, i64* %todo, align 8, !dbg !540
  br label %91, !dbg !541

; <label>:91                                      ; preds = %78, %30
  %92 = load i64* %4, align 8, !dbg !542
  %93 = load i64* %todo, align 8, !dbg !542
  %94 = sub i64 %92, %93, !dbg !542
  store i64 %94, i64* %1, !dbg !542
  br label %95, !dbg !542

; <label>:95                                      ; preds = %91, %9
  %96 = load i64* %1, !dbg !543
  ret i64 %96, !dbg !543
}

declare i64 @write(i32, i8*, i64) #2

; Function Attrs: nounwind uwtable
define i8* @mempcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !544
  store i8* %4, i8** %r1, align 8, !dbg !544
  %5 = load i8** %2, align 8, !dbg !545
  store i8* %5, i8** %r2, align 8, !dbg !545
  br label %6, !dbg !546

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !546
  %8 = icmp ne i64 %7, 0, !dbg !546
  br i1 %8, label %9, label %17, !dbg !546

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !547
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !547
  store i8* %11, i8** %r2, align 8, !dbg !547
  %12 = load i8* %10, align 1, !dbg !547
  %13 = load i8** %r1, align 8, !dbg !547
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !547
  store i8* %14, i8** %r1, align 8, !dbg !547
  store i8 %12, i8* %13, align 1, !dbg !547
  %15 = load i64* %3, align 8, !dbg !549
  %16 = add i64 %15, -1, !dbg !549
  store i64 %16, i64* %3, align 8, !dbg !549
  br label %6, !dbg !550

; <label>:17                                      ; preds = %6
  %18 = load i8** %r1, align 8, !dbg !551
  ret i8* %18, !dbg !551
}

define i32 @main(i32, i8**) {
entry:
  call void @__uClibc_main(i32 (i32, i8**, i8**)* bitcast (i32 ()* @__user_main to i32 (i32, i8**, i8**)*), i32 %0, i8** %1, void ()* null, void ()* null, void ()* null, i8* null)
  unreachable
}

; Function Attrs: nounwind ssp uwtable
define void @klee_div_zero_check(i64 %z) #9 {
  %1 = icmp eq i64 %z, 0, !dbg !552
  br i1 %1, label %2, label %3, !dbg !552

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([60 x i8]* @.str32, i64 0, i64 0), i32 14, i8* getelementptr inbounds ([15 x i8]* @.str133, i64 0, i64 0), i8* getelementptr inbounds ([8 x i8]* @.str234, i64 0, i64 0)) #15, !dbg !554
  unreachable, !dbg !554

; <label>:3                                       ; preds = %0
  ret void, !dbg !555
}

; Function Attrs: noreturn
declare void @klee_report_error(i8*, i32, i8*, i8*) #10

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata) #1

; Function Attrs: nounwind ssp uwtable
define i32 @klee_int(i8* %name) #9 {
  %x = alloca i32, align 4
  %1 = bitcast i32* %x to i8*, !dbg !556
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %1, i64 4, i8* %name) #13, !dbg !556
  %2 = load i32* %x, align 4, !dbg !557, !tbaa !558
  ret i32 %2, !dbg !557
}

; Function Attrs: nounwind ssp uwtable
define void @klee_overshift_check(i64 %bitWidth, i64 %shift) #9 {
  %1 = icmp ult i64 %shift, %bitWidth, !dbg !562
  br i1 %1, label %3, label %2, !dbg !562

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([8 x i8]* @.str335, i64 0, i64 0), i32 0, i8* getelementptr inbounds ([16 x i8]* @.str14, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8]* @.str25, i64 0, i64 0)) #15, !dbg !564
  unreachable, !dbg !564

; <label>:3                                       ; preds = %0
  ret void, !dbg !566
}

; Function Attrs: nounwind ssp uwtable
define i32 @klee_range(i32 %start, i32 %end, i8* %name) #9 {
  %x = alloca i32, align 4
  %1 = icmp slt i32 %start, %end, !dbg !567
  br i1 %1, label %3, label %2, !dbg !567

; <label>:2                                       ; preds = %0
  call void @klee_report_error(i8* getelementptr inbounds ([51 x i8]* @.str6, i64 0, i64 0), i32 17, i8* getelementptr inbounds ([14 x i8]* @.str17, i64 0, i64 0), i8* getelementptr inbounds ([5 x i8]* @.str28, i64 0, i64 0)) #15, !dbg !569
  unreachable, !dbg !569

; <label>:3                                       ; preds = %0
  %4 = add nsw i32 %start, 1, !dbg !570
  %5 = icmp eq i32 %4, %end, !dbg !570
  br i1 %5, label %21, label %6, !dbg !570

; <label>:6                                       ; preds = %3
  %7 = bitcast i32* %x to i8*, !dbg !572
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %7, i64 4, i8* %name) #13, !dbg !572
  %8 = icmp eq i32 %start, 0, !dbg !574
  %9 = load i32* %x, align 4, !dbg !576, !tbaa !558
  br i1 %8, label %10, label %13, !dbg !574

; <label>:10                                      ; preds = %6
  %11 = icmp ult i32 %9, %end, !dbg !576
  %12 = zext i1 %11 to i64, !dbg !576
  call void bitcast (i32 (...)* @klee_assume to void (i64)*)(i64 %12) #13, !dbg !576
  br label %19, !dbg !578

; <label>:13                                      ; preds = %6
  %14 = icmp sge i32 %9, %start, !dbg !579
  %15 = zext i1 %14 to i64, !dbg !579
  call void bitcast (i32 (...)* @klee_assume to void (i64)*)(i64 %15) #13, !dbg !579
  %16 = load i32* %x, align 4, !dbg !581, !tbaa !558
  %17 = icmp slt i32 %16, %end, !dbg !581
  %18 = zext i1 %17 to i64, !dbg !581
  call void bitcast (i32 (...)* @klee_assume to void (i64)*)(i64 %18) #13, !dbg !581
  br label %19

; <label>:19                                      ; preds = %13, %10
  %20 = load i32* %x, align 4, !dbg !582, !tbaa !558
  br label %21, !dbg !582

; <label>:21                                      ; preds = %19, %3
  %.0 = phi i32 [ %20, %19 ], [ %start, %3 ]
  ret i32 %.0, !dbg !583
}

; Function Attrs: nounwind ssp uwtable
define weak i8* @memmove(i8* %dst, i8* %src, i64 %count) #9 {
  %1 = icmp eq i8* %src, %dst, !dbg !584
  br i1 %1, label %.loopexit, label %2, !dbg !584

; <label>:2                                       ; preds = %0
  %3 = icmp ugt i8* %src, %dst, !dbg !586
  br i1 %3, label %.preheader, label %18, !dbg !586

.preheader:                                       ; preds = %2
  %4 = icmp eq i64 %count, 0, !dbg !588
  br i1 %4, label %.loopexit, label %.lr.ph.preheader, !dbg !588

.lr.ph.preheader:                                 ; preds = %.preheader
  %n.vec = and i64 %count, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %5 = add i64 %count, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep11 = getelementptr i8* %src, i64 %5
  %scevgep = getelementptr i8* %dst, i64 %5
  %bound1 = icmp uge i8* %scevgep, %src
  %bound0 = icmp uge i8* %scevgep11, %dst
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %src, i64 %n.vec
  %ptr.ind.end13 = getelementptr i8* %dst, i64 %n.vec
  %rev.ind.end = sub i64 %count, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %src, i64 %index
  %next.gep110 = getelementptr i8* %dst, i64 %index
  %6 = bitcast i8* %next.gep to <16 x i8>*, !dbg !588
  %wide.load = load <16 x i8>* %6, align 1, !dbg !588
  %next.gep.sum586 = or i64 %index, 16, !dbg !588
  %7 = getelementptr i8* %src, i64 %next.gep.sum586, !dbg !588
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !588
  %wide.load207 = load <16 x i8>* %8, align 1, !dbg !588
  %9 = bitcast i8* %next.gep110 to <16 x i8>*, !dbg !588
  store <16 x i8> %wide.load, <16 x i8>* %9, align 1, !dbg !588
  %10 = getelementptr i8* %dst, i64 %next.gep.sum586, !dbg !588
  %11 = bitcast i8* %10 to <16 x i8>*, !dbg !588
  store <16 x i8> %wide.load207, <16 x i8>* %11, align 1, !dbg !588
  %index.next = add i64 %index, 32
  %12 = icmp eq i64 %index.next, %n.vec
  br i1 %12, label %middle.block, label %vector.body, !llvm.loop !590

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %src, %.lr.ph.preheader ], [ %src, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val12 = phi i8* [ %dst, %.lr.ph.preheader ], [ %dst, %vector.memcheck ], [ %ptr.ind.end13, %vector.body ]
  %resume.val14 = phi i64 [ %count, %.lr.ph.preheader ], [ %count, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %count
  br i1 %cmp.n, label %.loopexit, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %b.04 = phi i8* [ %14, %.lr.ph ], [ %resume.val, %middle.block ]
  %a.03 = phi i8* [ %16, %.lr.ph ], [ %resume.val12, %middle.block ]
  %.02 = phi i64 [ %13, %.lr.ph ], [ %resume.val14, %middle.block ]
  %13 = add i64 %.02, -1, !dbg !588
  %14 = getelementptr inbounds i8* %b.04, i64 1, !dbg !588
  %15 = load i8* %b.04, align 1, !dbg !588, !tbaa !593
  %16 = getelementptr inbounds i8* %a.03, i64 1, !dbg !588
  store i8 %15, i8* %a.03, align 1, !dbg !588, !tbaa !593
  %17 = icmp eq i64 %13, 0, !dbg !588
  br i1 %17, label %.loopexit, label %.lr.ph, !dbg !588, !llvm.loop !594

; <label>:18                                      ; preds = %2
  %19 = add i64 %count, -1, !dbg !595
  %20 = icmp eq i64 %count, 0, !dbg !597
  br i1 %20, label %.loopexit, label %.lr.ph9, !dbg !597

.lr.ph9:                                          ; preds = %18
  %21 = getelementptr inbounds i8* %src, i64 %19, !dbg !598
  %22 = getelementptr inbounds i8* %dst, i64 %19, !dbg !595
  %n.vec215 = and i64 %count, -32
  %cmp.zero217 = icmp eq i64 %n.vec215, 0
  br i1 %cmp.zero217, label %middle.block210, label %vector.memcheck224

vector.memcheck224:                               ; preds = %.lr.ph9
  %bound1221 = icmp ule i8* %21, %dst
  %bound0220 = icmp ule i8* %22, %src
  %memcheck.conflict223 = and i1 %bound0220, %bound1221
  %.sum = sub i64 %19, %n.vec215
  %rev.ptr.ind.end = getelementptr i8* %src, i64 %.sum
  %rev.ptr.ind.end229 = getelementptr i8* %dst, i64 %.sum
  %rev.ind.end231 = sub i64 %count, %n.vec215
  br i1 %memcheck.conflict223, label %middle.block210, label %vector.body209

vector.body209:                                   ; preds = %vector.body209, %vector.memcheck224
  %index212 = phi i64 [ %index.next234, %vector.body209 ], [ 0, %vector.memcheck224 ]
  %.sum440 = sub i64 %19, %index212
  %next.gep236.sum = add i64 %.sum440, -15, !dbg !597
  %23 = getelementptr i8* %src, i64 %next.gep236.sum, !dbg !597
  %24 = bitcast i8* %23 to <16 x i8>*, !dbg !597
  %wide.load434 = load <16 x i8>* %24, align 1, !dbg !597
  %reverse = shufflevector <16 x i8> %wide.load434, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !597
  %.sum505 = add i64 %.sum440, -31, !dbg !597
  %25 = getelementptr i8* %src, i64 %.sum505, !dbg !597
  %26 = bitcast i8* %25 to <16 x i8>*, !dbg !597
  %wide.load435 = load <16 x i8>* %26, align 1, !dbg !597
  %reverse436 = shufflevector <16 x i8> %wide.load435, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !597
  %reverse437 = shufflevector <16 x i8> %reverse, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !597
  %27 = getelementptr i8* %dst, i64 %next.gep236.sum, !dbg !597
  %28 = bitcast i8* %27 to <16 x i8>*, !dbg !597
  store <16 x i8> %reverse437, <16 x i8>* %28, align 1, !dbg !597
  %reverse438 = shufflevector <16 x i8> %reverse436, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !597
  %29 = getelementptr i8* %dst, i64 %.sum505, !dbg !597
  %30 = bitcast i8* %29 to <16 x i8>*, !dbg !597
  store <16 x i8> %reverse438, <16 x i8>* %30, align 1, !dbg !597
  %index.next234 = add i64 %index212, 32
  %31 = icmp eq i64 %index.next234, %n.vec215
  br i1 %31, label %middle.block210, label %vector.body209, !llvm.loop !599

middle.block210:                                  ; preds = %vector.body209, %vector.memcheck224, %.lr.ph9
  %resume.val225 = phi i8* [ %21, %.lr.ph9 ], [ %21, %vector.memcheck224 ], [ %rev.ptr.ind.end, %vector.body209 ]
  %resume.val227 = phi i8* [ %22, %.lr.ph9 ], [ %22, %vector.memcheck224 ], [ %rev.ptr.ind.end229, %vector.body209 ]
  %resume.val230 = phi i64 [ %count, %.lr.ph9 ], [ %count, %vector.memcheck224 ], [ %rev.ind.end231, %vector.body209 ]
  %new.indc.resume.val232 = phi i64 [ 0, %.lr.ph9 ], [ 0, %vector.memcheck224 ], [ %n.vec215, %vector.body209 ]
  %cmp.n233 = icmp eq i64 %new.indc.resume.val232, %count
  br i1 %cmp.n233, label %.loopexit, label %scalar.ph211

scalar.ph211:                                     ; preds = %scalar.ph211, %middle.block210
  %b.18 = phi i8* [ %33, %scalar.ph211 ], [ %resume.val225, %middle.block210 ]
  %a.17 = phi i8* [ %35, %scalar.ph211 ], [ %resume.val227, %middle.block210 ]
  %.16 = phi i64 [ %32, %scalar.ph211 ], [ %resume.val230, %middle.block210 ]
  %32 = add i64 %.16, -1, !dbg !597
  %33 = getelementptr inbounds i8* %b.18, i64 -1, !dbg !597
  %34 = load i8* %b.18, align 1, !dbg !597, !tbaa !593
  %35 = getelementptr inbounds i8* %a.17, i64 -1, !dbg !597
  store i8 %34, i8* %a.17, align 1, !dbg !597, !tbaa !593
  %36 = icmp eq i64 %32, 0, !dbg !597
  br i1 %36, label %.loopexit, label %scalar.ph211, !dbg !597, !llvm.loop !600

.loopexit:                                        ; preds = %scalar.ph211, %middle.block210, %18, %.lr.ph, %middle.block, %.preheader, %0
  ret i8* %dst, !dbg !601
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind readonly uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #4 = { noreturn nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #5 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-floa
attributes #6 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false
attributes #7 = { inlinehint nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use
attributes #8 = { nounwind readnone uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #9 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nounwind readonly }
attributes #12 = { nobuiltin }
attributes #13 = { nobuiltin nounwind }
attributes #14 = { nobuiltin nounwind readnone }
attributes #15 = { nobuiltin noreturn nounwind }

!llvm.dbg.cu = !{!0, !9, !19, !58, !66, !71, !120, !151, !163, !171, !178, !203, !209, !241, !249, !259, !269, !279, !291, !305, !319, !333}
!llvm.module.flags = !{!348, !349}
!llvm.ident = !{!350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350, !350}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !2, metadata !2, metadata !""} ; [ 
!1 = metadata !{metadata !"test.c", metadata !"/home/klee/workspace/basic-tests/multi-stage"}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 5, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @__user_main, null, null, metadata !2, i32 5} ; [ DW_TAG_subprogra
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{metadata !8}
!8 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!9 = metadata !{i32 786449, metadata !10, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !11, metadata !2, metadata !2, metadata !""} ; 
!10 = metadata !{metadata !"libc/string/strcmp.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"strcmp", metadata !"strcmp", metadata !"", i32 20, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i8*, i8*)* @strcmp, null, null, metadata !2, i32 21} ; [ D
!13 = metadata !{i32 786473, metadata !10}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/strcmp.c]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{metadata !8, metadata !16, metadata !16}
!16 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !17} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!17 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !18} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from char]
!18 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!19 = metadata !{i32 786449, metadata !20, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !21, metadata !48, metadata !2, metadata !""} 
!20 = metadata !{metadata !"libc/misc/internals/__uClibc_main.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!21 = metadata !{metadata !22, metadata !26, metadata !27, metadata !37, metadata !40, metadata !47}
!22 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"__uClibc_init", metadata !"__uClibc_init", metadata !"", i32 187, metadata !24, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_init, null, null, metadata !2
!23 = metadata !{i32 786473, metadata !20}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!24 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !25, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!25 = metadata !{null}
!26 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"__uClibc_fini", metadata !"__uClibc_fini", metadata !"", i32 251, metadata !24, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_fini, null, null, metadata !2
!27 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"__uClibc_main", metadata !"__uClibc_main", metadata !"", i32 278, metadata !28, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32 (i32, i8**, i8**)*, i32, i8**, void ()
!28 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !29, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!29 = metadata !{null, metadata !30, metadata !8, metadata !33, metadata !35, metadata !35, metadata !35, metadata !36}
!30 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !31} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!31 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !32, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!32 = metadata !{metadata !8, metadata !8, metadata !33, metadata !33}
!33 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !34} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!34 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !18} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from char]
!35 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !24} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!36 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!37 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"__check_one_fd", metadata !"__check_one_fd", metadata !"", i32 136, metadata !38, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @__check_one_fd, null, null, m
!38 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !39, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!39 = metadata !{null, metadata !8, metadata !8}
!40 = metadata !{i32 786478, metadata !41, metadata !42, metadata !"gnu_dev_makedev", metadata !"gnu_dev_makedev", metadata !"", i32 54, metadata !43, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i32, i32)* @gnu_dev_makedev, null, null, 
!41 = metadata !{metadata !"./include/sys/sysmacros.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!42 = metadata !{i32 786473, metadata !41}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!43 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !44, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!44 = metadata !{metadata !45, metadata !46, metadata !46}
!45 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!46 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!47 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"__check_suid", metadata !"__check_suid", metadata !"", i32 155, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @__check_suid, null, null, metadata !2, i32 
!48 = metadata !{metadata !49, metadata !50, metadata !51, metadata !52, metadata !55, metadata !56, metadata !57}
!49 = metadata !{i32 786484, i32 0, null, metadata !"__libc_stack_end", metadata !"__libc_stack_end", metadata !"", metadata !23, i32 52, metadata !36, i32 0, i32 1, i8** @__libc_stack_end, null} ; [ DW_TAG_variable ] [__libc_stack_end] [line 52] [def]
!50 = metadata !{i32 786484, i32 0, null, metadata !"__uclibc_progname", metadata !"__uclibc_progname", metadata !"", metadata !23, i32 110, metadata !16, i32 0, i32 1, i8** @__uclibc_progname, null} ; [ DW_TAG_variable ] [__uclibc_progname] [line 110] [
!51 = metadata !{i32 786484, i32 0, null, metadata !"__environ", metadata !"__environ", metadata !"", metadata !23, i32 125, metadata !33, i32 0, i32 1, i8*** @__environ, null} ; [ DW_TAG_variable ] [__environ] [line 125] [def]
!52 = metadata !{i32 786484, i32 0, null, metadata !"__pagesize", metadata !"__pagesize", metadata !"", metadata !23, i32 129, metadata !53, i32 0, i32 1, i64* @__pagesize, null} ; [ DW_TAG_variable ] [__pagesize] [line 129] [def]
!53 = metadata !{i32 786454, metadata !20, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!54 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!55 = metadata !{i32 786484, i32 0, metadata !22, metadata !"been_there_done_that", metadata !"been_there_done_that", metadata !"", metadata !23, i32 189, metadata !8, i32 1, i32 1, i32* @__uClibc_init.been_there_done_that, null} ; [ DW_TAG_variable ] [b
!56 = metadata !{i32 786484, i32 0, null, metadata !"__app_fini", metadata !"__app_fini", metadata !"", metadata !23, i32 244, metadata !35, i32 0, i32 1, void ()** @__app_fini, null} ; [ DW_TAG_variable ] [__app_fini] [line 244] [def]
!57 = metadata !{i32 786484, i32 0, null, metadata !"__rtld_fini", metadata !"__rtld_fini", metadata !"", metadata !23, i32 247, metadata !35, i32 0, i32 1, void ()** @__rtld_fini, null} ; [ DW_TAG_variable ] [__rtld_fini] [line 247] [def]
!58 = metadata !{i32 786449, metadata !59, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !60, metadata !2, metadata !2, metadata !""} ;
!59 = metadata !{metadata !"libc/misc/internals/__errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!60 = metadata !{metadata !61}
!61 = metadata !{i32 786478, metadata !59, metadata !62, metadata !"__errno_location", metadata !"__errno_location", metadata !"", i32 11, metadata !63, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__errno_location, null, null, met
!62 = metadata !{i32 786473, metadata !59}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__errno_location.c]
!63 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !64, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!64 = metadata !{metadata !65}
!65 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!66 = metadata !{i32 786449, metadata !67, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !68, metadata !2, metadata !2, metadata !""} ;
!67 = metadata !{metadata !"libc/misc/internals/__h_errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!68 = metadata !{metadata !69}
!69 = metadata !{i32 786478, metadata !67, metadata !70, metadata !"__h_errno_location", metadata !"__h_errno_location", metadata !"", i32 10, metadata !63, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__h_errno_location, null, nul
!70 = metadata !{i32 786473, metadata !67}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__h_errno_location.c]
!71 = metadata !{i32 786449, metadata !72, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !73, metadata !77, metadata !2, metadata !""} 
!72 = metadata !{metadata !"libc/stdio/_stdio.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!73 = metadata !{metadata !74, metadata !76}
!74 = metadata !{i32 786478, metadata !72, metadata !75, metadata !"_stdio_term", metadata !"_stdio_term", metadata !"", i32 210, metadata !24, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_term, null, null, metadata !2, i32 
!75 = metadata !{i32 786473, metadata !72}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!76 = metadata !{i32 786478, metadata !72, metadata !75, metadata !"_stdio_init", metadata !"_stdio_init", metadata !"", i32 277, metadata !24, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_init, null, null, metadata !2, i32 
!77 = metadata !{metadata !78, metadata !111, metadata !112, metadata !113, metadata !114, metadata !115, metadata !116}
!78 = metadata !{i32 786484, i32 0, null, metadata !"stdin", metadata !"stdin", metadata !"", metadata !75, i32 154, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdin, null} ; [ DW_TAG_variable ] [stdin] [line 154] [def]
!79 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !80} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!80 = metadata !{i32 786454, metadata !72, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !81} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!81 = metadata !{i32 786451, metadata !82, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !83, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offset
!82 = metadata !{metadata !"./include/bits/uClibc_stdio.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!83 = metadata !{metadata !84, metadata !86, metadata !91, metadata !92, metadata !94, metadata !95, metadata !96, metadata !97, metadata !98, metadata !99, metadata !101, metadata !104}
!84 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !85} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!85 = metadata !{i32 786468, null, null, metadata !"unsigned short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned short] [line 0, size 16, align 16, offset 0, enc DW_ATE_unsigned]
!86 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !87} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!87 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 16, i64 8, i32 0, i32 0, metadata !88, metadata !89, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 16, align 8, offset 0] [from unsigned char]
!88 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!89 = metadata !{metadata !90}
!90 = metadata !{i32 786465, i64 0, i64 2}        ; [ DW_TAG_subrange_type ] [0, 1]
!91 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!92 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!93 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !88} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from unsigned char]
!94 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!95 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!96 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!97 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!98 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!99 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !100} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!100 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!101 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !102} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!102 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 32, i32 0, i32 0, metadata !103, metadata !89, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 64, align 32, offset 0] [from wchar_t]
!103 = metadata !{i32 786454, metadata !82, null, metadata !"wchar_t", i32 65, i64 0, i64 0, i64 0, i32 0, metadata !8} ; [ DW_TAG_typedef ] [wchar_t] [line 65, size 0, align 0, offset 0] [from int]
!104 = metadata !{i32 786445, metadata !82, metadata !81, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !105} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!105 = metadata !{i32 786454, metadata !82, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !106} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!106 = metadata !{i32 786451, metadata !107, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !108, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!107 = metadata !{metadata !"./include/wchar.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!108 = metadata !{metadata !109, metadata !110}
!109 = metadata !{i32 786445, metadata !107, metadata !106, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !103} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!110 = metadata !{i32 786445, metadata !107, metadata !106, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !103} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!111 = metadata !{i32 786484, i32 0, null, metadata !"stdout", metadata !"stdout", metadata !"", metadata !75, i32 155, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdout, null} ; [ DW_TAG_variable ] [stdout] [line 155] [def]
!112 = metadata !{i32 786484, i32 0, null, metadata !"stderr", metadata !"stderr", metadata !"", metadata !75, i32 156, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stderr, null} ; [ DW_TAG_variable ] [stderr] [line 156] [def]
!113 = metadata !{i32 786484, i32 0, null, metadata !"__stdin", metadata !"__stdin", metadata !"", metadata !75, i32 159, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdin, null} ; [ DW_TAG_variable ] [__stdin] [line 159] [def]
!114 = metadata !{i32 786484, i32 0, null, metadata !"__stdout", metadata !"__stdout", metadata !"", metadata !75, i32 162, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdout, null} ; [ DW_TAG_variable ] [__stdout] [line 162] [def]
!115 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_openlist", metadata !"_stdio_openlist", metadata !"", metadata !75, i32 180, metadata !79, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, null} ; [ DW_TAG_variable ] [_stdio_
!116 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_streams", metadata !"_stdio_streams", metadata !"", metadata !75, i32 131, metadata !117, i32 1, i32 1, [3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, null} ; [ DW_TAG_variable ] [_std
!117 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1920, i64 64, i32 0, i32 0, metadata !80, metadata !118, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 1920, align 64, offset 0] [from FILE]
!118 = metadata !{metadata !119}
!119 = metadata !{i32 786465, i64 0, i64 3}       ; [ DW_TAG_subrange_type ] [0, 2]
!120 = metadata !{i32 786449, metadata !121, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !122, metadata !2, metadata !2, metadata !""
!121 = metadata !{metadata !"libc/stdio/_wcommit.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!122 = metadata !{metadata !123}
!123 = metadata !{i32 786478, metadata !121, metadata !124, metadata !"__stdio_wcommit", metadata !"__stdio_wcommit", metadata !"", i32 17, metadata !125, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*)* @
!124 = metadata !{i32 786473, metadata !121}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!125 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !126, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!126 = metadata !{metadata !127, metadata !128}
!127 = metadata !{i32 786454, metadata !121, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!128 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !129} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!129 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !130} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!130 = metadata !{i32 786454, metadata !121, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !131} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!131 = metadata !{i32 786451, metadata !82, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !132, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!132 = metadata !{metadata !133, metadata !134, metadata !135, metadata !136, metadata !137, metadata !138, metadata !139, metadata !140, metadata !141, metadata !142, metadata !144, metadata !145}
!133 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !85} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!134 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !87} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!135 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!136 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!137 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!138 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!139 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!140 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!141 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!142 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !143} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!143 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !131} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!144 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !102} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!145 = metadata !{i32 786445, metadata !82, metadata !131, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !146} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!146 = metadata !{i32 786454, metadata !82, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !147} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!147 = metadata !{i32 786451, metadata !107, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !148, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!148 = metadata !{metadata !149, metadata !150}
!149 = metadata !{i32 786445, metadata !107, metadata !147, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !103} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!150 = metadata !{i32 786445, metadata !107, metadata !147, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !103} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!151 = metadata !{i32 786449, metadata !152, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !153, metadata !2, metadata !2, metadata !""
!152 = metadata !{metadata !"libc/string/memcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!153 = metadata !{metadata !154}
!154 = metadata !{i32 786478, metadata !152, metadata !155, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 18, metadata !156, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !2, i32 
!155 = metadata !{i32 786473, metadata !152}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!156 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !157, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!157 = metadata !{metadata !36, metadata !158, metadata !159, metadata !162}
!158 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !36} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!159 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !160} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!160 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !161} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!161 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!162 = metadata !{i32 786454, metadata !152, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!163 = metadata !{i32 786449, metadata !164, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !165, metadata !2, metadata !2, metadata !""
!164 = metadata !{metadata !"libc/string/memset.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!165 = metadata !{metadata !166}
!166 = metadata !{i32 786478, metadata !164, metadata !167, metadata !"memset", metadata !"memset", metadata !"", i32 17, metadata !168, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i32, i64)* @memset, null, null, metadata !2, i32 
!167 = metadata !{i32 786473, metadata !164}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!168 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !169, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!169 = metadata !{metadata !36, metadata !36, metadata !8, metadata !170}
!170 = metadata !{i32 786454, metadata !164, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!171 = metadata !{i32 786449, metadata !172, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !173, metadata !2, metadata !2, metadata !""
!172 = metadata !{metadata !"libc/termios/isatty.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!173 = metadata !{metadata !174}
!174 = metadata !{i32 786478, metadata !172, metadata !175, metadata !"isatty", metadata !"isatty", metadata !"", i32 26, metadata !176, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @isatty, null, null, metadata !2, i32 27} ; [ DW
!175 = metadata !{i32 786473, metadata !172}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/isatty.c]
!176 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !177, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!177 = metadata !{metadata !8, metadata !8}
!178 = metadata !{i32 786449, metadata !179, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !180, metadata !2, metadata !2, metadata !""
!179 = metadata !{metadata !"libc/termios/tcgetattr.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!180 = metadata !{metadata !181}
!181 = metadata !{i32 786478, metadata !179, metadata !182, metadata !"tcgetattr", metadata !"tcgetattr", metadata !"", i32 38, metadata !183, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, %struct.termios.442*)* @tcgetattr, null, nu
!182 = metadata !{i32 786473, metadata !179}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!183 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !184, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!184 = metadata !{metadata !8, metadata !8, metadata !185}
!185 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !186} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from termios]
!186 = metadata !{i32 786451, metadata !187, null, metadata !"termios", i32 30, i64 480, i64 32, i32 0, i32 0, null, metadata !188, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [termios] [line 30, size 480, align 32, offset 0] [def] [from ]
!187 = metadata !{metadata !"./include/bits/termios.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!188 = metadata !{metadata !189, metadata !191, metadata !192, metadata !193, metadata !194, metadata !196, metadata !200, metadata !202}
!189 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_iflag", i32 32, i64 32, i64 32, i64 0, i32 0, metadata !190} ; [ DW_TAG_member ] [c_iflag] [line 32, size 32, align 32, offset 0] [from tcflag_t]
!190 = metadata !{i32 786454, metadata !187, null, metadata !"tcflag_t", i32 27, i64 0, i64 0, i64 0, i32 0, metadata !46} ; [ DW_TAG_typedef ] [tcflag_t] [line 27, size 0, align 0, offset 0] [from unsigned int]
!191 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_oflag", i32 33, i64 32, i64 32, i64 32, i32 0, metadata !190} ; [ DW_TAG_member ] [c_oflag] [line 33, size 32, align 32, offset 32] [from tcflag_t]
!192 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_cflag", i32 34, i64 32, i64 32, i64 64, i32 0, metadata !190} ; [ DW_TAG_member ] [c_cflag] [line 34, size 32, align 32, offset 64] [from tcflag_t]
!193 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_lflag", i32 35, i64 32, i64 32, i64 96, i32 0, metadata !190} ; [ DW_TAG_member ] [c_lflag] [line 35, size 32, align 32, offset 96] [from tcflag_t]
!194 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_line", i32 36, i64 8, i64 8, i64 128, i32 0, metadata !195} ; [ DW_TAG_member ] [c_line] [line 36, size 8, align 8, offset 128] [from cc_t]
!195 = metadata !{i32 786454, metadata !187, null, metadata !"cc_t", i32 25, i64 0, i64 0, i64 0, i32 0, metadata !88} ; [ DW_TAG_typedef ] [cc_t] [line 25, size 0, align 0, offset 0] [from unsigned char]
!196 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_cc", i32 37, i64 256, i64 8, i64 136, i32 0, metadata !197} ; [ DW_TAG_member ] [c_cc] [line 37, size 256, align 8, offset 136] [from ]
!197 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 256, i64 8, i32 0, i32 0, metadata !195, metadata !198, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 256, align 8, offset 0] [from cc_t]
!198 = metadata !{metadata !199}
!199 = metadata !{i32 786465, i64 0, i64 32}      ; [ DW_TAG_subrange_type ] [0, 31]
!200 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_ispeed", i32 38, i64 32, i64 32, i64 416, i32 0, metadata !201} ; [ DW_TAG_member ] [c_ispeed] [line 38, size 32, align 32, offset 416] [from speed_t]
!201 = metadata !{i32 786454, metadata !187, null, metadata !"speed_t", i32 26, i64 0, i64 0, i64 0, i32 0, metadata !46} ; [ DW_TAG_typedef ] [speed_t] [line 26, size 0, align 0, offset 0] [from unsigned int]
!202 = metadata !{i32 786445, metadata !187, metadata !186, metadata !"c_ospeed", i32 39, i64 32, i64 32, i64 448, i32 0, metadata !201} ; [ DW_TAG_member ] [c_ospeed] [line 39, size 32, align 32, offset 448] [from speed_t]
!203 = metadata !{i32 786449, metadata !204, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !2, metadata !205, metadata !2, metadata !""
!204 = metadata !{metadata !"libc/misc/internals/errno.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!205 = metadata !{metadata !206, metadata !208}
!206 = metadata !{i32 786484, i32 0, null, metadata !"errno", metadata !"errno", metadata !"", metadata !207, i32 7, metadata !8, i32 0, i32 1, i32* @errno, null} ; [ DW_TAG_variable ] [errno] [line 7] [def]
!207 = metadata !{i32 786473, metadata !204}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/errno.c]
!208 = metadata !{i32 786484, i32 0, null, metadata !"h_errno", metadata !"h_errno", metadata !"", metadata !207, i32 8, metadata !8, i32 0, i32 1, i32* @h_errno, null} ; [ DW_TAG_variable ] [h_errno] [line 8] [def]
!209 = metadata !{i32 786449, metadata !210, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !211, metadata !2, metadata !2, metadata !""
!210 = metadata !{metadata !"libc/stdio/_WRITE.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!211 = metadata !{metadata !212}
!212 = metadata !{i32 786478, metadata !210, metadata !213, metadata !"__stdio_WRITE", metadata !"__stdio_WRITE", metadata !"", i32 33, metadata !214, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*, i8*, i
!213 = metadata !{i32 786473, metadata !210}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!214 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !215, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!215 = metadata !{metadata !216, metadata !217, metadata !239, metadata !216}
!216 = metadata !{i32 786454, metadata !210, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!217 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !218} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!218 = metadata !{i32 786454, metadata !210, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !219} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!219 = metadata !{i32 786451, metadata !82, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !220, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!220 = metadata !{metadata !221, metadata !222, metadata !223, metadata !224, metadata !225, metadata !226, metadata !227, metadata !228, metadata !229, metadata !230, metadata !232, metadata !233}
!221 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !85} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!222 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !87} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!223 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!224 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!225 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!226 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!227 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!228 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!229 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !93} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!230 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !231} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!231 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !219} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!232 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !102} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!233 = metadata !{i32 786445, metadata !82, metadata !219, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !234} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!234 = metadata !{i32 786454, metadata !82, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !235} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!235 = metadata !{i32 786451, metadata !107, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !236, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!236 = metadata !{metadata !237, metadata !238}
!237 = metadata !{i32 786445, metadata !107, metadata !235, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !103} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!238 = metadata !{i32 786445, metadata !107, metadata !235, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !103} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!239 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !240} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!240 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !88} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from unsigned char]
!241 = metadata !{i32 786449, metadata !242, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !243, metadata !2, metadata !2, metadata !""
!242 = metadata !{metadata !"libc/string/mempcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!243 = metadata !{metadata !244}
!244 = metadata !{i32 786478, metadata !242, metadata !245, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 20, metadata !246, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !2, i
!245 = metadata !{i32 786473, metadata !242}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!246 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !247, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!247 = metadata !{metadata !36, metadata !158, metadata !159, metadata !248}
!248 = metadata !{i32 786454, metadata !242, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!249 = metadata !{i32 786449, metadata !250, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !251, metadata !2, metadata !2, metadata !""} 
!250 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!251 = metadata !{metadata !252}
!252 = metadata !{i32 786478, metadata !250, metadata !253, metadata !"klee_div_zero_check", metadata !"klee_div_zero_check", metadata !"", i32 12, metadata !254, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64)* @klee_div_zero_check, 
!253 = metadata !{i32 786473, metadata !250}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!254 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !255, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!255 = metadata !{null, metadata !256}
!256 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!257 = metadata !{metadata !258}
!258 = metadata !{i32 786689, metadata !252, metadata !"z", metadata !253, i32 16777228, metadata !256, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [z] [line 12]
!259 = metadata !{i32 786449, metadata !260, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !261, metadata !2, metadata !2, metadata !""} 
!260 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_int.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!261 = metadata !{metadata !262}
!262 = metadata !{i32 786478, metadata !260, metadata !263, metadata !"klee_int", metadata !"klee_int", metadata !"", i32 13, metadata !264, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i8*)* @klee_int, null, null, metadata !266, i32 13}
!263 = metadata !{i32 786473, metadata !260}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_int.c]
!264 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !265, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!265 = metadata !{metadata !8, metadata !16}
!266 = metadata !{metadata !267, metadata !268}
!267 = metadata !{i32 786689, metadata !262, metadata !"name", metadata !263, i32 16777229, metadata !16, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!268 = metadata !{i32 786688, metadata !262, metadata !"x", metadata !263, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!269 = metadata !{i32 786449, metadata !270, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !271, metadata !2, metadata !2, metadata !""} 
!270 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!271 = metadata !{metadata !272}
!272 = metadata !{i32 786478, metadata !270, metadata !273, metadata !"klee_overshift_check", metadata !"klee_overshift_check", metadata !"", i32 20, metadata !274, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64, i64)* @klee_overshift
!273 = metadata !{i32 786473, metadata !270}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!274 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !275, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!275 = metadata !{null, metadata !45, metadata !45}
!276 = metadata !{metadata !277, metadata !278}
!277 = metadata !{i32 786689, metadata !272, metadata !"bitWidth", metadata !273, i32 16777236, metadata !45, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [bitWidth] [line 20]
!278 = metadata !{i32 786689, metadata !272, metadata !"shift", metadata !273, i32 33554452, metadata !45, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [shift] [line 20]
!279 = metadata !{i32 786449, metadata !280, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !281, metadata !2, metadata !2, metadata !""} 
!280 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_range.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!281 = metadata !{metadata !282}
!282 = metadata !{i32 786478, metadata !280, metadata !283, metadata !"klee_range", metadata !"klee_range", metadata !"", i32 13, metadata !284, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i32, i32, i8*)* @klee_range, null, null, metada
!283 = metadata !{i32 786473, metadata !280}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!284 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !285, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!285 = metadata !{metadata !8, metadata !8, metadata !8, metadata !16}
!286 = metadata !{metadata !287, metadata !288, metadata !289, metadata !290}
!287 = metadata !{i32 786689, metadata !282, metadata !"start", metadata !283, i32 16777229, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [start] [line 13]
!288 = metadata !{i32 786689, metadata !282, metadata !"end", metadata !283, i32 33554445, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [end] [line 13]
!289 = metadata !{i32 786689, metadata !282, metadata !"name", metadata !283, i32 50331661, metadata !16, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!290 = metadata !{i32 786688, metadata !282, metadata !"x", metadata !283, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!291 = metadata !{i32 786449, metadata !292, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !293, metadata !2, metadata !2, metadata !""} 
!292 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!293 = metadata !{metadata !294}
!294 = metadata !{i32 786478, metadata !292, metadata !295, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 12, metadata !296, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !299, i32
!295 = metadata !{i32 786473, metadata !292}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memcpy.c]
!296 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !297, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!297 = metadata !{metadata !36, metadata !36, metadata !160, metadata !298}
!298 = metadata !{i32 786454, metadata !292, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!299 = metadata !{metadata !300, metadata !301, metadata !302, metadata !303, metadata !304}
!300 = metadata !{i32 786689, metadata !294, metadata !"destaddr", metadata !295, i32 16777228, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 12]
!301 = metadata !{i32 786689, metadata !294, metadata !"srcaddr", metadata !295, i32 33554444, metadata !160, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 12]
!302 = metadata !{i32 786689, metadata !294, metadata !"len", metadata !295, i32 50331660, metadata !298, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 12]
!303 = metadata !{i32 786688, metadata !294, metadata !"dest", metadata !295, i32 13, metadata !34, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 13]
!304 = metadata !{i32 786688, metadata !294, metadata !"src", metadata !295, i32 14, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 14]
!305 = metadata !{i32 786449, metadata !306, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !307, metadata !2, metadata !2, metadata !""} 
!306 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memmove.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!307 = metadata !{metadata !308}
!308 = metadata !{i32 786478, metadata !306, metadata !309, metadata !"memmove", metadata !"memmove", metadata !"", i32 12, metadata !310, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memmove, null, null, metadata !313, 
!309 = metadata !{i32 786473, metadata !306}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!310 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !311, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!311 = metadata !{metadata !36, metadata !36, metadata !160, metadata !312}
!312 = metadata !{i32 786454, metadata !306, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!313 = metadata !{metadata !314, metadata !315, metadata !316, metadata !317, metadata !318}
!314 = metadata !{i32 786689, metadata !308, metadata !"dst", metadata !309, i32 16777228, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 12]
!315 = metadata !{i32 786689, metadata !308, metadata !"src", metadata !309, i32 33554444, metadata !160, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [src] [line 12]
!316 = metadata !{i32 786689, metadata !308, metadata !"count", metadata !309, i32 50331660, metadata !312, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 12]
!317 = metadata !{i32 786688, metadata !308, metadata !"a", metadata !309, i32 13, metadata !34, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 13]
!318 = metadata !{i32 786688, metadata !308, metadata !"b", metadata !309, i32 14, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [b] [line 14]
!319 = metadata !{i32 786449, metadata !320, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !321, metadata !2, metadata !2, metadata !""} 
!320 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/mempcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!321 = metadata !{metadata !322}
!322 = metadata !{i32 786478, metadata !320, metadata !323, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 11, metadata !324, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !327, 
!323 = metadata !{i32 786473, metadata !320}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/mempcpy.c]
!324 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !325, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!325 = metadata !{metadata !36, metadata !36, metadata !160, metadata !326}
!326 = metadata !{i32 786454, metadata !320, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!327 = metadata !{metadata !328, metadata !329, metadata !330, metadata !331, metadata !332}
!328 = metadata !{i32 786689, metadata !322, metadata !"destaddr", metadata !323, i32 16777227, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 11]
!329 = metadata !{i32 786689, metadata !322, metadata !"srcaddr", metadata !323, i32 33554443, metadata !160, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 11]
!330 = metadata !{i32 786689, metadata !322, metadata !"len", metadata !323, i32 50331659, metadata !326, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 11]
!331 = metadata !{i32 786688, metadata !322, metadata !"dest", metadata !323, i32 12, metadata !34, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 12]
!332 = metadata !{i32 786688, metadata !322, metadata !"src", metadata !323, i32 13, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 13]
!333 = metadata !{i32 786449, metadata !334, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !335, metadata !2, metadata !2, metadata !""} 
!334 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memset.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!335 = metadata !{metadata !336}
!336 = metadata !{i32 786478, metadata !334, metadata !337, metadata !"memset", metadata !"memset", metadata !"", i32 11, metadata !338, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i32, i64)* @memset, null, null, metadata !341, i32
!337 = metadata !{i32 786473, metadata !334}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memset.c]
!338 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !339, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!339 = metadata !{metadata !36, metadata !36, metadata !8, metadata !340}
!340 = metadata !{i32 786454, metadata !334, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !54} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!341 = metadata !{metadata !342, metadata !343, metadata !344, metadata !345}
!342 = metadata !{i32 786689, metadata !336, metadata !"dst", metadata !337, i32 16777227, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 11]
!343 = metadata !{i32 786689, metadata !336, metadata !"s", metadata !337, i32 33554443, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [s] [line 11]
!344 = metadata !{i32 786689, metadata !336, metadata !"count", metadata !337, i32 50331659, metadata !340, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 11]
!345 = metadata !{i32 786688, metadata !336, metadata !"a", metadata !337, i32 12, metadata !346, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 12]
!346 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !347} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!347 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !18} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from char]
!348 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!349 = metadata !{i32 1, metadata !"Debug Info Version", i32 1}
!350 = metadata !{metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)"}
!351 = metadata !{i32 8, i32 0, metadata !4, null} ; [ DW_TAG_imported_declaration ]
!352 = metadata !{i32 10, i32 0, metadata !4, null}
!353 = metadata !{i32 11, i32 0, metadata !4, null}
!354 = metadata !{i32 12, i32 0, metadata !4, null}
!355 = metadata !{i32 13, i32 0, metadata !4, null}
!356 = metadata !{i32 15, i32 0, metadata !4, null}
!357 = metadata !{i32 17, i32 0, metadata !358, null}
!358 = metadata !{i32 786443, metadata !1, metadata !359, i32 17, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!359 = metadata !{i32 786443, metadata !1, metadata !4, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!360 = metadata !{i32 18, i32 0, metadata !361, null}
!361 = metadata !{i32 786443, metadata !1, metadata !358, i32 18, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!362 = metadata !{i32 20, i32 0, metadata !363, null}
!363 = metadata !{i32 786443, metadata !1, metadata !361, i32 19, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!364 = metadata !{i32 21, i32 0, metadata !363, null}
!365 = metadata !{i32 24, i32 0, metadata !366, null}
!366 = metadata !{i32 786443, metadata !1, metadata !359, i32 24, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!367 = metadata !{i32 25, i32 0, metadata !368, null}
!368 = metadata !{i32 786443, metadata !1, metadata !366, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!369 = metadata !{i32 27, i32 0, metadata !370, null}
!370 = metadata !{i32 786443, metadata !1, metadata !368, i32 26, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/workspace/basic-tests/multi-stage/test.c]
!371 = metadata !{i32 32, i32 0, metadata !4, null}
!372 = metadata !{i32 34, i32 0, metadata !12, null}
!373 = metadata !{i32 37, i32 0, metadata !12, null}
!374 = metadata !{i32 191, i32 0, metadata !375, null}
!375 = metadata !{i32 786443, metadata !20, metadata !22, i32 191, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!376 = metadata !{i32 193, i32 0, metadata !22, null}
!377 = metadata !{i32 197, i32 0, metadata !22, null}
!378 = metadata !{i32 238, i32 0, metadata !379, null}
!379 = metadata !{i32 786443, metadata !20, metadata !22, i32 238, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!380 = metadata !{i32 239, i32 0, metadata !379, null}
!381 = metadata !{i32 240, i32 0, metadata !22, null}
!382 = metadata !{i32 263, i32 0, metadata !383, null}
!383 = metadata !{i32 786443, metadata !20, metadata !26, i32 263, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!384 = metadata !{i32 264, i32 0, metadata !383, null}
!385 = metadata !{i32 266, i32 0, metadata !386, null}
!386 = metadata !{i32 786443, metadata !20, metadata !26, i32 266, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!387 = metadata !{i32 267, i32 0, metadata !386, null}
!388 = metadata !{i32 268, i32 0, metadata !26, null}
!389 = metadata !{i32 288, i32 0, metadata !27, null}
!390 = metadata !{i32 291, i32 0, metadata !27, null}
!391 = metadata !{i32 294, i32 0, metadata !27, null}
!392 = metadata !{i32 298, i32 0, metadata !393, null}
!393 = metadata !{i32 786443, metadata !20, metadata !27, i32 298, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!394 = metadata !{i32 300, i32 0, metadata !395, null}
!395 = metadata !{i32 786443, metadata !20, metadata !393, i32 298, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!396 = metadata !{i32 301, i32 0, metadata !395, null}
!397 = metadata !{i32 305, i32 0, metadata !27, null}
!398 = metadata !{i32 306, i32 0, metadata !27, null}
!399 = metadata !{i32 307, i32 0, metadata !27, null}
!400 = metadata !{i32 308, i32 0, metadata !401, null}
!401 = metadata !{i32 786443, metadata !20, metadata !27, i32 307, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!402 = metadata !{i32 311, i32 0, metadata !27, null}
!403 = metadata !{i32 312, i32 0, metadata !404, null}
!404 = metadata !{i32 786443, metadata !20, metadata !27, i32 311, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!405 = metadata !{i32 313, i32 0, metadata !406, null}
!406 = metadata !{i32 786443, metadata !20, metadata !404, i32 313, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!407 = metadata !{i32 314, i32 0, metadata !408, null}
!408 = metadata !{i32 786443, metadata !20, metadata !406, i32 313, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!409 = metadata !{i32 315, i32 0, metadata !408, null}
!410 = metadata !{i32 316, i32 0, metadata !404, null}
!411 = metadata !{i32 317, i32 0, metadata !404, null}
!412 = metadata !{i32 323, i32 0, metadata !27, null}
!413 = metadata !{i32 327, i32 0, metadata !27, null}
!414 = metadata !{i32 331, i32 0, metadata !415, null}
!415 = metadata !{i32 786443, metadata !20, metadata !27, i32 331, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!416 = metadata !{i32 336, i32 0, metadata !417, null}
!417 = metadata !{i32 786443, metadata !20, metadata !415, i32 335, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!418 = metadata !{i32 337, i32 0, metadata !417, null}
!419 = metadata !{i32 338, i32 0, metadata !417, null}
!420 = metadata !{i32 339, i32 0, metadata !417, null}
!421 = metadata !{i32 342, i32 0, metadata !27, null}
!422 = metadata !{i32 354, i32 0, metadata !27, null}
!423 = metadata !{i32 370, i32 0, metadata !424, null}
!424 = metadata !{i32 786443, metadata !20, metadata !27, i32 370, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!425 = metadata !{i32 371, i32 0, metadata !426, null}
!426 = metadata !{i32 786443, metadata !20, metadata !424, i32 370, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!427 = metadata !{i32 372, i32 0, metadata !426, null}
!428 = metadata !{i32 391, i32 0, metadata !429, null}
!429 = metadata !{i32 786443, metadata !20, metadata !27, i32 391, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!430 = metadata !{i32 392, i32 0, metadata !429, null}
!431 = metadata !{i32 395, i32 0, metadata !432, null}
!432 = metadata !{i32 786443, metadata !20, metadata !27, i32 395, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!433 = metadata !{i32 396, i32 0, metadata !432, null}
!434 = metadata !{i32 401, i32 0, metadata !27, null}
!435 = metadata !{i32 160, i32 0, metadata !436, null}
!436 = metadata !{i32 786443, metadata !20, metadata !47} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!437 = metadata !{i32 161, i32 0, metadata !436, null}
!438 = metadata !{i32 162, i32 0, metadata !436, null}
!439 = metadata !{i32 163, i32 0, metadata !436, null}
!440 = metadata !{i32 165, i32 0, metadata !441, null}
!441 = metadata !{i32 786443, metadata !20, metadata !436, i32 165, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!442 = metadata !{i32 166, i32 0, metadata !443, null}
!443 = metadata !{i32 786443, metadata !20, metadata !441, i32 165, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!444 = metadata !{i32 168, i32 0, metadata !436, null}
!445 = metadata !{i32 169, i32 0, metadata !436, null}
!446 = metadata !{i32 139, i32 0, metadata !447, null}
!447 = metadata !{i32 786443, metadata !20, metadata !37, i32 139, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!448 = metadata !{i32 143, i32 0, metadata !449, null}
!449 = metadata !{i32 786443, metadata !20, metadata !447, i32 140, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!450 = metadata !{i32 147, i32 0, metadata !451, null}
!451 = metadata !{i32 786443, metadata !20, metadata !449, i32 147, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!452 = metadata !{i32 148, i32 18, metadata !451, null}
!453 = metadata !{i32 150, i32 0, metadata !454, null}
!454 = metadata !{i32 786443, metadata !20, metadata !451, i32 149, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!455 = metadata !{i32 153, i32 0, metadata !37, null}
!456 = metadata !{i32 56, i32 0, metadata !457, null}
!457 = metadata !{i32 786443, metadata !41, metadata !40} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!458 = metadata !{i32 13, i32 0, metadata !61, null}
!459 = metadata !{i32 12, i32 0, metadata !69, null}
!460 = metadata !{i32 258, i32 0, metadata !461, null}
!461 = metadata !{i32 786443, metadata !72, metadata !74, i32 258, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!462 = metadata !{i32 261, i32 0, metadata !463, null}
!463 = metadata !{i32 786443, metadata !72, metadata !464, i32 261, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!464 = metadata !{i32 786443, metadata !72, metadata !461, i32 258, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!465 = metadata !{i32 262, i32 0, metadata !466, null}
!466 = metadata !{i32 786443, metadata !72, metadata !463, i32 261, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!467 = metadata !{i32 263, i32 0, metadata !466, null}
!468 = metadata !{i32 274, i32 0, metadata !74, null}
!469 = metadata !{i32 280, i32 0, metadata !76, null}
!470 = metadata !{i32 282, i32 0, metadata !76, null}
!471 = metadata !{i32 283, i32 0, metadata !76, null}
!472 = metadata !{i32 284, i32 0, metadata !76, null}
!473 = metadata !{i32 291, i32 0, metadata !76, null}
!474 = metadata !{i32 23, i32 0, metadata !475, null}
!475 = metadata !{i32 786443, metadata !121, metadata !123, i32 23, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!476 = metadata !{i32 24, i32 0, metadata !477, null}
!477 = metadata !{i32 786443, metadata !121, metadata !475, i32 23, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!478 = metadata !{i32 25, i32 0, metadata !477, null}
!479 = metadata !{i32 26, i32 0, metadata !477, null}
!480 = metadata !{i32 28, i32 0, metadata !123, null}
!481 = metadata !{i32 20, i32 0, metadata !154, null}
!482 = metadata !{i32 21, i32 0, metadata !154, null}
!483 = metadata !{i32 28, i32 0, metadata !154, null}
!484 = metadata !{i32 29, i32 0, metadata !485, null}
!485 = metadata !{i32 786443, metadata !152, metadata !154, i32 28, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!486 = metadata !{i32 30, i32 0, metadata !485, null}
!487 = metadata !{i32 31, i32 0, metadata !485, null}
!488 = metadata !{i32 34, i32 0, metadata !154, null}
!489 = metadata !{i32 19, i32 0, metadata !166, null}
!490 = metadata !{i32 27, i32 0, metadata !166, null}
!491 = metadata !{i32 28, i32 0, metadata !492, null}
!492 = metadata !{i32 786443, metadata !164, metadata !166, i32 27, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!493 = metadata !{i32 29, i32 0, metadata !492, null}
!494 = metadata !{i32 30, i32 0, metadata !492, null}
!495 = metadata !{i32 32, i32 0, metadata !166, null}
!496 = metadata !{i32 30, i32 0, metadata !174, null}
!497 = metadata !{i32 43, i32 0, metadata !181, null}
!498 = metadata !{i32 45, i32 0, metadata !181, null}
!499 = metadata !{i32 46, i32 0, metadata !181, null}
!500 = metadata !{i32 47, i32 0, metadata !181, null}
!501 = metadata !{i32 48, i32 0, metadata !181, null}
!502 = metadata !{i32 49, i32 0, metadata !181, null}
!503 = metadata !{i32 61, i32 0, metadata !504, null}
!504 = metadata !{i32 786443, metadata !179, metadata !505, i32 60, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!505 = metadata !{i32 786443, metadata !179, metadata !181, i32 58, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!506 = metadata !{i32 79, i32 0, metadata !181, null}
!507 = metadata !{i32 44, i32 0, metadata !212, null}
!508 = metadata !{i32 46, i32 0, metadata !212, null}
!509 = metadata !{i32 47, i32 0, metadata !510, null}
!510 = metadata !{i32 786443, metadata !210, metadata !511, i32 47, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!511 = metadata !{i32 786443, metadata !210, metadata !212, i32 46, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!512 = metadata !{i32 49, i32 0, metadata !513, null}
!513 = metadata !{i32 786443, metadata !210, metadata !510, i32 47, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!514 = metadata !{i32 51, i32 0, metadata !511, null}
!515 = metadata !{i32 52, i32 0, metadata !516, null}
!516 = metadata !{i32 786443, metadata !210, metadata !511, i32 52, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!517 = metadata !{i32 62, i32 0, metadata !518, null}
!518 = metadata !{i32 786443, metadata !210, metadata !516, i32 52, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!519 = metadata !{i32 63, i32 0, metadata !518, null}
!520 = metadata !{i32 101, i32 0, metadata !511, null}
!521 = metadata !{i32 70, i32 0, metadata !522, null}
!522 = metadata !{i32 786443, metadata !210, metadata !516, i32 69, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!523 = metadata !{i32 73, i32 0, metadata !524, null}
!524 = metadata !{i32 786443, metadata !210, metadata !522, i32 73, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!525 = metadata !{i32 76, i32 0, metadata !526, null}
!526 = metadata !{i32 786443, metadata !210, metadata !527, i32 76, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!527 = metadata !{i32 786443, metadata !210, metadata !524, i32 73, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!528 = metadata !{i32 77, i32 0, metadata !529, null}
!529 = metadata !{i32 786443, metadata !210, metadata !526, i32 76, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!530 = metadata !{i32 78, i32 0, metadata !529, null}
!531 = metadata !{i32 80, i32 0, metadata !527, null}
!532 = metadata !{i32 82, i32 0, metadata !527, null}
!533 = metadata !{i32 83, i32 0, metadata !534, null}
!534 = metadata !{i32 786443, metadata !210, metadata !535, i32 83, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!535 = metadata !{i32 786443, metadata !210, metadata !527, i32 82, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!536 = metadata !{i32 88, i32 0, metadata !535, null}
!537 = metadata !{i32 89, i32 0, metadata !535, null}
!538 = metadata !{i32 90, i32 0, metadata !535, null}
!539 = metadata !{i32 92, i32 0, metadata !527, null}
!540 = metadata !{i32 94, i32 0, metadata !527, null}
!541 = metadata !{i32 95, i32 0, metadata !527, null}
!542 = metadata !{i32 99, i32 0, metadata !522, null}
!543 = metadata !{i32 102, i32 0, metadata !212, null}
!544 = metadata !{i32 22, i32 0, metadata !244, null}
!545 = metadata !{i32 23, i32 0, metadata !244, null}
!546 = metadata !{i32 30, i32 0, metadata !244, null}
!547 = metadata !{i32 31, i32 0, metadata !548, null}
!548 = metadata !{i32 786443, metadata !242, metadata !244, i32 30, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!549 = metadata !{i32 32, i32 0, metadata !548, null}
!550 = metadata !{i32 33, i32 0, metadata !548, null}
!551 = metadata !{i32 36, i32 0, metadata !244, null}
!552 = metadata !{i32 13, i32 0, metadata !553, null}
!553 = metadata !{i32 786443, metadata !250, metadata !252, i32 13, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!554 = metadata !{i32 14, i32 0, metadata !553, null}
!555 = metadata !{i32 15, i32 0, metadata !252, null}
!556 = metadata !{i32 15, i32 0, metadata !262, null}
!557 = metadata !{i32 16, i32 0, metadata !262, null}
!558 = metadata !{metadata !559, metadata !559, i64 0}
!559 = metadata !{metadata !"int", metadata !560, i64 0}
!560 = metadata !{metadata !"omnipotent char", metadata !561, i64 0}
!561 = metadata !{metadata !"Simple C/C++ TBAA"}
!562 = metadata !{i32 21, i32 0, metadata !563, null}
!563 = metadata !{i32 786443, metadata !270, metadata !272, i32 21, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!564 = metadata !{i32 27, i32 0, metadata !565, null}
!565 = metadata !{i32 786443, metadata !270, metadata !563, i32 21, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!566 = metadata !{i32 29, i32 0, metadata !272, null}
!567 = metadata !{i32 16, i32 0, metadata !568, null}
!568 = metadata !{i32 786443, metadata !280, metadata !282, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!569 = metadata !{i32 17, i32 0, metadata !568, null}
!570 = metadata !{i32 19, i32 0, metadata !571, null}
!571 = metadata !{i32 786443, metadata !280, metadata !282, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!572 = metadata !{i32 22, i32 0, metadata !573, null}
!573 = metadata !{i32 786443, metadata !280, metadata !571, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!574 = metadata !{i32 25, i32 0, metadata !575, null}
!575 = metadata !{i32 786443, metadata !280, metadata !573, i32 25, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!576 = metadata !{i32 26, i32 0, metadata !577, null}
!577 = metadata !{i32 786443, metadata !280, metadata !575, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!578 = metadata !{i32 27, i32 0, metadata !577, null}
!579 = metadata !{i32 28, i32 0, metadata !580, null}
!580 = metadata !{i32 786443, metadata !280, metadata !575, i32 27, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!581 = metadata !{i32 29, i32 0, metadata !580, null}
!582 = metadata !{i32 32, i32 0, metadata !573, null}
!583 = metadata !{i32 34, i32 0, metadata !282, null}
!584 = metadata !{i32 16, i32 0, metadata !585, null}
!585 = metadata !{i32 786443, metadata !306, metadata !308, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!586 = metadata !{i32 19, i32 0, metadata !587, null}
!587 = metadata !{i32 786443, metadata !306, metadata !308, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!588 = metadata !{i32 20, i32 0, metadata !589, null}
!589 = metadata !{i32 786443, metadata !306, metadata !587, i32 19, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!590 = metadata !{metadata !590, metadata !591, metadata !592}
!591 = metadata !{metadata !"llvm.vectorizer.width", i32 1}
!592 = metadata !{metadata !"llvm.vectorizer.unroll", i32 1}
!593 = metadata !{metadata !560, metadata !560, i64 0}
!594 = metadata !{metadata !594, metadata !591, metadata !592}
!595 = metadata !{i32 22, i32 0, metadata !596, null}
!596 = metadata !{i32 786443, metadata !306, metadata !587, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!597 = metadata !{i32 24, i32 0, metadata !596, null}
!598 = metadata !{i32 23, i32 0, metadata !596, null}
!599 = metadata !{metadata !599, metadata !591, metadata !592}
!600 = metadata !{metadata !600, metadata !591, metadata !592}
!601 = metadata !{i32 28, i32 0, metadata !308, null}
