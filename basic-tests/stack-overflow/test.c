#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char a[0x10];	
	char b[0x20];

  	klee_make_symbolic(a, sizeof(a), "a");
  	klee_make_symbolic(b, sizeof(b), "b");
	
	b[0x20 - 0x1] = 0;
	strcpy(a, b);
	
	return 0;
}

