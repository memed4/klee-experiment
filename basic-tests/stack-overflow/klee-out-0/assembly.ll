; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.__STDIO_FILE_STRUCT.273 = type { i16, [2 x i8], i32, i8*, i8*, i8*, i8*, i8*, i8*, %struct.__STDIO_FILE_STRUCT.273*, [2 x i32], %struct.__mbstate_t.272 }
%struct.__mbstate_t.272 = type { i32, i32 }
%struct.Elf64_auxv_t = type { i64, %union.anon.645 }
%union.anon.645 = type { i64 }
%struct.stat.644 = type { i64, i64, i64, i32, i32, i32, i32, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, [3 x i64] }
%struct.termios.442 = type { i32, i32, i32, i32, i8, [32 x i8], i32, i32 }
%struct.__kernel_termios = type { i32, i32, i32, i32, i8, [19 x i8] }

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str1 = private unnamed_addr constant [2 x i8] c"b\00", align 1
@__libc_stack_end = global i8* null, align 8
@.str3 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@__uclibc_progname = hidden global i8* getelementptr inbounds ([1 x i8]* @.str3, i32 0, i32 0), align 8
@__environ = global i8** null, align 8
@__pagesize = global i64 0, align 8
@__uClibc_init.been_there_done_that = internal global i32 0, align 4
@__app_fini = hidden global void ()* null, align 8
@__rtld_fini = hidden global void ()* null, align 8
@.str14 = private unnamed_addr constant [10 x i8] c"/dev/null\00", align 1
@_stdio_streams = internal global [3 x %struct.__STDIO_FILE_STRUCT.273] [%struct.__STDIO_FILE_STRUCT.273 { i16 544, [2 x i8] zeroinitializer, i32 0, i8* null, i8* null, i8* null, i8* null, i8* null, i8* null, %struct.__STDIO_FILE_STRUCT.273* bitcast (i8*
@stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@stderr = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 160) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@__stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@__stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@_stdio_openlist = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@errno = global i32 0, align 4
@h_errno = global i32 0, align 4
@.str31 = private unnamed_addr constant [60 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c\00", align 1
@.str132 = private unnamed_addr constant [15 x i8] c"divide by zero\00", align 1
@.str2 = private unnamed_addr constant [8 x i8] c"div.err\00", align 1
@.str333 = private unnamed_addr constant [8 x i8] c"IGNORED\00", align 1
@.str1434 = private unnamed_addr constant [16 x i8] c"overshift error\00", align 1
@.str25 = private unnamed_addr constant [14 x i8] c"overshift.err\00", align 1
@.str6 = private unnamed_addr constant [51 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_range.c\00", align 1
@.str17 = private unnamed_addr constant [14 x i8] c"invalid range\00", align 1
@.str28 = private unnamed_addr constant [5 x i8] c"user\00", align 1

@environ = alias weak i8*** @__environ

; Function Attrs: nounwind uwtable
define i32 @__user_main() #0 {
  %1 = alloca i32, align 4
  %a = alloca [16 x i8], align 16
  %b = alloca [32 x i8], align 16
  store i32 0, i32* %1
  %2 = getelementptr inbounds [16 x i8]* %a, i32 0, i32 0, !dbg !353
  %3 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %2, i64 16, i8* getelementptr inbounds ([2 x i8]* @.str, i32 0, i32 0)), !dbg !353
  %4 = getelementptr inbounds [32 x i8]* %b, i32 0, i32 0, !dbg !354
  %5 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %4, i64 32, i8* getelementptr inbounds ([2 x i8]* @.str1, i32 0, i32 0)), !dbg !354
  %6 = getelementptr inbounds [32 x i8]* %b, i32 0, i64 31, !dbg !355
  store i8 0, i8* %6, align 1, !dbg !355
  %7 = getelementptr inbounds [16 x i8]* %a, i32 0, i32 0, !dbg !356
  %8 = getelementptr inbounds [32 x i8]* %b, i32 0, i32 0, !dbg !356
  %9 = call i8* @strcpy(i8* %7, i8* %8) #11, !dbg !356
  ret i32 0, !dbg !357
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

declare i32 @klee_make_symbolic(...) #2

; Function Attrs: nounwind uwtable
define i8* @strcpy(i8* noalias %s1, i8* noalias %s2) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %s = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  %3 = load i8** %1, align 8, !dbg !358
  store i8* %3, i8** %s, align 8, !dbg !358
  br label %4, !dbg !359

; <label>:4                                       ; preds = %4, %0
  %5 = load i8** %2, align 8, !dbg !359
  %6 = getelementptr inbounds i8* %5, i32 1, !dbg !359
  store i8* %6, i8** %2, align 8, !dbg !359
  %7 = load i8* %5, align 1, !dbg !359
  %8 = load i8** %s, align 8, !dbg !359
  %9 = getelementptr inbounds i8* %8, i32 1, !dbg !359
  store i8* %9, i8** %s, align 8, !dbg !359
  store i8 %7, i8* %8, align 1, !dbg !359
  %10 = sext i8 %7 to i32, !dbg !359
  %11 = icmp ne i32 %10, 0, !dbg !359
  br i1 %11, label %4, label %12, !dbg !359

; <label>:12                                      ; preds = %4
  %13 = load i8** %1, align 8, !dbg !360
  ret i8* %13, !dbg !360
}

; Function Attrs: nounwind uwtable
define void @__uClibc_init() #0 {
  %1 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !361
  %2 = icmp ne i32 %1, 0, !dbg !361
  br i1 %2, label %8, label %3, !dbg !361

; <label>:3                                       ; preds = %0
  %4 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !363
  %5 = add nsw i32 %4, 1, !dbg !363
  store i32 %5, i32* @__uClibc_init.been_there_done_that, align 4, !dbg !363
  store i64 4096, i64* @__pagesize, align 8, !dbg !364
  %6 = icmp ne i64 1, 0, !dbg !365
  br i1 %6, label %7, label %8, !dbg !365

; <label>:7                                       ; preds = %3
  call void @_stdio_init() #12, !dbg !367
  br label %8, !dbg !367

; <label>:8                                       ; preds = %0, %7, %3
  ret void, !dbg !368
}

; Function Attrs: nounwind readnone
declare i64 @llvm.expect.i64(i64, i64) #1

; Function Attrs: nounwind uwtable
define void @__uClibc_fini() #0 {
  %1 = load void ()** @__app_fini, align 8, !dbg !369
  %2 = icmp ne void ()* %1, null, !dbg !369
  br i1 %2, label %3, label %5, !dbg !369

; <label>:3                                       ; preds = %0
  %4 = load void ()** @__app_fini, align 8, !dbg !371
  call void %4() #12, !dbg !371
  br label %5, !dbg !371

; <label>:5                                       ; preds = %3, %0
  %6 = load void ()** @__rtld_fini, align 8, !dbg !372
  %7 = icmp ne void ()* %6, null, !dbg !372
  br i1 %7, label %8, label %10, !dbg !372

; <label>:8                                       ; preds = %5
  %9 = load void ()** @__rtld_fini, align 8, !dbg !374
  call void %9() #12, !dbg !374
  br label %10, !dbg !374

; <label>:10                                      ; preds = %8, %5
  ret void, !dbg !375
}

; Function Attrs: noreturn nounwind uwtable
define void @__uClibc_main(i32 (i32, i8**, i8**)* %main, i32 %argc, i8** %argv, void ()* %app_init, void ()* %app_fini, void ()* %rtld_fini, i8* %stack_end) #3 {
  %1 = alloca i32 (i32, i8**, i8**)*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca void ()*, align 8
  %5 = alloca void ()*, align 8
  %6 = alloca void ()*, align 8
  %7 = alloca i8*, align 8
  %aux_dat = alloca i64*, align 8
  %auxvt = alloca [15 x %struct.Elf64_auxv_t], align 16
  %auxv_entry = alloca %struct.Elf64_auxv_t*, align 8
  store i32 (i32, i8**, i8**)* %main, i32 (i32, i8**, i8**)** %1, align 8
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  store void ()* %app_init, void ()** %4, align 8
  store void ()* %app_fini, void ()** %5, align 8
  store void ()* %rtld_fini, void ()** %6, align 8
  store i8* %stack_end, i8** %7, align 8
  %8 = load i8** %7, align 8, !dbg !376
  store i8* %8, i8** @__libc_stack_end, align 8, !dbg !376
  %9 = load void ()** %6, align 8, !dbg !377
  store void ()* %9, void ()** @__rtld_fini, align 8, !dbg !377
  %10 = load i32* %2, align 4, !dbg !378
  %11 = add nsw i32 %10, 1, !dbg !378
  %12 = sext i32 %11 to i64, !dbg !378
  %13 = load i8*** %3, align 8, !dbg !378
  %14 = getelementptr inbounds i8** %13, i64 %12, !dbg !378
  store i8** %14, i8*** @__environ, align 8, !dbg !378
  %15 = load i8*** @__environ, align 8, !dbg !379
  %16 = bitcast i8** %15 to i8*, !dbg !379
  %17 = load i8*** %3, align 8, !dbg !379
  %18 = load i8** %17, align 8, !dbg !379
  %19 = icmp eq i8* %16, %18, !dbg !379
  br i1 %19, label %20, label %25, !dbg !379

; <label>:20                                      ; preds = %0
  %21 = load i32* %2, align 4, !dbg !381
  %22 = sext i32 %21 to i64, !dbg !381
  %23 = load i8*** %3, align 8, !dbg !381
  %24 = getelementptr inbounds i8** %23, i64 %22, !dbg !381
  store i8** %24, i8*** @__environ, align 8, !dbg !381
  br label %25, !dbg !383

; <label>:25                                      ; preds = %20, %0
  %26 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i32 0, !dbg !384
  %27 = bitcast %struct.Elf64_auxv_t* %26 to i8*, !dbg !384
  %28 = call i8* @memset(i8* %27, i32 0, i64 240) #13, !dbg !384
  %29 = load i8*** @__environ, align 8, !dbg !385
  %30 = bitcast i8** %29 to i64*, !dbg !385
  store i64* %30, i64** %aux_dat, align 8, !dbg !385
  br label %31, !dbg !386

; <label>:31                                      ; preds = %31, %25
  %32 = load i64** %aux_dat, align 8, !dbg !386
  %33 = load i64* %32, align 8, !dbg !386
  %34 = icmp ne i64 %33, 0, !dbg !386
  %35 = load i64** %aux_dat, align 8, !dbg !387
  %36 = getelementptr inbounds i64* %35, i32 1, !dbg !387
  store i64* %36, i64** %aux_dat, align 8, !dbg !387
  br i1 %34, label %31, label %37, !dbg !386

; <label>:37                                      ; preds = %31, %57
  %38 = load i64** %aux_dat, align 8, !dbg !389
  %39 = load i64* %38, align 8, !dbg !389
  %40 = icmp ne i64 %39, 0, !dbg !389
  br i1 %40, label %41, label %60, !dbg !389

; <label>:41                                      ; preds = %37
  %42 = load i64** %aux_dat, align 8, !dbg !390
  %43 = bitcast i64* %42 to %struct.Elf64_auxv_t*, !dbg !390
  store %struct.Elf64_auxv_t* %43, %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !390
  %44 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !392
  %45 = getelementptr inbounds %struct.Elf64_auxv_t* %44, i32 0, i32 0, !dbg !392
  %46 = load i64* %45, align 8, !dbg !392
  %47 = icmp ule i64 %46, 14, !dbg !392
  br i1 %47, label %48, label %57, !dbg !392

; <label>:48                                      ; preds = %41
  %49 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !394
  %50 = getelementptr inbounds %struct.Elf64_auxv_t* %49, i32 0, i32 0, !dbg !394
  %51 = load i64* %50, align 8, !dbg !394
  %52 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 %51, !dbg !394
  %53 = bitcast %struct.Elf64_auxv_t* %52 to i8*, !dbg !394
  %54 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !394
  %55 = bitcast %struct.Elf64_auxv_t* %54 to i8*, !dbg !394
  %56 = call i8* @memcpy(i8* %53, i8* %55, i64 16) #13, !dbg !394
  br label %57, !dbg !396

; <label>:57                                      ; preds = %48, %41
  %58 = load i64** %aux_dat, align 8, !dbg !397
  %59 = getelementptr inbounds i64* %58, i64 2, !dbg !397
  store i64* %59, i64** %aux_dat, align 8, !dbg !397
  br label %37, !dbg !398

; <label>:60                                      ; preds = %37
  call void @__uClibc_init() #12, !dbg !399
  %61 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !400
  %62 = getelementptr inbounds %struct.Elf64_auxv_t* %61, i32 0, i32 1, !dbg !400
  %63 = bitcast %union.anon.645* %62 to i64*, !dbg !400
  %64 = load i64* %63, align 8, !dbg !400
  %65 = icmp ne i64 %64, 0, !dbg !400
  br i1 %65, label %66, label %71, !dbg !400

; <label>:66                                      ; preds = %60
  %67 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !400
  %68 = getelementptr inbounds %struct.Elf64_auxv_t* %67, i32 0, i32 1, !dbg !400
  %69 = bitcast %union.anon.645* %68 to i64*, !dbg !400
  %70 = load i64* %69, align 8, !dbg !400
  br label %71, !dbg !400

; <label>:71                                      ; preds = %60, %66
  %72 = phi i64 [ %70, %66 ], [ 4096, %60 ], !dbg !400
  store i64 %72, i64* @__pagesize, align 8, !dbg !400
  %73 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !401
  %74 = getelementptr inbounds %struct.Elf64_auxv_t* %73, i32 0, i32 1, !dbg !401
  %75 = bitcast %union.anon.645* %74 to i64*, !dbg !401
  %76 = load i64* %75, align 8, !dbg !401
  %77 = icmp eq i64 %76, -1, !dbg !401
  br i1 %77, label %78, label %81, !dbg !401

; <label>:78                                      ; preds = %71
  %79 = call i32 @__check_suid() #12, !dbg !401
  %80 = icmp ne i32 %79, 0, !dbg !401
  br i1 %80, label %107, label %81, !dbg !401

; <label>:81                                      ; preds = %78, %71
  %82 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !401
  %83 = getelementptr inbounds %struct.Elf64_auxv_t* %82, i32 0, i32 1, !dbg !401
  %84 = bitcast %union.anon.645* %83 to i64*, !dbg !401
  %85 = load i64* %84, align 8, !dbg !401
  %86 = icmp ne i64 %85, -1, !dbg !401
  br i1 %86, label %87, label %108, !dbg !401

; <label>:87                                      ; preds = %81
  %88 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !401
  %89 = getelementptr inbounds %struct.Elf64_auxv_t* %88, i32 0, i32 1, !dbg !401
  %90 = bitcast %union.anon.645* %89 to i64*, !dbg !401
  %91 = load i64* %90, align 8, !dbg !401
  %92 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 12, !dbg !401
  %93 = getelementptr inbounds %struct.Elf64_auxv_t* %92, i32 0, i32 1, !dbg !401
  %94 = bitcast %union.anon.645* %93 to i64*, !dbg !401
  %95 = load i64* %94, align 8, !dbg !401
  %96 = icmp ne i64 %91, %95, !dbg !401
  br i1 %96, label %107, label %97, !dbg !401

; <label>:97                                      ; preds = %87
  %98 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 13, !dbg !401
  %99 = getelementptr inbounds %struct.Elf64_auxv_t* %98, i32 0, i32 1, !dbg !401
  %100 = bitcast %union.anon.645* %99 to i64*, !dbg !401
  %101 = load i64* %100, align 8, !dbg !401
  %102 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 14, !dbg !401
  %103 = getelementptr inbounds %struct.Elf64_auxv_t* %102, i32 0, i32 1, !dbg !401
  %104 = bitcast %union.anon.645* %103 to i64*, !dbg !401
  %105 = load i64* %104, align 8, !dbg !401
  %106 = icmp ne i64 %101, %105, !dbg !401
  br i1 %106, label %107, label %108, !dbg !401

; <label>:107                                     ; preds = %97, %87, %78
  call void @__check_one_fd(i32 0, i32 131072) #12, !dbg !403
  call void @__check_one_fd(i32 1, i32 131074) #12, !dbg !405
  call void @__check_one_fd(i32 2, i32 131074) #12, !dbg !406
  br label %108, !dbg !407

; <label>:108                                     ; preds = %107, %97, %81
  %109 = load i8*** %3, align 8, !dbg !408
  %110 = load i8** %109, align 8, !dbg !408
  store i8* %110, i8** @__uclibc_progname, align 8, !dbg !408
  %111 = load void ()** %5, align 8, !dbg !409
  store void ()* %111, void ()** @__app_fini, align 8, !dbg !409
  %112 = load void ()** %4, align 8, !dbg !410
  %113 = icmp ne void ()* %112, null, !dbg !410
  br i1 %113, label %114, label %116, !dbg !410

; <label>:114                                     ; preds = %108
  %115 = load void ()** %4, align 8, !dbg !412
  call void %115() #12, !dbg !412
  br label %116, !dbg !414

; <label>:116                                     ; preds = %114, %108
  %117 = icmp ne i64 1, 0, !dbg !415
  br i1 %117, label %118, label %120, !dbg !415

; <label>:118                                     ; preds = %116
  %119 = call i32* @__errno_location() #14, !dbg !417
  store i32 0, i32* %119, align 4, !dbg !417
  br label %120, !dbg !417

; <label>:120                                     ; preds = %118, %116
  %121 = icmp ne i64 1, 0, !dbg !418
  br i1 %121, label %122, label %124, !dbg !418

; <label>:122                                     ; preds = %120
  %123 = call i32* @__h_errno_location() #14, !dbg !420
  store i32 0, i32* %123, align 4, !dbg !420
  br label %124, !dbg !420

; <label>:124                                     ; preds = %122, %120
  %125 = load i32 (i32, i8**, i8**)** %1, align 8, !dbg !421
  %126 = load i32* %2, align 4, !dbg !421
  %127 = load i8*** %3, align 8, !dbg !421
  %128 = load i8*** @__environ, align 8, !dbg !421
  %129 = call i32 %125(i32 %126, i8** %127, i8** %128) #12, !dbg !421
  call void @exit(i32 %129) #15, !dbg !421
  unreachable, !dbg !421
}

; Function Attrs: noreturn nounwind
declare void @exit(i32) #4

declare i32 @fcntl(i32, i32, ...) #2

declare i32 @open(i8*, i32, ...) #2

; Function Attrs: nounwind
declare i32 @fstat(i32, %struct.stat.644*) #5

; Function Attrs: noreturn nounwind
declare void @abort() #4

; Function Attrs: nounwind
declare i32 @getuid() #5

; Function Attrs: nounwind
declare i32 @geteuid() #5

; Function Attrs: nounwind
declare i32 @getgid() #5

; Function Attrs: nounwind
declare i32 @getegid() #5

; Function Attrs: nounwind uwtable
define internal i32 @__check_suid() #0 {
  %1 = alloca i32, align 4
  %uid = alloca i32, align 4
  %euid = alloca i32, align 4
  %gid = alloca i32, align 4
  %egid = alloca i32, align 4
  %2 = call i32 @getuid() #13, !dbg !422
  store i32 %2, i32* %uid, align 4, !dbg !422
  %3 = call i32 @geteuid() #13, !dbg !424
  store i32 %3, i32* %euid, align 4, !dbg !424
  %4 = call i32 @getgid() #13, !dbg !425
  store i32 %4, i32* %gid, align 4, !dbg !425
  %5 = call i32 @getegid() #13, !dbg !426
  store i32 %5, i32* %egid, align 4, !dbg !426
  %6 = load i32* %uid, align 4, !dbg !427
  %7 = load i32* %euid, align 4, !dbg !427
  %8 = icmp eq i32 %6, %7, !dbg !427
  br i1 %8, label %9, label %14, !dbg !427

; <label>:9                                       ; preds = %0
  %10 = load i32* %gid, align 4, !dbg !427
  %11 = load i32* %egid, align 4, !dbg !427
  %12 = icmp eq i32 %10, %11, !dbg !427
  br i1 %12, label %13, label %14, !dbg !427

; <label>:13                                      ; preds = %9
  store i32 0, i32* %1, !dbg !429
  br label %15, !dbg !429

; <label>:14                                      ; preds = %9, %0
  store i32 1, i32* %1, !dbg !431
  br label %15, !dbg !431

; <label>:15                                      ; preds = %14, %13
  %16 = load i32* %1, !dbg !432
  ret i32 %16, !dbg !432
}

; Function Attrs: nounwind uwtable
define internal void @__check_one_fd(i32 %fd, i32 %mode) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %st = alloca %struct.stat.644, align 8
  %nullfd = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store i32 %mode, i32* %2, align 4
  %3 = load i32* %1, align 4, !dbg !433
  %4 = call i32 (i32, i32, ...)* @fcntl(i32 %3, i32 1) #12, !dbg !433
  %5 = icmp eq i32 %4, -1, !dbg !433
  br i1 %5, label %6, label %10, !dbg !433

; <label>:6                                       ; preds = %0
  %7 = call i32* @__errno_location() #14, !dbg !433
  %8 = load i32* %7, align 4, !dbg !433
  %9 = icmp eq i32 %8, 9, !dbg !433
  br label %10

; <label>:10                                      ; preds = %6, %0
  %11 = phi i1 [ false, %0 ], [ %9, %6 ]
  %12 = xor i1 %11, true
  %13 = xor i1 %12, true
  %14 = zext i1 %13 to i32
  %15 = sext i32 %14 to i64
  %16 = icmp ne i64 %15, 0
  br i1 %16, label %17, label %38

; <label>:17                                      ; preds = %10
  %18 = load i32* %2, align 4, !dbg !435
  %19 = call i32 (i8*, i32, ...)* @open(i8* getelementptr inbounds ([10 x i8]* @.str14, i32 0, i32 0), i32 %18) #12, !dbg !435
  store i32 %19, i32* %nullfd, align 4, !dbg !435
  %20 = load i32* %nullfd, align 4, !dbg !437
  %21 = load i32* %1, align 4, !dbg !437
  %22 = icmp ne i32 %20, %21, !dbg !437
  br i1 %22, label %37, label %23, !dbg !437

; <label>:23                                      ; preds = %17
  %24 = load i32* %1, align 4, !dbg !437
  %25 = call i32 @fstat(i32 %24, %struct.stat.644* %st) #13, !dbg !437
  %26 = icmp ne i32 %25, 0, !dbg !437
  br i1 %26, label %37, label %27, !dbg !437

; <label>:27                                      ; preds = %23
  %28 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 3, !dbg !437
  %29 = load i32* %28, align 4, !dbg !437
  %30 = and i32 %29, 61440, !dbg !437
  %31 = icmp eq i32 %30, 8192, !dbg !437
  br i1 %31, label %32, label %37, !dbg !437

; <label>:32                                      ; preds = %27
  %33 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 7, !dbg !437
  %34 = load i64* %33, align 8, !dbg !437
  %35 = call i64 @gnu_dev_makedev(i32 1, i32 3) #13, !dbg !439
  %36 = icmp ne i64 %34, %35, !dbg !439
  br i1 %36, label %37, label %38, !dbg !439

; <label>:37                                      ; preds = %32, %27, %23, %17
  call void @abort() #15, !dbg !440
  unreachable, !dbg !440

; <label>:38                                      ; preds = %32, %10
  ret void, !dbg !442
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @gnu_dev_makedev(i32 %__major, i32 %__minor) #6 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 %__major, i32* %1, align 4
  store i32 %__minor, i32* %2, align 4
  %3 = load i32* %2, align 4, !dbg !443
  %4 = and i32 %3, 255, !dbg !443
  %5 = load i32* %1, align 4, !dbg !443
  %6 = and i32 %5, 4095, !dbg !443
  %int_cast_to_i64 = zext i32 8 to i64
  call void @klee_overshift_check(i64 32, i64 %int_cast_to_i64), !dbg !443
  %7 = shl i32 %6, 8, !dbg !443
  %8 = or i32 %4, %7, !dbg !443
  %9 = zext i32 %8 to i64, !dbg !443
  %10 = load i32* %2, align 4, !dbg !443
  %11 = and i32 %10, -256, !dbg !443
  %12 = zext i32 %11 to i64, !dbg !443
  %int_cast_to_i641 = bitcast i64 12 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i641), !dbg !443
  %13 = shl i64 %12, 12, !dbg !443
  %14 = or i64 %9, %13, !dbg !443
  %15 = load i32* %1, align 4, !dbg !443
  %16 = and i32 %15, -4096, !dbg !443
  %17 = zext i32 %16 to i64, !dbg !443
  %int_cast_to_i642 = bitcast i64 32 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i642), !dbg !443
  %18 = shl i64 %17, 32, !dbg !443
  %19 = or i64 %14, %18, !dbg !443
  ret i64 %19, !dbg !443
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__errno_location() #7 {
  ret i32* @errno, !dbg !445
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__h_errno_location() #7 {
  ret i32* @h_errno, !dbg !446
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_term() #0 {
  %ptr = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %1 = load %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, align 8, !dbg !447
  store %struct.__STDIO_FILE_STRUCT.273* %1, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !447
  br label %2, !dbg !447

; <label>:2                                       ; preds = %15, %0
  %3 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !447
  %4 = icmp ne %struct.__STDIO_FILE_STRUCT.273* %3, null, !dbg !447
  br i1 %4, label %5, label %19, !dbg !447

; <label>:5                                       ; preds = %2
  %6 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !449
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %6, i32 0, i32 0, !dbg !449
  %8 = load i16* %7, align 2, !dbg !449
  %9 = zext i16 %8 to i32, !dbg !449
  %10 = and i32 %9, 64, !dbg !449
  %11 = icmp ne i32 %10, 0, !dbg !449
  br i1 %11, label %12, label %15, !dbg !449

; <label>:12                                      ; preds = %5
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !452
  %14 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* %13) #12, !dbg !452
  br label %15, !dbg !454

; <label>:15                                      ; preds = %5, %12
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !447
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 9, !dbg !447
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %17, align 8, !dbg !447
  store %struct.__STDIO_FILE_STRUCT.273* %18, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !447
  br label %2, !dbg !447

; <label>:19                                      ; preds = %2
  ret void, !dbg !455
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_init() #0 {
  %old_errno = alloca i32, align 4
  %1 = load i32* @errno, align 4, !dbg !456
  store i32 %1, i32* %old_errno, align 4, !dbg !456
  %2 = call i32 @isatty(i32 0) #13, !dbg !457
  %3 = sub nsw i32 1, %2, !dbg !457
  %4 = mul i32 %3, 256, !dbg !457
  %5 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !457
  %6 = zext i16 %5 to i32, !dbg !457
  %7 = xor i32 %6, %4, !dbg !457
  %8 = trunc i32 %7 to i16, !dbg !457
  store i16 %8, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !457
  %9 = call i32 @isatty(i32 1) #13, !dbg !458
  %10 = sub nsw i32 1, %9, !dbg !458
  %11 = mul i32 %10, 256, !dbg !458
  %12 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !458
  %13 = zext i16 %12 to i32, !dbg !458
  %14 = xor i32 %13, %11, !dbg !458
  %15 = trunc i32 %14 to i16, !dbg !458
  store i16 %15, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !458
  %16 = load i32* %old_errno, align 4, !dbg !459
  store i32 %16, i32* @errno, align 4, !dbg !459
  ret void, !dbg !460
}

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* noalias %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %bufsize = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %1, align 8
  %2 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !461
  %3 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %2, i32 0, i32 5, !dbg !461
  %4 = load i8** %3, align 8, !dbg !461
  %5 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !461
  %6 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %5, i32 0, i32 3, !dbg !461
  %7 = load i8** %6, align 8, !dbg !461
  %8 = ptrtoint i8* %4 to i64, !dbg !461
  %9 = ptrtoint i8* %7 to i64, !dbg !461
  %10 = sub i64 %8, %9, !dbg !461
  store i64 %10, i64* %bufsize, align 8, !dbg !461
  %11 = icmp ne i64 %10, 0, !dbg !461
  br i1 %11, label %12, label %24, !dbg !461

; <label>:12                                      ; preds = %0
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !463
  %14 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %13, i32 0, i32 3, !dbg !463
  %15 = load i8** %14, align 8, !dbg !463
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !463
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 5, !dbg !463
  store i8* %15, i8** %17, align 8, !dbg !463
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !465
  %19 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !465
  %20 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %19, i32 0, i32 3, !dbg !465
  %21 = load i8** %20, align 8, !dbg !465
  %22 = load i64* %bufsize, align 8, !dbg !465
  %23 = call i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %18, i8* %21, i64 %22) #12, !dbg !465
  br label %24, !dbg !466

; <label>:24                                      ; preds = %12, %0
  %25 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !467
  %26 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %25, i32 0, i32 5, !dbg !467
  %27 = load i8** %26, align 8, !dbg !467
  %28 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !467
  %29 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %28, i32 0, i32 3, !dbg !467
  %30 = load i8** %29, align 8, !dbg !467
  %31 = ptrtoint i8* %27 to i64, !dbg !467
  %32 = ptrtoint i8* %30 to i64, !dbg !467
  %33 = sub i64 %31, %32, !dbg !467
  ret i64 %33, !dbg !467
}

; Function Attrs: nounwind uwtable
define i8* @memcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !468
  store i8* %4, i8** %r1, align 8, !dbg !468
  %5 = load i8** %2, align 8, !dbg !469
  store i8* %5, i8** %r2, align 8, !dbg !469
  br label %6, !dbg !470

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !470
  %8 = icmp ne i64 %7, 0, !dbg !470
  br i1 %8, label %9, label %17, !dbg !470

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !471
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !471
  store i8* %11, i8** %r2, align 8, !dbg !471
  %12 = load i8* %10, align 1, !dbg !471
  %13 = load i8** %r1, align 8, !dbg !471
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !471
  store i8* %14, i8** %r1, align 8, !dbg !471
  store i8 %12, i8* %13, align 1, !dbg !471
  %15 = load i64* %3, align 8, !dbg !473
  %16 = add i64 %15, -1, !dbg !473
  store i64 %16, i64* %3, align 8, !dbg !473
  br label %6, !dbg !474

; <label>:17                                      ; preds = %6
  %18 = load i8** %1, align 8, !dbg !475
  ret i8* %18, !dbg !475
}

; Function Attrs: nounwind uwtable
define i8* @memset(i8* %s, i32 %c, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i64, align 8
  %p = alloca i8*, align 8
  store i8* %s, i8** %1, align 8
  store i32 %c, i32* %2, align 4
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !476
  store i8* %4, i8** %p, align 8, !dbg !476
  br label %5, !dbg !477

; <label>:5                                       ; preds = %8, %0
  %6 = load i64* %3, align 8, !dbg !477
  %7 = icmp ne i64 %6, 0, !dbg !477
  br i1 %7, label %8, label %15, !dbg !477

; <label>:8                                       ; preds = %5
  %9 = load i32* %2, align 4, !dbg !478
  %10 = trunc i32 %9 to i8, !dbg !478
  %11 = load i8** %p, align 8, !dbg !478
  %12 = getelementptr inbounds i8* %11, i32 1, !dbg !478
  store i8* %12, i8** %p, align 8, !dbg !478
  store i8 %10, i8* %11, align 1, !dbg !478
  %13 = load i64* %3, align 8, !dbg !480
  %14 = add i64 %13, -1, !dbg !480
  store i64 %14, i64* %3, align 8, !dbg !480
  br label %5, !dbg !481

; <label>:15                                      ; preds = %5
  %16 = load i8** %1, align 8, !dbg !482
  ret i8* %16, !dbg !482
}

; Function Attrs: nounwind uwtable
define i32 @isatty(i32 %fd) #0 {
  %1 = alloca i32, align 4
  %term = alloca %struct.termios.442, align 4
  store i32 %fd, i32* %1, align 4
  %2 = load i32* %1, align 4, !dbg !483
  %3 = call i32 @tcgetattr(i32 %2, %struct.termios.442* %term) #13, !dbg !483
  %4 = icmp eq i32 %3, 0, !dbg !483
  %5 = zext i1 %4 to i32, !dbg !483
  ret i32 %5, !dbg !483
}

; Function Attrs: nounwind uwtable
define i32 @tcgetattr(i32 %fd, %struct.termios.442* %termios_p) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.termios.442*, align 8
  %k_termios = alloca %struct.__kernel_termios, align 4
  %retval = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store %struct.termios.442* %termios_p, %struct.termios.442** %2, align 8
  %3 = load i32* %1, align 4, !dbg !484
  %4 = call i32 (i32, i64, ...)* @ioctl(i32 %3, i64 21505, %struct.__kernel_termios* %k_termios) #13, !dbg !484
  store i32 %4, i32* %retval, align 4, !dbg !484
  %5 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 0, !dbg !485
  %6 = load i32* %5, align 4, !dbg !485
  %7 = load %struct.termios.442** %2, align 8, !dbg !485
  %8 = getelementptr inbounds %struct.termios.442* %7, i32 0, i32 0, !dbg !485
  store i32 %6, i32* %8, align 4, !dbg !485
  %9 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 1, !dbg !486
  %10 = load i32* %9, align 4, !dbg !486
  %11 = load %struct.termios.442** %2, align 8, !dbg !486
  %12 = getelementptr inbounds %struct.termios.442* %11, i32 0, i32 1, !dbg !486
  store i32 %10, i32* %12, align 4, !dbg !486
  %13 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 2, !dbg !487
  %14 = load i32* %13, align 4, !dbg !487
  %15 = load %struct.termios.442** %2, align 8, !dbg !487
  %16 = getelementptr inbounds %struct.termios.442* %15, i32 0, i32 2, !dbg !487
  store i32 %14, i32* %16, align 4, !dbg !487
  %17 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 3, !dbg !488
  %18 = load i32* %17, align 4, !dbg !488
  %19 = load %struct.termios.442** %2, align 8, !dbg !488
  %20 = getelementptr inbounds %struct.termios.442* %19, i32 0, i32 3, !dbg !488
  store i32 %18, i32* %20, align 4, !dbg !488
  %21 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 4, !dbg !489
  %22 = load i8* %21, align 1, !dbg !489
  %23 = load %struct.termios.442** %2, align 8, !dbg !489
  %24 = getelementptr inbounds %struct.termios.442* %23, i32 0, i32 4, !dbg !489
  store i8 %22, i8* %24, align 1, !dbg !489
  %25 = load %struct.termios.442** %2, align 8, !dbg !490
  %26 = getelementptr inbounds %struct.termios.442* %25, i32 0, i32 5, !dbg !490
  %27 = getelementptr inbounds [32 x i8]* %26, i32 0, i64 0, !dbg !490
  %28 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 5, !dbg !490
  %29 = getelementptr inbounds [19 x i8]* %28, i32 0, i64 0, !dbg !490
  %30 = call i8* @mempcpy(i8* %27, i8* %29, i64 19) #13, !dbg !490
  %31 = call i8* @memset(i8* %30, i32 0, i64 13) #13, !dbg !490
  %32 = load i32* %retval, align 4, !dbg !493
  ret i32 %32, !dbg !493
}

; Function Attrs: nounwind
declare i32 @ioctl(i32, i64, ...) #5

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %stream, i8* %buf, i64 %bufsize) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i64, align 8
  %todo = alloca i64, align 8
  %rv = alloca i64, align 8
  %stodo = alloca i64, align 8
  %s = alloca i8*, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %2, align 8
  store i8* %buf, i8** %3, align 8
  store i64 %bufsize, i64* %4, align 8
  %5 = load i64* %4, align 8, !dbg !494
  store i64 %5, i64* %todo, align 8, !dbg !494
  br label %6, !dbg !495

; <label>:6                                       ; preds = %23, %0
  %7 = load i64* %todo, align 8, !dbg !496
  %8 = icmp eq i64 %7, 0, !dbg !496
  br i1 %8, label %9, label %11, !dbg !496

; <label>:9                                       ; preds = %6
  %10 = load i64* %4, align 8, !dbg !499
  store i64 %10, i64* %1, !dbg !499
  br label %95, !dbg !499

; <label>:11                                      ; preds = %6
  %12 = load i64* %todo, align 8, !dbg !501
  %13 = icmp ule i64 %12, 9223372036854775807, !dbg !501
  %14 = load i64* %todo, align 8, !dbg !501
  %15 = select i1 %13, i64 %14, i64 9223372036854775807, !dbg !501
  store i64 %15, i64* %stodo, align 8, !dbg !501
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !502
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 2, !dbg !502
  %18 = load i32* %17, align 4, !dbg !502
  %19 = load i8** %3, align 8, !dbg !502
  %20 = load i64* %stodo, align 8, !dbg !502
  %21 = call i64 @write(i32 %18, i8* %19, i64 %20) #12, !dbg !502
  store i64 %21, i64* %rv, align 8, !dbg !502
  %22 = icmp sge i64 %21, 0, !dbg !502
  br i1 %22, label %23, label %30, !dbg !502

; <label>:23                                      ; preds = %11
  %24 = load i64* %rv, align 8, !dbg !504
  %25 = load i64* %todo, align 8, !dbg !504
  %26 = sub i64 %25, %24, !dbg !504
  store i64 %26, i64* %todo, align 8, !dbg !504
  %27 = load i64* %rv, align 8, !dbg !506
  %28 = load i8** %3, align 8, !dbg !506
  %29 = getelementptr inbounds i8* %28, i64 %27, !dbg !506
  store i8* %29, i8** %3, align 8, !dbg !506
  br label %6, !dbg !507

; <label>:30                                      ; preds = %11
  %31 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !508
  %32 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %31, i32 0, i32 0, !dbg !508
  %33 = load i16* %32, align 2, !dbg !508
  %34 = zext i16 %33 to i32, !dbg !508
  %35 = or i32 %34, 8, !dbg !508
  %36 = trunc i32 %35 to i16, !dbg !508
  store i16 %36, i16* %32, align 2, !dbg !508
  %37 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !510
  %38 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %37, i32 0, i32 4, !dbg !510
  %39 = load i8** %38, align 8, !dbg !510
  %40 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !510
  %41 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %40, i32 0, i32 3, !dbg !510
  %42 = load i8** %41, align 8, !dbg !510
  %43 = ptrtoint i8* %39 to i64, !dbg !510
  %44 = ptrtoint i8* %42 to i64, !dbg !510
  %45 = sub i64 %43, %44, !dbg !510
  store i64 %45, i64* %stodo, align 8, !dbg !510
  %46 = icmp ne i64 %45, 0, !dbg !510
  br i1 %46, label %47, label %91, !dbg !510

; <label>:47                                      ; preds = %30
  %48 = load i64* %stodo, align 8, !dbg !512
  %49 = load i64* %todo, align 8, !dbg !512
  %50 = icmp ugt i64 %48, %49, !dbg !512
  br i1 %50, label %51, label %53, !dbg !512

; <label>:51                                      ; preds = %47
  %52 = load i64* %todo, align 8, !dbg !515
  store i64 %52, i64* %stodo, align 8, !dbg !515
  br label %53, !dbg !517

; <label>:53                                      ; preds = %51, %47
  %54 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !518
  %55 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %54, i32 0, i32 3, !dbg !518
  %56 = load i8** %55, align 8, !dbg !518
  store i8* %56, i8** %s, align 8, !dbg !518
  br label %57, !dbg !519

; <label>:57                                      ; preds = %70, %53
  %58 = load i8** %3, align 8, !dbg !520
  %59 = load i8* %58, align 1, !dbg !520
  %60 = load i8** %s, align 8, !dbg !520
  store i8 %59, i8* %60, align 1, !dbg !520
  %61 = zext i8 %59 to i32, !dbg !520
  %62 = icmp eq i32 %61, 10, !dbg !520
  br i1 %62, label %63, label %70, !dbg !520

; <label>:63                                      ; preds = %57
  %64 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !520
  %65 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %64, i32 0, i32 0, !dbg !520
  %66 = load i16* %65, align 2, !dbg !520
  %67 = zext i16 %66 to i32, !dbg !520
  %68 = and i32 %67, 256, !dbg !520
  %69 = icmp ne i32 %68, 0, !dbg !520
  br i1 %69, label %78, label %70, !dbg !520

; <label>:70                                      ; preds = %63, %57
  %71 = load i8** %s, align 8, !dbg !523
  %72 = getelementptr inbounds i8* %71, i32 1, !dbg !523
  store i8* %72, i8** %s, align 8, !dbg !523
  %73 = load i8** %3, align 8, !dbg !524
  %74 = getelementptr inbounds i8* %73, i32 1, !dbg !524
  store i8* %74, i8** %3, align 8, !dbg !524
  %75 = load i64* %stodo, align 8, !dbg !525
  %76 = add nsw i64 %75, -1, !dbg !525
  store i64 %76, i64* %stodo, align 8, !dbg !525
  %77 = icmp ne i64 %76, 0, !dbg !525
  br i1 %77, label %57, label %78, !dbg !525

; <label>:78                                      ; preds = %63, %70
  %79 = load i8** %s, align 8, !dbg !526
  %80 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !526
  %81 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %80, i32 0, i32 5, !dbg !526
  store i8* %79, i8** %81, align 8, !dbg !526
  %82 = load i8** %s, align 8, !dbg !527
  %83 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !527
  %84 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %83, i32 0, i32 3, !dbg !527
  %85 = load i8** %84, align 8, !dbg !527
  %86 = ptrtoint i8* %82 to i64, !dbg !527
  %87 = ptrtoint i8* %85 to i64, !dbg !527
  %88 = sub i64 %86, %87, !dbg !527
  %89 = load i64* %todo, align 8, !dbg !527
  %90 = sub i64 %89, %88, !dbg !527
  store i64 %90, i64* %todo, align 8, !dbg !527
  br label %91, !dbg !528

; <label>:91                                      ; preds = %78, %30
  %92 = load i64* %4, align 8, !dbg !529
  %93 = load i64* %todo, align 8, !dbg !529
  %94 = sub i64 %92, %93, !dbg !529
  store i64 %94, i64* %1, !dbg !529
  br label %95, !dbg !529

; <label>:95                                      ; preds = %91, %9
  %96 = load i64* %1, !dbg !530
  ret i64 %96, !dbg !530
}

declare i64 @write(i32, i8*, i64) #2

; Function Attrs: nounwind uwtable
define i8* @mempcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !531
  store i8* %4, i8** %r1, align 8, !dbg !531
  %5 = load i8** %2, align 8, !dbg !532
  store i8* %5, i8** %r2, align 8, !dbg !532
  br label %6, !dbg !533

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !533
  %8 = icmp ne i64 %7, 0, !dbg !533
  br i1 %8, label %9, label %17, !dbg !533

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !534
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !534
  store i8* %11, i8** %r2, align 8, !dbg !534
  %12 = load i8* %10, align 1, !dbg !534
  %13 = load i8** %r1, align 8, !dbg !534
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !534
  store i8* %14, i8** %r1, align 8, !dbg !534
  store i8 %12, i8* %13, align 1, !dbg !534
  %15 = load i64* %3, align 8, !dbg !536
  %16 = add i64 %15, -1, !dbg !536
  store i64 %16, i64* %3, align 8, !dbg !536
  br label %6, !dbg !537

; <label>:17                                      ; preds = %6
  %18 = load i8** %r1, align 8, !dbg !538
  ret i8* %18, !dbg !538
}

define i32 @main(i32, i8**) {
entry:
  call void @__uClibc_main(i32 (i32, i8**, i8**)* bitcast (i32 ()* @__user_main to i32 (i32, i8**, i8**)*), i32 %0, i8** %1, void ()* null, void ()* null, void ()* null, i8* null)
  unreachable
}

; Function Attrs: nounwind ssp uwtable
define void @klee_div_zero_check(i64 %z) #8 {
  %1 = icmp eq i64 %z, 0, !dbg !539
  br i1 %1, label %2, label %3, !dbg !539

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([60 x i8]* @.str31, i64 0, i64 0), i32 14, i8* getelementptr inbounds ([15 x i8]* @.str132, i64 0, i64 0), i8* getelementptr inbounds ([8 x i8]* @.str2, i64 0, i64 0)) #15, !dbg !541
  unreachable, !dbg !541

; <label>:3                                       ; preds = %0
  ret void, !dbg !542
}

; Function Attrs: noreturn
declare void @klee_report_error(i8*, i32, i8*, i8*) #9

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata) #1

; Function Attrs: nounwind ssp uwtable
define i32 @klee_int(i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = bitcast i32* %x to i8*, !dbg !543
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %1, i64 4, i8* %name) #13, !dbg !543
  %2 = load i32* %x, align 4, !dbg !544, !tbaa !545
  ret i32 %2, !dbg !544
}

; Function Attrs: nounwind ssp uwtable
define void @klee_overshift_check(i64 %bitWidth, i64 %shift) #8 {
  %1 = icmp ult i64 %shift, %bitWidth, !dbg !549
  br i1 %1, label %3, label %2, !dbg !549

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([8 x i8]* @.str333, i64 0, i64 0), i32 0, i8* getelementptr inbounds ([16 x i8]* @.str1434, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8]* @.str25, i64 0, i64 0)) #15, !dbg !551
  unreachable, !dbg !551

; <label>:3                                       ; preds = %0
  ret void, !dbg !553
}

; Function Attrs: nounwind ssp uwtable
define i32 @klee_range(i32 %start, i32 %end, i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = icmp slt i32 %start, %end, !dbg !554
  br i1 %1, label %3, label %2, !dbg !554

; <label>:2                                       ; preds = %0
  call void @klee_report_error(i8* getelementptr inbounds ([51 x i8]* @.str6, i64 0, i64 0), i32 17, i8* getelementptr inbounds ([14 x i8]* @.str17, i64 0, i64 0), i8* getelementptr inbounds ([5 x i8]* @.str28, i64 0, i64 0)) #15, !dbg !556
  unreachable, !dbg !556

; <label>:3                                       ; preds = %0
  %4 = add nsw i32 %start, 1, !dbg !557
  %5 = icmp eq i32 %4, %end, !dbg !557
  br i1 %5, label %21, label %6, !dbg !557

; <label>:6                                       ; preds = %3
  %7 = bitcast i32* %x to i8*, !dbg !559
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %7, i64 4, i8* %name) #13, !dbg !559
  %8 = icmp eq i32 %start, 0, !dbg !561
  %9 = load i32* %x, align 4, !dbg !563, !tbaa !545
  br i1 %8, label %10, label %13, !dbg !561

; <label>:10                                      ; preds = %6
  %11 = icmp ult i32 %9, %end, !dbg !563
  %12 = zext i1 %11 to i64, !dbg !563
  call void @klee_assume(i64 %12) #13, !dbg !563
  br label %19, !dbg !565

; <label>:13                                      ; preds = %6
  %14 = icmp sge i32 %9, %start, !dbg !566
  %15 = zext i1 %14 to i64, !dbg !566
  call void @klee_assume(i64 %15) #13, !dbg !566
  %16 = load i32* %x, align 4, !dbg !568, !tbaa !545
  %17 = icmp slt i32 %16, %end, !dbg !568
  %18 = zext i1 %17 to i64, !dbg !568
  call void @klee_assume(i64 %18) #13, !dbg !568
  br label %19

; <label>:19                                      ; preds = %13, %10
  %20 = load i32* %x, align 4, !dbg !569, !tbaa !545
  br label %21, !dbg !569

; <label>:21                                      ; preds = %19, %3
  %.0 = phi i32 [ %20, %19 ], [ %start, %3 ]
  ret i32 %.0, !dbg !570
}

declare void @klee_assume(i64) #10

; Function Attrs: nounwind ssp uwtable
define weak i8* @memmove(i8* %dst, i8* %src, i64 %count) #8 {
  %1 = icmp eq i8* %src, %dst, !dbg !571
  br i1 %1, label %.loopexit, label %2, !dbg !571

; <label>:2                                       ; preds = %0
  %3 = icmp ugt i8* %src, %dst, !dbg !573
  br i1 %3, label %.preheader, label %18, !dbg !573

.preheader:                                       ; preds = %2
  %4 = icmp eq i64 %count, 0, !dbg !575
  br i1 %4, label %.loopexit, label %.lr.ph.preheader, !dbg !575

.lr.ph.preheader:                                 ; preds = %.preheader
  %n.vec = and i64 %count, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %5 = add i64 %count, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep11 = getelementptr i8* %src, i64 %5
  %scevgep = getelementptr i8* %dst, i64 %5
  %bound1 = icmp uge i8* %scevgep, %src
  %bound0 = icmp uge i8* %scevgep11, %dst
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %src, i64 %n.vec
  %ptr.ind.end13 = getelementptr i8* %dst, i64 %n.vec
  %rev.ind.end = sub i64 %count, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %src, i64 %index
  %next.gep110 = getelementptr i8* %dst, i64 %index
  %6 = bitcast i8* %next.gep to <16 x i8>*, !dbg !575
  %wide.load = load <16 x i8>* %6, align 1, !dbg !575
  %next.gep.sum586 = or i64 %index, 16, !dbg !575
  %7 = getelementptr i8* %src, i64 %next.gep.sum586, !dbg !575
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !575
  %wide.load207 = load <16 x i8>* %8, align 1, !dbg !575
  %9 = bitcast i8* %next.gep110 to <16 x i8>*, !dbg !575
  store <16 x i8> %wide.load, <16 x i8>* %9, align 1, !dbg !575
  %10 = getelementptr i8* %dst, i64 %next.gep.sum586, !dbg !575
  %11 = bitcast i8* %10 to <16 x i8>*, !dbg !575
  store <16 x i8> %wide.load207, <16 x i8>* %11, align 1, !dbg !575
  %index.next = add i64 %index, 32
  %12 = icmp eq i64 %index.next, %n.vec
  br i1 %12, label %middle.block, label %vector.body, !llvm.loop !577

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %src, %.lr.ph.preheader ], [ %src, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val12 = phi i8* [ %dst, %.lr.ph.preheader ], [ %dst, %vector.memcheck ], [ %ptr.ind.end13, %vector.body ]
  %resume.val14 = phi i64 [ %count, %.lr.ph.preheader ], [ %count, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %count
  br i1 %cmp.n, label %.loopexit, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %b.04 = phi i8* [ %14, %.lr.ph ], [ %resume.val, %middle.block ]
  %a.03 = phi i8* [ %16, %.lr.ph ], [ %resume.val12, %middle.block ]
  %.02 = phi i64 [ %13, %.lr.ph ], [ %resume.val14, %middle.block ]
  %13 = add i64 %.02, -1, !dbg !575
  %14 = getelementptr inbounds i8* %b.04, i64 1, !dbg !575
  %15 = load i8* %b.04, align 1, !dbg !575, !tbaa !580
  %16 = getelementptr inbounds i8* %a.03, i64 1, !dbg !575
  store i8 %15, i8* %a.03, align 1, !dbg !575, !tbaa !580
  %17 = icmp eq i64 %13, 0, !dbg !575
  br i1 %17, label %.loopexit, label %.lr.ph, !dbg !575, !llvm.loop !581

; <label>:18                                      ; preds = %2
  %19 = add i64 %count, -1, !dbg !582
  %20 = icmp eq i64 %count, 0, !dbg !584
  br i1 %20, label %.loopexit, label %.lr.ph9, !dbg !584

.lr.ph9:                                          ; preds = %18
  %21 = getelementptr inbounds i8* %src, i64 %19, !dbg !585
  %22 = getelementptr inbounds i8* %dst, i64 %19, !dbg !582
  %n.vec215 = and i64 %count, -32
  %cmp.zero217 = icmp eq i64 %n.vec215, 0
  br i1 %cmp.zero217, label %middle.block210, label %vector.memcheck224

vector.memcheck224:                               ; preds = %.lr.ph9
  %bound1221 = icmp ule i8* %21, %dst
  %bound0220 = icmp ule i8* %22, %src
  %memcheck.conflict223 = and i1 %bound0220, %bound1221
  %.sum = sub i64 %19, %n.vec215
  %rev.ptr.ind.end = getelementptr i8* %src, i64 %.sum
  %rev.ptr.ind.end229 = getelementptr i8* %dst, i64 %.sum
  %rev.ind.end231 = sub i64 %count, %n.vec215
  br i1 %memcheck.conflict223, label %middle.block210, label %vector.body209

vector.body209:                                   ; preds = %vector.body209, %vector.memcheck224
  %index212 = phi i64 [ %index.next234, %vector.body209 ], [ 0, %vector.memcheck224 ]
  %.sum440 = sub i64 %19, %index212
  %next.gep236.sum = add i64 %.sum440, -15, !dbg !584
  %23 = getelementptr i8* %src, i64 %next.gep236.sum, !dbg !584
  %24 = bitcast i8* %23 to <16 x i8>*, !dbg !584
  %wide.load434 = load <16 x i8>* %24, align 1, !dbg !584
  %reverse = shufflevector <16 x i8> %wide.load434, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !584
  %.sum505 = add i64 %.sum440, -31, !dbg !584
  %25 = getelementptr i8* %src, i64 %.sum505, !dbg !584
  %26 = bitcast i8* %25 to <16 x i8>*, !dbg !584
  %wide.load435 = load <16 x i8>* %26, align 1, !dbg !584
  %reverse436 = shufflevector <16 x i8> %wide.load435, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !584
  %reverse437 = shufflevector <16 x i8> %reverse, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !584
  %27 = getelementptr i8* %dst, i64 %next.gep236.sum, !dbg !584
  %28 = bitcast i8* %27 to <16 x i8>*, !dbg !584
  store <16 x i8> %reverse437, <16 x i8>* %28, align 1, !dbg !584
  %reverse438 = shufflevector <16 x i8> %reverse436, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !584
  %29 = getelementptr i8* %dst, i64 %.sum505, !dbg !584
  %30 = bitcast i8* %29 to <16 x i8>*, !dbg !584
  store <16 x i8> %reverse438, <16 x i8>* %30, align 1, !dbg !584
  %index.next234 = add i64 %index212, 32
  %31 = icmp eq i64 %index.next234, %n.vec215
  br i1 %31, label %middle.block210, label %vector.body209, !llvm.loop !586

middle.block210:                                  ; preds = %vector.body209, %vector.memcheck224, %.lr.ph9
  %resume.val225 = phi i8* [ %21, %.lr.ph9 ], [ %21, %vector.memcheck224 ], [ %rev.ptr.ind.end, %vector.body209 ]
  %resume.val227 = phi i8* [ %22, %.lr.ph9 ], [ %22, %vector.memcheck224 ], [ %rev.ptr.ind.end229, %vector.body209 ]
  %resume.val230 = phi i64 [ %count, %.lr.ph9 ], [ %count, %vector.memcheck224 ], [ %rev.ind.end231, %vector.body209 ]
  %new.indc.resume.val232 = phi i64 [ 0, %.lr.ph9 ], [ 0, %vector.memcheck224 ], [ %n.vec215, %vector.body209 ]
  %cmp.n233 = icmp eq i64 %new.indc.resume.val232, %count
  br i1 %cmp.n233, label %.loopexit, label %scalar.ph211

scalar.ph211:                                     ; preds = %scalar.ph211, %middle.block210
  %b.18 = phi i8* [ %33, %scalar.ph211 ], [ %resume.val225, %middle.block210 ]
  %a.17 = phi i8* [ %35, %scalar.ph211 ], [ %resume.val227, %middle.block210 ]
  %.16 = phi i64 [ %32, %scalar.ph211 ], [ %resume.val230, %middle.block210 ]
  %32 = add i64 %.16, -1, !dbg !584
  %33 = getelementptr inbounds i8* %b.18, i64 -1, !dbg !584
  %34 = load i8* %b.18, align 1, !dbg !584, !tbaa !580
  %35 = getelementptr inbounds i8* %a.17, i64 -1, !dbg !584
  store i8 %34, i8* %a.17, align 1, !dbg !584, !tbaa !580
  %36 = icmp eq i64 %32, 0, !dbg !584
  br i1 %36, label %.loopexit, label %scalar.ph211, !dbg !584, !llvm.loop !587

.loopexit:                                        ; preds = %scalar.ph211, %middle.block210, %18, %.lr.ph, %middle.block, %.preheader, %0
  ret i8* %dst, !dbg !588
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #4 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-floa
attributes #5 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false
attributes #6 = { inlinehint nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use
attributes #7 = { nounwind readnone uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #8 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nounwind }
attributes #12 = { nobuiltin }
attributes #13 = { nobuiltin nounwind }
attributes #14 = { nobuiltin nounwind readnone }
attributes #15 = { nobuiltin noreturn nounwind }

!llvm.dbg.cu = !{!0, !9, !22, !60, !68, !73, !122, !153, !165, !173, !180, !205, !211, !243, !251, !261, !271, !281, !293, !307, !321, !335}
!llvm.module.flags = !{!350, !351}
!llvm.ident = !{!352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352, !352}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !2, metadata !2, metadata !""} ; [ 
!1 = metadata !{metadata !"test.c", metadata !"/home/klee/workspace/basic-tests/stack-overflow"}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 5, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @__user_main, null, null, metadata !2, i32 5} ; [ DW_TAG_subprogra
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/klee/workspace/basic-tests/stack-overflow/test.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{metadata !8}
!8 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!9 = metadata !{i32 786449, metadata !10, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !11, metadata !2, metadata !2, metadata !""} ; 
!10 = metadata !{metadata !"libc/string/strcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"strcpy", metadata !"strcpy", metadata !"", i32 18, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*)* @strcpy, null, null, metadata !2, i32 19} ; [ D
!13 = metadata !{i32 786473, metadata !10}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/strcpy.c]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{metadata !16, metadata !18, metadata !19}
!16 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !17} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from char]
!17 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!18 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !16} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!19 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !20} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!20 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !21} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!21 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from char]
!22 = metadata !{i32 786449, metadata !23, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !24, metadata !50, metadata !2, metadata !""} 
!23 = metadata !{metadata !"libc/misc/internals/__uClibc_main.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!24 = metadata !{metadata !25, metadata !29, metadata !30, metadata !39, metadata !42, metadata !49}
!25 = metadata !{i32 786478, metadata !23, metadata !26, metadata !"__uClibc_init", metadata !"__uClibc_init", metadata !"", i32 187, metadata !27, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_init, null, null, metadata !2
!26 = metadata !{i32 786473, metadata !23}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!27 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !28, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!28 = metadata !{null}
!29 = metadata !{i32 786478, metadata !23, metadata !26, metadata !"__uClibc_fini", metadata !"__uClibc_fini", metadata !"", i32 251, metadata !27, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_fini, null, null, metadata !2
!30 = metadata !{i32 786478, metadata !23, metadata !26, metadata !"__uClibc_main", metadata !"__uClibc_main", metadata !"", i32 278, metadata !31, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32 (i32, i8**, i8**)*, i32, i8**, void ()
!31 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !32, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!32 = metadata !{null, metadata !33, metadata !8, metadata !36, metadata !37, metadata !37, metadata !37, metadata !38}
!33 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !34} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!34 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !35, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!35 = metadata !{metadata !8, metadata !8, metadata !36, metadata !36}
!36 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !16} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!37 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !27} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!38 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!39 = metadata !{i32 786478, metadata !23, metadata !26, metadata !"__check_one_fd", metadata !"__check_one_fd", metadata !"", i32 136, metadata !40, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @__check_one_fd, null, null, m
!40 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !41, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!41 = metadata !{null, metadata !8, metadata !8}
!42 = metadata !{i32 786478, metadata !43, metadata !44, metadata !"gnu_dev_makedev", metadata !"gnu_dev_makedev", metadata !"", i32 54, metadata !45, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i32, i32)* @gnu_dev_makedev, null, null, 
!43 = metadata !{metadata !"./include/sys/sysmacros.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!44 = metadata !{i32 786473, metadata !43}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!45 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !46, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!46 = metadata !{metadata !47, metadata !48, metadata !48}
!47 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!48 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!49 = metadata !{i32 786478, metadata !23, metadata !26, metadata !"__check_suid", metadata !"__check_suid", metadata !"", i32 155, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @__check_suid, null, null, metadata !2, i32 
!50 = metadata !{metadata !51, metadata !52, metadata !53, metadata !54, metadata !57, metadata !58, metadata !59}
!51 = metadata !{i32 786484, i32 0, null, metadata !"__libc_stack_end", metadata !"__libc_stack_end", metadata !"", metadata !26, i32 52, metadata !38, i32 0, i32 1, i8** @__libc_stack_end, null} ; [ DW_TAG_variable ] [__libc_stack_end] [line 52] [def]
!52 = metadata !{i32 786484, i32 0, null, metadata !"__uclibc_progname", metadata !"__uclibc_progname", metadata !"", metadata !26, i32 110, metadata !20, i32 0, i32 1, i8** @__uclibc_progname, null} ; [ DW_TAG_variable ] [__uclibc_progname] [line 110] [
!53 = metadata !{i32 786484, i32 0, null, metadata !"__environ", metadata !"__environ", metadata !"", metadata !26, i32 125, metadata !36, i32 0, i32 1, i8*** @__environ, null} ; [ DW_TAG_variable ] [__environ] [line 125] [def]
!54 = metadata !{i32 786484, i32 0, null, metadata !"__pagesize", metadata !"__pagesize", metadata !"", metadata !26, i32 129, metadata !55, i32 0, i32 1, i64* @__pagesize, null} ; [ DW_TAG_variable ] [__pagesize] [line 129] [def]
!55 = metadata !{i32 786454, metadata !23, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!56 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!57 = metadata !{i32 786484, i32 0, metadata !25, metadata !"been_there_done_that", metadata !"been_there_done_that", metadata !"", metadata !26, i32 189, metadata !8, i32 1, i32 1, i32* @__uClibc_init.been_there_done_that, null} ; [ DW_TAG_variable ] [b
!58 = metadata !{i32 786484, i32 0, null, metadata !"__app_fini", metadata !"__app_fini", metadata !"", metadata !26, i32 244, metadata !37, i32 0, i32 1, void ()** @__app_fini, null} ; [ DW_TAG_variable ] [__app_fini] [line 244] [def]
!59 = metadata !{i32 786484, i32 0, null, metadata !"__rtld_fini", metadata !"__rtld_fini", metadata !"", metadata !26, i32 247, metadata !37, i32 0, i32 1, void ()** @__rtld_fini, null} ; [ DW_TAG_variable ] [__rtld_fini] [line 247] [def]
!60 = metadata !{i32 786449, metadata !61, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !62, metadata !2, metadata !2, metadata !""} ;
!61 = metadata !{metadata !"libc/misc/internals/__errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!62 = metadata !{metadata !63}
!63 = metadata !{i32 786478, metadata !61, metadata !64, metadata !"__errno_location", metadata !"__errno_location", metadata !"", i32 11, metadata !65, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__errno_location, null, null, met
!64 = metadata !{i32 786473, metadata !61}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__errno_location.c]
!65 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !66, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!66 = metadata !{metadata !67}
!67 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!68 = metadata !{i32 786449, metadata !69, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !70, metadata !2, metadata !2, metadata !""} ;
!69 = metadata !{metadata !"libc/misc/internals/__h_errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!70 = metadata !{metadata !71}
!71 = metadata !{i32 786478, metadata !69, metadata !72, metadata !"__h_errno_location", metadata !"__h_errno_location", metadata !"", i32 10, metadata !65, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__h_errno_location, null, nul
!72 = metadata !{i32 786473, metadata !69}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__h_errno_location.c]
!73 = metadata !{i32 786449, metadata !74, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !75, metadata !79, metadata !2, metadata !""} 
!74 = metadata !{metadata !"libc/stdio/_stdio.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!75 = metadata !{metadata !76, metadata !78}
!76 = metadata !{i32 786478, metadata !74, metadata !77, metadata !"_stdio_term", metadata !"_stdio_term", metadata !"", i32 210, metadata !27, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_term, null, null, metadata !2, i32 
!77 = metadata !{i32 786473, metadata !74}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!78 = metadata !{i32 786478, metadata !74, metadata !77, metadata !"_stdio_init", metadata !"_stdio_init", metadata !"", i32 277, metadata !27, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_init, null, null, metadata !2, i32 
!79 = metadata !{metadata !80, metadata !113, metadata !114, metadata !115, metadata !116, metadata !117, metadata !118}
!80 = metadata !{i32 786484, i32 0, null, metadata !"stdin", metadata !"stdin", metadata !"", metadata !77, i32 154, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdin, null} ; [ DW_TAG_variable ] [stdin] [line 154] [def]
!81 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !82} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!82 = metadata !{i32 786454, metadata !74, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !83} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!83 = metadata !{i32 786451, metadata !84, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !85, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offset
!84 = metadata !{metadata !"./include/bits/uClibc_stdio.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!85 = metadata !{metadata !86, metadata !88, metadata !93, metadata !94, metadata !96, metadata !97, metadata !98, metadata !99, metadata !100, metadata !101, metadata !103, metadata !106}
!86 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !87} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!87 = metadata !{i32 786468, null, null, metadata !"unsigned short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned short] [line 0, size 16, align 16, offset 0, enc DW_ATE_unsigned]
!88 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !89} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!89 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 16, i64 8, i32 0, i32 0, metadata !90, metadata !91, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 16, align 8, offset 0] [from unsigned char]
!90 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!91 = metadata !{metadata !92}
!92 = metadata !{i32 786465, i64 0, i64 2}        ; [ DW_TAG_subrange_type ] [0, 1]
!93 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!94 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!95 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !90} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from unsigned char]
!96 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!97 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!98 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!99 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!100 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!101 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !102} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!102 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !83} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!103 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !104} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!104 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 32, i32 0, i32 0, metadata !105, metadata !91, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 64, align 32, offset 0] [from wchar_t]
!105 = metadata !{i32 786454, metadata !84, null, metadata !"wchar_t", i32 65, i64 0, i64 0, i64 0, i32 0, metadata !8} ; [ DW_TAG_typedef ] [wchar_t] [line 65, size 0, align 0, offset 0] [from int]
!106 = metadata !{i32 786445, metadata !84, metadata !83, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !107} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!107 = metadata !{i32 786454, metadata !84, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !108} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!108 = metadata !{i32 786451, metadata !109, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !110, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!109 = metadata !{metadata !"./include/wchar.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!110 = metadata !{metadata !111, metadata !112}
!111 = metadata !{i32 786445, metadata !109, metadata !108, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !105} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!112 = metadata !{i32 786445, metadata !109, metadata !108, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !105} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!113 = metadata !{i32 786484, i32 0, null, metadata !"stdout", metadata !"stdout", metadata !"", metadata !77, i32 155, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdout, null} ; [ DW_TAG_variable ] [stdout] [line 155] [def]
!114 = metadata !{i32 786484, i32 0, null, metadata !"stderr", metadata !"stderr", metadata !"", metadata !77, i32 156, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stderr, null} ; [ DW_TAG_variable ] [stderr] [line 156] [def]
!115 = metadata !{i32 786484, i32 0, null, metadata !"__stdin", metadata !"__stdin", metadata !"", metadata !77, i32 159, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdin, null} ; [ DW_TAG_variable ] [__stdin] [line 159] [def]
!116 = metadata !{i32 786484, i32 0, null, metadata !"__stdout", metadata !"__stdout", metadata !"", metadata !77, i32 162, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdout, null} ; [ DW_TAG_variable ] [__stdout] [line 162] [def]
!117 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_openlist", metadata !"_stdio_openlist", metadata !"", metadata !77, i32 180, metadata !81, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, null} ; [ DW_TAG_variable ] [_stdio_
!118 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_streams", metadata !"_stdio_streams", metadata !"", metadata !77, i32 131, metadata !119, i32 1, i32 1, [3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, null} ; [ DW_TAG_variable ] [_std
!119 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1920, i64 64, i32 0, i32 0, metadata !82, metadata !120, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 1920, align 64, offset 0] [from FILE]
!120 = metadata !{metadata !121}
!121 = metadata !{i32 786465, i64 0, i64 3}       ; [ DW_TAG_subrange_type ] [0, 2]
!122 = metadata !{i32 786449, metadata !123, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !124, metadata !2, metadata !2, metadata !""
!123 = metadata !{metadata !"libc/stdio/_wcommit.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!124 = metadata !{metadata !125}
!125 = metadata !{i32 786478, metadata !123, metadata !126, metadata !"__stdio_wcommit", metadata !"__stdio_wcommit", metadata !"", i32 17, metadata !127, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*)* @
!126 = metadata !{i32 786473, metadata !123}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!127 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !128, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!128 = metadata !{metadata !129, metadata !130}
!129 = metadata !{i32 786454, metadata !123, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!130 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !131} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!131 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !132} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!132 = metadata !{i32 786454, metadata !123, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !133} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!133 = metadata !{i32 786451, metadata !84, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !134, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!134 = metadata !{metadata !135, metadata !136, metadata !137, metadata !138, metadata !139, metadata !140, metadata !141, metadata !142, metadata !143, metadata !144, metadata !146, metadata !147}
!135 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !87} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!136 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !89} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!137 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!138 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!139 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!140 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!141 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!142 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!143 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!144 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !145} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!145 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !133} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!146 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !104} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!147 = metadata !{i32 786445, metadata !84, metadata !133, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !148} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!148 = metadata !{i32 786454, metadata !84, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !149} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!149 = metadata !{i32 786451, metadata !109, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !150, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!150 = metadata !{metadata !151, metadata !152}
!151 = metadata !{i32 786445, metadata !109, metadata !149, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !105} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!152 = metadata !{i32 786445, metadata !109, metadata !149, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !105} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!153 = metadata !{i32 786449, metadata !154, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !155, metadata !2, metadata !2, metadata !""
!154 = metadata !{metadata !"libc/string/memcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!155 = metadata !{metadata !156}
!156 = metadata !{i32 786478, metadata !154, metadata !157, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 18, metadata !158, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !2, i32 
!157 = metadata !{i32 786473, metadata !154}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!158 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !159, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!159 = metadata !{metadata !38, metadata !160, metadata !161, metadata !164}
!160 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !38} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!161 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !162} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!162 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !163} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!163 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!164 = metadata !{i32 786454, metadata !154, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!165 = metadata !{i32 786449, metadata !166, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !167, metadata !2, metadata !2, metadata !""
!166 = metadata !{metadata !"libc/string/memset.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!167 = metadata !{metadata !168}
!168 = metadata !{i32 786478, metadata !166, metadata !169, metadata !"memset", metadata !"memset", metadata !"", i32 17, metadata !170, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i32, i64)* @memset, null, null, metadata !2, i32 
!169 = metadata !{i32 786473, metadata !166}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!170 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !171, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!171 = metadata !{metadata !38, metadata !38, metadata !8, metadata !172}
!172 = metadata !{i32 786454, metadata !166, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!173 = metadata !{i32 786449, metadata !174, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !175, metadata !2, metadata !2, metadata !""
!174 = metadata !{metadata !"libc/termios/isatty.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!175 = metadata !{metadata !176}
!176 = metadata !{i32 786478, metadata !174, metadata !177, metadata !"isatty", metadata !"isatty", metadata !"", i32 26, metadata !178, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @isatty, null, null, metadata !2, i32 27} ; [ DW
!177 = metadata !{i32 786473, metadata !174}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/isatty.c]
!178 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !179, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!179 = metadata !{metadata !8, metadata !8}
!180 = metadata !{i32 786449, metadata !181, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !182, metadata !2, metadata !2, metadata !""
!181 = metadata !{metadata !"libc/termios/tcgetattr.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!182 = metadata !{metadata !183}
!183 = metadata !{i32 786478, metadata !181, metadata !184, metadata !"tcgetattr", metadata !"tcgetattr", metadata !"", i32 38, metadata !185, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, %struct.termios.442*)* @tcgetattr, null, nu
!184 = metadata !{i32 786473, metadata !181}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!185 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !186, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!186 = metadata !{metadata !8, metadata !8, metadata !187}
!187 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !188} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from termios]
!188 = metadata !{i32 786451, metadata !189, null, metadata !"termios", i32 30, i64 480, i64 32, i32 0, i32 0, null, metadata !190, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [termios] [line 30, size 480, align 32, offset 0] [def] [from ]
!189 = metadata !{metadata !"./include/bits/termios.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!190 = metadata !{metadata !191, metadata !193, metadata !194, metadata !195, metadata !196, metadata !198, metadata !202, metadata !204}
!191 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_iflag", i32 32, i64 32, i64 32, i64 0, i32 0, metadata !192} ; [ DW_TAG_member ] [c_iflag] [line 32, size 32, align 32, offset 0] [from tcflag_t]
!192 = metadata !{i32 786454, metadata !189, null, metadata !"tcflag_t", i32 27, i64 0, i64 0, i64 0, i32 0, metadata !48} ; [ DW_TAG_typedef ] [tcflag_t] [line 27, size 0, align 0, offset 0] [from unsigned int]
!193 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_oflag", i32 33, i64 32, i64 32, i64 32, i32 0, metadata !192} ; [ DW_TAG_member ] [c_oflag] [line 33, size 32, align 32, offset 32] [from tcflag_t]
!194 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_cflag", i32 34, i64 32, i64 32, i64 64, i32 0, metadata !192} ; [ DW_TAG_member ] [c_cflag] [line 34, size 32, align 32, offset 64] [from tcflag_t]
!195 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_lflag", i32 35, i64 32, i64 32, i64 96, i32 0, metadata !192} ; [ DW_TAG_member ] [c_lflag] [line 35, size 32, align 32, offset 96] [from tcflag_t]
!196 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_line", i32 36, i64 8, i64 8, i64 128, i32 0, metadata !197} ; [ DW_TAG_member ] [c_line] [line 36, size 8, align 8, offset 128] [from cc_t]
!197 = metadata !{i32 786454, metadata !189, null, metadata !"cc_t", i32 25, i64 0, i64 0, i64 0, i32 0, metadata !90} ; [ DW_TAG_typedef ] [cc_t] [line 25, size 0, align 0, offset 0] [from unsigned char]
!198 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_cc", i32 37, i64 256, i64 8, i64 136, i32 0, metadata !199} ; [ DW_TAG_member ] [c_cc] [line 37, size 256, align 8, offset 136] [from ]
!199 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 256, i64 8, i32 0, i32 0, metadata !197, metadata !200, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 256, align 8, offset 0] [from cc_t]
!200 = metadata !{metadata !201}
!201 = metadata !{i32 786465, i64 0, i64 32}      ; [ DW_TAG_subrange_type ] [0, 31]
!202 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_ispeed", i32 38, i64 32, i64 32, i64 416, i32 0, metadata !203} ; [ DW_TAG_member ] [c_ispeed] [line 38, size 32, align 32, offset 416] [from speed_t]
!203 = metadata !{i32 786454, metadata !189, null, metadata !"speed_t", i32 26, i64 0, i64 0, i64 0, i32 0, metadata !48} ; [ DW_TAG_typedef ] [speed_t] [line 26, size 0, align 0, offset 0] [from unsigned int]
!204 = metadata !{i32 786445, metadata !189, metadata !188, metadata !"c_ospeed", i32 39, i64 32, i64 32, i64 448, i32 0, metadata !203} ; [ DW_TAG_member ] [c_ospeed] [line 39, size 32, align 32, offset 448] [from speed_t]
!205 = metadata !{i32 786449, metadata !206, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !2, metadata !207, metadata !2, metadata !""
!206 = metadata !{metadata !"libc/misc/internals/errno.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!207 = metadata !{metadata !208, metadata !210}
!208 = metadata !{i32 786484, i32 0, null, metadata !"errno", metadata !"errno", metadata !"", metadata !209, i32 7, metadata !8, i32 0, i32 1, i32* @errno, null} ; [ DW_TAG_variable ] [errno] [line 7] [def]
!209 = metadata !{i32 786473, metadata !206}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/errno.c]
!210 = metadata !{i32 786484, i32 0, null, metadata !"h_errno", metadata !"h_errno", metadata !"", metadata !209, i32 8, metadata !8, i32 0, i32 1, i32* @h_errno, null} ; [ DW_TAG_variable ] [h_errno] [line 8] [def]
!211 = metadata !{i32 786449, metadata !212, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !213, metadata !2, metadata !2, metadata !""
!212 = metadata !{metadata !"libc/stdio/_WRITE.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!213 = metadata !{metadata !214}
!214 = metadata !{i32 786478, metadata !212, metadata !215, metadata !"__stdio_WRITE", metadata !"__stdio_WRITE", metadata !"", i32 33, metadata !216, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*, i8*, i
!215 = metadata !{i32 786473, metadata !212}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!216 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !217, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!217 = metadata !{metadata !218, metadata !219, metadata !241, metadata !218}
!218 = metadata !{i32 786454, metadata !212, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!219 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !220} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!220 = metadata !{i32 786454, metadata !212, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !221} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!221 = metadata !{i32 786451, metadata !84, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !222, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!222 = metadata !{metadata !223, metadata !224, metadata !225, metadata !226, metadata !227, metadata !228, metadata !229, metadata !230, metadata !231, metadata !232, metadata !234, metadata !235}
!223 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !87} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!224 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !89} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!225 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!226 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!227 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!228 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!229 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!230 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!231 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !95} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!232 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !233} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!233 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !221} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!234 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !104} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!235 = metadata !{i32 786445, metadata !84, metadata !221, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !236} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!236 = metadata !{i32 786454, metadata !84, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !237} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!237 = metadata !{i32 786451, metadata !109, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !238, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!238 = metadata !{metadata !239, metadata !240}
!239 = metadata !{i32 786445, metadata !109, metadata !237, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !105} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!240 = metadata !{i32 786445, metadata !109, metadata !237, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !105} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!241 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !242} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!242 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !90} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from unsigned char]
!243 = metadata !{i32 786449, metadata !244, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !245, metadata !2, metadata !2, metadata !""
!244 = metadata !{metadata !"libc/string/mempcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!245 = metadata !{metadata !246}
!246 = metadata !{i32 786478, metadata !244, metadata !247, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 20, metadata !248, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !2, i
!247 = metadata !{i32 786473, metadata !244}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!248 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !249, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!249 = metadata !{metadata !38, metadata !160, metadata !161, metadata !250}
!250 = metadata !{i32 786454, metadata !244, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!251 = metadata !{i32 786449, metadata !252, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !253, metadata !2, metadata !2, metadata !""} 
!252 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!253 = metadata !{metadata !254}
!254 = metadata !{i32 786478, metadata !252, metadata !255, metadata !"klee_div_zero_check", metadata !"klee_div_zero_check", metadata !"", i32 12, metadata !256, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64)* @klee_div_zero_check, 
!255 = metadata !{i32 786473, metadata !252}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!256 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !257, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!257 = metadata !{null, metadata !258}
!258 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!259 = metadata !{metadata !260}
!260 = metadata !{i32 786689, metadata !254, metadata !"z", metadata !255, i32 16777228, metadata !258, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [z] [line 12]
!261 = metadata !{i32 786449, metadata !262, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !263, metadata !2, metadata !2, metadata !""} 
!262 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_int.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!263 = metadata !{metadata !264}
!264 = metadata !{i32 786478, metadata !262, metadata !265, metadata !"klee_int", metadata !"klee_int", metadata !"", i32 13, metadata !266, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i8*)* @klee_int, null, null, metadata !268, i32 13}
!265 = metadata !{i32 786473, metadata !262}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_int.c]
!266 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !267, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!267 = metadata !{metadata !8, metadata !20}
!268 = metadata !{metadata !269, metadata !270}
!269 = metadata !{i32 786689, metadata !264, metadata !"name", metadata !265, i32 16777229, metadata !20, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!270 = metadata !{i32 786688, metadata !264, metadata !"x", metadata !265, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!271 = metadata !{i32 786449, metadata !272, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !273, metadata !2, metadata !2, metadata !""} 
!272 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!273 = metadata !{metadata !274}
!274 = metadata !{i32 786478, metadata !272, metadata !275, metadata !"klee_overshift_check", metadata !"klee_overshift_check", metadata !"", i32 20, metadata !276, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64, i64)* @klee_overshift
!275 = metadata !{i32 786473, metadata !272}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!276 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !277, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!277 = metadata !{null, metadata !47, metadata !47}
!278 = metadata !{metadata !279, metadata !280}
!279 = metadata !{i32 786689, metadata !274, metadata !"bitWidth", metadata !275, i32 16777236, metadata !47, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [bitWidth] [line 20]
!280 = metadata !{i32 786689, metadata !274, metadata !"shift", metadata !275, i32 33554452, metadata !47, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [shift] [line 20]
!281 = metadata !{i32 786449, metadata !282, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !283, metadata !2, metadata !2, metadata !""} 
!282 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_range.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!283 = metadata !{metadata !284}
!284 = metadata !{i32 786478, metadata !282, metadata !285, metadata !"klee_range", metadata !"klee_range", metadata !"", i32 13, metadata !286, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i32, i32, i8*)* @klee_range, null, null, metada
!285 = metadata !{i32 786473, metadata !282}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!286 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !287, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!287 = metadata !{metadata !8, metadata !8, metadata !8, metadata !20}
!288 = metadata !{metadata !289, metadata !290, metadata !291, metadata !292}
!289 = metadata !{i32 786689, metadata !284, metadata !"start", metadata !285, i32 16777229, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [start] [line 13]
!290 = metadata !{i32 786689, metadata !284, metadata !"end", metadata !285, i32 33554445, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [end] [line 13]
!291 = metadata !{i32 786689, metadata !284, metadata !"name", metadata !285, i32 50331661, metadata !20, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!292 = metadata !{i32 786688, metadata !284, metadata !"x", metadata !285, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!293 = metadata !{i32 786449, metadata !294, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !295, metadata !2, metadata !2, metadata !""} 
!294 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!295 = metadata !{metadata !296}
!296 = metadata !{i32 786478, metadata !294, metadata !297, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 12, metadata !298, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !301, i32
!297 = metadata !{i32 786473, metadata !294}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memcpy.c]
!298 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !299, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!299 = metadata !{metadata !38, metadata !38, metadata !162, metadata !300}
!300 = metadata !{i32 786454, metadata !294, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!301 = metadata !{metadata !302, metadata !303, metadata !304, metadata !305, metadata !306}
!302 = metadata !{i32 786689, metadata !296, metadata !"destaddr", metadata !297, i32 16777228, metadata !38, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 12]
!303 = metadata !{i32 786689, metadata !296, metadata !"srcaddr", metadata !297, i32 33554444, metadata !162, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 12]
!304 = metadata !{i32 786689, metadata !296, metadata !"len", metadata !297, i32 50331660, metadata !300, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 12]
!305 = metadata !{i32 786688, metadata !296, metadata !"dest", metadata !297, i32 13, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 13]
!306 = metadata !{i32 786688, metadata !296, metadata !"src", metadata !297, i32 14, metadata !20, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 14]
!307 = metadata !{i32 786449, metadata !308, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !309, metadata !2, metadata !2, metadata !""} 
!308 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memmove.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!309 = metadata !{metadata !310}
!310 = metadata !{i32 786478, metadata !308, metadata !311, metadata !"memmove", metadata !"memmove", metadata !"", i32 12, metadata !312, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memmove, null, null, metadata !315, 
!311 = metadata !{i32 786473, metadata !308}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!312 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !313, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!313 = metadata !{metadata !38, metadata !38, metadata !162, metadata !314}
!314 = metadata !{i32 786454, metadata !308, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!315 = metadata !{metadata !316, metadata !317, metadata !318, metadata !319, metadata !320}
!316 = metadata !{i32 786689, metadata !310, metadata !"dst", metadata !311, i32 16777228, metadata !38, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 12]
!317 = metadata !{i32 786689, metadata !310, metadata !"src", metadata !311, i32 33554444, metadata !162, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [src] [line 12]
!318 = metadata !{i32 786689, metadata !310, metadata !"count", metadata !311, i32 50331660, metadata !314, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 12]
!319 = metadata !{i32 786688, metadata !310, metadata !"a", metadata !311, i32 13, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 13]
!320 = metadata !{i32 786688, metadata !310, metadata !"b", metadata !311, i32 14, metadata !20, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [b] [line 14]
!321 = metadata !{i32 786449, metadata !322, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !323, metadata !2, metadata !2, metadata !""} 
!322 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/mempcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!323 = metadata !{metadata !324}
!324 = metadata !{i32 786478, metadata !322, metadata !325, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 11, metadata !326, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !329, 
!325 = metadata !{i32 786473, metadata !322}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/mempcpy.c]
!326 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !327, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!327 = metadata !{metadata !38, metadata !38, metadata !162, metadata !328}
!328 = metadata !{i32 786454, metadata !322, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!329 = metadata !{metadata !330, metadata !331, metadata !332, metadata !333, metadata !334}
!330 = metadata !{i32 786689, metadata !324, metadata !"destaddr", metadata !325, i32 16777227, metadata !38, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 11]
!331 = metadata !{i32 786689, metadata !324, metadata !"srcaddr", metadata !325, i32 33554443, metadata !162, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 11]
!332 = metadata !{i32 786689, metadata !324, metadata !"len", metadata !325, i32 50331659, metadata !328, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 11]
!333 = metadata !{i32 786688, metadata !324, metadata !"dest", metadata !325, i32 12, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 12]
!334 = metadata !{i32 786688, metadata !324, metadata !"src", metadata !325, i32 13, metadata !20, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 13]
!335 = metadata !{i32 786449, metadata !336, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !337, metadata !2, metadata !2, metadata !""} 
!336 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memset.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!337 = metadata !{metadata !338}
!338 = metadata !{i32 786478, metadata !336, metadata !339, metadata !"memset", metadata !"memset", metadata !"", i32 11, metadata !340, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i32, i64)* @memset, null, null, metadata !343, i32
!339 = metadata !{i32 786473, metadata !336}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memset.c]
!340 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !341, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!341 = metadata !{metadata !38, metadata !38, metadata !8, metadata !342}
!342 = metadata !{i32 786454, metadata !336, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !56} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!343 = metadata !{metadata !344, metadata !345, metadata !346, metadata !347}
!344 = metadata !{i32 786689, metadata !338, metadata !"dst", metadata !339, i32 16777227, metadata !38, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 11]
!345 = metadata !{i32 786689, metadata !338, metadata !"s", metadata !339, i32 33554443, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [s] [line 11]
!346 = metadata !{i32 786689, metadata !338, metadata !"count", metadata !339, i32 50331659, metadata !342, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 11]
!347 = metadata !{i32 786688, metadata !338, metadata !"a", metadata !339, i32 12, metadata !348, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 12]
!348 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !349} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!349 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !17} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from char]
!350 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!351 = metadata !{i32 1, metadata !"Debug Info Version", i32 1}
!352 = metadata !{metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)"}
!353 = metadata !{i32 9, i32 0, metadata !4, null}
!354 = metadata !{i32 10, i32 0, metadata !4, null}
!355 = metadata !{i32 12, i32 0, metadata !4, null}
!356 = metadata !{i32 13, i32 0, metadata !4, null}
!357 = metadata !{i32 15, i32 0, metadata !4, null}
!358 = metadata !{i32 20, i32 0, metadata !12, null}
!359 = metadata !{i32 27, i32 0, metadata !12, null}
!360 = metadata !{i32 30, i32 0, metadata !12, null}
!361 = metadata !{i32 191, i32 0, metadata !362, null}
!362 = metadata !{i32 786443, metadata !23, metadata !25, i32 191, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!363 = metadata !{i32 193, i32 0, metadata !25, null}
!364 = metadata !{i32 197, i32 0, metadata !25, null}
!365 = metadata !{i32 238, i32 0, metadata !366, null}
!366 = metadata !{i32 786443, metadata !23, metadata !25, i32 238, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!367 = metadata !{i32 239, i32 0, metadata !366, null}
!368 = metadata !{i32 240, i32 0, metadata !25, null}
!369 = metadata !{i32 263, i32 0, metadata !370, null}
!370 = metadata !{i32 786443, metadata !23, metadata !29, i32 263, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!371 = metadata !{i32 264, i32 0, metadata !370, null}
!372 = metadata !{i32 266, i32 0, metadata !373, null}
!373 = metadata !{i32 786443, metadata !23, metadata !29, i32 266, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!374 = metadata !{i32 267, i32 0, metadata !373, null}
!375 = metadata !{i32 268, i32 0, metadata !29, null}
!376 = metadata !{i32 288, i32 0, metadata !30, null}
!377 = metadata !{i32 291, i32 0, metadata !30, null}
!378 = metadata !{i32 294, i32 0, metadata !30, null}
!379 = metadata !{i32 298, i32 0, metadata !380, null}
!380 = metadata !{i32 786443, metadata !23, metadata !30, i32 298, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!381 = metadata !{i32 300, i32 0, metadata !382, null}
!382 = metadata !{i32 786443, metadata !23, metadata !380, i32 298, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!383 = metadata !{i32 301, i32 0, metadata !382, null}
!384 = metadata !{i32 305, i32 0, metadata !30, null}
!385 = metadata !{i32 306, i32 0, metadata !30, null}
!386 = metadata !{i32 307, i32 0, metadata !30, null}
!387 = metadata !{i32 308, i32 0, metadata !388, null}
!388 = metadata !{i32 786443, metadata !23, metadata !30, i32 307, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!389 = metadata !{i32 311, i32 0, metadata !30, null}
!390 = metadata !{i32 312, i32 0, metadata !391, null}
!391 = metadata !{i32 786443, metadata !23, metadata !30, i32 311, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!392 = metadata !{i32 313, i32 0, metadata !393, null}
!393 = metadata !{i32 786443, metadata !23, metadata !391, i32 313, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!394 = metadata !{i32 314, i32 0, metadata !395, null}
!395 = metadata !{i32 786443, metadata !23, metadata !393, i32 313, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!396 = metadata !{i32 315, i32 0, metadata !395, null}
!397 = metadata !{i32 316, i32 0, metadata !391, null}
!398 = metadata !{i32 317, i32 0, metadata !391, null}
!399 = metadata !{i32 323, i32 0, metadata !30, null}
!400 = metadata !{i32 327, i32 0, metadata !30, null}
!401 = metadata !{i32 331, i32 0, metadata !402, null}
!402 = metadata !{i32 786443, metadata !23, metadata !30, i32 331, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!403 = metadata !{i32 336, i32 0, metadata !404, null}
!404 = metadata !{i32 786443, metadata !23, metadata !402, i32 335, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!405 = metadata !{i32 337, i32 0, metadata !404, null}
!406 = metadata !{i32 338, i32 0, metadata !404, null}
!407 = metadata !{i32 339, i32 0, metadata !404, null}
!408 = metadata !{i32 342, i32 0, metadata !30, null}
!409 = metadata !{i32 354, i32 0, metadata !30, null}
!410 = metadata !{i32 370, i32 0, metadata !411, null}
!411 = metadata !{i32 786443, metadata !23, metadata !30, i32 370, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!412 = metadata !{i32 371, i32 0, metadata !413, null}
!413 = metadata !{i32 786443, metadata !23, metadata !411, i32 370, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!414 = metadata !{i32 372, i32 0, metadata !413, null}
!415 = metadata !{i32 391, i32 0, metadata !416, null}
!416 = metadata !{i32 786443, metadata !23, metadata !30, i32 391, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!417 = metadata !{i32 392, i32 0, metadata !416, null}
!418 = metadata !{i32 395, i32 0, metadata !419, null}
!419 = metadata !{i32 786443, metadata !23, metadata !30, i32 395, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!420 = metadata !{i32 396, i32 0, metadata !419, null}
!421 = metadata !{i32 401, i32 0, metadata !30, null}
!422 = metadata !{i32 160, i32 0, metadata !423, null}
!423 = metadata !{i32 786443, metadata !23, metadata !49} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!424 = metadata !{i32 161, i32 0, metadata !423, null}
!425 = metadata !{i32 162, i32 0, metadata !423, null}
!426 = metadata !{i32 163, i32 0, metadata !423, null}
!427 = metadata !{i32 165, i32 0, metadata !428, null}
!428 = metadata !{i32 786443, metadata !23, metadata !423, i32 165, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!429 = metadata !{i32 166, i32 0, metadata !430, null}
!430 = metadata !{i32 786443, metadata !23, metadata !428, i32 165, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!431 = metadata !{i32 168, i32 0, metadata !423, null}
!432 = metadata !{i32 169, i32 0, metadata !423, null}
!433 = metadata !{i32 139, i32 0, metadata !434, null}
!434 = metadata !{i32 786443, metadata !23, metadata !39, i32 139, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!435 = metadata !{i32 143, i32 0, metadata !436, null}
!436 = metadata !{i32 786443, metadata !23, metadata !434, i32 140, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!437 = metadata !{i32 147, i32 0, metadata !438, null}
!438 = metadata !{i32 786443, metadata !23, metadata !436, i32 147, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!439 = metadata !{i32 148, i32 18, metadata !438, null}
!440 = metadata !{i32 150, i32 0, metadata !441, null}
!441 = metadata !{i32 786443, metadata !23, metadata !438, i32 149, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!442 = metadata !{i32 153, i32 0, metadata !39, null}
!443 = metadata !{i32 56, i32 0, metadata !444, null}
!444 = metadata !{i32 786443, metadata !43, metadata !42} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!445 = metadata !{i32 13, i32 0, metadata !63, null}
!446 = metadata !{i32 12, i32 0, metadata !71, null}
!447 = metadata !{i32 258, i32 0, metadata !448, null}
!448 = metadata !{i32 786443, metadata !74, metadata !76, i32 258, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!449 = metadata !{i32 261, i32 0, metadata !450, null}
!450 = metadata !{i32 786443, metadata !74, metadata !451, i32 261, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!451 = metadata !{i32 786443, metadata !74, metadata !448, i32 258, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!452 = metadata !{i32 262, i32 0, metadata !453, null}
!453 = metadata !{i32 786443, metadata !74, metadata !450, i32 261, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!454 = metadata !{i32 263, i32 0, metadata !453, null}
!455 = metadata !{i32 274, i32 0, metadata !76, null}
!456 = metadata !{i32 280, i32 0, metadata !78, null}
!457 = metadata !{i32 282, i32 0, metadata !78, null}
!458 = metadata !{i32 283, i32 0, metadata !78, null}
!459 = metadata !{i32 284, i32 0, metadata !78, null}
!460 = metadata !{i32 291, i32 0, metadata !78, null}
!461 = metadata !{i32 23, i32 0, metadata !462, null}
!462 = metadata !{i32 786443, metadata !123, metadata !125, i32 23, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!463 = metadata !{i32 24, i32 0, metadata !464, null}
!464 = metadata !{i32 786443, metadata !123, metadata !462, i32 23, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!465 = metadata !{i32 25, i32 0, metadata !464, null}
!466 = metadata !{i32 26, i32 0, metadata !464, null}
!467 = metadata !{i32 28, i32 0, metadata !125, null}
!468 = metadata !{i32 20, i32 0, metadata !156, null}
!469 = metadata !{i32 21, i32 0, metadata !156, null}
!470 = metadata !{i32 28, i32 0, metadata !156, null}
!471 = metadata !{i32 29, i32 0, metadata !472, null}
!472 = metadata !{i32 786443, metadata !154, metadata !156, i32 28, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!473 = metadata !{i32 30, i32 0, metadata !472, null}
!474 = metadata !{i32 31, i32 0, metadata !472, null}
!475 = metadata !{i32 34, i32 0, metadata !156, null}
!476 = metadata !{i32 19, i32 0, metadata !168, null}
!477 = metadata !{i32 27, i32 0, metadata !168, null}
!478 = metadata !{i32 28, i32 0, metadata !479, null}
!479 = metadata !{i32 786443, metadata !166, metadata !168, i32 27, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!480 = metadata !{i32 29, i32 0, metadata !479, null}
!481 = metadata !{i32 30, i32 0, metadata !479, null}
!482 = metadata !{i32 32, i32 0, metadata !168, null}
!483 = metadata !{i32 30, i32 0, metadata !176, null}
!484 = metadata !{i32 43, i32 0, metadata !183, null}
!485 = metadata !{i32 45, i32 0, metadata !183, null}
!486 = metadata !{i32 46, i32 0, metadata !183, null}
!487 = metadata !{i32 47, i32 0, metadata !183, null}
!488 = metadata !{i32 48, i32 0, metadata !183, null}
!489 = metadata !{i32 49, i32 0, metadata !183, null}
!490 = metadata !{i32 61, i32 0, metadata !491, null}
!491 = metadata !{i32 786443, metadata !181, metadata !492, i32 60, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!492 = metadata !{i32 786443, metadata !181, metadata !183, i32 58, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!493 = metadata !{i32 79, i32 0, metadata !183, null}
!494 = metadata !{i32 44, i32 0, metadata !214, null}
!495 = metadata !{i32 46, i32 0, metadata !214, null}
!496 = metadata !{i32 47, i32 0, metadata !497, null}
!497 = metadata !{i32 786443, metadata !212, metadata !498, i32 47, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!498 = metadata !{i32 786443, metadata !212, metadata !214, i32 46, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!499 = metadata !{i32 49, i32 0, metadata !500, null}
!500 = metadata !{i32 786443, metadata !212, metadata !497, i32 47, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!501 = metadata !{i32 51, i32 0, metadata !498, null}
!502 = metadata !{i32 52, i32 0, metadata !503, null}
!503 = metadata !{i32 786443, metadata !212, metadata !498, i32 52, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!504 = metadata !{i32 62, i32 0, metadata !505, null}
!505 = metadata !{i32 786443, metadata !212, metadata !503, i32 52, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!506 = metadata !{i32 63, i32 0, metadata !505, null}
!507 = metadata !{i32 101, i32 0, metadata !498, null}
!508 = metadata !{i32 70, i32 0, metadata !509, null}
!509 = metadata !{i32 786443, metadata !212, metadata !503, i32 69, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!510 = metadata !{i32 73, i32 0, metadata !511, null}
!511 = metadata !{i32 786443, metadata !212, metadata !509, i32 73, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!512 = metadata !{i32 76, i32 0, metadata !513, null}
!513 = metadata !{i32 786443, metadata !212, metadata !514, i32 76, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!514 = metadata !{i32 786443, metadata !212, metadata !511, i32 73, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!515 = metadata !{i32 77, i32 0, metadata !516, null}
!516 = metadata !{i32 786443, metadata !212, metadata !513, i32 76, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!517 = metadata !{i32 78, i32 0, metadata !516, null}
!518 = metadata !{i32 80, i32 0, metadata !514, null}
!519 = metadata !{i32 82, i32 0, metadata !514, null}
!520 = metadata !{i32 83, i32 0, metadata !521, null}
!521 = metadata !{i32 786443, metadata !212, metadata !522, i32 83, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!522 = metadata !{i32 786443, metadata !212, metadata !514, i32 82, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!523 = metadata !{i32 88, i32 0, metadata !522, null}
!524 = metadata !{i32 89, i32 0, metadata !522, null}
!525 = metadata !{i32 90, i32 0, metadata !522, null}
!526 = metadata !{i32 92, i32 0, metadata !514, null}
!527 = metadata !{i32 94, i32 0, metadata !514, null}
!528 = metadata !{i32 95, i32 0, metadata !514, null}
!529 = metadata !{i32 99, i32 0, metadata !509, null}
!530 = metadata !{i32 102, i32 0, metadata !214, null}
!531 = metadata !{i32 22, i32 0, metadata !246, null}
!532 = metadata !{i32 23, i32 0, metadata !246, null}
!533 = metadata !{i32 30, i32 0, metadata !246, null}
!534 = metadata !{i32 31, i32 0, metadata !535, null}
!535 = metadata !{i32 786443, metadata !244, metadata !246, i32 30, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!536 = metadata !{i32 32, i32 0, metadata !535, null}
!537 = metadata !{i32 33, i32 0, metadata !535, null}
!538 = metadata !{i32 36, i32 0, metadata !246, null}
!539 = metadata !{i32 13, i32 0, metadata !540, null}
!540 = metadata !{i32 786443, metadata !252, metadata !254, i32 13, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!541 = metadata !{i32 14, i32 0, metadata !540, null}
!542 = metadata !{i32 15, i32 0, metadata !254, null}
!543 = metadata !{i32 15, i32 0, metadata !264, null}
!544 = metadata !{i32 16, i32 0, metadata !264, null}
!545 = metadata !{metadata !546, metadata !546, i64 0}
!546 = metadata !{metadata !"int", metadata !547, i64 0}
!547 = metadata !{metadata !"omnipotent char", metadata !548, i64 0}
!548 = metadata !{metadata !"Simple C/C++ TBAA"}
!549 = metadata !{i32 21, i32 0, metadata !550, null}
!550 = metadata !{i32 786443, metadata !272, metadata !274, i32 21, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!551 = metadata !{i32 27, i32 0, metadata !552, null}
!552 = metadata !{i32 786443, metadata !272, metadata !550, i32 21, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!553 = metadata !{i32 29, i32 0, metadata !274, null}
!554 = metadata !{i32 16, i32 0, metadata !555, null}
!555 = metadata !{i32 786443, metadata !282, metadata !284, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!556 = metadata !{i32 17, i32 0, metadata !555, null}
!557 = metadata !{i32 19, i32 0, metadata !558, null}
!558 = metadata !{i32 786443, metadata !282, metadata !284, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!559 = metadata !{i32 22, i32 0, metadata !560, null}
!560 = metadata !{i32 786443, metadata !282, metadata !558, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!561 = metadata !{i32 25, i32 0, metadata !562, null}
!562 = metadata !{i32 786443, metadata !282, metadata !560, i32 25, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!563 = metadata !{i32 26, i32 0, metadata !564, null}
!564 = metadata !{i32 786443, metadata !282, metadata !562, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!565 = metadata !{i32 27, i32 0, metadata !564, null}
!566 = metadata !{i32 28, i32 0, metadata !567, null}
!567 = metadata !{i32 786443, metadata !282, metadata !562, i32 27, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!568 = metadata !{i32 29, i32 0, metadata !567, null}
!569 = metadata !{i32 32, i32 0, metadata !560, null}
!570 = metadata !{i32 34, i32 0, metadata !284, null}
!571 = metadata !{i32 16, i32 0, metadata !572, null}
!572 = metadata !{i32 786443, metadata !308, metadata !310, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!573 = metadata !{i32 19, i32 0, metadata !574, null}
!574 = metadata !{i32 786443, metadata !308, metadata !310, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!575 = metadata !{i32 20, i32 0, metadata !576, null}
!576 = metadata !{i32 786443, metadata !308, metadata !574, i32 19, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!577 = metadata !{metadata !577, metadata !578, metadata !579}
!578 = metadata !{metadata !"llvm.vectorizer.width", i32 1}
!579 = metadata !{metadata !"llvm.vectorizer.unroll", i32 1}
!580 = metadata !{metadata !547, metadata !547, i64 0}
!581 = metadata !{metadata !581, metadata !578, metadata !579}
!582 = metadata !{i32 22, i32 0, metadata !583, null}
!583 = metadata !{i32 786443, metadata !308, metadata !574, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!584 = metadata !{i32 24, i32 0, metadata !583, null}
!585 = metadata !{i32 23, i32 0, metadata !583, null}
!586 = metadata !{metadata !586, metadata !578, metadata !579}
!587 = metadata !{metadata !587, metadata !578, metadata !579}
!588 = metadata !{i32 28, i32 0, metadata !310, null}
