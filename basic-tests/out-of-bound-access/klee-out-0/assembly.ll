; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct.__STDIO_FILE_STRUCT.273 = type { i16, [2 x i8], i32, i8*, i8*, i8*, i8*, i8*, i8*, %struct.__STDIO_FILE_STRUCT.273*, [2 x i32], %struct.__mbstate_t.272 }
%struct.__mbstate_t.272 = type { i32, i32 }
%struct.Elf64_auxv_t = type { i64, %union.anon.645 }
%union.anon.645 = type { i64 }
%struct.stat.644 = type { i64, i64, i64, i32, i32, i32, i32, i64, i64, i64, i64, i64, i64, i64, i64, i64, i64, [3 x i64] }
%struct.termios.442 = type { i32, i32, i32, i32, i8, [32 x i8], i32, i32 }
%struct.__kernel_termios = type { i32, i32, i32, i32, i8, [19 x i8] }

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str1 = private unnamed_addr constant [2 x i8] c"x\00", align 1
@.str2 = private unnamed_addr constant [2 x i8] c"y\00", align 1
@.str3 = private unnamed_addr constant [2 x i8] c"z\00", align 1
@__libc_stack_end = global i8* null, align 8
@.str4 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@__uclibc_progname = hidden global i8* getelementptr inbounds ([1 x i8]* @.str4, i32 0, i32 0), align 8
@__environ = global i8** null, align 8
@__pagesize = global i64 0, align 8
@__uClibc_init.been_there_done_that = internal global i32 0, align 4
@__app_fini = hidden global void ()* null, align 8
@__rtld_fini = hidden global void ()* null, align 8
@.str15 = private unnamed_addr constant [10 x i8] c"/dev/null\00", align 1
@_stdio_streams = internal global [3 x %struct.__STDIO_FILE_STRUCT.273] [%struct.__STDIO_FILE_STRUCT.273 { i16 544, [2 x i8] zeroinitializer, i32 0, i8* null, i8* null, i8* null, i8* null, i8* null, i8* null, %struct.__STDIO_FILE_STRUCT.273* bitcast (i8*
@stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@stderr = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 160) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@__stdin = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@__stdout = global %struct.__STDIO_FILE_STRUCT.273* bitcast (i8* getelementptr (i8* bitcast ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams to i8*), i64 80) to %struct.__STDIO_FILE_STRUCT.273*), align 8
@_stdio_openlist = global %struct.__STDIO_FILE_STRUCT.273* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i32 0), align 8
@errno = global i32 0, align 4
@h_errno = global i32 0, align 4
@.str32 = private unnamed_addr constant [60 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c\00", align 1
@.str133 = private unnamed_addr constant [15 x i8] c"divide by zero\00", align 1
@.str234 = private unnamed_addr constant [8 x i8] c"div.err\00", align 1
@.str335 = private unnamed_addr constant [8 x i8] c"IGNORED\00", align 1
@.str14 = private unnamed_addr constant [16 x i8] c"overshift error\00", align 1
@.str25 = private unnamed_addr constant [14 x i8] c"overshift.err\00", align 1
@.str6 = private unnamed_addr constant [51 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_range.c\00", align 1
@.str17 = private unnamed_addr constant [14 x i8] c"invalid range\00", align 1
@.str28 = private unnamed_addr constant [5 x i8] c"user\00", align 1

@environ = alias weak i8*** @__environ

; Function Attrs: nounwind uwtable
define i32 @__user_main() #0 {
  %1 = alloca i32, align 4
  %a = alloca [64 x i32], align 16
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %z = alloca i32, align 4
  store i32 0, i32* %1
  %2 = getelementptr inbounds [64 x i32]* %a, i32 0, i32 0, !dbg !344
  %3 = call i32 (i32*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i32*, i64, i8*, ...)*)(i32* %2, i64 256, i8* getelementptr inbounds ([2 x i8]* @.str, i32 0, i32 0)), !dbg !344
  %4 = call i32 (i32*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i32*, i64, i8*, ...)*)(i32* %x, i64 4, i8* getelementptr inbounds ([2 x i8]* @.str1, i32 0, i32 0)), !dbg !345
  %5 = call i32 (i32*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i32*, i64, i8*, ...)*)(i32* %y, i64 4, i8* getelementptr inbounds ([2 x i8]* @.str2, i32 0, i32 0)), !dbg !346
  %6 = call i32 (i32*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i32*, i64, i8*, ...)*)(i32* %z, i64 4, i8* getelementptr inbounds ([2 x i8]* @.str3, i32 0, i32 0)), !dbg !347
  %7 = load i32* %z, align 4, !dbg !348
  %8 = load i32* %x, align 4, !dbg !348
  %9 = load i32* %y, align 4, !dbg !348
  %10 = add nsw i32 %8, %9, !dbg !348
  %11 = sext i32 %10 to i64, !dbg !348
  %12 = getelementptr inbounds [64 x i32]* %a, i32 0, i64 %11, !dbg !348
  store i32 %7, i32* %12, align 4, !dbg !348
  ret i32 0, !dbg !349
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

declare i32 @klee_make_symbolic(...) #2

; Function Attrs: nounwind uwtable
define void @__uClibc_init() #0 {
  %1 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !350
  %2 = icmp ne i32 %1, 0, !dbg !350
  br i1 %2, label %8, label %3, !dbg !350

; <label>:3                                       ; preds = %0
  %4 = load i32* @__uClibc_init.been_there_done_that, align 4, !dbg !352
  %5 = add nsw i32 %4, 1, !dbg !352
  store i32 %5, i32* @__uClibc_init.been_there_done_that, align 4, !dbg !352
  store i64 4096, i64* @__pagesize, align 8, !dbg !353
  %6 = icmp ne i64 1, 0, !dbg !354
  br i1 %6, label %7, label %8, !dbg !354

; <label>:7                                       ; preds = %3
  call void @_stdio_init() #11, !dbg !356
  br label %8, !dbg !356

; <label>:8                                       ; preds = %0, %7, %3
  ret void, !dbg !357
}

; Function Attrs: nounwind readnone
declare i64 @llvm.expect.i64(i64, i64) #1

; Function Attrs: nounwind uwtable
define void @__uClibc_fini() #0 {
  %1 = load void ()** @__app_fini, align 8, !dbg !358
  %2 = icmp ne void ()* %1, null, !dbg !358
  br i1 %2, label %3, label %5, !dbg !358

; <label>:3                                       ; preds = %0
  %4 = load void ()** @__app_fini, align 8, !dbg !360
  call void %4() #11, !dbg !360
  br label %5, !dbg !360

; <label>:5                                       ; preds = %3, %0
  %6 = load void ()** @__rtld_fini, align 8, !dbg !361
  %7 = icmp ne void ()* %6, null, !dbg !361
  br i1 %7, label %8, label %10, !dbg !361

; <label>:8                                       ; preds = %5
  %9 = load void ()** @__rtld_fini, align 8, !dbg !363
  call void %9() #11, !dbg !363
  br label %10, !dbg !363

; <label>:10                                      ; preds = %8, %5
  ret void, !dbg !364
}

; Function Attrs: noreturn nounwind uwtable
define void @__uClibc_main(i32 (i32, i8**, i8**)* %main, i32 %argc, i8** %argv, void ()* %app_init, void ()* %app_fini, void ()* %rtld_fini, i8* %stack_end) #3 {
  %1 = alloca i32 (i32, i8**, i8**)*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i8**, align 8
  %4 = alloca void ()*, align 8
  %5 = alloca void ()*, align 8
  %6 = alloca void ()*, align 8
  %7 = alloca i8*, align 8
  %aux_dat = alloca i64*, align 8
  %auxvt = alloca [15 x %struct.Elf64_auxv_t], align 16
  %auxv_entry = alloca %struct.Elf64_auxv_t*, align 8
  store i32 (i32, i8**, i8**)* %main, i32 (i32, i8**, i8**)** %1, align 8
  store i32 %argc, i32* %2, align 4
  store i8** %argv, i8*** %3, align 8
  store void ()* %app_init, void ()** %4, align 8
  store void ()* %app_fini, void ()** %5, align 8
  store void ()* %rtld_fini, void ()** %6, align 8
  store i8* %stack_end, i8** %7, align 8
  %8 = load i8** %7, align 8, !dbg !365
  store i8* %8, i8** @__libc_stack_end, align 8, !dbg !365
  %9 = load void ()** %6, align 8, !dbg !366
  store void ()* %9, void ()** @__rtld_fini, align 8, !dbg !366
  %10 = load i32* %2, align 4, !dbg !367
  %11 = add nsw i32 %10, 1, !dbg !367
  %12 = sext i32 %11 to i64, !dbg !367
  %13 = load i8*** %3, align 8, !dbg !367
  %14 = getelementptr inbounds i8** %13, i64 %12, !dbg !367
  store i8** %14, i8*** @__environ, align 8, !dbg !367
  %15 = load i8*** @__environ, align 8, !dbg !368
  %16 = bitcast i8** %15 to i8*, !dbg !368
  %17 = load i8*** %3, align 8, !dbg !368
  %18 = load i8** %17, align 8, !dbg !368
  %19 = icmp eq i8* %16, %18, !dbg !368
  br i1 %19, label %20, label %25, !dbg !368

; <label>:20                                      ; preds = %0
  %21 = load i32* %2, align 4, !dbg !370
  %22 = sext i32 %21 to i64, !dbg !370
  %23 = load i8*** %3, align 8, !dbg !370
  %24 = getelementptr inbounds i8** %23, i64 %22, !dbg !370
  store i8** %24, i8*** @__environ, align 8, !dbg !370
  br label %25, !dbg !372

; <label>:25                                      ; preds = %20, %0
  %26 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i32 0, !dbg !373
  %27 = bitcast %struct.Elf64_auxv_t* %26 to i8*, !dbg !373
  %28 = call i8* @memset(i8* %27, i32 0, i64 240) #12, !dbg !373
  %29 = load i8*** @__environ, align 8, !dbg !374
  %30 = bitcast i8** %29 to i64*, !dbg !374
  store i64* %30, i64** %aux_dat, align 8, !dbg !374
  br label %31, !dbg !375

; <label>:31                                      ; preds = %31, %25
  %32 = load i64** %aux_dat, align 8, !dbg !375
  %33 = load i64* %32, align 8, !dbg !375
  %34 = icmp ne i64 %33, 0, !dbg !375
  %35 = load i64** %aux_dat, align 8, !dbg !376
  %36 = getelementptr inbounds i64* %35, i32 1, !dbg !376
  store i64* %36, i64** %aux_dat, align 8, !dbg !376
  br i1 %34, label %31, label %37, !dbg !375

; <label>:37                                      ; preds = %31, %57
  %38 = load i64** %aux_dat, align 8, !dbg !378
  %39 = load i64* %38, align 8, !dbg !378
  %40 = icmp ne i64 %39, 0, !dbg !378
  br i1 %40, label %41, label %60, !dbg !378

; <label>:41                                      ; preds = %37
  %42 = load i64** %aux_dat, align 8, !dbg !379
  %43 = bitcast i64* %42 to %struct.Elf64_auxv_t*, !dbg !379
  store %struct.Elf64_auxv_t* %43, %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !379
  %44 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !381
  %45 = getelementptr inbounds %struct.Elf64_auxv_t* %44, i32 0, i32 0, !dbg !381
  %46 = load i64* %45, align 8, !dbg !381
  %47 = icmp ule i64 %46, 14, !dbg !381
  br i1 %47, label %48, label %57, !dbg !381

; <label>:48                                      ; preds = %41
  %49 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !383
  %50 = getelementptr inbounds %struct.Elf64_auxv_t* %49, i32 0, i32 0, !dbg !383
  %51 = load i64* %50, align 8, !dbg !383
  %52 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 %51, !dbg !383
  %53 = bitcast %struct.Elf64_auxv_t* %52 to i8*, !dbg !383
  %54 = load %struct.Elf64_auxv_t** %auxv_entry, align 8, !dbg !383
  %55 = bitcast %struct.Elf64_auxv_t* %54 to i8*, !dbg !383
  %56 = call i8* @memcpy(i8* %53, i8* %55, i64 16) #12, !dbg !383
  br label %57, !dbg !385

; <label>:57                                      ; preds = %48, %41
  %58 = load i64** %aux_dat, align 8, !dbg !386
  %59 = getelementptr inbounds i64* %58, i64 2, !dbg !386
  store i64* %59, i64** %aux_dat, align 8, !dbg !386
  br label %37, !dbg !387

; <label>:60                                      ; preds = %37
  call void @__uClibc_init() #11, !dbg !388
  %61 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !389
  %62 = getelementptr inbounds %struct.Elf64_auxv_t* %61, i32 0, i32 1, !dbg !389
  %63 = bitcast %union.anon.645* %62 to i64*, !dbg !389
  %64 = load i64* %63, align 8, !dbg !389
  %65 = icmp ne i64 %64, 0, !dbg !389
  br i1 %65, label %66, label %71, !dbg !389

; <label>:66                                      ; preds = %60
  %67 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 6, !dbg !389
  %68 = getelementptr inbounds %struct.Elf64_auxv_t* %67, i32 0, i32 1, !dbg !389
  %69 = bitcast %union.anon.645* %68 to i64*, !dbg !389
  %70 = load i64* %69, align 8, !dbg !389
  br label %71, !dbg !389

; <label>:71                                      ; preds = %60, %66
  %72 = phi i64 [ %70, %66 ], [ 4096, %60 ], !dbg !389
  store i64 %72, i64* @__pagesize, align 8, !dbg !389
  %73 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !390
  %74 = getelementptr inbounds %struct.Elf64_auxv_t* %73, i32 0, i32 1, !dbg !390
  %75 = bitcast %union.anon.645* %74 to i64*, !dbg !390
  %76 = load i64* %75, align 8, !dbg !390
  %77 = icmp eq i64 %76, -1, !dbg !390
  br i1 %77, label %78, label %81, !dbg !390

; <label>:78                                      ; preds = %71
  %79 = call i32 @__check_suid() #11, !dbg !390
  %80 = icmp ne i32 %79, 0, !dbg !390
  br i1 %80, label %107, label %81, !dbg !390

; <label>:81                                      ; preds = %78, %71
  %82 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !390
  %83 = getelementptr inbounds %struct.Elf64_auxv_t* %82, i32 0, i32 1, !dbg !390
  %84 = bitcast %union.anon.645* %83 to i64*, !dbg !390
  %85 = load i64* %84, align 8, !dbg !390
  %86 = icmp ne i64 %85, -1, !dbg !390
  br i1 %86, label %87, label %108, !dbg !390

; <label>:87                                      ; preds = %81
  %88 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 11, !dbg !390
  %89 = getelementptr inbounds %struct.Elf64_auxv_t* %88, i32 0, i32 1, !dbg !390
  %90 = bitcast %union.anon.645* %89 to i64*, !dbg !390
  %91 = load i64* %90, align 8, !dbg !390
  %92 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 12, !dbg !390
  %93 = getelementptr inbounds %struct.Elf64_auxv_t* %92, i32 0, i32 1, !dbg !390
  %94 = bitcast %union.anon.645* %93 to i64*, !dbg !390
  %95 = load i64* %94, align 8, !dbg !390
  %96 = icmp ne i64 %91, %95, !dbg !390
  br i1 %96, label %107, label %97, !dbg !390

; <label>:97                                      ; preds = %87
  %98 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 13, !dbg !390
  %99 = getelementptr inbounds %struct.Elf64_auxv_t* %98, i32 0, i32 1, !dbg !390
  %100 = bitcast %union.anon.645* %99 to i64*, !dbg !390
  %101 = load i64* %100, align 8, !dbg !390
  %102 = getelementptr inbounds [15 x %struct.Elf64_auxv_t]* %auxvt, i32 0, i64 14, !dbg !390
  %103 = getelementptr inbounds %struct.Elf64_auxv_t* %102, i32 0, i32 1, !dbg !390
  %104 = bitcast %union.anon.645* %103 to i64*, !dbg !390
  %105 = load i64* %104, align 8, !dbg !390
  %106 = icmp ne i64 %101, %105, !dbg !390
  br i1 %106, label %107, label %108, !dbg !390

; <label>:107                                     ; preds = %97, %87, %78
  call void @__check_one_fd(i32 0, i32 131072) #11, !dbg !392
  call void @__check_one_fd(i32 1, i32 131074) #11, !dbg !394
  call void @__check_one_fd(i32 2, i32 131074) #11, !dbg !395
  br label %108, !dbg !396

; <label>:108                                     ; preds = %107, %97, %81
  %109 = load i8*** %3, align 8, !dbg !397
  %110 = load i8** %109, align 8, !dbg !397
  store i8* %110, i8** @__uclibc_progname, align 8, !dbg !397
  %111 = load void ()** %5, align 8, !dbg !398
  store void ()* %111, void ()** @__app_fini, align 8, !dbg !398
  %112 = load void ()** %4, align 8, !dbg !399
  %113 = icmp ne void ()* %112, null, !dbg !399
  br i1 %113, label %114, label %116, !dbg !399

; <label>:114                                     ; preds = %108
  %115 = load void ()** %4, align 8, !dbg !401
  call void %115() #11, !dbg !401
  br label %116, !dbg !403

; <label>:116                                     ; preds = %114, %108
  %117 = icmp ne i64 1, 0, !dbg !404
  br i1 %117, label %118, label %120, !dbg !404

; <label>:118                                     ; preds = %116
  %119 = call i32* @__errno_location() #13, !dbg !406
  store i32 0, i32* %119, align 4, !dbg !406
  br label %120, !dbg !406

; <label>:120                                     ; preds = %118, %116
  %121 = icmp ne i64 1, 0, !dbg !407
  br i1 %121, label %122, label %124, !dbg !407

; <label>:122                                     ; preds = %120
  %123 = call i32* @__h_errno_location() #13, !dbg !409
  store i32 0, i32* %123, align 4, !dbg !409
  br label %124, !dbg !409

; <label>:124                                     ; preds = %122, %120
  %125 = load i32 (i32, i8**, i8**)** %1, align 8, !dbg !410
  %126 = load i32* %2, align 4, !dbg !410
  %127 = load i8*** %3, align 8, !dbg !410
  %128 = load i8*** @__environ, align 8, !dbg !410
  %129 = call i32 %125(i32 %126, i8** %127, i8** %128) #11, !dbg !410
  call void @exit(i32 %129) #14, !dbg !410
  unreachable, !dbg !410
}

; Function Attrs: noreturn nounwind
declare void @exit(i32) #4

declare i32 @fcntl(i32, i32, ...) #2

declare i32 @open(i8*, i32, ...) #2

; Function Attrs: nounwind
declare i32 @fstat(i32, %struct.stat.644*) #5

; Function Attrs: noreturn nounwind
declare void @abort() #4

; Function Attrs: nounwind
declare i32 @getuid() #5

; Function Attrs: nounwind
declare i32 @geteuid() #5

; Function Attrs: nounwind
declare i32 @getgid() #5

; Function Attrs: nounwind
declare i32 @getegid() #5

; Function Attrs: nounwind uwtable
define internal i32 @__check_suid() #0 {
  %1 = alloca i32, align 4
  %uid = alloca i32, align 4
  %euid = alloca i32, align 4
  %gid = alloca i32, align 4
  %egid = alloca i32, align 4
  %2 = call i32 @getuid() #12, !dbg !411
  store i32 %2, i32* %uid, align 4, !dbg !411
  %3 = call i32 @geteuid() #12, !dbg !413
  store i32 %3, i32* %euid, align 4, !dbg !413
  %4 = call i32 @getgid() #12, !dbg !414
  store i32 %4, i32* %gid, align 4, !dbg !414
  %5 = call i32 @getegid() #12, !dbg !415
  store i32 %5, i32* %egid, align 4, !dbg !415
  %6 = load i32* %uid, align 4, !dbg !416
  %7 = load i32* %euid, align 4, !dbg !416
  %8 = icmp eq i32 %6, %7, !dbg !416
  br i1 %8, label %9, label %14, !dbg !416

; <label>:9                                       ; preds = %0
  %10 = load i32* %gid, align 4, !dbg !416
  %11 = load i32* %egid, align 4, !dbg !416
  %12 = icmp eq i32 %10, %11, !dbg !416
  br i1 %12, label %13, label %14, !dbg !416

; <label>:13                                      ; preds = %9
  store i32 0, i32* %1, !dbg !418
  br label %15, !dbg !418

; <label>:14                                      ; preds = %9, %0
  store i32 1, i32* %1, !dbg !420
  br label %15, !dbg !420

; <label>:15                                      ; preds = %14, %13
  %16 = load i32* %1, !dbg !421
  ret i32 %16, !dbg !421
}

; Function Attrs: nounwind uwtable
define internal void @__check_one_fd(i32 %fd, i32 %mode) #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %st = alloca %struct.stat.644, align 8
  %nullfd = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store i32 %mode, i32* %2, align 4
  %3 = load i32* %1, align 4, !dbg !422
  %4 = call i32 (i32, i32, ...)* @fcntl(i32 %3, i32 1) #11, !dbg !422
  %5 = icmp eq i32 %4, -1, !dbg !422
  br i1 %5, label %6, label %10, !dbg !422

; <label>:6                                       ; preds = %0
  %7 = call i32* @__errno_location() #13, !dbg !422
  %8 = load i32* %7, align 4, !dbg !422
  %9 = icmp eq i32 %8, 9, !dbg !422
  br label %10

; <label>:10                                      ; preds = %6, %0
  %11 = phi i1 [ false, %0 ], [ %9, %6 ]
  %12 = xor i1 %11, true
  %13 = xor i1 %12, true
  %14 = zext i1 %13 to i32
  %15 = sext i32 %14 to i64
  %16 = icmp ne i64 %15, 0
  br i1 %16, label %17, label %38

; <label>:17                                      ; preds = %10
  %18 = load i32* %2, align 4, !dbg !424
  %19 = call i32 (i8*, i32, ...)* @open(i8* getelementptr inbounds ([10 x i8]* @.str15, i32 0, i32 0), i32 %18) #11, !dbg !424
  store i32 %19, i32* %nullfd, align 4, !dbg !424
  %20 = load i32* %nullfd, align 4, !dbg !426
  %21 = load i32* %1, align 4, !dbg !426
  %22 = icmp ne i32 %20, %21, !dbg !426
  br i1 %22, label %37, label %23, !dbg !426

; <label>:23                                      ; preds = %17
  %24 = load i32* %1, align 4, !dbg !426
  %25 = call i32 @fstat(i32 %24, %struct.stat.644* %st) #12, !dbg !426
  %26 = icmp ne i32 %25, 0, !dbg !426
  br i1 %26, label %37, label %27, !dbg !426

; <label>:27                                      ; preds = %23
  %28 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 3, !dbg !426
  %29 = load i32* %28, align 4, !dbg !426
  %30 = and i32 %29, 61440, !dbg !426
  %31 = icmp eq i32 %30, 8192, !dbg !426
  br i1 %31, label %32, label %37, !dbg !426

; <label>:32                                      ; preds = %27
  %33 = getelementptr inbounds %struct.stat.644* %st, i32 0, i32 7, !dbg !426
  %34 = load i64* %33, align 8, !dbg !426
  %35 = call i64 @gnu_dev_makedev(i32 1, i32 3) #12, !dbg !428
  %36 = icmp ne i64 %34, %35, !dbg !428
  br i1 %36, label %37, label %38, !dbg !428

; <label>:37                                      ; preds = %32, %27, %23, %17
  call void @abort() #14, !dbg !429
  unreachable, !dbg !429

; <label>:38                                      ; preds = %32, %10
  ret void, !dbg !431
}

; Function Attrs: inlinehint nounwind uwtable
define internal i64 @gnu_dev_makedev(i32 %__major, i32 %__minor) #6 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  store i32 %__major, i32* %1, align 4
  store i32 %__minor, i32* %2, align 4
  %3 = load i32* %2, align 4, !dbg !432
  %4 = and i32 %3, 255, !dbg !432
  %5 = load i32* %1, align 4, !dbg !432
  %6 = and i32 %5, 4095, !dbg !432
  %int_cast_to_i64 = zext i32 8 to i64
  call void @klee_overshift_check(i64 32, i64 %int_cast_to_i64), !dbg !432
  %7 = shl i32 %6, 8, !dbg !432
  %8 = or i32 %4, %7, !dbg !432
  %9 = zext i32 %8 to i64, !dbg !432
  %10 = load i32* %2, align 4, !dbg !432
  %11 = and i32 %10, -256, !dbg !432
  %12 = zext i32 %11 to i64, !dbg !432
  %int_cast_to_i641 = bitcast i64 12 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i641), !dbg !432
  %13 = shl i64 %12, 12, !dbg !432
  %14 = or i64 %9, %13, !dbg !432
  %15 = load i32* %1, align 4, !dbg !432
  %16 = and i32 %15, -4096, !dbg !432
  %17 = zext i32 %16 to i64, !dbg !432
  %int_cast_to_i642 = bitcast i64 32 to i64
  call void @klee_overshift_check(i64 64, i64 %int_cast_to_i642), !dbg !432
  %18 = shl i64 %17, 32, !dbg !432
  %19 = or i64 %14, %18, !dbg !432
  ret i64 %19, !dbg !432
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__errno_location() #7 {
  ret i32* @errno, !dbg !434
}

; Function Attrs: nounwind readnone uwtable
define weak i32* @__h_errno_location() #7 {
  ret i32* @h_errno, !dbg !435
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_term() #0 {
  %ptr = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %1 = load %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, align 8, !dbg !436
  store %struct.__STDIO_FILE_STRUCT.273* %1, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !436
  br label %2, !dbg !436

; <label>:2                                       ; preds = %15, %0
  %3 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !436
  %4 = icmp ne %struct.__STDIO_FILE_STRUCT.273* %3, null, !dbg !436
  br i1 %4, label %5, label %19, !dbg !436

; <label>:5                                       ; preds = %2
  %6 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !438
  %7 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %6, i32 0, i32 0, !dbg !438
  %8 = load i16* %7, align 2, !dbg !438
  %9 = zext i16 %8 to i32, !dbg !438
  %10 = and i32 %9, 64, !dbg !438
  %11 = icmp ne i32 %10, 0, !dbg !438
  br i1 %11, label %12, label %15, !dbg !438

; <label>:12                                      ; preds = %5
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !441
  %14 = call i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* %13) #11, !dbg !441
  br label %15, !dbg !443

; <label>:15                                      ; preds = %5, %12
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !436
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 9, !dbg !436
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %17, align 8, !dbg !436
  store %struct.__STDIO_FILE_STRUCT.273* %18, %struct.__STDIO_FILE_STRUCT.273** %ptr, align 8, !dbg !436
  br label %2, !dbg !436

; <label>:19                                      ; preds = %2
  ret void, !dbg !444
}

; Function Attrs: nounwind uwtable
define hidden void @_stdio_init() #0 {
  %old_errno = alloca i32, align 4
  %1 = load i32* @errno, align 4, !dbg !445
  store i32 %1, i32* %old_errno, align 4, !dbg !445
  %2 = call i32 @isatty(i32 0) #12, !dbg !446
  %3 = sub nsw i32 1, %2, !dbg !446
  %4 = mul i32 %3, 256, !dbg !446
  %5 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !446
  %6 = zext i16 %5 to i32, !dbg !446
  %7 = xor i32 %6, %4, !dbg !446
  %8 = trunc i32 %7 to i16, !dbg !446
  store i16 %8, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 0, i32 0), align 2, !dbg !446
  %9 = call i32 @isatty(i32 1) #12, !dbg !447
  %10 = sub nsw i32 1, %9, !dbg !447
  %11 = mul i32 %10, 256, !dbg !447
  %12 = load i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !447
  %13 = zext i16 %12 to i32, !dbg !447
  %14 = xor i32 %13, %11, !dbg !447
  %15 = trunc i32 %14 to i16, !dbg !447
  store i16 %15, i16* getelementptr inbounds ([3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, i32 0, i64 1, i32 0), align 2, !dbg !447
  %16 = load i32* %old_errno, align 4, !dbg !448
  store i32 %16, i32* @errno, align 4, !dbg !448
  ret void, !dbg !449
}

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_wcommit(%struct.__STDIO_FILE_STRUCT.273* noalias %stream) #0 {
  %1 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %bufsize = alloca i64, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %1, align 8
  %2 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !450
  %3 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %2, i32 0, i32 5, !dbg !450
  %4 = load i8** %3, align 8, !dbg !450
  %5 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !450
  %6 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %5, i32 0, i32 3, !dbg !450
  %7 = load i8** %6, align 8, !dbg !450
  %8 = ptrtoint i8* %4 to i64, !dbg !450
  %9 = ptrtoint i8* %7 to i64, !dbg !450
  %10 = sub i64 %8, %9, !dbg !450
  store i64 %10, i64* %bufsize, align 8, !dbg !450
  %11 = icmp ne i64 %10, 0, !dbg !450
  br i1 %11, label %12, label %24, !dbg !450

; <label>:12                                      ; preds = %0
  %13 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !452
  %14 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %13, i32 0, i32 3, !dbg !452
  %15 = load i8** %14, align 8, !dbg !452
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !452
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 5, !dbg !452
  store i8* %15, i8** %17, align 8, !dbg !452
  %18 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !454
  %19 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !454
  %20 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %19, i32 0, i32 3, !dbg !454
  %21 = load i8** %20, align 8, !dbg !454
  %22 = load i64* %bufsize, align 8, !dbg !454
  %23 = call i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %18, i8* %21, i64 %22) #11, !dbg !454
  br label %24, !dbg !455

; <label>:24                                      ; preds = %12, %0
  %25 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !456
  %26 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %25, i32 0, i32 5, !dbg !456
  %27 = load i8** %26, align 8, !dbg !456
  %28 = load %struct.__STDIO_FILE_STRUCT.273** %1, align 8, !dbg !456
  %29 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %28, i32 0, i32 3, !dbg !456
  %30 = load i8** %29, align 8, !dbg !456
  %31 = ptrtoint i8* %27 to i64, !dbg !456
  %32 = ptrtoint i8* %30 to i64, !dbg !456
  %33 = sub i64 %31, %32, !dbg !456
  ret i64 %33, !dbg !456
}

; Function Attrs: nounwind uwtable
define i8* @memcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !457
  store i8* %4, i8** %r1, align 8, !dbg !457
  %5 = load i8** %2, align 8, !dbg !458
  store i8* %5, i8** %r2, align 8, !dbg !458
  br label %6, !dbg !459

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !459
  %8 = icmp ne i64 %7, 0, !dbg !459
  br i1 %8, label %9, label %17, !dbg !459

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !460
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !460
  store i8* %11, i8** %r2, align 8, !dbg !460
  %12 = load i8* %10, align 1, !dbg !460
  %13 = load i8** %r1, align 8, !dbg !460
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !460
  store i8* %14, i8** %r1, align 8, !dbg !460
  store i8 %12, i8* %13, align 1, !dbg !460
  %15 = load i64* %3, align 8, !dbg !462
  %16 = add i64 %15, -1, !dbg !462
  store i64 %16, i64* %3, align 8, !dbg !462
  br label %6, !dbg !463

; <label>:17                                      ; preds = %6
  %18 = load i8** %1, align 8, !dbg !464
  ret i8* %18, !dbg !464
}

; Function Attrs: nounwind uwtable
define i8* @memset(i8* %s, i32 %c, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i32, align 4
  %3 = alloca i64, align 8
  %p = alloca i8*, align 8
  store i8* %s, i8** %1, align 8
  store i32 %c, i32* %2, align 4
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !465
  store i8* %4, i8** %p, align 8, !dbg !465
  br label %5, !dbg !466

; <label>:5                                       ; preds = %8, %0
  %6 = load i64* %3, align 8, !dbg !466
  %7 = icmp ne i64 %6, 0, !dbg !466
  br i1 %7, label %8, label %15, !dbg !466

; <label>:8                                       ; preds = %5
  %9 = load i32* %2, align 4, !dbg !467
  %10 = trunc i32 %9 to i8, !dbg !467
  %11 = load i8** %p, align 8, !dbg !467
  %12 = getelementptr inbounds i8* %11, i32 1, !dbg !467
  store i8* %12, i8** %p, align 8, !dbg !467
  store i8 %10, i8* %11, align 1, !dbg !467
  %13 = load i64* %3, align 8, !dbg !469
  %14 = add i64 %13, -1, !dbg !469
  store i64 %14, i64* %3, align 8, !dbg !469
  br label %5, !dbg !470

; <label>:15                                      ; preds = %5
  %16 = load i8** %1, align 8, !dbg !471
  ret i8* %16, !dbg !471
}

; Function Attrs: nounwind uwtable
define i32 @isatty(i32 %fd) #0 {
  %1 = alloca i32, align 4
  %term = alloca %struct.termios.442, align 4
  store i32 %fd, i32* %1, align 4
  %2 = load i32* %1, align 4, !dbg !472
  %3 = call i32 @tcgetattr(i32 %2, %struct.termios.442* %term) #12, !dbg !472
  %4 = icmp eq i32 %3, 0, !dbg !472
  %5 = zext i1 %4 to i32, !dbg !472
  ret i32 %5, !dbg !472
}

; Function Attrs: nounwind uwtable
define i32 @tcgetattr(i32 %fd, %struct.termios.442* %termios_p) #0 {
  %1 = alloca i32, align 4
  %2 = alloca %struct.termios.442*, align 8
  %k_termios = alloca %struct.__kernel_termios, align 4
  %retval = alloca i32, align 4
  store i32 %fd, i32* %1, align 4
  store %struct.termios.442* %termios_p, %struct.termios.442** %2, align 8
  %3 = load i32* %1, align 4, !dbg !473
  %4 = call i32 (i32, i64, ...)* @ioctl(i32 %3, i64 21505, %struct.__kernel_termios* %k_termios) #12, !dbg !473
  store i32 %4, i32* %retval, align 4, !dbg !473
  %5 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 0, !dbg !474
  %6 = load i32* %5, align 4, !dbg !474
  %7 = load %struct.termios.442** %2, align 8, !dbg !474
  %8 = getelementptr inbounds %struct.termios.442* %7, i32 0, i32 0, !dbg !474
  store i32 %6, i32* %8, align 4, !dbg !474
  %9 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 1, !dbg !475
  %10 = load i32* %9, align 4, !dbg !475
  %11 = load %struct.termios.442** %2, align 8, !dbg !475
  %12 = getelementptr inbounds %struct.termios.442* %11, i32 0, i32 1, !dbg !475
  store i32 %10, i32* %12, align 4, !dbg !475
  %13 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 2, !dbg !476
  %14 = load i32* %13, align 4, !dbg !476
  %15 = load %struct.termios.442** %2, align 8, !dbg !476
  %16 = getelementptr inbounds %struct.termios.442* %15, i32 0, i32 2, !dbg !476
  store i32 %14, i32* %16, align 4, !dbg !476
  %17 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 3, !dbg !477
  %18 = load i32* %17, align 4, !dbg !477
  %19 = load %struct.termios.442** %2, align 8, !dbg !477
  %20 = getelementptr inbounds %struct.termios.442* %19, i32 0, i32 3, !dbg !477
  store i32 %18, i32* %20, align 4, !dbg !477
  %21 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 4, !dbg !478
  %22 = load i8* %21, align 1, !dbg !478
  %23 = load %struct.termios.442** %2, align 8, !dbg !478
  %24 = getelementptr inbounds %struct.termios.442* %23, i32 0, i32 4, !dbg !478
  store i8 %22, i8* %24, align 1, !dbg !478
  %25 = load %struct.termios.442** %2, align 8, !dbg !479
  %26 = getelementptr inbounds %struct.termios.442* %25, i32 0, i32 5, !dbg !479
  %27 = getelementptr inbounds [32 x i8]* %26, i32 0, i64 0, !dbg !479
  %28 = getelementptr inbounds %struct.__kernel_termios* %k_termios, i32 0, i32 5, !dbg !479
  %29 = getelementptr inbounds [19 x i8]* %28, i32 0, i64 0, !dbg !479
  %30 = call i8* @mempcpy(i8* %27, i8* %29, i64 19) #12, !dbg !479
  %31 = call i8* @memset(i8* %30, i32 0, i64 13) #12, !dbg !479
  %32 = load i32* %retval, align 4, !dbg !482
  ret i32 %32, !dbg !482
}

; Function Attrs: nounwind
declare i32 @ioctl(i32, i64, ...) #5

; Function Attrs: nounwind uwtable
define hidden i64 @__stdio_WRITE(%struct.__STDIO_FILE_STRUCT.273* %stream, i8* %buf, i64 %bufsize) #0 {
  %1 = alloca i64, align 8
  %2 = alloca %struct.__STDIO_FILE_STRUCT.273*, align 8
  %3 = alloca i8*, align 8
  %4 = alloca i64, align 8
  %todo = alloca i64, align 8
  %rv = alloca i64, align 8
  %stodo = alloca i64, align 8
  %s = alloca i8*, align 8
  store %struct.__STDIO_FILE_STRUCT.273* %stream, %struct.__STDIO_FILE_STRUCT.273** %2, align 8
  store i8* %buf, i8** %3, align 8
  store i64 %bufsize, i64* %4, align 8
  %5 = load i64* %4, align 8, !dbg !483
  store i64 %5, i64* %todo, align 8, !dbg !483
  br label %6, !dbg !484

; <label>:6                                       ; preds = %23, %0
  %7 = load i64* %todo, align 8, !dbg !485
  %8 = icmp eq i64 %7, 0, !dbg !485
  br i1 %8, label %9, label %11, !dbg !485

; <label>:9                                       ; preds = %6
  %10 = load i64* %4, align 8, !dbg !488
  store i64 %10, i64* %1, !dbg !488
  br label %95, !dbg !488

; <label>:11                                      ; preds = %6
  %12 = load i64* %todo, align 8, !dbg !490
  %13 = icmp ule i64 %12, 9223372036854775807, !dbg !490
  %14 = load i64* %todo, align 8, !dbg !490
  %15 = select i1 %13, i64 %14, i64 9223372036854775807, !dbg !490
  store i64 %15, i64* %stodo, align 8, !dbg !490
  %16 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !491
  %17 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %16, i32 0, i32 2, !dbg !491
  %18 = load i32* %17, align 4, !dbg !491
  %19 = load i8** %3, align 8, !dbg !491
  %20 = load i64* %stodo, align 8, !dbg !491
  %21 = call i64 @write(i32 %18, i8* %19, i64 %20) #11, !dbg !491
  store i64 %21, i64* %rv, align 8, !dbg !491
  %22 = icmp sge i64 %21, 0, !dbg !491
  br i1 %22, label %23, label %30, !dbg !491

; <label>:23                                      ; preds = %11
  %24 = load i64* %rv, align 8, !dbg !493
  %25 = load i64* %todo, align 8, !dbg !493
  %26 = sub i64 %25, %24, !dbg !493
  store i64 %26, i64* %todo, align 8, !dbg !493
  %27 = load i64* %rv, align 8, !dbg !495
  %28 = load i8** %3, align 8, !dbg !495
  %29 = getelementptr inbounds i8* %28, i64 %27, !dbg !495
  store i8* %29, i8** %3, align 8, !dbg !495
  br label %6, !dbg !496

; <label>:30                                      ; preds = %11
  %31 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !497
  %32 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %31, i32 0, i32 0, !dbg !497
  %33 = load i16* %32, align 2, !dbg !497
  %34 = zext i16 %33 to i32, !dbg !497
  %35 = or i32 %34, 8, !dbg !497
  %36 = trunc i32 %35 to i16, !dbg !497
  store i16 %36, i16* %32, align 2, !dbg !497
  %37 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !499
  %38 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %37, i32 0, i32 4, !dbg !499
  %39 = load i8** %38, align 8, !dbg !499
  %40 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !499
  %41 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %40, i32 0, i32 3, !dbg !499
  %42 = load i8** %41, align 8, !dbg !499
  %43 = ptrtoint i8* %39 to i64, !dbg !499
  %44 = ptrtoint i8* %42 to i64, !dbg !499
  %45 = sub i64 %43, %44, !dbg !499
  store i64 %45, i64* %stodo, align 8, !dbg !499
  %46 = icmp ne i64 %45, 0, !dbg !499
  br i1 %46, label %47, label %91, !dbg !499

; <label>:47                                      ; preds = %30
  %48 = load i64* %stodo, align 8, !dbg !501
  %49 = load i64* %todo, align 8, !dbg !501
  %50 = icmp ugt i64 %48, %49, !dbg !501
  br i1 %50, label %51, label %53, !dbg !501

; <label>:51                                      ; preds = %47
  %52 = load i64* %todo, align 8, !dbg !504
  store i64 %52, i64* %stodo, align 8, !dbg !504
  br label %53, !dbg !506

; <label>:53                                      ; preds = %51, %47
  %54 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !507
  %55 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %54, i32 0, i32 3, !dbg !507
  %56 = load i8** %55, align 8, !dbg !507
  store i8* %56, i8** %s, align 8, !dbg !507
  br label %57, !dbg !508

; <label>:57                                      ; preds = %70, %53
  %58 = load i8** %3, align 8, !dbg !509
  %59 = load i8* %58, align 1, !dbg !509
  %60 = load i8** %s, align 8, !dbg !509
  store i8 %59, i8* %60, align 1, !dbg !509
  %61 = zext i8 %59 to i32, !dbg !509
  %62 = icmp eq i32 %61, 10, !dbg !509
  br i1 %62, label %63, label %70, !dbg !509

; <label>:63                                      ; preds = %57
  %64 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !509
  %65 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %64, i32 0, i32 0, !dbg !509
  %66 = load i16* %65, align 2, !dbg !509
  %67 = zext i16 %66 to i32, !dbg !509
  %68 = and i32 %67, 256, !dbg !509
  %69 = icmp ne i32 %68, 0, !dbg !509
  br i1 %69, label %78, label %70, !dbg !509

; <label>:70                                      ; preds = %63, %57
  %71 = load i8** %s, align 8, !dbg !512
  %72 = getelementptr inbounds i8* %71, i32 1, !dbg !512
  store i8* %72, i8** %s, align 8, !dbg !512
  %73 = load i8** %3, align 8, !dbg !513
  %74 = getelementptr inbounds i8* %73, i32 1, !dbg !513
  store i8* %74, i8** %3, align 8, !dbg !513
  %75 = load i64* %stodo, align 8, !dbg !514
  %76 = add nsw i64 %75, -1, !dbg !514
  store i64 %76, i64* %stodo, align 8, !dbg !514
  %77 = icmp ne i64 %76, 0, !dbg !514
  br i1 %77, label %57, label %78, !dbg !514

; <label>:78                                      ; preds = %63, %70
  %79 = load i8** %s, align 8, !dbg !515
  %80 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !515
  %81 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %80, i32 0, i32 5, !dbg !515
  store i8* %79, i8** %81, align 8, !dbg !515
  %82 = load i8** %s, align 8, !dbg !516
  %83 = load %struct.__STDIO_FILE_STRUCT.273** %2, align 8, !dbg !516
  %84 = getelementptr inbounds %struct.__STDIO_FILE_STRUCT.273* %83, i32 0, i32 3, !dbg !516
  %85 = load i8** %84, align 8, !dbg !516
  %86 = ptrtoint i8* %82 to i64, !dbg !516
  %87 = ptrtoint i8* %85 to i64, !dbg !516
  %88 = sub i64 %86, %87, !dbg !516
  %89 = load i64* %todo, align 8, !dbg !516
  %90 = sub i64 %89, %88, !dbg !516
  store i64 %90, i64* %todo, align 8, !dbg !516
  br label %91, !dbg !517

; <label>:91                                      ; preds = %78, %30
  %92 = load i64* %4, align 8, !dbg !518
  %93 = load i64* %todo, align 8, !dbg !518
  %94 = sub i64 %92, %93, !dbg !518
  store i64 %94, i64* %1, !dbg !518
  br label %95, !dbg !518

; <label>:95                                      ; preds = %91, %9
  %96 = load i64* %1, !dbg !519
  ret i64 %96, !dbg !519
}

declare i64 @write(i32, i8*, i64) #2

; Function Attrs: nounwind uwtable
define i8* @mempcpy(i8* noalias %s1, i8* noalias %s2, i64 %n) #0 {
  %1 = alloca i8*, align 8
  %2 = alloca i8*, align 8
  %3 = alloca i64, align 8
  %r1 = alloca i8*, align 8
  %r2 = alloca i8*, align 8
  store i8* %s1, i8** %1, align 8
  store i8* %s2, i8** %2, align 8
  store i64 %n, i64* %3, align 8
  %4 = load i8** %1, align 8, !dbg !520
  store i8* %4, i8** %r1, align 8, !dbg !520
  %5 = load i8** %2, align 8, !dbg !521
  store i8* %5, i8** %r2, align 8, !dbg !521
  br label %6, !dbg !522

; <label>:6                                       ; preds = %9, %0
  %7 = load i64* %3, align 8, !dbg !522
  %8 = icmp ne i64 %7, 0, !dbg !522
  br i1 %8, label %9, label %17, !dbg !522

; <label>:9                                       ; preds = %6
  %10 = load i8** %r2, align 8, !dbg !523
  %11 = getelementptr inbounds i8* %10, i32 1, !dbg !523
  store i8* %11, i8** %r2, align 8, !dbg !523
  %12 = load i8* %10, align 1, !dbg !523
  %13 = load i8** %r1, align 8, !dbg !523
  %14 = getelementptr inbounds i8* %13, i32 1, !dbg !523
  store i8* %14, i8** %r1, align 8, !dbg !523
  store i8 %12, i8* %13, align 1, !dbg !523
  %15 = load i64* %3, align 8, !dbg !525
  %16 = add i64 %15, -1, !dbg !525
  store i64 %16, i64* %3, align 8, !dbg !525
  br label %6, !dbg !526

; <label>:17                                      ; preds = %6
  %18 = load i8** %r1, align 8, !dbg !527
  ret i8* %18, !dbg !527
}

define i32 @main(i32, i8**) {
entry:
  call void @__uClibc_main(i32 (i32, i8**, i8**)* bitcast (i32 ()* @__user_main to i32 (i32, i8**, i8**)*), i32 %0, i8** %1, void ()* null, void ()* null, void ()* null, i8* null)
  unreachable
}

; Function Attrs: nounwind ssp uwtable
define void @klee_div_zero_check(i64 %z) #8 {
  %1 = icmp eq i64 %z, 0, !dbg !528
  br i1 %1, label %2, label %3, !dbg !528

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([60 x i8]* @.str32, i64 0, i64 0), i32 14, i8* getelementptr inbounds ([15 x i8]* @.str133, i64 0, i64 0), i8* getelementptr inbounds ([8 x i8]* @.str234, i64 0, i64 0)) #14, !dbg !530
  unreachable, !dbg !530

; <label>:3                                       ; preds = %0
  ret void, !dbg !531
}

; Function Attrs: noreturn
declare void @klee_report_error(i8*, i32, i8*, i8*) #9

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata) #1

; Function Attrs: nounwind ssp uwtable
define i32 @klee_int(i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = bitcast i32* %x to i8*, !dbg !532
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %1, i64 4, i8* %name) #12, !dbg !532
  %2 = load i32* %x, align 4, !dbg !533, !tbaa !534
  ret i32 %2, !dbg !533
}

; Function Attrs: nounwind ssp uwtable
define void @klee_overshift_check(i64 %bitWidth, i64 %shift) #8 {
  %1 = icmp ult i64 %shift, %bitWidth, !dbg !538
  br i1 %1, label %3, label %2, !dbg !538

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([8 x i8]* @.str335, i64 0, i64 0), i32 0, i8* getelementptr inbounds ([16 x i8]* @.str14, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8]* @.str25, i64 0, i64 0)) #14, !dbg !540
  unreachable, !dbg !540

; <label>:3                                       ; preds = %0
  ret void, !dbg !542
}

; Function Attrs: nounwind ssp uwtable
define i32 @klee_range(i32 %start, i32 %end, i8* %name) #8 {
  %x = alloca i32, align 4
  %1 = icmp slt i32 %start, %end, !dbg !543
  br i1 %1, label %3, label %2, !dbg !543

; <label>:2                                       ; preds = %0
  call void @klee_report_error(i8* getelementptr inbounds ([51 x i8]* @.str6, i64 0, i64 0), i32 17, i8* getelementptr inbounds ([14 x i8]* @.str17, i64 0, i64 0), i8* getelementptr inbounds ([5 x i8]* @.str28, i64 0, i64 0)) #14, !dbg !545
  unreachable, !dbg !545

; <label>:3                                       ; preds = %0
  %4 = add nsw i32 %start, 1, !dbg !546
  %5 = icmp eq i32 %4, %end, !dbg !546
  br i1 %5, label %21, label %6, !dbg !546

; <label>:6                                       ; preds = %3
  %7 = bitcast i32* %x to i8*, !dbg !548
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %7, i64 4, i8* %name) #12, !dbg !548
  %8 = icmp eq i32 %start, 0, !dbg !550
  %9 = load i32* %x, align 4, !dbg !552, !tbaa !534
  br i1 %8, label %10, label %13, !dbg !550

; <label>:10                                      ; preds = %6
  %11 = icmp ult i32 %9, %end, !dbg !552
  %12 = zext i1 %11 to i64, !dbg !552
  call void @klee_assume(i64 %12) #12, !dbg !552
  br label %19, !dbg !554

; <label>:13                                      ; preds = %6
  %14 = icmp sge i32 %9, %start, !dbg !555
  %15 = zext i1 %14 to i64, !dbg !555
  call void @klee_assume(i64 %15) #12, !dbg !555
  %16 = load i32* %x, align 4, !dbg !557, !tbaa !534
  %17 = icmp slt i32 %16, %end, !dbg !557
  %18 = zext i1 %17 to i64, !dbg !557
  call void @klee_assume(i64 %18) #12, !dbg !557
  br label %19

; <label>:19                                      ; preds = %13, %10
  %20 = load i32* %x, align 4, !dbg !558, !tbaa !534
  br label %21, !dbg !558

; <label>:21                                      ; preds = %19, %3
  %.0 = phi i32 [ %20, %19 ], [ %start, %3 ]
  ret i32 %.0, !dbg !559
}

declare void @klee_assume(i64) #10

; Function Attrs: nounwind ssp uwtable
define weak i8* @memmove(i8* %dst, i8* %src, i64 %count) #8 {
  %1 = icmp eq i8* %src, %dst, !dbg !560
  br i1 %1, label %.loopexit, label %2, !dbg !560

; <label>:2                                       ; preds = %0
  %3 = icmp ugt i8* %src, %dst, !dbg !562
  br i1 %3, label %.preheader, label %18, !dbg !562

.preheader:                                       ; preds = %2
  %4 = icmp eq i64 %count, 0, !dbg !564
  br i1 %4, label %.loopexit, label %.lr.ph.preheader, !dbg !564

.lr.ph.preheader:                                 ; preds = %.preheader
  %n.vec = and i64 %count, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %5 = add i64 %count, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep11 = getelementptr i8* %src, i64 %5
  %scevgep = getelementptr i8* %dst, i64 %5
  %bound1 = icmp uge i8* %scevgep, %src
  %bound0 = icmp uge i8* %scevgep11, %dst
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %src, i64 %n.vec
  %ptr.ind.end13 = getelementptr i8* %dst, i64 %n.vec
  %rev.ind.end = sub i64 %count, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %src, i64 %index
  %next.gep110 = getelementptr i8* %dst, i64 %index
  %6 = bitcast i8* %next.gep to <16 x i8>*, !dbg !564
  %wide.load = load <16 x i8>* %6, align 1, !dbg !564
  %next.gep.sum586 = or i64 %index, 16, !dbg !564
  %7 = getelementptr i8* %src, i64 %next.gep.sum586, !dbg !564
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !564
  %wide.load207 = load <16 x i8>* %8, align 1, !dbg !564
  %9 = bitcast i8* %next.gep110 to <16 x i8>*, !dbg !564
  store <16 x i8> %wide.load, <16 x i8>* %9, align 1, !dbg !564
  %10 = getelementptr i8* %dst, i64 %next.gep.sum586, !dbg !564
  %11 = bitcast i8* %10 to <16 x i8>*, !dbg !564
  store <16 x i8> %wide.load207, <16 x i8>* %11, align 1, !dbg !564
  %index.next = add i64 %index, 32
  %12 = icmp eq i64 %index.next, %n.vec
  br i1 %12, label %middle.block, label %vector.body, !llvm.loop !566

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %src, %.lr.ph.preheader ], [ %src, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val12 = phi i8* [ %dst, %.lr.ph.preheader ], [ %dst, %vector.memcheck ], [ %ptr.ind.end13, %vector.body ]
  %resume.val14 = phi i64 [ %count, %.lr.ph.preheader ], [ %count, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %count
  br i1 %cmp.n, label %.loopexit, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %b.04 = phi i8* [ %14, %.lr.ph ], [ %resume.val, %middle.block ]
  %a.03 = phi i8* [ %16, %.lr.ph ], [ %resume.val12, %middle.block ]
  %.02 = phi i64 [ %13, %.lr.ph ], [ %resume.val14, %middle.block ]
  %13 = add i64 %.02, -1, !dbg !564
  %14 = getelementptr inbounds i8* %b.04, i64 1, !dbg !564
  %15 = load i8* %b.04, align 1, !dbg !564, !tbaa !569
  %16 = getelementptr inbounds i8* %a.03, i64 1, !dbg !564
  store i8 %15, i8* %a.03, align 1, !dbg !564, !tbaa !569
  %17 = icmp eq i64 %13, 0, !dbg !564
  br i1 %17, label %.loopexit, label %.lr.ph, !dbg !564, !llvm.loop !570

; <label>:18                                      ; preds = %2
  %19 = add i64 %count, -1, !dbg !571
  %20 = icmp eq i64 %count, 0, !dbg !573
  br i1 %20, label %.loopexit, label %.lr.ph9, !dbg !573

.lr.ph9:                                          ; preds = %18
  %21 = getelementptr inbounds i8* %src, i64 %19, !dbg !574
  %22 = getelementptr inbounds i8* %dst, i64 %19, !dbg !571
  %n.vec215 = and i64 %count, -32
  %cmp.zero217 = icmp eq i64 %n.vec215, 0
  br i1 %cmp.zero217, label %middle.block210, label %vector.memcheck224

vector.memcheck224:                               ; preds = %.lr.ph9
  %bound1221 = icmp ule i8* %21, %dst
  %bound0220 = icmp ule i8* %22, %src
  %memcheck.conflict223 = and i1 %bound0220, %bound1221
  %.sum = sub i64 %19, %n.vec215
  %rev.ptr.ind.end = getelementptr i8* %src, i64 %.sum
  %rev.ptr.ind.end229 = getelementptr i8* %dst, i64 %.sum
  %rev.ind.end231 = sub i64 %count, %n.vec215
  br i1 %memcheck.conflict223, label %middle.block210, label %vector.body209

vector.body209:                                   ; preds = %vector.body209, %vector.memcheck224
  %index212 = phi i64 [ %index.next234, %vector.body209 ], [ 0, %vector.memcheck224 ]
  %.sum440 = sub i64 %19, %index212
  %next.gep236.sum = add i64 %.sum440, -15, !dbg !573
  %23 = getelementptr i8* %src, i64 %next.gep236.sum, !dbg !573
  %24 = bitcast i8* %23 to <16 x i8>*, !dbg !573
  %wide.load434 = load <16 x i8>* %24, align 1, !dbg !573
  %reverse = shufflevector <16 x i8> %wide.load434, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !573
  %.sum505 = add i64 %.sum440, -31, !dbg !573
  %25 = getelementptr i8* %src, i64 %.sum505, !dbg !573
  %26 = bitcast i8* %25 to <16 x i8>*, !dbg !573
  %wide.load435 = load <16 x i8>* %26, align 1, !dbg !573
  %reverse436 = shufflevector <16 x i8> %wide.load435, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !573
  %reverse437 = shufflevector <16 x i8> %reverse, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !573
  %27 = getelementptr i8* %dst, i64 %next.gep236.sum, !dbg !573
  %28 = bitcast i8* %27 to <16 x i8>*, !dbg !573
  store <16 x i8> %reverse437, <16 x i8>* %28, align 1, !dbg !573
  %reverse438 = shufflevector <16 x i8> %reverse436, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !573
  %29 = getelementptr i8* %dst, i64 %.sum505, !dbg !573
  %30 = bitcast i8* %29 to <16 x i8>*, !dbg !573
  store <16 x i8> %reverse438, <16 x i8>* %30, align 1, !dbg !573
  %index.next234 = add i64 %index212, 32
  %31 = icmp eq i64 %index.next234, %n.vec215
  br i1 %31, label %middle.block210, label %vector.body209, !llvm.loop !575

middle.block210:                                  ; preds = %vector.body209, %vector.memcheck224, %.lr.ph9
  %resume.val225 = phi i8* [ %21, %.lr.ph9 ], [ %21, %vector.memcheck224 ], [ %rev.ptr.ind.end, %vector.body209 ]
  %resume.val227 = phi i8* [ %22, %.lr.ph9 ], [ %22, %vector.memcheck224 ], [ %rev.ptr.ind.end229, %vector.body209 ]
  %resume.val230 = phi i64 [ %count, %.lr.ph9 ], [ %count, %vector.memcheck224 ], [ %rev.ind.end231, %vector.body209 ]
  %new.indc.resume.val232 = phi i64 [ 0, %.lr.ph9 ], [ 0, %vector.memcheck224 ], [ %n.vec215, %vector.body209 ]
  %cmp.n233 = icmp eq i64 %new.indc.resume.val232, %count
  br i1 %cmp.n233, label %.loopexit, label %scalar.ph211

scalar.ph211:                                     ; preds = %scalar.ph211, %middle.block210
  %b.18 = phi i8* [ %33, %scalar.ph211 ], [ %resume.val225, %middle.block210 ]
  %a.17 = phi i8* [ %35, %scalar.ph211 ], [ %resume.val227, %middle.block210 ]
  %.16 = phi i64 [ %32, %scalar.ph211 ], [ %resume.val230, %middle.block210 ]
  %32 = add i64 %.16, -1, !dbg !573
  %33 = getelementptr inbounds i8* %b.18, i64 -1, !dbg !573
  %34 = load i8* %b.18, align 1, !dbg !573, !tbaa !569
  %35 = getelementptr inbounds i8* %a.17, i64 -1, !dbg !573
  store i8 %34, i8* %a.17, align 1, !dbg !573, !tbaa !569
  %36 = icmp eq i64 %32, 0, !dbg !573
  br i1 %36, label %.loopexit, label %scalar.ph211, !dbg !573, !llvm.loop !576

.loopexit:                                        ; preds = %scalar.ph211, %middle.block210, %18, %.lr.ph, %middle.block, %.preheader, %0
  ret i8* %dst, !dbg !577
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #4 = { noreturn nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-floa
attributes #5 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false
attributes #6 = { inlinehint nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use
attributes #7 = { nounwind readnone uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-s
attributes #8 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #11 = { nobuiltin }
attributes #12 = { nobuiltin nounwind }
attributes #13 = { nobuiltin nounwind readnone }
attributes #14 = { nobuiltin noreturn nounwind }

!llvm.dbg.cu = !{!0, !9, !51, !59, !64, !113, !144, !156, !164, !171, !196, !202, !234, !242, !252, !262, !272, !284, !298, !312, !326}
!llvm.module.flags = !{!341, !342}
!llvm.ident = !{!343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343, !343}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !2, metadata !2, metadata !""} ; [ 
!1 = metadata !{metadata !"test.c", metadata !"/home/klee/workspace/basic-tests/out-of-bound-access"}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 5, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @__user_main, null, null, metadata !2, i32 5} ; [ DW_TAG_subprogra
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/klee/workspace/basic-tests/out-of-bound-access/test.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{metadata !8}
!8 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!9 = metadata !{i32 786449, metadata !10, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !11, metadata !39, metadata !2, metadata !""} ;
!10 = metadata !{metadata !"libc/misc/internals/__uClibc_main.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!11 = metadata !{metadata !12, metadata !16, metadata !17, metadata !28, metadata !31, metadata !38}
!12 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"__uClibc_init", metadata !"__uClibc_init", metadata !"", i32 187, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_init, null, null, metadata !2
!13 = metadata !{i32 786473, metadata !10}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{null}
!16 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"__uClibc_fini", metadata !"__uClibc_fini", metadata !"", i32 251, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__uClibc_fini, null, null, metadata !2
!17 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"__uClibc_main", metadata !"__uClibc_main", metadata !"", i32 278, metadata !18, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32 (i32, i8**, i8**)*, i32, i8**, void ()
!18 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !19, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!19 = metadata !{null, metadata !20, metadata !8, metadata !23, metadata !26, metadata !26, metadata !26, metadata !27}
!20 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !21} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!21 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !22, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!22 = metadata !{metadata !8, metadata !8, metadata !23, metadata !23}
!23 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !24} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!24 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !25} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from char]
!25 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!26 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !14} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!27 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!28 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"__check_one_fd", metadata !"__check_one_fd", metadata !"", i32 136, metadata !29, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i32)* @__check_one_fd, null, null, m
!29 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !30, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!30 = metadata !{null, metadata !8, metadata !8}
!31 = metadata !{i32 786478, metadata !32, metadata !33, metadata !"gnu_dev_makedev", metadata !"gnu_dev_makedev", metadata !"", i32 54, metadata !34, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (i32, i32)* @gnu_dev_makedev, null, null, 
!32 = metadata !{metadata !"./include/sys/sysmacros.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!33 = metadata !{i32 786473, metadata !32}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!34 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !35, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!35 = metadata !{metadata !36, metadata !37, metadata !37}
!36 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!37 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!38 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"__check_suid", metadata !"__check_suid", metadata !"", i32 155, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @__check_suid, null, null, metadata !2, i32 
!39 = metadata !{metadata !40, metadata !41, metadata !44, metadata !45, metadata !48, metadata !49, metadata !50}
!40 = metadata !{i32 786484, i32 0, null, metadata !"__libc_stack_end", metadata !"__libc_stack_end", metadata !"", metadata !13, i32 52, metadata !27, i32 0, i32 1, i8** @__libc_stack_end, null} ; [ DW_TAG_variable ] [__libc_stack_end] [line 52] [def]
!41 = metadata !{i32 786484, i32 0, null, metadata !"__uclibc_progname", metadata !"__uclibc_progname", metadata !"", metadata !13, i32 110, metadata !42, i32 0, i32 1, i8** @__uclibc_progname, null} ; [ DW_TAG_variable ] [__uclibc_progname] [line 110] [
!42 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !43} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!43 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !25} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from char]
!44 = metadata !{i32 786484, i32 0, null, metadata !"__environ", metadata !"__environ", metadata !"", metadata !13, i32 125, metadata !23, i32 0, i32 1, i8*** @__environ, null} ; [ DW_TAG_variable ] [__environ] [line 125] [def]
!45 = metadata !{i32 786484, i32 0, null, metadata !"__pagesize", metadata !"__pagesize", metadata !"", metadata !13, i32 129, metadata !46, i32 0, i32 1, i64* @__pagesize, null} ; [ DW_TAG_variable ] [__pagesize] [line 129] [def]
!46 = metadata !{i32 786454, metadata !10, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!47 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!48 = metadata !{i32 786484, i32 0, metadata !12, metadata !"been_there_done_that", metadata !"been_there_done_that", metadata !"", metadata !13, i32 189, metadata !8, i32 1, i32 1, i32* @__uClibc_init.been_there_done_that, null} ; [ DW_TAG_variable ] [b
!49 = metadata !{i32 786484, i32 0, null, metadata !"__app_fini", metadata !"__app_fini", metadata !"", metadata !13, i32 244, metadata !26, i32 0, i32 1, void ()** @__app_fini, null} ; [ DW_TAG_variable ] [__app_fini] [line 244] [def]
!50 = metadata !{i32 786484, i32 0, null, metadata !"__rtld_fini", metadata !"__rtld_fini", metadata !"", metadata !13, i32 247, metadata !26, i32 0, i32 1, void ()** @__rtld_fini, null} ; [ DW_TAG_variable ] [__rtld_fini] [line 247] [def]
!51 = metadata !{i32 786449, metadata !52, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !53, metadata !2, metadata !2, metadata !""} ;
!52 = metadata !{metadata !"libc/misc/internals/__errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!53 = metadata !{metadata !54}
!54 = metadata !{i32 786478, metadata !52, metadata !55, metadata !"__errno_location", metadata !"__errno_location", metadata !"", i32 11, metadata !56, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__errno_location, null, null, met
!55 = metadata !{i32 786473, metadata !52}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__errno_location.c]
!56 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !57, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!57 = metadata !{metadata !58}
!58 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !8} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from int]
!59 = metadata !{i32 786449, metadata !60, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !61, metadata !2, metadata !2, metadata !""} ;
!60 = metadata !{metadata !"libc/misc/internals/__h_errno_location.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!61 = metadata !{metadata !62}
!62 = metadata !{i32 786478, metadata !60, metadata !63, metadata !"__h_errno_location", metadata !"__h_errno_location", metadata !"", i32 10, metadata !56, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32* ()* @__h_errno_location, null, nul
!63 = metadata !{i32 786473, metadata !60}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__h_errno_location.c]
!64 = metadata !{i32 786449, metadata !65, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !66, metadata !70, metadata !2, metadata !""} 
!65 = metadata !{metadata !"libc/stdio/_stdio.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!66 = metadata !{metadata !67, metadata !69}
!67 = metadata !{i32 786478, metadata !65, metadata !68, metadata !"_stdio_term", metadata !"_stdio_term", metadata !"", i32 210, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_term, null, null, metadata !2, i32 
!68 = metadata !{i32 786473, metadata !65}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!69 = metadata !{i32 786478, metadata !65, metadata !68, metadata !"_stdio_init", metadata !"_stdio_init", metadata !"", i32 277, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @_stdio_init, null, null, metadata !2, i32 
!70 = metadata !{metadata !71, metadata !104, metadata !105, metadata !106, metadata !107, metadata !108, metadata !109}
!71 = metadata !{i32 786484, i32 0, null, metadata !"stdin", metadata !"stdin", metadata !"", metadata !68, i32 154, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdin, null} ; [ DW_TAG_variable ] [stdin] [line 154] [def]
!72 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !73} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!73 = metadata !{i32 786454, metadata !65, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !74} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!74 = metadata !{i32 786451, metadata !75, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !76, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offset
!75 = metadata !{metadata !"./include/bits/uClibc_stdio.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!76 = metadata !{metadata !77, metadata !79, metadata !84, metadata !85, metadata !87, metadata !88, metadata !89, metadata !90, metadata !91, metadata !92, metadata !94, metadata !97}
!77 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !78} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!78 = metadata !{i32 786468, null, null, metadata !"unsigned short", i32 0, i64 16, i64 16, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned short] [line 0, size 16, align 16, offset 0, enc DW_ATE_unsigned]
!79 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !80} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!80 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 16, i64 8, i32 0, i32 0, metadata !81, metadata !82, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 16, align 8, offset 0] [from unsigned char]
!81 = metadata !{i32 786468, null, null, metadata !"unsigned char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 8} ; [ DW_TAG_base_type ] [unsigned char] [line 0, size 8, align 8, offset 0, enc DW_ATE_unsigned_char]
!82 = metadata !{metadata !83}
!83 = metadata !{i32 786465, i64 0, i64 2}        ; [ DW_TAG_subrange_type ] [0, 1]
!84 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!85 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!86 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !81} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from unsigned char]
!87 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!88 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!89 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!90 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!91 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!92 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !93} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!93 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !74} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!94 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !95} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!95 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 64, i64 32, i32 0, i32 0, metadata !96, metadata !82, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 64, align 32, offset 0] [from wchar_t]
!96 = metadata !{i32 786454, metadata !75, null, metadata !"wchar_t", i32 65, i64 0, i64 0, i64 0, i32 0, metadata !8} ; [ DW_TAG_typedef ] [wchar_t] [line 65, size 0, align 0, offset 0] [from int]
!97 = metadata !{i32 786445, metadata !75, metadata !74, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !98} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!98 = metadata !{i32 786454, metadata !75, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !99} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!99 = metadata !{i32 786451, metadata !100, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !101, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!100 = metadata !{metadata !"./include/wchar.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!101 = metadata !{metadata !102, metadata !103}
!102 = metadata !{i32 786445, metadata !100, metadata !99, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!103 = metadata !{i32 786445, metadata !100, metadata !99, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !96} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!104 = metadata !{i32 786484, i32 0, null, metadata !"stdout", metadata !"stdout", metadata !"", metadata !68, i32 155, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stdout, null} ; [ DW_TAG_variable ] [stdout] [line 155] [def]
!105 = metadata !{i32 786484, i32 0, null, metadata !"stderr", metadata !"stderr", metadata !"", metadata !68, i32 156, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @stderr, null} ; [ DW_TAG_variable ] [stderr] [line 156] [def]
!106 = metadata !{i32 786484, i32 0, null, metadata !"__stdin", metadata !"__stdin", metadata !"", metadata !68, i32 159, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdin, null} ; [ DW_TAG_variable ] [__stdin] [line 159] [def]
!107 = metadata !{i32 786484, i32 0, null, metadata !"__stdout", metadata !"__stdout", metadata !"", metadata !68, i32 162, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @__stdout, null} ; [ DW_TAG_variable ] [__stdout] [line 162] [def]
!108 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_openlist", metadata !"_stdio_openlist", metadata !"", metadata !68, i32 180, metadata !72, i32 0, i32 1, %struct.__STDIO_FILE_STRUCT.273** @_stdio_openlist, null} ; [ DW_TAG_variable ] [_stdio_
!109 = metadata !{i32 786484, i32 0, null, metadata !"_stdio_streams", metadata !"_stdio_streams", metadata !"", metadata !68, i32 131, metadata !110, i32 1, i32 1, [3 x %struct.__STDIO_FILE_STRUCT.273]* @_stdio_streams, null} ; [ DW_TAG_variable ] [_std
!110 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 1920, i64 64, i32 0, i32 0, metadata !73, metadata !111, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 1920, align 64, offset 0] [from FILE]
!111 = metadata !{metadata !112}
!112 = metadata !{i32 786465, i64 0, i64 3}       ; [ DW_TAG_subrange_type ] [0, 2]
!113 = metadata !{i32 786449, metadata !114, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !115, metadata !2, metadata !2, metadata !""
!114 = metadata !{metadata !"libc/stdio/_wcommit.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!115 = metadata !{metadata !116}
!116 = metadata !{i32 786478, metadata !114, metadata !117, metadata !"__stdio_wcommit", metadata !"__stdio_wcommit", metadata !"", i32 17, metadata !118, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*)* @
!117 = metadata !{i32 786473, metadata !114}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!118 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !119, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!119 = metadata !{metadata !120, metadata !121}
!120 = metadata !{i32 786454, metadata !114, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!121 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !122} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!122 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !123} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!123 = metadata !{i32 786454, metadata !114, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !124} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!124 = metadata !{i32 786451, metadata !75, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !125, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!125 = metadata !{metadata !126, metadata !127, metadata !128, metadata !129, metadata !130, metadata !131, metadata !132, metadata !133, metadata !134, metadata !135, metadata !137, metadata !138}
!126 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !78} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!127 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !80} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!128 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!129 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!130 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!131 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!132 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!133 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!134 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!135 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !136} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!136 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !124} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!137 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !95} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!138 = metadata !{i32 786445, metadata !75, metadata !124, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !139} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!139 = metadata !{i32 786454, metadata !75, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !140} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!140 = metadata !{i32 786451, metadata !100, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !141, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!141 = metadata !{metadata !142, metadata !143}
!142 = metadata !{i32 786445, metadata !100, metadata !140, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!143 = metadata !{i32 786445, metadata !100, metadata !140, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !96} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!144 = metadata !{i32 786449, metadata !145, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !146, metadata !2, metadata !2, metadata !""
!145 = metadata !{metadata !"libc/string/memcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!146 = metadata !{metadata !147}
!147 = metadata !{i32 786478, metadata !145, metadata !148, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 18, metadata !149, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !2, i32 
!148 = metadata !{i32 786473, metadata !145}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!149 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !150, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!150 = metadata !{metadata !27, metadata !151, metadata !152, metadata !155}
!151 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !27} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!152 = metadata !{i32 786487, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !153} ; [ DW_TAG_restrict_type ] [line 0, size 0, align 0, offset 0] [from ]
!153 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !154} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!154 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!155 = metadata !{i32 786454, metadata !145, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!156 = metadata !{i32 786449, metadata !157, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !158, metadata !2, metadata !2, metadata !""
!157 = metadata !{metadata !"libc/string/memset.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!158 = metadata !{metadata !159}
!159 = metadata !{i32 786478, metadata !157, metadata !160, metadata !"memset", metadata !"memset", metadata !"", i32 17, metadata !161, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i32, i64)* @memset, null, null, metadata !2, i32 
!160 = metadata !{i32 786473, metadata !157}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!161 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !162, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!162 = metadata !{metadata !27, metadata !27, metadata !8, metadata !163}
!163 = metadata !{i32 786454, metadata !157, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!164 = metadata !{i32 786449, metadata !165, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !166, metadata !2, metadata !2, metadata !""
!165 = metadata !{metadata !"libc/termios/isatty.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!166 = metadata !{metadata !167}
!167 = metadata !{i32 786478, metadata !165, metadata !168, metadata !"isatty", metadata !"isatty", metadata !"", i32 26, metadata !169, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32)* @isatty, null, null, metadata !2, i32 27} ; [ DW
!168 = metadata !{i32 786473, metadata !165}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/isatty.c]
!169 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !170, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!170 = metadata !{metadata !8, metadata !8}
!171 = metadata !{i32 786449, metadata !172, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !173, metadata !2, metadata !2, metadata !""
!172 = metadata !{metadata !"libc/termios/tcgetattr.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!173 = metadata !{metadata !174}
!174 = metadata !{i32 786478, metadata !172, metadata !175, metadata !"tcgetattr", metadata !"tcgetattr", metadata !"", i32 38, metadata !176, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, %struct.termios.442*)* @tcgetattr, null, nu
!175 = metadata !{i32 786473, metadata !172}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!176 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !177, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!177 = metadata !{metadata !8, metadata !8, metadata !178}
!178 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !179} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from termios]
!179 = metadata !{i32 786451, metadata !180, null, metadata !"termios", i32 30, i64 480, i64 32, i32 0, i32 0, null, metadata !181, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [termios] [line 30, size 480, align 32, offset 0] [def] [from ]
!180 = metadata !{metadata !"./include/bits/termios.h", metadata !"/home/klee/klee_build/klee-uclibc"}
!181 = metadata !{metadata !182, metadata !184, metadata !185, metadata !186, metadata !187, metadata !189, metadata !193, metadata !195}
!182 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_iflag", i32 32, i64 32, i64 32, i64 0, i32 0, metadata !183} ; [ DW_TAG_member ] [c_iflag] [line 32, size 32, align 32, offset 0] [from tcflag_t]
!183 = metadata !{i32 786454, metadata !180, null, metadata !"tcflag_t", i32 27, i64 0, i64 0, i64 0, i32 0, metadata !37} ; [ DW_TAG_typedef ] [tcflag_t] [line 27, size 0, align 0, offset 0] [from unsigned int]
!184 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_oflag", i32 33, i64 32, i64 32, i64 32, i32 0, metadata !183} ; [ DW_TAG_member ] [c_oflag] [line 33, size 32, align 32, offset 32] [from tcflag_t]
!185 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_cflag", i32 34, i64 32, i64 32, i64 64, i32 0, metadata !183} ; [ DW_TAG_member ] [c_cflag] [line 34, size 32, align 32, offset 64] [from tcflag_t]
!186 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_lflag", i32 35, i64 32, i64 32, i64 96, i32 0, metadata !183} ; [ DW_TAG_member ] [c_lflag] [line 35, size 32, align 32, offset 96] [from tcflag_t]
!187 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_line", i32 36, i64 8, i64 8, i64 128, i32 0, metadata !188} ; [ DW_TAG_member ] [c_line] [line 36, size 8, align 8, offset 128] [from cc_t]
!188 = metadata !{i32 786454, metadata !180, null, metadata !"cc_t", i32 25, i64 0, i64 0, i64 0, i32 0, metadata !81} ; [ DW_TAG_typedef ] [cc_t] [line 25, size 0, align 0, offset 0] [from unsigned char]
!189 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_cc", i32 37, i64 256, i64 8, i64 136, i32 0, metadata !190} ; [ DW_TAG_member ] [c_cc] [line 37, size 256, align 8, offset 136] [from ]
!190 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 256, i64 8, i32 0, i32 0, metadata !188, metadata !191, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 256, align 8, offset 0] [from cc_t]
!191 = metadata !{metadata !192}
!192 = metadata !{i32 786465, i64 0, i64 32}      ; [ DW_TAG_subrange_type ] [0, 31]
!193 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_ispeed", i32 38, i64 32, i64 32, i64 416, i32 0, metadata !194} ; [ DW_TAG_member ] [c_ispeed] [line 38, size 32, align 32, offset 416] [from speed_t]
!194 = metadata !{i32 786454, metadata !180, null, metadata !"speed_t", i32 26, i64 0, i64 0, i64 0, i32 0, metadata !37} ; [ DW_TAG_typedef ] [speed_t] [line 26, size 0, align 0, offset 0] [from unsigned int]
!195 = metadata !{i32 786445, metadata !180, metadata !179, metadata !"c_ospeed", i32 39, i64 32, i64 32, i64 448, i32 0, metadata !194} ; [ DW_TAG_member ] [c_ospeed] [line 39, size 32, align 32, offset 448] [from speed_t]
!196 = metadata !{i32 786449, metadata !197, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !2, metadata !198, metadata !2, metadata !""
!197 = metadata !{metadata !"libc/misc/internals/errno.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!198 = metadata !{metadata !199, metadata !201}
!199 = metadata !{i32 786484, i32 0, null, metadata !"errno", metadata !"errno", metadata !"", metadata !200, i32 7, metadata !8, i32 0, i32 1, i32* @errno, null} ; [ DW_TAG_variable ] [errno] [line 7] [def]
!200 = metadata !{i32 786473, metadata !197}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/errno.c]
!201 = metadata !{i32 786484, i32 0, null, metadata !"h_errno", metadata !"h_errno", metadata !"", metadata !200, i32 8, metadata !8, i32 0, i32 1, i32* @h_errno, null} ; [ DW_TAG_variable ] [h_errno] [line 8] [def]
!202 = metadata !{i32 786449, metadata !203, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !204, metadata !2, metadata !2, metadata !""
!203 = metadata !{metadata !"libc/stdio/_WRITE.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!204 = metadata !{metadata !205}
!205 = metadata !{i32 786478, metadata !203, metadata !206, metadata !"__stdio_WRITE", metadata !"__stdio_WRITE", metadata !"", i32 33, metadata !207, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i64 (%struct.__STDIO_FILE_STRUCT.273*, i8*, i
!206 = metadata !{i32 786473, metadata !203}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!207 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !208, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!208 = metadata !{metadata !209, metadata !210, metadata !232, metadata !209}
!209 = metadata !{i32 786454, metadata !203, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!210 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !211} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from FILE]
!211 = metadata !{i32 786454, metadata !203, null, metadata !"FILE", i32 46, i64 0, i64 0, i64 0, i32 0, metadata !212} ; [ DW_TAG_typedef ] [FILE] [line 46, size 0, align 0, offset 0] [from __STDIO_FILE_STRUCT]
!212 = metadata !{i32 786451, metadata !75, null, metadata !"__STDIO_FILE_STRUCT", i32 233, i64 640, i64 64, i32 0, i32 0, null, metadata !213, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [__STDIO_FILE_STRUCT] [line 233, size 640, align 64, offs
!213 = metadata !{metadata !214, metadata !215, metadata !216, metadata !217, metadata !218, metadata !219, metadata !220, metadata !221, metadata !222, metadata !223, metadata !225, metadata !226}
!214 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__modeflags", i32 234, i64 16, i64 16, i64 0, i32 0, metadata !78} ; [ DW_TAG_member ] [__modeflags] [line 234, size 16, align 16, offset 0] [from unsigned short]
!215 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__ungot_width", i32 237, i64 16, i64 8, i64 16, i32 0, metadata !80} ; [ DW_TAG_member ] [__ungot_width] [line 237, size 16, align 8, offset 16] [from ]
!216 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__filedes", i32 244, i64 32, i64 32, i64 32, i32 0, metadata !8} ; [ DW_TAG_member ] [__filedes] [line 244, size 32, align 32, offset 32] [from int]
!217 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufstart", i32 246, i64 64, i64 64, i64 64, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufstart] [line 246, size 64, align 64, offset 64] [from ]
!218 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufend", i32 247, i64 64, i64 64, i64 128, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufend] [line 247, size 64, align 64, offset 128] [from ]
!219 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufpos", i32 248, i64 64, i64 64, i64 192, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufpos] [line 248, size 64, align 64, offset 192] [from ]
!220 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufread", i32 249, i64 64, i64 64, i64 256, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufread] [line 249, size 64, align 64, offset 256] [from ]
!221 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufgetc_u", i32 252, i64 64, i64 64, i64 320, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufgetc_u] [line 252, size 64, align 64, offset 320] [from ]
!222 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__bufputc_u", i32 255, i64 64, i64 64, i64 384, i32 0, metadata !86} ; [ DW_TAG_member ] [__bufputc_u] [line 255, size 64, align 64, offset 384] [from ]
!223 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__nextopen", i32 261, i64 64, i64 64, i64 448, i32 0, metadata !224} ; [ DW_TAG_member ] [__nextopen] [line 261, size 64, align 64, offset 448] [from ]
!224 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !212} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from __STDIO_FILE_STRUCT]
!225 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__ungot", i32 268, i64 64, i64 32, i64 512, i32 0, metadata !95} ; [ DW_TAG_member ] [__ungot] [line 268, size 64, align 32, offset 512] [from ]
!226 = metadata !{i32 786445, metadata !75, metadata !212, metadata !"__state", i32 271, i64 64, i64 32, i64 576, i32 0, metadata !227} ; [ DW_TAG_member ] [__state] [line 271, size 64, align 32, offset 576] [from __mbstate_t]
!227 = metadata !{i32 786454, metadata !75, null, metadata !"__mbstate_t", i32 85, i64 0, i64 0, i64 0, i32 0, metadata !228} ; [ DW_TAG_typedef ] [__mbstate_t] [line 85, size 0, align 0, offset 0] [from ]
!228 = metadata !{i32 786451, metadata !100, null, metadata !"", i32 81, i64 64, i64 32, i32 0, i32 0, null, metadata !229, i32 0, null, null, null} ; [ DW_TAG_structure_type ] [line 81, size 64, align 32, offset 0] [def] [from ]
!229 = metadata !{metadata !230, metadata !231}
!230 = metadata !{i32 786445, metadata !100, metadata !228, metadata !"__mask", i32 83, i64 32, i64 32, i64 0, i32 0, metadata !96} ; [ DW_TAG_member ] [__mask] [line 83, size 32, align 32, offset 0] [from wchar_t]
!231 = metadata !{i32 786445, metadata !100, metadata !228, metadata !"__wc", i32 84, i64 32, i64 32, i64 32, i32 0, metadata !96} ; [ DW_TAG_member ] [__wc] [line 84, size 32, align 32, offset 32] [from wchar_t]
!232 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !233} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!233 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !81} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from unsigned char]
!234 = metadata !{i32 786449, metadata !235, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !236, metadata !2, metadata !2, metadata !""
!235 = metadata !{metadata !"libc/string/mempcpy.c", metadata !"/home/klee/klee_build/klee-uclibc"}
!236 = metadata !{metadata !237}
!237 = metadata !{i32 786478, metadata !235, metadata !238, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 20, metadata !239, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !2, i
!238 = metadata !{i32 786473, metadata !235}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!239 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !240, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!240 = metadata !{metadata !27, metadata !151, metadata !152, metadata !241}
!241 = metadata !{i32 786454, metadata !235, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!242 = metadata !{i32 786449, metadata !243, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !244, metadata !2, metadata !2, metadata !""} 
!243 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!244 = metadata !{metadata !245}
!245 = metadata !{i32 786478, metadata !243, metadata !246, metadata !"klee_div_zero_check", metadata !"klee_div_zero_check", metadata !"", i32 12, metadata !247, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64)* @klee_div_zero_check, 
!246 = metadata !{i32 786473, metadata !243}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!247 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !248, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!248 = metadata !{null, metadata !249}
!249 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!250 = metadata !{metadata !251}
!251 = metadata !{i32 786689, metadata !245, metadata !"z", metadata !246, i32 16777228, metadata !249, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [z] [line 12]
!252 = metadata !{i32 786449, metadata !253, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !254, metadata !2, metadata !2, metadata !""} 
!253 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_int.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!254 = metadata !{metadata !255}
!255 = metadata !{i32 786478, metadata !253, metadata !256, metadata !"klee_int", metadata !"klee_int", metadata !"", i32 13, metadata !257, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i8*)* @klee_int, null, null, metadata !259, i32 13}
!256 = metadata !{i32 786473, metadata !253}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_int.c]
!257 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !258, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!258 = metadata !{metadata !8, metadata !42}
!259 = metadata !{metadata !260, metadata !261}
!260 = metadata !{i32 786689, metadata !255, metadata !"name", metadata !256, i32 16777229, metadata !42, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!261 = metadata !{i32 786688, metadata !255, metadata !"x", metadata !256, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!262 = metadata !{i32 786449, metadata !263, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !264, metadata !2, metadata !2, metadata !""} 
!263 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!264 = metadata !{metadata !265}
!265 = metadata !{i32 786478, metadata !263, metadata !266, metadata !"klee_overshift_check", metadata !"klee_overshift_check", metadata !"", i32 20, metadata !267, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64, i64)* @klee_overshift
!266 = metadata !{i32 786473, metadata !263}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!267 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !268, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!268 = metadata !{null, metadata !36, metadata !36}
!269 = metadata !{metadata !270, metadata !271}
!270 = metadata !{i32 786689, metadata !265, metadata !"bitWidth", metadata !266, i32 16777236, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [bitWidth] [line 20]
!271 = metadata !{i32 786689, metadata !265, metadata !"shift", metadata !266, i32 33554452, metadata !36, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [shift] [line 20]
!272 = metadata !{i32 786449, metadata !273, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !274, metadata !2, metadata !2, metadata !""} 
!273 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_range.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!274 = metadata !{metadata !275}
!275 = metadata !{i32 786478, metadata !273, metadata !276, metadata !"klee_range", metadata !"klee_range", metadata !"", i32 13, metadata !277, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i32, i32, i8*)* @klee_range, null, null, metada
!276 = metadata !{i32 786473, metadata !273}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!277 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !278, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!278 = metadata !{metadata !8, metadata !8, metadata !8, metadata !42}
!279 = metadata !{metadata !280, metadata !281, metadata !282, metadata !283}
!280 = metadata !{i32 786689, metadata !275, metadata !"start", metadata !276, i32 16777229, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [start] [line 13]
!281 = metadata !{i32 786689, metadata !275, metadata !"end", metadata !276, i32 33554445, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [end] [line 13]
!282 = metadata !{i32 786689, metadata !275, metadata !"name", metadata !276, i32 50331661, metadata !42, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!283 = metadata !{i32 786688, metadata !275, metadata !"x", metadata !276, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!284 = metadata !{i32 786449, metadata !285, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !286, metadata !2, metadata !2, metadata !""} 
!285 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!286 = metadata !{metadata !287}
!287 = metadata !{i32 786478, metadata !285, metadata !288, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 12, metadata !289, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !292, i32
!288 = metadata !{i32 786473, metadata !285}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memcpy.c]
!289 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !290, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!290 = metadata !{metadata !27, metadata !27, metadata !153, metadata !291}
!291 = metadata !{i32 786454, metadata !285, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!292 = metadata !{metadata !293, metadata !294, metadata !295, metadata !296, metadata !297}
!293 = metadata !{i32 786689, metadata !287, metadata !"destaddr", metadata !288, i32 16777228, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 12]
!294 = metadata !{i32 786689, metadata !287, metadata !"srcaddr", metadata !288, i32 33554444, metadata !153, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 12]
!295 = metadata !{i32 786689, metadata !287, metadata !"len", metadata !288, i32 50331660, metadata !291, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 12]
!296 = metadata !{i32 786688, metadata !287, metadata !"dest", metadata !288, i32 13, metadata !24, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 13]
!297 = metadata !{i32 786688, metadata !287, metadata !"src", metadata !288, i32 14, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 14]
!298 = metadata !{i32 786449, metadata !299, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !300, metadata !2, metadata !2, metadata !""} 
!299 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memmove.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!300 = metadata !{metadata !301}
!301 = metadata !{i32 786478, metadata !299, metadata !302, metadata !"memmove", metadata !"memmove", metadata !"", i32 12, metadata !303, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memmove, null, null, metadata !306, 
!302 = metadata !{i32 786473, metadata !299}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!303 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !304, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!304 = metadata !{metadata !27, metadata !27, metadata !153, metadata !305}
!305 = metadata !{i32 786454, metadata !299, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!306 = metadata !{metadata !307, metadata !308, metadata !309, metadata !310, metadata !311}
!307 = metadata !{i32 786689, metadata !301, metadata !"dst", metadata !302, i32 16777228, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 12]
!308 = metadata !{i32 786689, metadata !301, metadata !"src", metadata !302, i32 33554444, metadata !153, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [src] [line 12]
!309 = metadata !{i32 786689, metadata !301, metadata !"count", metadata !302, i32 50331660, metadata !305, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 12]
!310 = metadata !{i32 786688, metadata !301, metadata !"a", metadata !302, i32 13, metadata !24, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 13]
!311 = metadata !{i32 786688, metadata !301, metadata !"b", metadata !302, i32 14, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [b] [line 14]
!312 = metadata !{i32 786449, metadata !313, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !314, metadata !2, metadata !2, metadata !""} 
!313 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/mempcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!314 = metadata !{metadata !315}
!315 = metadata !{i32 786478, metadata !313, metadata !316, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 11, metadata !317, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !320, 
!316 = metadata !{i32 786473, metadata !313}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/mempcpy.c]
!317 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !318, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!318 = metadata !{metadata !27, metadata !27, metadata !153, metadata !319}
!319 = metadata !{i32 786454, metadata !313, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!320 = metadata !{metadata !321, metadata !322, metadata !323, metadata !324, metadata !325}
!321 = metadata !{i32 786689, metadata !315, metadata !"destaddr", metadata !316, i32 16777227, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 11]
!322 = metadata !{i32 786689, metadata !315, metadata !"srcaddr", metadata !316, i32 33554443, metadata !153, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 11]
!323 = metadata !{i32 786689, metadata !315, metadata !"len", metadata !316, i32 50331659, metadata !319, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 11]
!324 = metadata !{i32 786688, metadata !315, metadata !"dest", metadata !316, i32 12, metadata !24, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 12]
!325 = metadata !{i32 786688, metadata !315, metadata !"src", metadata !316, i32 13, metadata !42, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 13]
!326 = metadata !{i32 786449, metadata !327, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !328, metadata !2, metadata !2, metadata !""} 
!327 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memset.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!328 = metadata !{metadata !329}
!329 = metadata !{i32 786478, metadata !327, metadata !330, metadata !"memset", metadata !"memset", metadata !"", i32 11, metadata !331, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i32, i64)* @memset, null, null, metadata !334, i32
!330 = metadata !{i32 786473, metadata !327}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memset.c]
!331 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !332, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!332 = metadata !{metadata !27, metadata !27, metadata !8, metadata !333}
!333 = metadata !{i32 786454, metadata !327, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !47} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!334 = metadata !{metadata !335, metadata !336, metadata !337, metadata !338}
!335 = metadata !{i32 786689, metadata !329, metadata !"dst", metadata !330, i32 16777227, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 11]
!336 = metadata !{i32 786689, metadata !329, metadata !"s", metadata !330, i32 33554443, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [s] [line 11]
!337 = metadata !{i32 786689, metadata !329, metadata !"count", metadata !330, i32 50331659, metadata !333, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 11]
!338 = metadata !{i32 786688, metadata !329, metadata !"a", metadata !330, i32 12, metadata !339, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 12]
!339 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !340} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!340 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !25} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from char]
!341 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!342 = metadata !{i32 1, metadata !"Debug Info Version", i32 1}
!343 = metadata !{metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)"}
!344 = metadata !{i32 11, i32 0, metadata !4, null}
!345 = metadata !{i32 12, i32 0, metadata !4, null}
!346 = metadata !{i32 13, i32 0, metadata !4, null}
!347 = metadata !{i32 14, i32 0, metadata !4, null}
!348 = metadata !{i32 16, i32 0, metadata !4, null}
!349 = metadata !{i32 18, i32 0, metadata !4, null}
!350 = metadata !{i32 191, i32 0, metadata !351, null}
!351 = metadata !{i32 786443, metadata !10, metadata !12, i32 191, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!352 = metadata !{i32 193, i32 0, metadata !12, null}
!353 = metadata !{i32 197, i32 0, metadata !12, null}
!354 = metadata !{i32 238, i32 0, metadata !355, null}
!355 = metadata !{i32 786443, metadata !10, metadata !12, i32 238, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!356 = metadata !{i32 239, i32 0, metadata !355, null}
!357 = metadata !{i32 240, i32 0, metadata !12, null}
!358 = metadata !{i32 263, i32 0, metadata !359, null}
!359 = metadata !{i32 786443, metadata !10, metadata !16, i32 263, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!360 = metadata !{i32 264, i32 0, metadata !359, null}
!361 = metadata !{i32 266, i32 0, metadata !362, null}
!362 = metadata !{i32 786443, metadata !10, metadata !16, i32 266, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!363 = metadata !{i32 267, i32 0, metadata !362, null}
!364 = metadata !{i32 268, i32 0, metadata !16, null}
!365 = metadata !{i32 288, i32 0, metadata !17, null}
!366 = metadata !{i32 291, i32 0, metadata !17, null}
!367 = metadata !{i32 294, i32 0, metadata !17, null}
!368 = metadata !{i32 298, i32 0, metadata !369, null}
!369 = metadata !{i32 786443, metadata !10, metadata !17, i32 298, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!370 = metadata !{i32 300, i32 0, metadata !371, null}
!371 = metadata !{i32 786443, metadata !10, metadata !369, i32 298, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!372 = metadata !{i32 301, i32 0, metadata !371, null}
!373 = metadata !{i32 305, i32 0, metadata !17, null}
!374 = metadata !{i32 306, i32 0, metadata !17, null}
!375 = metadata !{i32 307, i32 0, metadata !17, null}
!376 = metadata !{i32 308, i32 0, metadata !377, null}
!377 = metadata !{i32 786443, metadata !10, metadata !17, i32 307, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!378 = metadata !{i32 311, i32 0, metadata !17, null}
!379 = metadata !{i32 312, i32 0, metadata !380, null}
!380 = metadata !{i32 786443, metadata !10, metadata !17, i32 311, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!381 = metadata !{i32 313, i32 0, metadata !382, null}
!382 = metadata !{i32 786443, metadata !10, metadata !380, i32 313, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!383 = metadata !{i32 314, i32 0, metadata !384, null}
!384 = metadata !{i32 786443, metadata !10, metadata !382, i32 313, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!385 = metadata !{i32 315, i32 0, metadata !384, null}
!386 = metadata !{i32 316, i32 0, metadata !380, null}
!387 = metadata !{i32 317, i32 0, metadata !380, null}
!388 = metadata !{i32 323, i32 0, metadata !17, null}
!389 = metadata !{i32 327, i32 0, metadata !17, null}
!390 = metadata !{i32 331, i32 0, metadata !391, null}
!391 = metadata !{i32 786443, metadata !10, metadata !17, i32 331, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!392 = metadata !{i32 336, i32 0, metadata !393, null}
!393 = metadata !{i32 786443, metadata !10, metadata !391, i32 335, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!394 = metadata !{i32 337, i32 0, metadata !393, null}
!395 = metadata !{i32 338, i32 0, metadata !393, null}
!396 = metadata !{i32 339, i32 0, metadata !393, null}
!397 = metadata !{i32 342, i32 0, metadata !17, null}
!398 = metadata !{i32 354, i32 0, metadata !17, null}
!399 = metadata !{i32 370, i32 0, metadata !400, null}
!400 = metadata !{i32 786443, metadata !10, metadata !17, i32 370, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!401 = metadata !{i32 371, i32 0, metadata !402, null}
!402 = metadata !{i32 786443, metadata !10, metadata !400, i32 370, i32 0, i32 13} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!403 = metadata !{i32 372, i32 0, metadata !402, null}
!404 = metadata !{i32 391, i32 0, metadata !405, null}
!405 = metadata !{i32 786443, metadata !10, metadata !17, i32 391, i32 0, i32 14} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!406 = metadata !{i32 392, i32 0, metadata !405, null}
!407 = metadata !{i32 395, i32 0, metadata !408, null}
!408 = metadata !{i32 786443, metadata !10, metadata !17, i32 395, i32 0, i32 15} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!409 = metadata !{i32 396, i32 0, metadata !408, null}
!410 = metadata !{i32 401, i32 0, metadata !17, null}
!411 = metadata !{i32 160, i32 0, metadata !412, null}
!412 = metadata !{i32 786443, metadata !10, metadata !38} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!413 = metadata !{i32 161, i32 0, metadata !412, null}
!414 = metadata !{i32 162, i32 0, metadata !412, null}
!415 = metadata !{i32 163, i32 0, metadata !412, null}
!416 = metadata !{i32 165, i32 0, metadata !417, null}
!417 = metadata !{i32 786443, metadata !10, metadata !412, i32 165, i32 0, i32 20} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!418 = metadata !{i32 166, i32 0, metadata !419, null}
!419 = metadata !{i32 786443, metadata !10, metadata !417, i32 165, i32 0, i32 21} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!420 = metadata !{i32 168, i32 0, metadata !412, null}
!421 = metadata !{i32 169, i32 0, metadata !412, null}
!422 = metadata !{i32 139, i32 0, metadata !423, null}
!423 = metadata !{i32 786443, metadata !10, metadata !28, i32 139, i32 0, i32 16} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!424 = metadata !{i32 143, i32 0, metadata !425, null}
!425 = metadata !{i32 786443, metadata !10, metadata !423, i32 140, i32 0, i32 17} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!426 = metadata !{i32 147, i32 0, metadata !427, null}
!427 = metadata !{i32 786443, metadata !10, metadata !425, i32 147, i32 0, i32 18} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!428 = metadata !{i32 148, i32 18, metadata !427, null}
!429 = metadata !{i32 150, i32 0, metadata !430, null}
!430 = metadata !{i32 786443, metadata !10, metadata !427, i32 149, i32 0, i32 19} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/misc/internals/__uClibc_main.c]
!431 = metadata !{i32 153, i32 0, metadata !28, null}
!432 = metadata !{i32 56, i32 0, metadata !433, null}
!433 = metadata !{i32 786443, metadata !32, metadata !31} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/./include/sys/sysmacros.h]
!434 = metadata !{i32 13, i32 0, metadata !54, null}
!435 = metadata !{i32 12, i32 0, metadata !62, null}
!436 = metadata !{i32 258, i32 0, metadata !437, null}
!437 = metadata !{i32 786443, metadata !65, metadata !67, i32 258, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!438 = metadata !{i32 261, i32 0, metadata !439, null}
!439 = metadata !{i32 786443, metadata !65, metadata !440, i32 261, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!440 = metadata !{i32 786443, metadata !65, metadata !437, i32 258, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!441 = metadata !{i32 262, i32 0, metadata !442, null}
!442 = metadata !{i32 786443, metadata !65, metadata !439, i32 261, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_stdio.c]
!443 = metadata !{i32 263, i32 0, metadata !442, null}
!444 = metadata !{i32 274, i32 0, metadata !67, null}
!445 = metadata !{i32 280, i32 0, metadata !69, null}
!446 = metadata !{i32 282, i32 0, metadata !69, null}
!447 = metadata !{i32 283, i32 0, metadata !69, null}
!448 = metadata !{i32 284, i32 0, metadata !69, null}
!449 = metadata !{i32 291, i32 0, metadata !69, null}
!450 = metadata !{i32 23, i32 0, metadata !451, null}
!451 = metadata !{i32 786443, metadata !114, metadata !116, i32 23, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!452 = metadata !{i32 24, i32 0, metadata !453, null}
!453 = metadata !{i32 786443, metadata !114, metadata !451, i32 23, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_wcommit.c]
!454 = metadata !{i32 25, i32 0, metadata !453, null}
!455 = metadata !{i32 26, i32 0, metadata !453, null}
!456 = metadata !{i32 28, i32 0, metadata !116, null}
!457 = metadata !{i32 20, i32 0, metadata !147, null}
!458 = metadata !{i32 21, i32 0, metadata !147, null}
!459 = metadata !{i32 28, i32 0, metadata !147, null}
!460 = metadata !{i32 29, i32 0, metadata !461, null}
!461 = metadata !{i32 786443, metadata !145, metadata !147, i32 28, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memcpy.c]
!462 = metadata !{i32 30, i32 0, metadata !461, null}
!463 = metadata !{i32 31, i32 0, metadata !461, null}
!464 = metadata !{i32 34, i32 0, metadata !147, null}
!465 = metadata !{i32 19, i32 0, metadata !159, null}
!466 = metadata !{i32 27, i32 0, metadata !159, null}
!467 = metadata !{i32 28, i32 0, metadata !468, null}
!468 = metadata !{i32 786443, metadata !157, metadata !159, i32 27, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/memset.c]
!469 = metadata !{i32 29, i32 0, metadata !468, null}
!470 = metadata !{i32 30, i32 0, metadata !468, null}
!471 = metadata !{i32 32, i32 0, metadata !159, null}
!472 = metadata !{i32 30, i32 0, metadata !167, null}
!473 = metadata !{i32 43, i32 0, metadata !174, null}
!474 = metadata !{i32 45, i32 0, metadata !174, null}
!475 = metadata !{i32 46, i32 0, metadata !174, null}
!476 = metadata !{i32 47, i32 0, metadata !174, null}
!477 = metadata !{i32 48, i32 0, metadata !174, null}
!478 = metadata !{i32 49, i32 0, metadata !174, null}
!479 = metadata !{i32 61, i32 0, metadata !480, null}
!480 = metadata !{i32 786443, metadata !172, metadata !481, i32 60, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!481 = metadata !{i32 786443, metadata !172, metadata !174, i32 58, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/termios/tcgetattr.c]
!482 = metadata !{i32 79, i32 0, metadata !174, null}
!483 = metadata !{i32 44, i32 0, metadata !205, null}
!484 = metadata !{i32 46, i32 0, metadata !205, null}
!485 = metadata !{i32 47, i32 0, metadata !486, null}
!486 = metadata !{i32 786443, metadata !203, metadata !487, i32 47, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!487 = metadata !{i32 786443, metadata !203, metadata !205, i32 46, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!488 = metadata !{i32 49, i32 0, metadata !489, null}
!489 = metadata !{i32 786443, metadata !203, metadata !486, i32 47, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!490 = metadata !{i32 51, i32 0, metadata !487, null}
!491 = metadata !{i32 52, i32 0, metadata !492, null}
!492 = metadata !{i32 786443, metadata !203, metadata !487, i32 52, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!493 = metadata !{i32 62, i32 0, metadata !494, null}
!494 = metadata !{i32 786443, metadata !203, metadata !492, i32 52, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!495 = metadata !{i32 63, i32 0, metadata !494, null}
!496 = metadata !{i32 101, i32 0, metadata !487, null}
!497 = metadata !{i32 70, i32 0, metadata !498, null}
!498 = metadata !{i32 786443, metadata !203, metadata !492, i32 69, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!499 = metadata !{i32 73, i32 0, metadata !500, null}
!500 = metadata !{i32 786443, metadata !203, metadata !498, i32 73, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!501 = metadata !{i32 76, i32 0, metadata !502, null}
!502 = metadata !{i32 786443, metadata !203, metadata !503, i32 76, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!503 = metadata !{i32 786443, metadata !203, metadata !500, i32 73, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!504 = metadata !{i32 77, i32 0, metadata !505, null}
!505 = metadata !{i32 786443, metadata !203, metadata !502, i32 76, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!506 = metadata !{i32 78, i32 0, metadata !505, null}
!507 = metadata !{i32 80, i32 0, metadata !503, null}
!508 = metadata !{i32 82, i32 0, metadata !503, null}
!509 = metadata !{i32 83, i32 0, metadata !510, null}
!510 = metadata !{i32 786443, metadata !203, metadata !511, i32 83, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!511 = metadata !{i32 786443, metadata !203, metadata !503, i32 82, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/stdio/_WRITE.c]
!512 = metadata !{i32 88, i32 0, metadata !511, null}
!513 = metadata !{i32 89, i32 0, metadata !511, null}
!514 = metadata !{i32 90, i32 0, metadata !511, null}
!515 = metadata !{i32 92, i32 0, metadata !503, null}
!516 = metadata !{i32 94, i32 0, metadata !503, null}
!517 = metadata !{i32 95, i32 0, metadata !503, null}
!518 = metadata !{i32 99, i32 0, metadata !498, null}
!519 = metadata !{i32 102, i32 0, metadata !205, null}
!520 = metadata !{i32 22, i32 0, metadata !237, null}
!521 = metadata !{i32 23, i32 0, metadata !237, null}
!522 = metadata !{i32 30, i32 0, metadata !237, null}
!523 = metadata !{i32 31, i32 0, metadata !524, null}
!524 = metadata !{i32 786443, metadata !235, metadata !237, i32 30, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee-uclibc/libc/string/mempcpy.c]
!525 = metadata !{i32 32, i32 0, metadata !524, null}
!526 = metadata !{i32 33, i32 0, metadata !524, null}
!527 = metadata !{i32 36, i32 0, metadata !237, null}
!528 = metadata !{i32 13, i32 0, metadata !529, null}
!529 = metadata !{i32 786443, metadata !243, metadata !245, i32 13, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!530 = metadata !{i32 14, i32 0, metadata !529, null}
!531 = metadata !{i32 15, i32 0, metadata !245, null}
!532 = metadata !{i32 15, i32 0, metadata !255, null}
!533 = metadata !{i32 16, i32 0, metadata !255, null}
!534 = metadata !{metadata !535, metadata !535, i64 0}
!535 = metadata !{metadata !"int", metadata !536, i64 0}
!536 = metadata !{metadata !"omnipotent char", metadata !537, i64 0}
!537 = metadata !{metadata !"Simple C/C++ TBAA"}
!538 = metadata !{i32 21, i32 0, metadata !539, null}
!539 = metadata !{i32 786443, metadata !263, metadata !265, i32 21, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!540 = metadata !{i32 27, i32 0, metadata !541, null}
!541 = metadata !{i32 786443, metadata !263, metadata !539, i32 21, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!542 = metadata !{i32 29, i32 0, metadata !265, null}
!543 = metadata !{i32 16, i32 0, metadata !544, null}
!544 = metadata !{i32 786443, metadata !273, metadata !275, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!545 = metadata !{i32 17, i32 0, metadata !544, null}
!546 = metadata !{i32 19, i32 0, metadata !547, null}
!547 = metadata !{i32 786443, metadata !273, metadata !275, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!548 = metadata !{i32 22, i32 0, metadata !549, null}
!549 = metadata !{i32 786443, metadata !273, metadata !547, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!550 = metadata !{i32 25, i32 0, metadata !551, null}
!551 = metadata !{i32 786443, metadata !273, metadata !549, i32 25, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!552 = metadata !{i32 26, i32 0, metadata !553, null}
!553 = metadata !{i32 786443, metadata !273, metadata !551, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!554 = metadata !{i32 27, i32 0, metadata !553, null}
!555 = metadata !{i32 28, i32 0, metadata !556, null}
!556 = metadata !{i32 786443, metadata !273, metadata !551, i32 27, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!557 = metadata !{i32 29, i32 0, metadata !556, null}
!558 = metadata !{i32 32, i32 0, metadata !549, null}
!559 = metadata !{i32 34, i32 0, metadata !275, null}
!560 = metadata !{i32 16, i32 0, metadata !561, null}
!561 = metadata !{i32 786443, metadata !299, metadata !301, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!562 = metadata !{i32 19, i32 0, metadata !563, null}
!563 = metadata !{i32 786443, metadata !299, metadata !301, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!564 = metadata !{i32 20, i32 0, metadata !565, null}
!565 = metadata !{i32 786443, metadata !299, metadata !563, i32 19, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!566 = metadata !{metadata !566, metadata !567, metadata !568}
!567 = metadata !{metadata !"llvm.vectorizer.width", i32 1}
!568 = metadata !{metadata !"llvm.vectorizer.unroll", i32 1}
!569 = metadata !{metadata !536, metadata !536, i64 0}
!570 = metadata !{metadata !570, metadata !567, metadata !568}
!571 = metadata !{i32 22, i32 0, metadata !572, null}
!572 = metadata !{i32 786443, metadata !299, metadata !563, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!573 = metadata !{i32 24, i32 0, metadata !572, null}
!574 = metadata !{i32 23, i32 0, metadata !572, null}
!575 = metadata !{metadata !575, metadata !567, metadata !568}
!576 = metadata !{metadata !576, metadata !567, metadata !568}
!577 = metadata !{i32 28, i32 0, metadata !301, null}
