#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	int a[0x40];
	int x;
	int y;
	int z;

    klee_make_symbolic(a, sizeof(a), "a");
  	klee_make_symbolic(&x, sizeof(x), "x");
  	klee_make_symbolic(&y, sizeof(x), "y");
  	klee_make_symbolic(&z, sizeof(x), "z");
	
	a[x + y] = z;
	
	return 0;
}

