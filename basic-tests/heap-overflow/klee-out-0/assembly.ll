; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@.str = private unnamed_addr constant [2 x i8] c"a\00", align 1
@.str1 = private unnamed_addr constant [2 x i8] c"b\00", align 1
@.str2 = private unnamed_addr constant [60 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c\00", align 1
@.str13 = private unnamed_addr constant [15 x i8] c"divide by zero\00", align 1
@.str24 = private unnamed_addr constant [8 x i8] c"div.err\00", align 1
@.str3 = private unnamed_addr constant [8 x i8] c"IGNORED\00", align 1
@.str14 = private unnamed_addr constant [16 x i8] c"overshift error\00", align 1
@.str25 = private unnamed_addr constant [14 x i8] c"overshift.err\00", align 1
@.str6 = private unnamed_addr constant [51 x i8] c"/home/klee/klee_src/runtime/Intrinsic/klee_range.c\00", align 1
@.str17 = private unnamed_addr constant [14 x i8] c"invalid range\00", align 1
@.str28 = private unnamed_addr constant [5 x i8] c"user\00", align 1

; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4
  %a = alloca i8*, align 8
  %b = alloca [32 x i8], align 16
  store i32 0, i32* %1
  %2 = call noalias i8* @malloc(i64 16) #4, !dbg !120
  store i8* %2, i8** %a, align 8, !dbg !120
  %3 = load i8** %a, align 8, !dbg !121
  %4 = call i32 (i8*, i32, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i32, i8*, ...)*)(i8* %3, i32 16, i8* getelementptr inbounds ([2 x i8]* @.str, i32 0, i32 0)), !dbg !121
  %5 = getelementptr inbounds [32 x i8]* %b, i32 0, i32 0, !dbg !122
  %6 = call i32 (i8*, i64, i8*, ...)* bitcast (i32 (...)* @klee_make_symbolic to i32 (i8*, i64, i8*, ...)*)(i8* %5, i64 32, i8* getelementptr inbounds ([2 x i8]* @.str1, i32 0, i32 0)), !dbg !122
  %7 = load i8** %a, align 8, !dbg !123
  %8 = bitcast [32 x i8]* %b to i8*, !dbg !123
  %9 = call i8* @memcpy(i8* %7, i8* %8, i64 32)
  ret i32 0, !dbg !124
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) #2

declare i32 @klee_make_symbolic(...) #3

; Function Attrs: nounwind
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1) #4

; Function Attrs: nounwind ssp uwtable
define void @klee_div_zero_check(i64 %z) #5 {
  %1 = icmp eq i64 %z, 0, !dbg !125
  br i1 %1, label %2, label %3, !dbg !125

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([60 x i8]* @.str2, i64 0, i64 0), i32 14, i8* getelementptr inbounds ([15 x i8]* @.str13, i64 0, i64 0), i8* getelementptr inbounds ([8 x i8]* @.str24, i64 0, i64 0)) #8, !dbg !127
  unreachable, !dbg !127

; <label>:3                                       ; preds = %0
  ret void, !dbg !128
}

; Function Attrs: noreturn
declare void @klee_report_error(i8*, i32, i8*, i8*) #6

; Function Attrs: nounwind readnone
declare void @llvm.dbg.value(metadata, i64, metadata) #1

; Function Attrs: nounwind ssp uwtable
define i32 @klee_int(i8* %name) #5 {
  %x = alloca i32, align 4
  %1 = bitcast i32* %x to i8*, !dbg !129
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %1, i64 4, i8* %name) #9, !dbg !129
  %2 = load i32* %x, align 4, !dbg !130, !tbaa !131
  ret i32 %2, !dbg !130
}

; Function Attrs: nounwind ssp uwtable
define void @klee_overshift_check(i64 %bitWidth, i64 %shift) #5 {
  %1 = icmp ult i64 %shift, %bitWidth, !dbg !135
  br i1 %1, label %3, label %2, !dbg !135

; <label>:2                                       ; preds = %0
  tail call void @klee_report_error(i8* getelementptr inbounds ([8 x i8]* @.str3, i64 0, i64 0), i32 0, i8* getelementptr inbounds ([16 x i8]* @.str14, i64 0, i64 0), i8* getelementptr inbounds ([14 x i8]* @.str25, i64 0, i64 0)) #8, !dbg !137
  unreachable, !dbg !137

; <label>:3                                       ; preds = %0
  ret void, !dbg !139
}

; Function Attrs: nounwind ssp uwtable
define i32 @klee_range(i32 %start, i32 %end, i8* %name) #5 {
  %x = alloca i32, align 4
  %1 = icmp slt i32 %start, %end, !dbg !140
  br i1 %1, label %3, label %2, !dbg !140

; <label>:2                                       ; preds = %0
  call void @klee_report_error(i8* getelementptr inbounds ([51 x i8]* @.str6, i64 0, i64 0), i32 17, i8* getelementptr inbounds ([14 x i8]* @.str17, i64 0, i64 0), i8* getelementptr inbounds ([5 x i8]* @.str28, i64 0, i64 0)) #8, !dbg !142
  unreachable, !dbg !142

; <label>:3                                       ; preds = %0
  %4 = add nsw i32 %start, 1, !dbg !143
  %5 = icmp eq i32 %4, %end, !dbg !143
  br i1 %5, label %21, label %6, !dbg !143

; <label>:6                                       ; preds = %3
  %7 = bitcast i32* %x to i8*, !dbg !145
  call void bitcast (i32 (...)* @klee_make_symbolic to void (i8*, i64, i8*)*)(i8* %7, i64 4, i8* %name) #9, !dbg !145
  %8 = icmp eq i32 %start, 0, !dbg !147
  %9 = load i32* %x, align 4, !dbg !149, !tbaa !131
  br i1 %8, label %10, label %13, !dbg !147

; <label>:10                                      ; preds = %6
  %11 = icmp ult i32 %9, %end, !dbg !149
  %12 = zext i1 %11 to i64, !dbg !149
  call void @klee_assume(i64 %12) #9, !dbg !149
  br label %19, !dbg !151

; <label>:13                                      ; preds = %6
  %14 = icmp sge i32 %9, %start, !dbg !152
  %15 = zext i1 %14 to i64, !dbg !152
  call void @klee_assume(i64 %15) #9, !dbg !152
  %16 = load i32* %x, align 4, !dbg !154, !tbaa !131
  %17 = icmp slt i32 %16, %end, !dbg !154
  %18 = zext i1 %17 to i64, !dbg !154
  call void @klee_assume(i64 %18) #9, !dbg !154
  br label %19

; <label>:19                                      ; preds = %13, %10
  %20 = load i32* %x, align 4, !dbg !155, !tbaa !131
  br label %21, !dbg !155

; <label>:21                                      ; preds = %19, %3
  %.0 = phi i32 [ %20, %19 ], [ %start, %3 ]
  ret i32 %.0, !dbg !156
}

declare void @klee_assume(i64) #7

; Function Attrs: nounwind ssp uwtable
define weak i8* @memcpy(i8* %destaddr, i8* %srcaddr, i64 %len) #5 {
  %1 = icmp eq i64 %len, 0, !dbg !157
  br i1 %1, label %._crit_edge, label %.lr.ph.preheader, !dbg !157

.lr.ph.preheader:                                 ; preds = %0
  %n.vec = and i64 %len, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %2 = add i64 %len, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep4 = getelementptr i8* %srcaddr, i64 %2
  %scevgep = getelementptr i8* %destaddr, i64 %2
  %bound1 = icmp uge i8* %scevgep, %srcaddr
  %bound0 = icmp uge i8* %scevgep4, %destaddr
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %srcaddr, i64 %n.vec
  %ptr.ind.end6 = getelementptr i8* %destaddr, i64 %n.vec
  %rev.ind.end = sub i64 %len, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %srcaddr, i64 %index
  %next.gep103 = getelementptr i8* %destaddr, i64 %index
  %3 = bitcast i8* %next.gep to <16 x i8>*, !dbg !158
  %wide.load = load <16 x i8>* %3, align 1, !dbg !158
  %next.gep.sum279 = or i64 %index, 16, !dbg !158
  %4 = getelementptr i8* %srcaddr, i64 %next.gep.sum279, !dbg !158
  %5 = bitcast i8* %4 to <16 x i8>*, !dbg !158
  %wide.load200 = load <16 x i8>* %5, align 1, !dbg !158
  %6 = bitcast i8* %next.gep103 to <16 x i8>*, !dbg !158
  store <16 x i8> %wide.load, <16 x i8>* %6, align 1, !dbg !158
  %7 = getelementptr i8* %destaddr, i64 %next.gep.sum279, !dbg !158
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !158
  store <16 x i8> %wide.load200, <16 x i8>* %8, align 1, !dbg !158
  %index.next = add i64 %index, 32
  %9 = icmp eq i64 %index.next, %n.vec
  br i1 %9, label %middle.block, label %vector.body, !llvm.loop !159

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %srcaddr, %.lr.ph.preheader ], [ %srcaddr, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val5 = phi i8* [ %destaddr, %.lr.ph.preheader ], [ %destaddr, %vector.memcheck ], [ %ptr.ind.end6, %vector.body ]
  %resume.val7 = phi i64 [ %len, %.lr.ph.preheader ], [ %len, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %len
  br i1 %cmp.n, label %._crit_edge, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %src.03 = phi i8* [ %11, %.lr.ph ], [ %resume.val, %middle.block ]
  %dest.02 = phi i8* [ %13, %.lr.ph ], [ %resume.val5, %middle.block ]
  %.01 = phi i64 [ %10, %.lr.ph ], [ %resume.val7, %middle.block ]
  %10 = add i64 %.01, -1, !dbg !157
  %11 = getelementptr inbounds i8* %src.03, i64 1, !dbg !158
  %12 = load i8* %src.03, align 1, !dbg !158, !tbaa !162
  %13 = getelementptr inbounds i8* %dest.02, i64 1, !dbg !158
  store i8 %12, i8* %dest.02, align 1, !dbg !158, !tbaa !162
  %14 = icmp eq i64 %10, 0, !dbg !157
  br i1 %14, label %._crit_edge, label %.lr.ph, !dbg !157, !llvm.loop !163

._crit_edge:                                      ; preds = %.lr.ph, %middle.block, %0
  ret i8* %destaddr, !dbg !164
}

; Function Attrs: nounwind ssp uwtable
define weak i8* @memmove(i8* %dst, i8* %src, i64 %count) #5 {
  %1 = icmp eq i8* %src, %dst, !dbg !165
  br i1 %1, label %.loopexit, label %2, !dbg !165

; <label>:2                                       ; preds = %0
  %3 = icmp ugt i8* %src, %dst, !dbg !167
  br i1 %3, label %.preheader, label %18, !dbg !167

.preheader:                                       ; preds = %2
  %4 = icmp eq i64 %count, 0, !dbg !169
  br i1 %4, label %.loopexit, label %.lr.ph.preheader, !dbg !169

.lr.ph.preheader:                                 ; preds = %.preheader
  %n.vec = and i64 %count, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %5 = add i64 %count, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep11 = getelementptr i8* %src, i64 %5
  %scevgep = getelementptr i8* %dst, i64 %5
  %bound1 = icmp uge i8* %scevgep, %src
  %bound0 = icmp uge i8* %scevgep11, %dst
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %src, i64 %n.vec
  %ptr.ind.end13 = getelementptr i8* %dst, i64 %n.vec
  %rev.ind.end = sub i64 %count, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %src, i64 %index
  %next.gep110 = getelementptr i8* %dst, i64 %index
  %6 = bitcast i8* %next.gep to <16 x i8>*, !dbg !169
  %wide.load = load <16 x i8>* %6, align 1, !dbg !169
  %next.gep.sum586 = or i64 %index, 16, !dbg !169
  %7 = getelementptr i8* %src, i64 %next.gep.sum586, !dbg !169
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !169
  %wide.load207 = load <16 x i8>* %8, align 1, !dbg !169
  %9 = bitcast i8* %next.gep110 to <16 x i8>*, !dbg !169
  store <16 x i8> %wide.load, <16 x i8>* %9, align 1, !dbg !169
  %10 = getelementptr i8* %dst, i64 %next.gep.sum586, !dbg !169
  %11 = bitcast i8* %10 to <16 x i8>*, !dbg !169
  store <16 x i8> %wide.load207, <16 x i8>* %11, align 1, !dbg !169
  %index.next = add i64 %index, 32
  %12 = icmp eq i64 %index.next, %n.vec
  br i1 %12, label %middle.block, label %vector.body, !llvm.loop !171

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %src, %.lr.ph.preheader ], [ %src, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val12 = phi i8* [ %dst, %.lr.ph.preheader ], [ %dst, %vector.memcheck ], [ %ptr.ind.end13, %vector.body ]
  %resume.val14 = phi i64 [ %count, %.lr.ph.preheader ], [ %count, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %count
  br i1 %cmp.n, label %.loopexit, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %b.04 = phi i8* [ %14, %.lr.ph ], [ %resume.val, %middle.block ]
  %a.03 = phi i8* [ %16, %.lr.ph ], [ %resume.val12, %middle.block ]
  %.02 = phi i64 [ %13, %.lr.ph ], [ %resume.val14, %middle.block ]
  %13 = add i64 %.02, -1, !dbg !169
  %14 = getelementptr inbounds i8* %b.04, i64 1, !dbg !169
  %15 = load i8* %b.04, align 1, !dbg !169, !tbaa !162
  %16 = getelementptr inbounds i8* %a.03, i64 1, !dbg !169
  store i8 %15, i8* %a.03, align 1, !dbg !169, !tbaa !162
  %17 = icmp eq i64 %13, 0, !dbg !169
  br i1 %17, label %.loopexit, label %.lr.ph, !dbg !169, !llvm.loop !172

; <label>:18                                      ; preds = %2
  %19 = add i64 %count, -1, !dbg !173
  %20 = icmp eq i64 %count, 0, !dbg !175
  br i1 %20, label %.loopexit, label %.lr.ph9, !dbg !175

.lr.ph9:                                          ; preds = %18
  %21 = getelementptr inbounds i8* %src, i64 %19, !dbg !176
  %22 = getelementptr inbounds i8* %dst, i64 %19, !dbg !173
  %n.vec215 = and i64 %count, -32
  %cmp.zero217 = icmp eq i64 %n.vec215, 0
  br i1 %cmp.zero217, label %middle.block210, label %vector.memcheck224

vector.memcheck224:                               ; preds = %.lr.ph9
  %bound1221 = icmp ule i8* %21, %dst
  %bound0220 = icmp ule i8* %22, %src
  %memcheck.conflict223 = and i1 %bound0220, %bound1221
  %.sum = sub i64 %19, %n.vec215
  %rev.ptr.ind.end = getelementptr i8* %src, i64 %.sum
  %rev.ptr.ind.end229 = getelementptr i8* %dst, i64 %.sum
  %rev.ind.end231 = sub i64 %count, %n.vec215
  br i1 %memcheck.conflict223, label %middle.block210, label %vector.body209

vector.body209:                                   ; preds = %vector.body209, %vector.memcheck224
  %index212 = phi i64 [ %index.next234, %vector.body209 ], [ 0, %vector.memcheck224 ]
  %.sum440 = sub i64 %19, %index212
  %next.gep236.sum = add i64 %.sum440, -15, !dbg !175
  %23 = getelementptr i8* %src, i64 %next.gep236.sum, !dbg !175
  %24 = bitcast i8* %23 to <16 x i8>*, !dbg !175
  %wide.load434 = load <16 x i8>* %24, align 1, !dbg !175
  %reverse = shufflevector <16 x i8> %wide.load434, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !175
  %.sum505 = add i64 %.sum440, -31, !dbg !175
  %25 = getelementptr i8* %src, i64 %.sum505, !dbg !175
  %26 = bitcast i8* %25 to <16 x i8>*, !dbg !175
  %wide.load435 = load <16 x i8>* %26, align 1, !dbg !175
  %reverse436 = shufflevector <16 x i8> %wide.load435, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !175
  %reverse437 = shufflevector <16 x i8> %reverse, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !175
  %27 = getelementptr i8* %dst, i64 %next.gep236.sum, !dbg !175
  %28 = bitcast i8* %27 to <16 x i8>*, !dbg !175
  store <16 x i8> %reverse437, <16 x i8>* %28, align 1, !dbg !175
  %reverse438 = shufflevector <16 x i8> %reverse436, <16 x i8> undef, <16 x i32> <i32 15, i32 14, i32 13, i32 12, i32 11, i32 10, i32 9, i32 8, i32 7, i32 6, i32 5, i32 4, i32 3, i32 2, i32 1, i32 0>, !dbg !175
  %29 = getelementptr i8* %dst, i64 %.sum505, !dbg !175
  %30 = bitcast i8* %29 to <16 x i8>*, !dbg !175
  store <16 x i8> %reverse438, <16 x i8>* %30, align 1, !dbg !175
  %index.next234 = add i64 %index212, 32
  %31 = icmp eq i64 %index.next234, %n.vec215
  br i1 %31, label %middle.block210, label %vector.body209, !llvm.loop !177

middle.block210:                                  ; preds = %vector.body209, %vector.memcheck224, %.lr.ph9
  %resume.val225 = phi i8* [ %21, %.lr.ph9 ], [ %21, %vector.memcheck224 ], [ %rev.ptr.ind.end, %vector.body209 ]
  %resume.val227 = phi i8* [ %22, %.lr.ph9 ], [ %22, %vector.memcheck224 ], [ %rev.ptr.ind.end229, %vector.body209 ]
  %resume.val230 = phi i64 [ %count, %.lr.ph9 ], [ %count, %vector.memcheck224 ], [ %rev.ind.end231, %vector.body209 ]
  %new.indc.resume.val232 = phi i64 [ 0, %.lr.ph9 ], [ 0, %vector.memcheck224 ], [ %n.vec215, %vector.body209 ]
  %cmp.n233 = icmp eq i64 %new.indc.resume.val232, %count
  br i1 %cmp.n233, label %.loopexit, label %scalar.ph211

scalar.ph211:                                     ; preds = %scalar.ph211, %middle.block210
  %b.18 = phi i8* [ %33, %scalar.ph211 ], [ %resume.val225, %middle.block210 ]
  %a.17 = phi i8* [ %35, %scalar.ph211 ], [ %resume.val227, %middle.block210 ]
  %.16 = phi i64 [ %32, %scalar.ph211 ], [ %resume.val230, %middle.block210 ]
  %32 = add i64 %.16, -1, !dbg !175
  %33 = getelementptr inbounds i8* %b.18, i64 -1, !dbg !175
  %34 = load i8* %b.18, align 1, !dbg !175, !tbaa !162
  %35 = getelementptr inbounds i8* %a.17, i64 -1, !dbg !175
  store i8 %34, i8* %a.17, align 1, !dbg !175, !tbaa !162
  %36 = icmp eq i64 %32, 0, !dbg !175
  br i1 %36, label %.loopexit, label %scalar.ph211, !dbg !175, !llvm.loop !178

.loopexit:                                        ; preds = %scalar.ph211, %middle.block210, %18, %.lr.ph, %middle.block, %.preheader, %0
  ret i8* %dst, !dbg !179
}

; Function Attrs: nounwind ssp uwtable
define weak i8* @mempcpy(i8* %destaddr, i8* %srcaddr, i64 %len) #5 {
  %1 = icmp eq i64 %len, 0, !dbg !180
  br i1 %1, label %15, label %.lr.ph.preheader, !dbg !180

.lr.ph.preheader:                                 ; preds = %0
  %n.vec = and i64 %len, -32
  %cmp.zero = icmp eq i64 %n.vec, 0
  %2 = add i64 %len, -1
  br i1 %cmp.zero, label %middle.block, label %vector.memcheck

vector.memcheck:                                  ; preds = %.lr.ph.preheader
  %scevgep5 = getelementptr i8* %srcaddr, i64 %2
  %scevgep4 = getelementptr i8* %destaddr, i64 %2
  %bound1 = icmp uge i8* %scevgep4, %srcaddr
  %bound0 = icmp uge i8* %scevgep5, %destaddr
  %memcheck.conflict = and i1 %bound0, %bound1
  %ptr.ind.end = getelementptr i8* %srcaddr, i64 %n.vec
  %ptr.ind.end7 = getelementptr i8* %destaddr, i64 %n.vec
  %rev.ind.end = sub i64 %len, %n.vec
  br i1 %memcheck.conflict, label %middle.block, label %vector.body

vector.body:                                      ; preds = %vector.body, %vector.memcheck
  %index = phi i64 [ %index.next, %vector.body ], [ 0, %vector.memcheck ]
  %next.gep = getelementptr i8* %srcaddr, i64 %index
  %next.gep104 = getelementptr i8* %destaddr, i64 %index
  %3 = bitcast i8* %next.gep to <16 x i8>*, !dbg !181
  %wide.load = load <16 x i8>* %3, align 1, !dbg !181
  %next.gep.sum280 = or i64 %index, 16, !dbg !181
  %4 = getelementptr i8* %srcaddr, i64 %next.gep.sum280, !dbg !181
  %5 = bitcast i8* %4 to <16 x i8>*, !dbg !181
  %wide.load201 = load <16 x i8>* %5, align 1, !dbg !181
  %6 = bitcast i8* %next.gep104 to <16 x i8>*, !dbg !181
  store <16 x i8> %wide.load, <16 x i8>* %6, align 1, !dbg !181
  %7 = getelementptr i8* %destaddr, i64 %next.gep.sum280, !dbg !181
  %8 = bitcast i8* %7 to <16 x i8>*, !dbg !181
  store <16 x i8> %wide.load201, <16 x i8>* %8, align 1, !dbg !181
  %index.next = add i64 %index, 32
  %9 = icmp eq i64 %index.next, %n.vec
  br i1 %9, label %middle.block, label %vector.body, !llvm.loop !182

middle.block:                                     ; preds = %vector.body, %vector.memcheck, %.lr.ph.preheader
  %resume.val = phi i8* [ %srcaddr, %.lr.ph.preheader ], [ %srcaddr, %vector.memcheck ], [ %ptr.ind.end, %vector.body ]
  %resume.val6 = phi i8* [ %destaddr, %.lr.ph.preheader ], [ %destaddr, %vector.memcheck ], [ %ptr.ind.end7, %vector.body ]
  %resume.val8 = phi i64 [ %len, %.lr.ph.preheader ], [ %len, %vector.memcheck ], [ %rev.ind.end, %vector.body ]
  %new.indc.resume.val = phi i64 [ 0, %.lr.ph.preheader ], [ 0, %vector.memcheck ], [ %n.vec, %vector.body ]
  %cmp.n = icmp eq i64 %new.indc.resume.val, %len
  br i1 %cmp.n, label %._crit_edge, label %.lr.ph

.lr.ph:                                           ; preds = %.lr.ph, %middle.block
  %src.03 = phi i8* [ %11, %.lr.ph ], [ %resume.val, %middle.block ]
  %dest.02 = phi i8* [ %13, %.lr.ph ], [ %resume.val6, %middle.block ]
  %.01 = phi i64 [ %10, %.lr.ph ], [ %resume.val8, %middle.block ]
  %10 = add i64 %.01, -1, !dbg !180
  %11 = getelementptr inbounds i8* %src.03, i64 1, !dbg !181
  %12 = load i8* %src.03, align 1, !dbg !181, !tbaa !162
  %13 = getelementptr inbounds i8* %dest.02, i64 1, !dbg !181
  store i8 %12, i8* %dest.02, align 1, !dbg !181, !tbaa !162
  %14 = icmp eq i64 %10, 0, !dbg !180
  br i1 %14, label %._crit_edge, label %.lr.ph, !dbg !180, !llvm.loop !183

._crit_edge:                                      ; preds = %.lr.ph, %middle.block
  %scevgep = getelementptr i8* %destaddr, i64 %len
  br label %15, !dbg !180

; <label>:15                                      ; preds = %._crit_edge, %0
  %dest.0.lcssa = phi i8* [ %scevgep, %._crit_edge ], [ %destaddr, %0 ]
  ret i8* %dest.0.lcssa, !dbg !184
}

; Function Attrs: nounwind ssp uwtable
define weak i8* @memset(i8* %dst, i32 %s, i64 %count) #5 {
  %1 = icmp eq i64 %count, 0, !dbg !185
  br i1 %1, label %._crit_edge, label %.lr.ph, !dbg !185

.lr.ph:                                           ; preds = %0
  %2 = trunc i32 %s to i8, !dbg !186
  br label %3, !dbg !185

; <label>:3                                       ; preds = %3, %.lr.ph
  %a.02 = phi i8* [ %dst, %.lr.ph ], [ %5, %3 ]
  %.01 = phi i64 [ %count, %.lr.ph ], [ %4, %3 ]
  %4 = add i64 %.01, -1, !dbg !185
  %5 = getelementptr inbounds i8* %a.02, i64 1, !dbg !186
  store volatile i8 %2, i8* %a.02, align 1, !dbg !186, !tbaa !162
  %6 = icmp eq i64 %4, 0, !dbg !185
  br i1 %6, label %._crit_edge, label %3, !dbg !185

._crit_edge:                                      ; preds = %3, %0
  ret i8* %dst, !dbg !187
}

attributes #0 = { nounwind uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float
attributes #1 = { nounwind readnone }
attributes #2 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false
attributes #3 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { nounwind ssp uwtable "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="4" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nobuiltin noreturn nounwind }
attributes #9 = { nobuiltin nounwind }

!llvm.dbg.cu = !{!0, !9, !19, !32, !43, !55, !74, !88, !102}
!llvm.module.flags = !{!117, !118}
!llvm.ident = !{!119, !119, !119, !119, !119, !119, !119, !119, !119}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !2, metadata !2, metadata !""} ; [ 
!1 = metadata !{metadata !"test.c", metadata !"/home/klee/workspace/basic-tests/heap-overflow"}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 5, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @main, null, null, metadata !2, i32 5} ; [ DW_TAG_subprogram ] [li
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/klee/workspace/basic-tests/heap-overflow/test.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{metadata !8}
!8 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!9 = metadata !{i32 786449, metadata !10, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !11, metadata !2, metadata !2, metadata !""} ; [ 
!10 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 786478, metadata !10, metadata !13, metadata !"klee_div_zero_check", metadata !"klee_div_zero_check", metadata !"", i32 12, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64)* @klee_div_zero_check, null
!13 = metadata !{i32 786473, metadata !10}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{null, metadata !16}
!16 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 64, offset 0, enc DW_ATE_signed]
!17 = metadata !{metadata !18}
!18 = metadata !{i32 786689, metadata !12, metadata !"z", metadata !13, i32 16777228, metadata !16, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [z] [line 12]
!19 = metadata !{i32 786449, metadata !20, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !21, metadata !2, metadata !2, metadata !""} ; [
!20 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_int.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 786478, metadata !20, metadata !23, metadata !"klee_int", metadata !"klee_int", metadata !"", i32 13, metadata !24, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i8*)* @klee_int, null, null, metadata !29, i32 13} ; [ 
!23 = metadata !{i32 786473, metadata !20}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_int.c]
!24 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !25, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!25 = metadata !{metadata !8, metadata !26}
!26 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !27} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!27 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !28} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from char]
!28 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!29 = metadata !{metadata !30, metadata !31}
!30 = metadata !{i32 786689, metadata !22, metadata !"name", metadata !23, i32 16777229, metadata !26, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!31 = metadata !{i32 786688, metadata !22, metadata !"x", metadata !23, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!32 = metadata !{i32 786449, metadata !33, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !34, metadata !2, metadata !2, metadata !""} ; [
!33 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!34 = metadata !{metadata !35}
!35 = metadata !{i32 786478, metadata !33, metadata !36, metadata !"klee_overshift_check", metadata !"klee_overshift_check", metadata !"", i32 20, metadata !37, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, void (i64, i64)* @klee_overshift_che
!36 = metadata !{i32 786473, metadata !33}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!37 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !38, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!38 = metadata !{null, metadata !39, metadata !39}
!39 = metadata !{i32 786468, null, null, metadata !"long long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!40 = metadata !{metadata !41, metadata !42}
!41 = metadata !{i32 786689, metadata !35, metadata !"bitWidth", metadata !36, i32 16777236, metadata !39, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [bitWidth] [line 20]
!42 = metadata !{i32 786689, metadata !35, metadata !"shift", metadata !36, i32 33554452, metadata !39, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [shift] [line 20]
!43 = metadata !{i32 786449, metadata !44, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !45, metadata !2, metadata !2, metadata !""} ; [
!44 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/klee_range.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!45 = metadata !{metadata !46}
!46 = metadata !{i32 786478, metadata !44, metadata !47, metadata !"klee_range", metadata !"klee_range", metadata !"", i32 13, metadata !48, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i32 (i32, i32, i8*)* @klee_range, null, null, metadata !
!47 = metadata !{i32 786473, metadata !44}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!48 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !49, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!49 = metadata !{metadata !8, metadata !8, metadata !8, metadata !26}
!50 = metadata !{metadata !51, metadata !52, metadata !53, metadata !54}
!51 = metadata !{i32 786689, metadata !46, metadata !"start", metadata !47, i32 16777229, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [start] [line 13]
!52 = metadata !{i32 786689, metadata !46, metadata !"end", metadata !47, i32 33554445, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [end] [line 13]
!53 = metadata !{i32 786689, metadata !46, metadata !"name", metadata !47, i32 50331661, metadata !26, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [name] [line 13]
!54 = metadata !{i32 786688, metadata !46, metadata !"x", metadata !47, i32 14, metadata !8, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [x] [line 14]
!55 = metadata !{i32 786449, metadata !56, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !57, metadata !2, metadata !2, metadata !""} ; [
!56 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!57 = metadata !{metadata !58}
!58 = metadata !{i32 786478, metadata !56, metadata !59, metadata !"memcpy", metadata !"memcpy", metadata !"", i32 12, metadata !60, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memcpy, null, null, metadata !67, i32 12} 
!59 = metadata !{i32 786473, metadata !56}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memcpy.c]
!60 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !61, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!61 = metadata !{metadata !62, metadata !62, metadata !63, metadata !65}
!62 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, null} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!63 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !64} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!64 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from ]
!65 = metadata !{i32 786454, metadata !56, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !66} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!66 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 64, i64 64, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 64, align 64, offset 0, enc DW_ATE_unsigned]
!67 = metadata !{metadata !68, metadata !69, metadata !70, metadata !71, metadata !73}
!68 = metadata !{i32 786689, metadata !58, metadata !"destaddr", metadata !59, i32 16777228, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 12]
!69 = metadata !{i32 786689, metadata !58, metadata !"srcaddr", metadata !59, i32 33554444, metadata !63, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 12]
!70 = metadata !{i32 786689, metadata !58, metadata !"len", metadata !59, i32 50331660, metadata !65, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 12]
!71 = metadata !{i32 786688, metadata !58, metadata !"dest", metadata !59, i32 13, metadata !72, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 13]
!72 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !28} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from char]
!73 = metadata !{i32 786688, metadata !58, metadata !"src", metadata !59, i32 14, metadata !26, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 14]
!74 = metadata !{i32 786449, metadata !75, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !76, metadata !2, metadata !2, metadata !""} ; [
!75 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memmove.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!76 = metadata !{metadata !77}
!77 = metadata !{i32 786478, metadata !75, metadata !78, metadata !"memmove", metadata !"memmove", metadata !"", i32 12, metadata !79, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @memmove, null, null, metadata !82, i32 1
!78 = metadata !{i32 786473, metadata !75}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!79 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !80, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!80 = metadata !{metadata !62, metadata !62, metadata !63, metadata !81}
!81 = metadata !{i32 786454, metadata !75, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !66} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!82 = metadata !{metadata !83, metadata !84, metadata !85, metadata !86, metadata !87}
!83 = metadata !{i32 786689, metadata !77, metadata !"dst", metadata !78, i32 16777228, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 12]
!84 = metadata !{i32 786689, metadata !77, metadata !"src", metadata !78, i32 33554444, metadata !63, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [src] [line 12]
!85 = metadata !{i32 786689, metadata !77, metadata !"count", metadata !78, i32 50331660, metadata !81, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 12]
!86 = metadata !{i32 786688, metadata !77, metadata !"a", metadata !78, i32 13, metadata !72, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 13]
!87 = metadata !{i32 786688, metadata !77, metadata !"b", metadata !78, i32 14, metadata !26, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [b] [line 14]
!88 = metadata !{i32 786449, metadata !89, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !90, metadata !2, metadata !2, metadata !""} ; [
!89 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/mempcpy.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!90 = metadata !{metadata !91}
!91 = metadata !{i32 786478, metadata !89, metadata !92, metadata !"mempcpy", metadata !"mempcpy", metadata !"", i32 11, metadata !93, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i8*, i64)* @mempcpy, null, null, metadata !96, i32 1
!92 = metadata !{i32 786473, metadata !89}        ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/mempcpy.c]
!93 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !94, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!94 = metadata !{metadata !62, metadata !62, metadata !63, metadata !95}
!95 = metadata !{i32 786454, metadata !89, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !66} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!96 = metadata !{metadata !97, metadata !98, metadata !99, metadata !100, metadata !101}
!97 = metadata !{i32 786689, metadata !91, metadata !"destaddr", metadata !92, i32 16777227, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [destaddr] [line 11]
!98 = metadata !{i32 786689, metadata !91, metadata !"srcaddr", metadata !92, i32 33554443, metadata !63, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [srcaddr] [line 11]
!99 = metadata !{i32 786689, metadata !91, metadata !"len", metadata !92, i32 50331659, metadata !95, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [len] [line 11]
!100 = metadata !{i32 786688, metadata !91, metadata !"dest", metadata !92, i32 12, metadata !72, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [dest] [line 12]
!101 = metadata !{i32 786688, metadata !91, metadata !"src", metadata !92, i32 13, metadata !26, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [src] [line 13]
!102 = metadata !{i32 786449, metadata !103, i32 1, metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)", i1 true, metadata !"", i32 0, metadata !2, metadata !2, metadata !104, metadata !2, metadata !2, metadata !""} 
!103 = metadata !{metadata !"/home/klee/klee_src/runtime/Intrinsic/memset.c", metadata !"/home/klee/klee_build/klee/runtime/Intrinsic"}
!104 = metadata !{metadata !105}
!105 = metadata !{i32 786478, metadata !103, metadata !106, metadata !"memset", metadata !"memset", metadata !"", i32 11, metadata !107, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 true, i8* (i8*, i32, i64)* @memset, null, null, metadata !110, i32
!106 = metadata !{i32 786473, metadata !103}      ; [ DW_TAG_file_type ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memset.c]
!107 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !108, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!108 = metadata !{metadata !62, metadata !62, metadata !8, metadata !109}
!109 = metadata !{i32 786454, metadata !103, null, metadata !"size_t", i32 42, i64 0, i64 0, i64 0, i32 0, metadata !66} ; [ DW_TAG_typedef ] [size_t] [line 42, size 0, align 0, offset 0] [from long unsigned int]
!110 = metadata !{metadata !111, metadata !112, metadata !113, metadata !114}
!111 = metadata !{i32 786689, metadata !105, metadata !"dst", metadata !106, i32 16777227, metadata !62, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [dst] [line 11]
!112 = metadata !{i32 786689, metadata !105, metadata !"s", metadata !106, i32 33554443, metadata !8, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [s] [line 11]
!113 = metadata !{i32 786689, metadata !105, metadata !"count", metadata !106, i32 50331659, metadata !109, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [count] [line 11]
!114 = metadata !{i32 786688, metadata !105, metadata !"a", metadata !106, i32 12, metadata !115, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [a] [line 12]
!115 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 64, i64 64, i64 0, i32 0, metadata !116} ; [ DW_TAG_pointer_type ] [line 0, size 64, align 64, offset 0] [from ]
!116 = metadata !{i32 786485, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !28} ; [ DW_TAG_volatile_type ] [line 0, size 0, align 0, offset 0] [from char]
!117 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!118 = metadata !{i32 1, metadata !"Debug Info Version", i32 1}
!119 = metadata !{metadata !"Ubuntu clang version 3.4-1ubuntu3 (tags/RELEASE_34/final) (based on LLVM 3.4)"}
!120 = metadata !{i32 6, i32 0, metadata !4, null}
!121 = metadata !{i32 9, i32 0, metadata !4, null}
!122 = metadata !{i32 10, i32 0, metadata !4, null}
!123 = metadata !{i32 12, i32 0, metadata !4, null}
!124 = metadata !{i32 14, i32 0, metadata !4, null}
!125 = metadata !{i32 13, i32 0, metadata !126, null}
!126 = metadata !{i32 786443, metadata !10, metadata !12, i32 13, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_div_zero_check.c]
!127 = metadata !{i32 14, i32 0, metadata !126, null}
!128 = metadata !{i32 15, i32 0, metadata !12, null}
!129 = metadata !{i32 15, i32 0, metadata !22, null}
!130 = metadata !{i32 16, i32 0, metadata !22, null}
!131 = metadata !{metadata !132, metadata !132, i64 0}
!132 = metadata !{metadata !"int", metadata !133, i64 0}
!133 = metadata !{metadata !"omnipotent char", metadata !134, i64 0}
!134 = metadata !{metadata !"Simple C/C++ TBAA"}
!135 = metadata !{i32 21, i32 0, metadata !136, null}
!136 = metadata !{i32 786443, metadata !33, metadata !35, i32 21, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!137 = metadata !{i32 27, i32 0, metadata !138, null}
!138 = metadata !{i32 786443, metadata !33, metadata !136, i32 21, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_overshift_check.c]
!139 = metadata !{i32 29, i32 0, metadata !35, null}
!140 = metadata !{i32 16, i32 0, metadata !141, null}
!141 = metadata !{i32 786443, metadata !44, metadata !46, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!142 = metadata !{i32 17, i32 0, metadata !141, null}
!143 = metadata !{i32 19, i32 0, metadata !144, null}
!144 = metadata !{i32 786443, metadata !44, metadata !46, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!145 = metadata !{i32 22, i32 0, metadata !146, null}
!146 = metadata !{i32 786443, metadata !44, metadata !144, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!147 = metadata !{i32 25, i32 0, metadata !148, null}
!148 = metadata !{i32 786443, metadata !44, metadata !146, i32 25, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!149 = metadata !{i32 26, i32 0, metadata !150, null}
!150 = metadata !{i32 786443, metadata !44, metadata !148, i32 25, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!151 = metadata !{i32 27, i32 0, metadata !150, null}
!152 = metadata !{i32 28, i32 0, metadata !153, null}
!153 = metadata !{i32 786443, metadata !44, metadata !148, i32 27, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/klee_range.c]
!154 = metadata !{i32 29, i32 0, metadata !153, null}
!155 = metadata !{i32 32, i32 0, metadata !146, null}
!156 = metadata !{i32 34, i32 0, metadata !46, null}
!157 = metadata !{i32 16, i32 0, metadata !58, null}
!158 = metadata !{i32 17, i32 0, metadata !58, null}
!159 = metadata !{metadata !159, metadata !160, metadata !161}
!160 = metadata !{metadata !"llvm.vectorizer.width", i32 1}
!161 = metadata !{metadata !"llvm.vectorizer.unroll", i32 1}
!162 = metadata !{metadata !133, metadata !133, i64 0}
!163 = metadata !{metadata !163, metadata !160, metadata !161}
!164 = metadata !{i32 18, i32 0, metadata !58, null}
!165 = metadata !{i32 16, i32 0, metadata !166, null}
!166 = metadata !{i32 786443, metadata !75, metadata !77, i32 16, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!167 = metadata !{i32 19, i32 0, metadata !168, null}
!168 = metadata !{i32 786443, metadata !75, metadata !77, i32 19, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!169 = metadata !{i32 20, i32 0, metadata !170, null}
!170 = metadata !{i32 786443, metadata !75, metadata !168, i32 19, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!171 = metadata !{metadata !171, metadata !160, metadata !161}
!172 = metadata !{metadata !172, metadata !160, metadata !161}
!173 = metadata !{i32 22, i32 0, metadata !174, null}
!174 = metadata !{i32 786443, metadata !75, metadata !168, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/klee/klee_build/klee/runtime/Intrinsic//home/klee/klee_src/runtime/Intrinsic/memmove.c]
!175 = metadata !{i32 24, i32 0, metadata !174, null}
!176 = metadata !{i32 23, i32 0, metadata !174, null}
!177 = metadata !{metadata !177, metadata !160, metadata !161}
!178 = metadata !{metadata !178, metadata !160, metadata !161}
!179 = metadata !{i32 28, i32 0, metadata !77, null}
!180 = metadata !{i32 15, i32 0, metadata !91, null}
!181 = metadata !{i32 16, i32 0, metadata !91, null}
!182 = metadata !{metadata !182, metadata !160, metadata !161}
!183 = metadata !{metadata !183, metadata !160, metadata !161}
!184 = metadata !{i32 17, i32 0, metadata !91, null}
!185 = metadata !{i32 13, i32 0, metadata !105, null}
!186 = metadata !{i32 14, i32 0, metadata !105, null}
!187 = metadata !{i32 15, i32 0, metadata !105, null}
