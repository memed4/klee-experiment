#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char *a = malloc(0x10);	
	char b[0x20];

  	klee_make_symbolic(a, 0x10, "a");
  	klee_make_symbolic(b, sizeof(b), "b");
	
	memcpy(a, b, sizeof(b));
	
	return 0;
}

