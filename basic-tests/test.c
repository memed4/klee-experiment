#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	char *buf = malloc(0x100);
	char b[0x10];

  	klee_make_symbolic(b, 0x10, "b");
  	klee_make_symbolic(buf, sizeof(buf), "buf");
	
	buf[0x102] = 0;
	strcpy(buf, b);
	
	return 0;
}

