#!/bin/sh

klee --simplify-sym-indices --allow-external-sym-calls --posix-runtime -libc=uclibc test.bc -sym-stdin 8
